﻿using System;
using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class Menu
    {
        public int? Id { get; set; }

        public DateTime DateToShow { get; set; }

        public int CreatorId { get; set; }

        public User Creator { get; set; }

        public ICollection<Dish> Dishes { get; set; }

        public ICollection<Order> Orders { get; set; }

        public bool IsDeleted { get; set; }
    }
}
