﻿using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Entities
{
    using System.Collections.Generic;

    public class User
    {
        public int? Id { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string LastName { get; set; }

        public decimal Balance { get; set; }

        public string Email { get; set; }

        public int GenderId { get; set; }

        public Gender Gender { get; set; }

        public int RoleId { get; set; }

        public Role Role { get; set; }

        public ICollection<Order> Orders { get; set; }

        public ICollection<Menu> CreatedMenus { get; set; }

        public ICollection<UserSetting> UserSettings { get; set; }

        public bool IsDeleted { get; set; }
    }
}
