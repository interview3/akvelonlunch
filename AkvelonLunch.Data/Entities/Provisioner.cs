﻿namespace AkvelonLunch.Data.Entities
{
    using System.Collections.Generic;

    public class Provisioner
    {
        public int? Id { get; set; }

        public string SupplierName { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string ContactPersonName { get; set; }

        public ICollection<Dish> Dishes { get; set; }

        public bool IsDeleted { get; set; }

        public Discount Discount { get; set; }
    }
}
