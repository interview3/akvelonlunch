﻿namespace AkvelonLunch.Data.Entities
{
    using System.Collections.Generic;

    using AkvelonLunch.Data.Enums;

    public class Dish
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }

        public string Description { get; set; }

        public int WeightInGramms { get; set; }

        public string DishPreviewFileName { get; set; }

        public int? ProvisionerId { get; set; }

        public Provisioner Provisioner { get; set; }

        public ICollection<Menu> Menus { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }

        public DishType DishType { get; set; }

        public int? DishTypeId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
