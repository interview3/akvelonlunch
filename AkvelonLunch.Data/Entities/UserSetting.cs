﻿namespace AkvelonLunch.Data.Entities
{
    public class UserSetting
    {
        public int? Id { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int SettingId { get; set; }

        public Setting Setting { get; set; }

        public string Value { get; set; }
    }

    public class MoneyValueSetting
    {
        public User User { get; set; }

        public int SettingId { get; set; }

        public decimal Value { get; set; }

        public string StringValue { get; set; }
    }
    
}
