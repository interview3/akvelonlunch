﻿using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class Gender
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public ICollection<User> Users { set; get; }
    }
}
