﻿using System;
using System.Collections.Generic;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Entities
{
    public class Order
    {
        public int? Id { get; set; }

        public decimal Cost { get; set; }

        public DateTime OrderTime { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int MenuId { get; set; }

        public Menu Menu { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }

        public int OrderStatusId { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<MoneyTransactionLog> MoneyTransactionLogs { get; set; }
    }
}

