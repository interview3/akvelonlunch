﻿using System;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Entities
{
    public class MoneyTransactionLog
    {
        public int? TransactionId { get; set; }

        public int AffectedUserId { get; set; }

        public User AffectedUser { get; set; }

        public int InitiatorUserId { get; set; }

        public User InitiatorUser { get; set; }

        public int OperationId { get; set; }

        public MoneyTransactionOperation Operation { get; set; }

        public DateTime TransactionTime { get; set; }

        public decimal BalanceBefore { get; set; }

        public decimal BalanceAfter { get; set; }

        public string Comment { get; set; }

        public int? OrderId { get; set; }

        public Order Order { get; set; }
    }
}
