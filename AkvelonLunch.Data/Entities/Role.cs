﻿using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class Role
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public ICollection<User> Users { set; get; }
    }
}
