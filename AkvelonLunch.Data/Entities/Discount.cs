﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonLunch.Data.Entities
{
    public class Discount
    {
        public int? ProvisionerId { get; set; }
        public Provisioner Provisioner { get; set; }
        public int NumberOfDishes { get; set; }
        public decimal DiscountSum { get; set; }

    }
}
