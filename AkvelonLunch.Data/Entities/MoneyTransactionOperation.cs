﻿using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class MoneyTransactionOperation
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<MoneyTransactionLog> TransactionLogs  { get; set; }

    }
}
