﻿using AkvelonLunch.Data.Enums;
using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class Setting
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }

        public ICollection<UserSetting> UserSettings { get; set; }
    }
}
