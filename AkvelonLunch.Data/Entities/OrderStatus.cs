﻿using System.Collections.Generic;

namespace AkvelonLunch.Data.Entities
{
    public class OrderStatus
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public ICollection<Order> Orders { set; get; }
    }
}
