﻿using System;

namespace AkvelonLunch.Data
{
    public class DalOperationStatus<T>
    {
        public DalOperationStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public T Entity { get; set; }
        public int Count { get; set; } //The number of objects written to the underlying database.
        public string BuildErrorMessageString()
        {
            string errorMessage;
            if (this.Exception != null)
                errorMessage = this.Message + '\n' + this.Exception.Message;
            else
                errorMessage = this.Message;
            return errorMessage;
        }
    }

    public enum DalOperationStatusCode
    {
        Error,
        EntityNotFound,
        NoChanges,
        ChangesSaved,
        OperationSuccessful,
        UserIsDeletedError
    }
}
