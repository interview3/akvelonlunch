﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Notifications;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Data.Repositories.UserRepository
{
    /// <summary>
    /// Class containes method access users in DB
    /// </summary>
    public class UserRepository : IUserRepository
    {
        private AkvelonLunchContext _ctx;

        public UserRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<User> GetUserById(int id)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = _ctx.Users
                    .FirstOrDefault(u => u.Id == id);
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully found.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not found.";
                }
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User find error occured.";
                os.Exception = ex;
                GlobalLogger.GetInstance().Error(ex, "Unable to find user by id.");
            }
            return os;
        }

        public DalOperationStatus<User> GetUserByLogin(string login)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = _ctx.Users.Where(user => user.Login.Equals(login, StringComparison.OrdinalIgnoreCase)).AsNoTracking().FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully found.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not found.";
                }
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User find error occured.";
                os.Exception = ex;
                GlobalLogger.GetInstance().Error(ex, "Unable to find user by login.");
            }
            return os;
        }

        public DalOperationStatus<bool> UpdateUserBalance(int userId, decimal balance, int adminId)
        {
            var os = new DalOperationStatus<bool>();
            var user = _ctx.Users.Find(userId);
            decimal balanceBefore = user.Balance;
            if (user != null)
            {
                try
                {
                    User userWithNewBalance = user;
                    userWithNewBalance.Balance = balance;
                    _ctx.Entry(user).CurrentValues.SetValues(userWithNewBalance);
                    _ctx.SaveChanges();

                    Task moneyTransactionTask = new Task(() => new MoneyTransactionLogRepository.MoneyTransactionLogRepository(this._ctx).Create(balanceBefore,
                        balance, userId, 3, null, adminId, "Зачисление на счет " + (balance - balanceBefore) + " рублей"));
                    moneyTransactionTask.Start();

                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User's balance was updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User's balance wasn't updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a user's balance.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
                os.Entity = false;
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteUser(int userId)
        {
            var os = new DalOperationStatus<bool>();
            var user = _ctx.Users.Find(userId);
            if (user != null)
            {
                try
                {
                    user.IsDeleted = true;
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not deleted.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete an user.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> UpdateUser(User user, int adminId)
        {
            var os = new DalOperationStatus<bool>();
            var userOriginal = _ctx.Users.Find(user.Id);
            if (userOriginal != null)
            {
                try
                {
                    user.GenderId = userOriginal.GenderId;

                    userOriginal.FirstName = user.FirstName;
                    userOriginal.LastName = user.LastName;
                    userOriginal.Login = user.Login;
                    userOriginal.Email = user.Email;

                    decimal balanceBefore = userOriginal.Balance;
                    decimal changedMoney = user.Balance - balanceBefore;
                    userOriginal.Balance = user.Balance;
                    userOriginal.RoleId = user.RoleId;
                    //_ctx.Entry(userOriginal).CurrentValues.SetValues(user);
                    _ctx.SaveChanges();

                    if (changedMoney != 0)
                    {
                        string comment = ((changedMoney > 0) ? "Пополнение счета " : "Списание со счета ") +
                                         Math.Abs(changedMoney) + " рублей";

                        Task moneyTransactionTask =
                            new Task(
                                () =>
                                    new MoneyTransactionLogRepository.MoneyTransactionLogRepository(_ctx).Create(
                                        balanceBefore, user.Balance, user.Id.Value, (changedMoney > 0) ? 3 : 1,
                                        null, adminId, comment));
                        moneyTransactionTask.Start();
                    }

                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User was updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User wasn't updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a user.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
                os.Entity = false;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<User>> GetUserList()
        {
            var os = new DalOperationStatus<IQueryable<User>>();
            try
            {
                os.Entity = _ctx.Users.Where(us => !us.IsDeleted).AsNoTracking().AsQueryable();
                var usersCount = os.Entity.ToList<User>().Count;
                if (usersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of users successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of users is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get users list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(User user)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                _ctx.Users.Add(user);
                if (_ctx.SaveChanges() > 0)
                {
                    SendGreetingEmail(user);
                    os.Entity = user.Id;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully added.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not added.";
                }
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User add error occured.";
                os.Exception = ex;
                GlobalLogger.GetInstance().Error(ex, "User add error occured.");
            }
            return os;
        }

        public DalOperationStatus<decimal?> GetTotalCash()
        {
            var os = new DalOperationStatus<decimal?>();
            os.Entity = null;
            try
            {
                var users = _ctx.Users;
                decimal usersBalanceSum = users.Sum(us => us.Balance);
                var orders = new OrderRepository.OrderRepository(_ctx).GetTodayMenuOrdersList().Entity;
                decimal ordersCost = orders.Where(o => !o.IsDeleted && o.OrderStatus.Id == (int)OrderStatusesEnum.Pending).Select(o => o.Cost).Sum();
                os.Entity = usersBalanceSum + ordersCost;
                os.Message = "All users balance sum was calculated successfully";
                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "Users balance sum calculation error occured.";
                os.Exception = ex;
                GlobalLogger.GetInstance().Error(ex, "Users balance sum calculation error occured.");
            }

            return os;
        }

        public DalOperationStatus<bool> UpdateUserSetting(UserSetting userSetting)
        {
            var os = new DalOperationStatus<bool>();
            try
            {
                var oldUserSetting = _ctx.UserSettings.FirstOrDefault(us => us.SettingId == userSetting.SettingId && us.UserId == userSetting.UserId);
                if (oldUserSetting != null)
                {
                    oldUserSetting.Value = userSetting.Value;
                }
                else
                {
                    _ctx.UserSettings.Add(userSetting);
                }

                if (_ctx.SaveChanges() > 0)
                {
                    os.Entity = true;
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User setting successfully updated.";
                }
                else
                {
                    os.Entity = false;
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User setting not updated.";
                }
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User setting update error occured.";
                os.Exception = ex;
                GlobalLogger.GetInstance().Error(ex, "User setting update error occured.");
            }
            return os;
        }

        /// <summary>
        /// Get users who should receive the notification of refill balance.
        /// </summary>
        /// <returns></returns>
        public DalOperationStatus<List<User>> GetUsersForBalanceChecker()
        {
            var os = new DalOperationStatus<List<User>>();

            if (_ctx.Users.Any())
            {
                var notificationsEnabledSetting = _ctx.Settings.Where(s => s.Id == (int)SettingEnum.NotificationsEnabled).AsNoTracking().FirstOrDefault();
                var notificationsEnabled = notificationsEnabledSetting != null && bool.Parse(notificationsEnabledSetting.Value);
                var minBalanceSetting = _ctx.Settings.Where(s => s.Id == (int)SettingEnum.MinBalanceValueWhenShowRefillBalancePopup).AsNoTracking().FirstOrDefault();
                if (minBalanceSetting != null)
                {
                    var minBalanceValueWhenShowRefillBalancePopup = decimal.Parse(minBalanceSetting.Value);
                    if (_ctx.UserSettings.AsNoTracking().Any())
                    {
                        os.Entity = _ctx.Users.Include("UserSettings").Where(u => u.Balance < minBalanceValueWhenShowRefillBalancePopup &&
                                                                              (u.UserSettings.Any(us => us.SettingId == (int)SettingEnum.NotificationsEnabled) &&
                                                                               u.UserSettings.FirstOrDefault(us => us.SettingId == (int)SettingEnum.NotificationsEnabled).Value.Equals("True")
                                                                               || u.UserSettings.All(us => us.SettingId != (int)SettingEnum.NotificationsEnabled) && notificationsEnabled))
                                                                               .AsNoTracking()
                                                                               .ToList();
                    }
                    else
                    {
                        os.Entity = _ctx.Users.Where(u => u.Balance < minBalanceValueWhenShowRefillBalancePopup && notificationsEnabled).AsNoTracking().ToList();
                    }
                }
            }
            else
            {
                os.Entity = null;
            }
            return os;
        }

        /// <summary>
        /// Get users who should receive the notification of refill balance.
        /// </summary>
        /// <returns></returns>
        public DalOperationStatus<List<User>> GetUsersForCreateOrderReminder()
        {
            var os = new DalOperationStatus<List<User>>();

            if (_ctx.Users.AsNoTracking().Any())
            {
                var setting = _ctx.Settings.Where(s => s.Id == (int)SettingEnum.NotificationsEnabled).AsNoTracking().FirstOrDefault();
                var notificationsEnabled = setting != null && bool.Parse(setting.Value);
                if (_ctx.UserSettings.AsNoTracking().Any())
                {
                    os.Entity = _ctx.Users.Include("UserSettings").Include("Orders")
                                    .Where(u => u.Orders.All(o => DbFunctions.TruncateTime(o.OrderTime) != DbFunctions.TruncateTime(DateTime.Today)) &&
                                (u.UserSettings.Any(us => us.SettingId == (int)SettingEnum.NotificationsEnabled) &&
                                 u.UserSettings.FirstOrDefault(us => us.SettingId == (int)SettingEnum.NotificationsEnabled).Value.Equals("True")
                                                || u.UserSettings.All(us => us.SettingId != (int)SettingEnum.NotificationsEnabled) && notificationsEnabled))
                                 .AsNoTracking()
                                 .ToList();
                }
                else
                {
                    os.Entity = _ctx.Users.Include("Orders").Where(u => u.Orders.All(o => DbFunctions.TruncateTime(o.OrderTime) != DbFunctions.TruncateTime(DateTime.Today)) && notificationsEnabled)
                        .AsNoTracking()
                        .ToList();
                }
            }
            else
            {
                os.Entity = null;
            }
            return os;
        }

        public bool GetEnabledNotificationsValue(int userId)
        {
            bool settingValue;
            var customerSetting = _ctx.UserSettings.Where(us => us.UserId == userId && us.SettingId == (int)SettingEnum.NotificationsEnabled).AsNoTracking().FirstOrDefault();
            if (customerSetting != null)
            {
                settingValue = bool.Parse(customerSetting.Value);
            }
            else
            {
                var setting = _ctx.Settings.Where(s => s.Id == (int)SettingEnum.NotificationsEnabled).AsNoTracking().FirstOrDefault();
                if (setting != null)
                {
                    settingValue = bool.Parse(setting.Value);
                }
                else
                {
                    settingValue = false;
                }
            }
            return settingValue;
        }

        private void SendGreetingEmail(User user)
        {
            string header = this.GetGreetingEmailHeader();
            string body = this.GetGreetingEmailBody(user);

            new EmailSender().Send(user.Email, header, body);
        }

        private string GetGreetingEmailHeader()
        {
            string header = "AKVELON LUNCH: Добро пожаловать!";
            return header;
        }

        private string GetGreetingEmailBody(User user)
        {
            string body = "<b>" + user.FirstName + "</b>, добро пожаловать на Akvelon Lunch - сервис заказа обедов в офис!<br/>";
            body += "Помимо веб-сайта, также доступна мобильная версия приложения под <a href=\"" + ConfigurationManager.AppSettings["AndroidApkDownloadUrl"] + "\"> Android</a> и <a href=\"" + ConfigurationManager.AppSettings["IosIpaDownloadUrl"] + "\"> iOS</a>. " +
                    "Приложение для iOS распространяется под девелоперским корпоративным аккаунтом, поэтому желающим установить приложение на iPhone-устройства необходимо отправить <a href=\"http://whatsmyudid.com/\"> свои UDID`ы</a> на почту " +
                    ConfigurationManager.AppSettings["MobileAppsDistributorEmail"] + " и получить ссылку для скачивания и установки.<br/>";
            body += "Большая просьба сообщать о пожеланиях или об ошибках в работе приложения на почту " + ConfigurationManager.AppSettings["MobileAppsDistributorEmail"] + "<br/>";
            body += "<i>Приятного аппетита!</i>";
            return body;
        }
    }
}