﻿using System.Collections.Generic;
using System.Linq;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.UserRepository
{
    public interface IUserRepository
    {
        DalOperationStatus<User> GetUserById(int id);
        DalOperationStatus<User> GetUserByLogin(string login);
        DalOperationStatus<bool> UpdateUserBalance(int userId, decimal balance, int adminId);
        DalOperationStatus<bool> DeleteUser(int userId);
        DalOperationStatus<bool> UpdateUser(User user, int adminId);
        DalOperationStatus<IQueryable<User>> GetUserList();
        DalOperationStatus<int?> Create(User user);
        DalOperationStatus<decimal?> GetTotalCash();
        DalOperationStatus<bool> UpdateUserSetting(UserSetting userSetting);
        DalOperationStatus<List<User>> GetUsersForBalanceChecker();
        DalOperationStatus<List<User>> GetUsersForCreateOrderReminder();
        bool GetEnabledNotificationsValue(int userId);
    }
}
