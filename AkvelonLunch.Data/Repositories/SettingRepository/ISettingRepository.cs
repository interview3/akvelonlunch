﻿using System.Collections.Generic;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Repositories.SettingRepository
{
    public interface ISettingRepository
    {
        DalOperationStatus<int?> Add(Setting setting);
        DalOperationStatus<Setting> GetSettingById(int id);
        DalOperationStatus<List<UserSetting>> GetSettingsOfUser(int userId);
        DalOperationStatus<UserSetting> GetSettingOfUserBySettingId(int userId, int settingId);
        DalOperationStatus<bool> Update(Setting setting);
    }
}
