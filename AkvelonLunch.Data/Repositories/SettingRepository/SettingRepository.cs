﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Data.Repositories.SettingRepository
{
    public class SettingRepository : ISettingRepository
    {
        private AkvelonLunchContext _ctx;
        public SettingRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<int?> Add(Setting setting)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                _ctx.Settings.Add(setting);
                os.Count = _ctx.SaveChanges();

                os.Entity = setting.Id;

                if (os.Entity != null)
                {
                    os.Message = "Setting created.";
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                }
                else
                {
                    os.Message = "Setting is not created.";
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Count = 0;
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you add a setting.");
                os.Message = "Unable to save changes when you add a setting.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Setting> GetSettingById(int id)
        {
            var os = new DalOperationStatus<Setting>();
            try
            {
                os.Entity = _ctx.Settings
                    .Where(c => c.Id == id)
                    .AsNoTracking()
                    .FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Setting successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Setting not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get setting by id.");
                os.Message = "Unable to get setting by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<List<UserSetting>> GetSettingsOfUser(int userId)
        {
            var os = new DalOperationStatus<List<UserSetting>>();
            try
            {
                os.Entity = _ctx.UserSettings.Where(us => us.UserId == userId).AsNoTracking().ToList();
                if (os.Entity.Count > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User settings successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.EntityNotFound;
                    os.Message = "User settings not found.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get user settings by user id.");
                os.Message = "Unable to get user settings by user id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<UserSetting> GetSettingOfUserBySettingId(int userId, int settingId)
        {
            var os = new DalOperationStatus<UserSetting>();
            try
            {
                os.Entity = _ctx.UserSettings.Where(us => us.UserId == userId && us.SettingId == settingId).AsNoTracking().FirstOrDefault();
                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User setting by setting id successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.EntityNotFound;
                    os.Message = "User settings by setting id not found.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get user setting by setting id.");
                os.Message = "Unable to get user settings by setting id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Setting setting)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Settings.Find(setting.Id);
            if (original != null)
            {
                try
                {
                    if (setting.Name == null)
                    {
                        setting.Name = original.Name;
                    }
                    _ctx.Entry(original).CurrentValues.SetValues(setting);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Setting updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Setting not updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a setting.");
                    os.Entity = true;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Setting not found.";
                os.Entity = true;
            }
            return os;
        }
    }
}
