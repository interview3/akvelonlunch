﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Utilities;
using EntityFramework.Extensions;

namespace AkvelonLunch.Data.Repositories.DishRepository
{
    public class DishRepository : IDishRepository
    {
        private AkvelonLunchContext _ctx;
        public DishRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<IList<DishType>> GetDishTypes()
        {
            var os = new DalOperationStatus<IList<DishType>>();
            try
            {
                os.Entity = _ctx.MyDishTypes.AsNoTracking().ToList();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Some problems with getting dish types!");
                os.Message = "Some problems with getting dish types!";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(Dish dish)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                dish.IsDeleted = false;
                _ctx.Dishes.Add(dish);
                os.Count = _ctx.SaveChanges();


                os.Entity = dish.Id;
                if (os.Entity != null)
                {
                    os.Message = "Dish created.";
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                }
                else
                {
                    os.Message = "Dish is not created.";
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Count = 0;
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a dish.");
                os.Message = "Unable to save changes when you create a dish.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteDish(int id, int adminId)
        {
            var os = new DalOperationStatus<bool>();
            if (_ctx.Dishes.Find(id) != null)
            {
                try
                {
                    var orderRepository = new OrderRepository.OrderRepository(_ctx);
                    DalOperationStatus<List<int?>> ordersIdList = orderRepository.GetOrderIdsByDishId(id);
                    var dish = _ctx.Dishes.Include("Menus").SingleOrDefault(d => d.Id == id);
                    if (dish != null)
                        dish.IsDeleted = true;
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Dish deleted.";
                    orderRepository.RecalculateOrdersCostAfterDishChanged(ordersIdList.Entity, id, adminId);
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not deleted.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a dish.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Dish not found.";
            }
            return os;
        }

        public DalOperationStatus<Dish> GetDish(int id)
        {
            var os = new DalOperationStatus<Dish>();
            try
            {
                os.Entity = _ctx.Dishes.Include("DishType")
                .Include("Provisioner").Where(c => c.Id == id).AsNoTracking()
                .SingleOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Dish successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get dish by id.");
                os.Message = "Unable to get dish by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Dish>> GetDishList()
        {
            var os = new DalOperationStatus<IQueryable<Dish>>();
            try
            {
                os.Entity = _ctx.Dishes.Include("DishType").Include("Provisioner").Where(c => c.IsDeleted == false).AsQueryable().AsNoTracking();
                var dishesCount = os.Entity.ToList<Dish>().Count;
                if (dishesCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Dishes list successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dishes list is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get dishes list.");
                os.Message = "Unable to get dishes list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Dish dish, int adminId)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Dishes.Include("DishType").FirstOrDefault(d => d.Id == dish.Id);
            if (original != null)
            {
                try
                {
                    var ordersIdList = _ctx.Orders.Include("OrderItems")
                        .Where(oo => oo.OrderItems
                            .Any(o => o.DishId == dish.Id))
                        .Where(o => o.IsDeleted == false)
                        .Select(o => o.Id).ToList();
                    if (ordersIdList.Count != 0)
                    {
                        os.StatusCode = DalOperationStatusCode.NoChanges;
                        os.Message = "Updating ordered dishes is prohibited";
                        os.Entity = false;
                        return os;
                    }
                    _ctx.Entry(original).CurrentValues.SetValues(dish);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Dish updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a dish.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Dish not found.";
                os.Entity = false;
            }
            return os;
        }
    }
}
