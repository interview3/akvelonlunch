﻿using System.Collections.Generic;
using System.Linq;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.DishRepository
{
    public interface IDishRepository
    {
        DalOperationStatus<IList<DishType>> GetDishTypes();
        DalOperationStatus<int?> Create(Dish dish);
        DalOperationStatus<bool> DeleteDish(int id, int adminId);
        DalOperationStatus<Dish> GetDish(int id);
        DalOperationStatus<IQueryable<Dish>> GetDishList();
        DalOperationStatus<bool> Update(Dish dish, int adminId);
    }
}
