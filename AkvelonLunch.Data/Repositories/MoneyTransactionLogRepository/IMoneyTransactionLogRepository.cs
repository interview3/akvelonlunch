﻿using System;
using System.Linq;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.MoneyTransactionLogRepository
{
    public interface IMoneyTransactionLogRepository
    {
        void Create(decimal amountBefore, decimal amountAfter, int affectedUserId, int operationId, int? entityId, int initiatorUserId, string comment = null);
        DalOperationStatus<MoneyTransactionLog> GetById(int id);
        DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>> GetFullMoneyTransactionLog();
        DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>> GetListByCountTransactionsOnPageAndPageNumber(int transactionsOnThePage, int pageNumber);
        DalOperationStatus<IQueryable<MoneyTransactionLog>> GetTransactionsOfTimeSpan(DateTime beginning, DateTime ending);
        DalOperationStatus<IQueryable<MoneyTransactionLog>> GetUserTransactions(int userId);
        DalOperationStatus<IQueryable<MoneyTransactionLog>> GetUserTransactionsOfTimeSpan(int userId, DateTime beginning, DateTime ending);
    }
}
