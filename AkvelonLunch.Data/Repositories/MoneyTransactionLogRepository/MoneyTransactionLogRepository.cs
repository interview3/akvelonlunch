﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Data.Repositories.MoneyTransactionLogRepository
{
    public class MoneyTransactionLogRepository : IMoneyTransactionLogRepository
    {
        private AkvelonLunchContext _ctx;
        public MoneyTransactionLogRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public void Create(decimal balanceBefore, decimal balanceAfter, int affectedUserId, int operationId, int? entityId, int initiatorUserId, string comment = null)
        {
            var transaction = new MoneyTransactionLog()
            {
                BalanceBefore = balanceBefore,
                BalanceAfter = balanceAfter,
                AffectedUserId = affectedUserId,
                InitiatorUserId = initiatorUserId,
                OperationId = operationId,
                TransactionTime = DateTime.Now,
                Comment = comment,
                OrderId = entityId
            };
            try
            {
                _ctx.MoneyTransactionLogs.Add(transaction);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a transaction.");
            }
        }

        public DalOperationStatus<MoneyTransactionLog> GetById(int id)
        {
            var os = new DalOperationStatus<MoneyTransactionLog>();
            try
            {
                os.Entity = _ctx.MoneyTransactionLogs
                .Where(tr => tr.TransactionId == id).AsNoTracking().SingleOrDefault();
                if (os.Entity.TransactionId > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transaction successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transaction not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transaction by id.");
                os.Message = "Unable to get money transaction by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>> GetFullMoneyTransactionLog()
        {
            var os = new DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>>();
            os.Entity = new MoneyTransactionListWithCount<MoneyTransactionLog>();
            try
            {
                os.Entity.LogList = _ctx.MoneyTransactionLogs
                    .Include("AffectedUser")
                    .Include("InitiatorUser")
                    .Include("Operation")
                    .Include("Order")
                    .AsNoTracking()
                    .ToList();
                os.Entity.LogCount = _ctx.MoneyTransactionLogs.AsNoTracking().Count();

                if (os.Entity.LogList.Count > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transactions list successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transactions list not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transactions list.");
                os.Message = "Unable to get money transactions list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>> GetListByCountTransactionsOnPageAndPageNumber(int transactionsOnThePage, int pageNumber)
        {
            var os = new DalOperationStatus<MoneyTransactionListWithCount<MoneyTransactionLog>>();
            try
            {
                os.Entity.LogList = _ctx.MoneyTransactionLogs
                    .Where(tr => (tr.TransactionId > (pageNumber - 1) * transactionsOnThePage) &
                                 (tr.TransactionId <= pageNumber * transactionsOnThePage)).AsNoTracking().ToList();

                os.Entity.LogCount = _ctx.MoneyTransactionLogs.AsNoTracking().Count();
                if (os.Entity.LogList.Count > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transactions list successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transactions list not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transactions list.");
                os.Message = "Unable to get money transactions list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<MoneyTransactionLog>> GetTransactionsOfTimeSpan(DateTime beginning, DateTime ending)
        {
            var os = new DalOperationStatus<IQueryable<MoneyTransactionLog>>();
            try
            {
                os.Entity = _ctx.MoneyTransactionLogs
                    .Include("AffectedUser").Include("InitiatorUser").Include("Operation")
                    .Where(tr => DbFunctions.TruncateTime(tr.TransactionTime) >= DbFunctions.TruncateTime(beginning)
                                 && DbFunctions.TruncateTime(tr.TransactionTime) <= DbFunctions.TruncateTime(ending))
                    .AsNoTracking()
                    .AsQueryable();
                if (os.Entity.Any())
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transactions successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transactions not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transactions.");
                os.Message = "Unable to get money transactions.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<MoneyTransactionLog>> GetUserTransactions(int userId)
        {
            var os = new DalOperationStatus<IQueryable<MoneyTransactionLog>>();
            try
            {
                os.Entity = _ctx.MoneyTransactionLogs.Include("AffectedUser").Include("InitiatorUser").Include("Operation")
                .Where(tr => tr.AffectedUserId == userId).AsNoTracking().AsQueryable();
                if (os.Entity.Any())
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transactions of user successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transactions of user not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transactions of user.");
                os.Message = "Unable to get money transactions of user.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<MoneyTransactionLog>> GetUserTransactionsOfTimeSpan(int userId, DateTime beginning, DateTime ending)
        {
            var os = new DalOperationStatus<IQueryable<MoneyTransactionLog>>();
            try
            {
                os.Entity = _ctx.MoneyTransactionLogs.Include("AffectedUser").Include("InitiatorUser").Include("Operation")
                    .Where(tr => DbFunctions.TruncateTime(tr.TransactionTime) >= DbFunctions.TruncateTime(beginning)
                && DbFunctions.TruncateTime(tr.TransactionTime) <= DbFunctions.TruncateTime(ending) && tr.AffectedUserId == userId)
                .AsNoTracking()
                .AsQueryable();
                if (os.Entity.Any())
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Money transactions of user successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Money transactions of user not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get money transactions of user.");
                os.Message = "Unable to get money transactions of user.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
    }
}
