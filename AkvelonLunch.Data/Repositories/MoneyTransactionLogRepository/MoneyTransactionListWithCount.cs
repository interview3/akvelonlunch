﻿using System.Collections.Generic;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.MoneyTransactionLogRepository
{
    public class MoneyTransactionListWithCount<T>
    {
        public List<T> LogList;
        public int LogCount;
    }
}
