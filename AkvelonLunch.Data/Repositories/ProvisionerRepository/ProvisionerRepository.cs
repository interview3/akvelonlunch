﻿using System;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Data.Repositories.ProvisionerRepository
{
    public class ProvisionerRepository : IProvisionerRepository
    {
        private AkvelonLunchContext _ctx;

        public ProvisionerRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<int?> Create(Provisioner provisioner)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                provisioner.IsDeleted = false;
                _ctx.Provisioners.Add(provisioner);
                os.Count = _ctx.SaveChanges();
                os.Entity = provisioner.Id;
                if (os.Entity != null)
                {
                    os.Message = "The provisioner is created.";
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                }
                else
                {
                    os.Message = "The provisioner is not created.";
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Count = 0;
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a provisioner.");
                os.Message = "Unable to save changes when you create a provisioner.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteProvisioner(int id)
        {
            var os = new DalOperationStatus<bool>();
            var provis = _ctx.Provisioners.Find(id);
            if (provis != null)
            {
                try
                {
                    provis.IsDeleted = true;
                    var dishes = _ctx.Dishes.Where(d => d.ProvisionerId == id);
                    foreach (var dish in dishes)
                    {
                        dish.IsDeleted = true;
                    }

                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Provisioner deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not deleted.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a provisioner.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Provisioner not found.";
            }
            return os;
        }

        public DalOperationStatus<Provisioner> GetProvisioner(int id)
        {
            var os = new DalOperationStatus<Provisioner>();
            try
            {
                os.Entity = _ctx.Provisioners
                    .Include("Discount")
                    .Where(c => c.Id == id)
                    .AsNoTracking()
                    .FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Provisioner successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioner by id.");
                os.Message = "Unable to get provisioner by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Provisioner> GetProvisionerWithDishes(int id)
        {
            var os = new DalOperationStatus<Provisioner>();
            try
            {
                os.Entity = _ctx.Provisioners
                .Include("Dishes")
                .Where(c => c.Id == id)
                .AsNoTracking()
                .SingleOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Provisioner successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioner with dishes by id.");
                os.Message = "Unable to get provisioner with dishes by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Provisioner>> GetProvisionerList()
        {
            var os = new DalOperationStatus<IQueryable<Provisioner>>();
            try
            {
                os.Entity = _ctx.Provisioners.Where(p =>p.IsDeleted == false).AsNoTracking().AsQueryable();
                var provisionersCount = os.Entity.ToList<Provisioner>().Count;
                if (provisionersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of provisioners successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of provisioners is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioners list.");
                os.Message = "Unable to get provisioners list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Provisioner provisioner)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Provisioners.Find(provisioner.Id);
            if (original != null)
            {
                try
                {
                    _ctx.Entry(original).CurrentValues.SetValues(provisioner);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Provisioner updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a provisioner.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Provisioner not found.";
                os.Entity = false;
            }
            return os;
        }

        public DalOperationStatus<bool> UpdateDiscount(Discount discount)
        {
            var os = new DalOperationStatus<bool>();
            var discountOriginal = _ctx.Discounts.Find(discount.ProvisionerId);
            if (discountOriginal != null)
            {
                try
                {
                    discountOriginal.DiscountSum = discount.DiscountSum;
                    discountOriginal.NumberOfDishes = discount.NumberOfDishes;
                    _ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Discount wasn't updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a discount.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Discount not found!";
                os.Entity = false;
            }
            return os;
        }
    }
}
