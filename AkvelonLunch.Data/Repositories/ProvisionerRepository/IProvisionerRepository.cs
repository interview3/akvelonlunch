﻿using System.Linq;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.ProvisionerRepository
{
    public interface IProvisionerRepository
    {
        DalOperationStatus<int?> Create(Provisioner provisioner);
        DalOperationStatus<bool> DeleteProvisioner(int id);
        DalOperationStatus<Provisioner> GetProvisioner(int id);
        DalOperationStatus<Provisioner> GetProvisionerWithDishes(int id);
        DalOperationStatus<IQueryable<Provisioner>> GetProvisionerList();
        DalOperationStatus<bool> Update(Provisioner provisioner);
        DalOperationStatus<bool> UpdateDiscount(Discount discount);
    }
}
