﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Utilities;
using MoreLinq;
using NUnit.Framework.Constraints;

namespace AkvelonLunch.Data.Repositories.OrderRepository
{
    internal class OrderBusinessLogic
    {
        private AkvelonLunchContext _ctx;
        public OrderBusinessLogic(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        internal decimal CalculateDiscountForOrder(Order order)
        {
            var discount = 0.0m;
            var dishes = _ctx.Dishes.AsNoTracking();
            var discounts = _ctx.Discounts.AsNoTracking();
            var provisioners = _ctx.Provisioners.Include("Discount").AsNoTracking();

            var dishesFromOrder = new List<Dish>();
            var discountsFromOrder = new List<Discount>();
            var provisionersFromOrder = new List<Provisioner>();

            foreach (var orderItem in order.OrderItems)
            {
                var dishFromCurrentOrderItem = dishes.SingleOrDefault(d => d.Id == orderItem.DishId);
                dishesFromOrder.Add(dishFromCurrentOrderItem);
                var provisionerFromCurrentOrderItem = provisioners.SingleOrDefault(p => p.Id == dishFromCurrentOrderItem.ProvisionerId);
                if (provisionerFromCurrentOrderItem.Discount != null)
                {
                    provisionersFromOrder.Add(provisionerFromCurrentOrderItem);
                    var discountFromCurrentOrderItem = discounts.SingleOrDefault(dsc => dsc.ProvisionerId == provisionerFromCurrentOrderItem.Id); //TODO: поиск, если скидки нету, то убрать из списка поставщиков в заказе
                    discountsFromOrder.Add(discountFromCurrentOrderItem);
                }

            }

            var distinctProvisioners = provisionersFromOrder.DistinctBy(p=>p.Id);
            foreach (var provisioner in distinctProvisioners)
            {
                var currentDiscount = discountsFromOrder.Find(dsc => dsc.ProvisionerId == provisioner.Id);
                if (currentDiscount.NumberOfDishes == 0)
                {
                    continue;
                }


                int saladCount = 0;
                int soupCount = 0;
                int secondCourseCount = 0;
                int dishCount = 0;
                foreach (var orderItem in order.OrderItems)
                {
                    var dishFromCurrentOrderItem = dishesFromOrder.Find(d => d.Id == orderItem.DishId);
                    if (dishFromCurrentOrderItem.ProvisionerId == provisioner.Id && (
                            dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.Salad ||
                            dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.Soup ||
                            dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.SecondCourse)
                        )
                    {
                        if (dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.Salad)
                        {
                            saladCount += orderItem.Count;
                        }
                        if (dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.Soup)
                        {
                            soupCount += orderItem.Count;
                        }
                        if (dishFromCurrentOrderItem.DishTypeId == (int)DishTypeEnum.SecondCourse)
                        {
                            secondCourseCount += orderItem.Count;
                        }
                    }
                }

                if (saladCount != 0 && soupCount != 0 && secondCourseCount != 0)
                {
                    int[] dishCountArray = { saladCount, soupCount, secondCourseCount };
                    dishCount = dishCountArray.Min() * 3;
                }
                else
                {
                    dishCount = 0;
                }

                discount += currentDiscount.DiscountSum * (dishCount - dishCount % currentDiscount.NumberOfDishes) / currentDiscount.NumberOfDishes;
            }
            return discount;
        }

        internal bool VerifyOrderDishesIsInTodaysMenu(Order order, Menu menu)
        {
            if (menu == null || menu.Dishes.Count == 0 || order.OrderItems.Count == 0)
            {
                return false;
            }

            foreach (var orderItem in order.OrderItems) //empty order is not valid
            {
                if (menu.Dishes.All(d => d.Id != orderItem.DishId))
                {
                    return false;
                }
            }

            return true;
        }

        internal bool VerifyCurrentOrderTime(Order order)
        {
            var result = false;
            var settings = new SettingRepository.SettingRepository(_ctx).GetSettingById(1);
            try
            {
                if (order.OrderTime < DateTime.Parse(settings.Entity.Value))
                    result = true;
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex);
            }
            return result;
        }

        internal decimal CalculateCostOfOrder(Order order)
        {

            var cost = 0.0m;
            var dishes = _ctx.Dishes.AsNoTracking();
            var dishesFromOrder = new List<Dish>();
            foreach (var orderItem in order.OrderItems)
            {
                var dishFromOrderItem = dishes.SingleOrDefault(d => d.Id == orderItem.DishId);
                dishesFromOrder.Add(dishFromOrderItem);
            }
            foreach (var orderItem in order.OrderItems)
            {
                cost += dishesFromOrder.SingleOrDefault(d => d.Id == orderItem.DishId).Cost * orderItem.Count;
            }
            var discount = CalculateDiscountForOrder(order);
            if (discount >= cost)
            {
                return cost;
            }
            cost -= discount;
            return cost;
        }

        internal bool CheckUserHasMadeOrderToday(int userId, int? todaysMenuId)
        {
            var userOrders = _ctx.Orders.Where(o => o.UserId == userId).AsNoTracking();
            if (!userOrders.Any())
            {
                return false;
            }
            var todaysMenuOrders = _ctx.Orders.Where(o => o.MenuId == todaysMenuId).AsNoTracking();

            if (!todaysMenuOrders.Any())
            {
                return false;
            }

            foreach (var order in todaysMenuOrders)
            {
                if (order.UserId == userId && order.IsDeleted == false)
                { return true; }
            }
            return false;
        }

        internal bool VerifyIfUserIsAbleToDeleteOrder(int orderId, int currentUserId)
        {
            User currentUser = new UserRepository.UserRepository(_ctx).GetUserById(currentUserId).Entity;
            Order order = _ctx.Orders.FirstOrDefault(o => o.Id == orderId);
            return currentUser.Id == order.UserId || currentUser.RoleId == (int)RoleEnum.Admin;
        }
    }
}
