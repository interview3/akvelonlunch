﻿using System;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using AkvelonLunch.Notifications;

namespace AkvelonLunch.Data.Repositories.OrderRepository
{
    public class OrderRepository : IOrderRepository
    {
        private AkvelonLunchContext _ctx;
        private MoneyTransactionLogRepository.MoneyTransactionLogRepository TransactLogRepository;
        private OrderBusinessLogic BusinessLogic;

        public OrderRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
            BusinessLogic = new OrderBusinessLogic(ctx);
            TransactLogRepository = new MoneyTransactionLogRepository.MoneyTransactionLogRepository(_ctx);
        }

        public DalOperationStatus<List<decimal>> GetSumsOfOrdersCostBySomeTimeSpan(DateTime[] dateTimes)
        {
            var os = new DalOperationStatus<List<decimal>>();
            try
            {
                os.Entity = new List<decimal>();
                for (var i = 0; i < dateTimes.Length-1; i += 2)
                {
                    //Too much requests to database
                    var orderCostsSumOfOneTimeSpan = _ctx.Orders
                            .Where(o => DbFunctions.TruncateTime(dateTimes[i]) <= DbFunctions.TruncateTime(o.OrderTime)
                                   && DbFunctions.TruncateTime(o.OrderTime) <= DbFunctions.TruncateTime(dateTimes[i + 1])
                                   && o.IsDeleted==false)
                            .AsNoTracking()
                            .Select(o => o.Cost).Sum();
                    os.Entity.Add(orderCostsSumOfOneTimeSpan);
                }

                if (os.Entity.Count > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of orders cost sum by list of time span successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders cost sum by list of time span not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get list of orders cost sum by list of time span.");
                os.Message = "Unable to get list of orders cost sum by list of time span.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<List<int?>> GetOrderIdsByDishId(int dishId)
        {
            var os = new DalOperationStatus<List<int?>>();
            try
            {
                os.Entity = _ctx.Orders
                    .Include("OrderItems")
                    .Where(o => DbFunctions.TruncateTime(o.OrderTime) == DbFunctions.TruncateTime(DateTime.Today))
                    .Where(oo => oo.OrderItems.Any(o => o.DishId == dishId))
                    .Where(o => o.IsDeleted == false)
                    .AsNoTracking()
                    .Select(o => o.Id)
                    .ToList();

                if (os.Entity.Count > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of order's id successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of order's id not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders including dish with given id.");
                os.Message = "Unable to get orders including dish with given id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(Order order)
        {
            var os = new DalOperationStatus<int?>();
            order.OrderTime = DateTime.Now;
            bool TimeIsCorrect = BusinessLogic.VerifyCurrentOrderTime(order);
            Menu todaysMenu = new MenuRepository.MenuRepository(_ctx).GetMenuByDate(DateTime.Now).Entity;
            if (todaysMenu == null)
            {
                os.Message = "Меню не создано";
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                return os;
            }
            User user = new UserRepository.UserRepository(_ctx).GetUserById(order.UserId).Entity;

            if (user != null && user.IsDeleted)
            {
                os.StatusCode = DalOperationStatusCode.UserIsDeletedError;
                os.Message = "Пользователь удалён";
                os.Count = 0;
                return os;
            }

            decimal balanceBefore = user.Balance;
            decimal balanceAfter;
            if (TimeIsCorrect)
            {
                try
                {
                    if (BusinessLogic.CheckUserHasMadeOrderToday(order.UserId, todaysMenu.Id))
                    {
                        os.StatusCode = DalOperationStatusCode.NoChanges;
                        os.Message = "Вы сегодня уже делали заказ!";
                        os.Count = 0;
                        return os;
                    }

                    var verifyingCondition = BusinessLogic.VerifyOrderDishesIsInTodaysMenu(order, todaysMenu);
                    if (verifyingCondition)
                    {
                        order.Cost = BusinessLogic.CalculateCostOfOrder(order);
                        foreach (var orderItem in order.OrderItems)
                        {
                            orderItem.Dish = null;
                        }
                        decimal userBalance =
                            new UserRepository.UserRepository(_ctx).GetUserById(order.UserId).Entity.Balance;
                        if (userBalance < order.Cost)
                        {
                            os.StatusCode = DalOperationStatusCode.NoChanges;
                            os.Message = "Недостаточно средств на счету";
                            os.Count = 0;
                            return os;
                        }
                        else
                        {
                            order.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            order.IsDeleted = false;
                            _ctx.Orders.Add(order);

                            user.Balance -= order.Cost;
                            _ctx.Entry(user).State = EntityState.Modified;

                            os.Count = _ctx.SaveChanges();

                            balanceAfter = user.Balance;
                            os.Entity = order.Id;
                            os.Message = "The order is created.";
                            os.StatusCode = DalOperationStatusCode.ChangesSaved;
                            string comment = "Оформлен заказ с номером " + order.Id;
                            new Task(() => TransactLogRepository.Create(balanceBefore, balanceAfter,
                                order.UserId, 1, order.Id, order.UserId, comment)).Start();
                        }
                    }
                    else
                    {
                        os.Message = "Блюда нет в сегодняшнем меню";
                        os.StatusCode = DalOperationStatusCode.Error;
                    }
                }
                catch (Exception ex)
                {
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create an order.");
                    os.Message = "Unable to save changes when you create an order.";
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Exception = ex;
                }
            }
            else
            {
                os.Message = "Unable to create order: Order can be made only before 12 hours.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteOrder(int id, int currentUserId)
        {
            var os = new DalOperationStatus<bool>();
            if (BusinessLogic.VerifyIfUserIsAbleToDeleteOrder(id, currentUserId))
            {
                var order = _ctx.Orders.FirstOrDefault(o => o.Id == id);
                var settings = new SettingRepository.SettingRepository(_ctx).GetSettingById(1);
                var currentTime = DateTime.Now.TimeOfDay;
                var blockingTime = TimeSpan.Parse(settings.Entity.Value);
                User user = new UserRepository.UserRepository(_ctx).GetUserById(order.UserId).Entity;

                if (user != null && user.IsDeleted)
                {
                    os.StatusCode = DalOperationStatusCode.UserIsDeletedError;
                    os.Message = "Пользователь удалён";
                    os.Count = 0;
                    return os;
                }

                decimal balanceBefore = user.Balance;
                decimal balanceAfter;
                if (order != null)
                {
                    if (order.OrderStatusId == (int)OrderStatusesEnum.Canceled)
                    {
                        os.StatusCode = DalOperationStatusCode.Error;
                        os.Message = "Заказ уже удалён.";
                        os.Entity = false;
                        return os;
                    }

                    if (order.OrderStatusId == (int)OrderStatusesEnum.Pending && currentTime < blockingTime)
                    {
                        try
                        {
                            if (order.IsDeleted == true)
                            {
                                os.StatusCode = DalOperationStatusCode.NoChanges;
                                os.Message = "This order is already deleted!";
                                GlobalLogger.GetInstance().Error("Unable to cancel one order twice.");
                                return os;

                            }
                            user.Balance += order.Cost;
                            _ctx.Entry(user).State = EntityState.Modified;

                            order.IsDeleted = true;
                            order.OrderStatusId = 1;
                            _ctx.SaveChanges();

                            os.StatusCode = DalOperationStatusCode.ChangesSaved;
                            os.Message = "Order deleted.";
                            os.Entity = true;
                            balanceAfter = user.Balance;
                            string comment = "Отменен заказ с номером " + id;
                            Task moneyTransactionTask = new Task(() => TransactLogRepository.Create(balanceBefore,
                                balanceAfter, order.UserId, 2, id, order.UserId, comment));
                            moneyTransactionTask.Start();
                        }
                        catch (Exception ex)
                        {
                            os.StatusCode = DalOperationStatusCode.Error;
                            os.Message = "Order not deleted.";
                            os.Exception = ex;
                            GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete an order.");
                        }
                    }

                    if (order.OrderStatusId == (int)OrderStatusesEnum.Completed)
                    {
                        os.StatusCode = DalOperationStatusCode.Error;
                        os.Message = "Нельзя отменить заказ, который уже доставлен.";
                        os.Entity = false;
                        GlobalLogger.GetInstance().Error("Unable to cancel order which is not pending.");
                        return os;
                    }

                    if (currentTime >= blockingTime)
                    {
                        os.StatusCode = DalOperationStatusCode.Error;
                        os.Message = "Нельзя отменить заказ после окончания приёма заказов.";
                        os.Entity = false;
                        GlobalLogger.GetInstance().Error("Unable to cancel order after blocking hour.");
                        return os;
                    }
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.EntityNotFound;
                    os.Message = "Order not found.";
                }
                return os;
            } else
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "Отказано в доступе, невозможно удалить заказ";
                os.Entity = false;
                GlobalLogger.GetInstance().Error("Permission error. Unable to cancel order.");
            }
            return os;
        }

        public DalOperationStatus<Order> GetOrder(int id)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                    .Include("OrderItems")
                    .Include("OrderItems.Dish")
                    .Where(o => o.Id == id)
                    .AsNoTracking()
                    .SingleOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by id.");
                os.Message = "Unable to get order by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetOrderIncludingDishes(int id)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems.Dish.DishType")
                .Include("OrderItems.Dish.Provisioner")
                .Where(o => o.Id == id)
                .AsNoTracking()
                .SingleOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by id.");
                os.Message = "Unable to get order by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetPendingOrderByUserId(int userId)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                    .Include("OrderItems")
                    .Where(c =>
                        c.UserId == userId
                        && c.OrderStatusId == (int) OrderStatusesEnum.Pending
                        && c.IsDeleted == false)
                    .AsNoTracking()
                    .FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Pending order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Pending order not found.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get pending order by user's id.");
                os.Message = "Unable to get pending order by user's id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Order>> GetOrdersByUserId(int userId)
        {
            var os = new DalOperationStatus<IQueryable<Order>>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("User").Include("OrderStatus").Include("OrderItems.Dish")
                .Where(o => 
                    o.UserId == userId
                    && o.IsDeleted == false)
                .AsNoTracking()
                .AsQueryable();
                if (os.Entity.Any())
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Orders list successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Orders list not found.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get Orders list by user's id.");
                os.Message = "Unable to get Orders list by user's id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetOrderOfUserByDate(int userId, DateTime date)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                User user = new UserRepository.UserRepository(_ctx).GetUserById(userId).Entity;

                if (user != null && user.IsDeleted)
                {
                    os.StatusCode = DalOperationStatusCode.UserIsDeletedError;
                    os.Message = "Пользователь удалён";
                    os.Count = 0;
                    return os;
                }

                os.Entity = _ctx.Orders
                    .Include("OrderItems.Dish.DishType")
                    .Include("OrderItems.Dish.Provisioner")
                    .Where(o =>
                        o.UserId == userId
                        && o.IsDeleted == false
                        && DbFunctions.TruncateTime(o.OrderTime) == DbFunctions.TruncateTime(date))
                    .AsNoTracking()
                    .FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by user's id and date.");
                os.Message = "Unable to get order by user's id and date.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<List<Order>> GetOrdersOfUserInPeriod(int userId, DateTime start, DateTime finish)
        {
            var os = new DalOperationStatus<List<Order>>();
            try
            {
                os.Entity =_ctx.Orders
                        .Include("User").Include("OrderStatus").Include("OrderItems.Dish")
                        .Where(ord => ord.UserId == userId &&
                                      ord.IsDeleted == false &&
                                      DbFunctions.TruncateTime(ord.OrderTime) >= DbFunctions.TruncateTime(start) &&
                                      DbFunctions.TruncateTime(ord.OrderTime) <= DbFunctions.TruncateTime(finish))
                        .AsNoTracking()
                        .ToList();

                os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                os.Message = "Orders successfully received.";
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by user's id and date.");
                os.Message = "Unable to get order by user's id and date.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Order>> GetOrderList()
        {
            var os = new DalOperationStatus<IQueryable<Order>>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems.Dish")
                .Include("User")
                .Where(o => o.IsDeleted == false)
                .AsNoTracking()
                .AsQueryable();
                var ordersCount = os.Entity.ToList<Order>().Count;
                if (ordersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of orders successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list.");
                os.Message = "Unable to get orders list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<List<Order>> GetTodayMenuOrdersList()
        {
            var os = new DalOperationStatus<List<Order>>();
            try
            {
                Menu todaysMenu = new MenuRepository.MenuRepository(_ctx).GetMenuByDate(DateTime.Now).Entity;

                os.Entity =
                    _ctx.Orders
                    .Include("OrderItems.Dish.DishType")
                    .Include("OrderStatus")
                    .Include("User")
                    .Where(o => o.MenuId == todaysMenu.Id.Value && o.IsDeleted == false)
                    .AsNoTracking()
                    .ToList();

                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of today orders successfully received.";
                }
                else
                {
                    os.Entity = Enumerable.Empty<Order>().ToList();
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list.");
                os.Message = "Unable to get orders list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Order>> GetOrdersListByTimeSpan(DateTime startDate, DateTime finishDate)
        {
            var os = new DalOperationStatus<IQueryable<Order>>();
            try
            {
                os.Entity = _ctx.Orders.Include("User").Include("OrderStatus").Include("OrderItems.Dish")
                .Where(ord => DbFunctions.TruncateTime(startDate) <= DbFunctions.TruncateTime(ord.OrderTime))
                .Where(ord => DbFunctions.TruncateTime(ord.OrderTime) <= DbFunctions.TruncateTime(finishDate))
                .Where(o => o.IsDeleted == false)
                .AsNoTracking()
                .AsQueryable();

                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of orders by time span successfully received.";
                }
                else
                {
                    os.Entity = Enumerable.Empty<Order>().AsQueryable();
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list by time span.");
                os.Message = "Unable to get orders list by time span.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Order>> GetOrdersListByDate(DateTime date)
        {
            var os = new DalOperationStatus<IQueryable<Order>>();
            try
            {
                os.Entity = _ctx.Orders
                .Where(ord => DbFunctions.TruncateTime(ord.OrderTime) == DbFunctions.TruncateTime(date))
                .Where(o => o.IsDeleted == false)
                .AsNoTracking()
                .AsQueryable();

                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of orders by date successfully received.";
                }
                else
                {
                    os.Entity = Enumerable.Empty<Order>().AsQueryable();
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list by date.");
                os.Message = "Unable to get orders list by date.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<OrderItem>> GetTodayOrderItemsOfProvisioner(int provisionerId)
        {
            var os = new DalOperationStatus<IQueryable<OrderItem>>();
            try
            {
                var allItemsOfProvisioner =
                    _ctx.OrderItems
                    .Where(ord => ord.Dish.ProvisionerId == provisionerId)
                    .Where(oi => oi.Order.IsDeleted == false)
                    .Include("Dish")
                    .AsNoTracking();
                var todayOrdersId = this.GetTodayMenuOrdersList().Entity.Select(ord => ord.Id);
                os.Entity = allItemsOfProvisioner.Where(item => todayOrdersId.Contains(item.OrderId));

                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of today orders successfully received.";
                }
                else
                {
                    os.Entity = Enumerable.Empty<OrderItem>().AsQueryable();
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list.");
                os.Message = "Unable to get orders list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<List<User>> GetUsersWhoMadeOrderByProvisionerId(int provisionerId, DateTime start, DateTime finish)
        {
            var os = new DalOperationStatus<List<User>>();
            os.Entity = new List<User>();
            try
            {
                var users = _ctx.Users;
                foreach (User user in users)
                {
                    var orders =
                        (_ctx).Orders
                            .Where(ord => ord.UserId == user.Id && ord.IsDeleted==false &&
                            DbFunctions.TruncateTime(ord.OrderTime) >= DbFunctions.TruncateTime(start) &&
                            DbFunctions.TruncateTime(ord.OrderTime) <= DbFunctions.TruncateTime(finish))
                            .Include("OrderItems.Dish.DishType")
                            .Include("OrderItems.Dish.Provisioner")
                            .Where(ord => ord.OrderItems.Count(it => it.Dish.ProvisionerId == provisionerId) > 0)
                            .AsNoTracking()
                            .AsQueryable();

                    if (orders.Any())
                    {
                        os.Entity.Add(user);
                    }
                }

                if (os.Entity != null)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of users who made orders today successfully received.";
                }
                else
                {
                    os.Entity = new List<User>();
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of users is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list.");
                os.Message = "Unable to get orders list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> NotifyAboutDelivering(DateTime date)
        {
            var os = new DalOperationStatus<bool>();
            try
            {
                var orders = _ctx.Orders
                    .Where(ord => ord.IsDeleted == false)
                    .Where(ord => DbFunctions.TruncateTime(ord.OrderTime) == DbFunctions.TruncateTime(date))
                    .Where(ord => ord.OrderStatusId == (int)OrderStatusesEnum.Pending)
                    .Include("User").Include("OrderItems.Dish");

                var ordersToNofify = orders.ToList();

                foreach (Order order in orders)
                {
                    order.OrderStatusId = (int)OrderStatusesEnum.Completed;
                }
                _ctx.SaveChanges();

                try
                {
                    new Task(() => SendEmailsToOrdersUsers(ordersToNofify)).Start();
                }
                catch (Exception ex)
                {
                    GlobalLogger.GetInstance().Error(ex, "Unable to send email.");
                }


                os.Entity = true;
                os.StatusCode = DalOperationStatusCode.ChangesSaved;
                os.Message = "Orders delivering notification changed success";
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to modify orders.");
                os.Message = "Unable to modify orders.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }

            return os;
        }
        
        public DalOperationStatus<string> GetOrderStatusString(int statusId)
        {
            var os = new DalOperationStatus<string>();
            try
            {
                var status = _ctx.OrderStatuses.Where(st => st.Id == statusId).AsNoTracking().FirstOrDefault();
                if (status != null)
                {
                    os.Entity = status.Name;
                    os.Message = "Status name was received successfully";
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                }
            }
            catch (Exception ex)
            {
                os.Entity = null;
                os.Message = "Status was not found";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
                GlobalLogger.GetInstance().Error(ex, "Status was not found");
            }

            return os;
        }

        public DalOperationStatus<List<Order>> GetFilteredOrders(int provisionerId, DateTime start, DateTime finish)
        {
            var os = new DalOperationStatus<List<Order>>();
            try
            {

                os.Entity = _ctx.Orders
                    .Include("OrderItems.Dish")
                    .Include("User")
                    .Where(ord => ord.IsDeleted == false &&
                                  ord.OrderItems.Count(it => it.Dish.ProvisionerId == provisionerId) > 0 &&
                                  DbFunctions.TruncateTime(ord.OrderTime) >= DbFunctions.TruncateTime(start) &&
                                  DbFunctions.TruncateTime(ord.OrderTime) <= DbFunctions.TruncateTime(finish))
                    .AsNoTracking()
                    .ToList();
                os.Message = "Filtered list received successfully";
                os.StatusCode=DalOperationStatusCode.OperationSuccessful;
            }
            catch (Exception ex)
            {
                os.Entity = new List<Order>();
                os.Message = "Cannot receive filtered orders list";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
                GlobalLogger.GetInstance().Error(ex, "Cannot receive filtered orders list");
            }

            return os;
        }

        public void RecalculateOrdersCostAfterDishChanged(List<int?> ordersIdList, int ChangedDishId, int adminId, bool ItIsDelete = true)
        {
            Order order;
            User user;
            foreach (var orderId in ordersIdList)
            {
                if (orderId != null)
                {
                    order = GetOrderIncludingDishes(orderId.Value).Entity;
                    user = new UserRepository.UserRepository(_ctx).GetUserById(order.UserId).Entity;
                    decimal orderCostBefore = order.Cost;
                    decimal balanceBefore = user.Balance;
                    order.Cost = BusinessLogic.CalculateCostOfOrder(order);
                    decimal orderCostAfter = order.Cost;
                    decimal balanceAfter = balanceBefore + orderCostBefore - orderCostAfter;
                    string comment;
                    if (order.Cost == 0)
                    {
                        DeleteOrder(order.Id.Value, adminId);
                    }
                    if (ItIsDelete)
                    {
                        comment = "Возрат средств по причине удаления из базы блюда №" + ChangedDishId;
                    }
                    else
                    {
                        comment = "В базе изменена стоимость блюда №" + ChangedDishId;
                    }
                    new Task(() => TransactLogRepository.Create(balanceBefore, balanceAfter, user.Id.Value,
                        (int)MoneyTransactionOperationEnum.CacheWithdrawal, ChangedDishId, user.Id.Value, comment)).Start();

                    new UserRepository.UserRepository(_ctx).UpdateUserBalance(user.Id.Value, balanceAfter, adminId);
                }
            }
            _ctx.SaveChanges();
        }

        public bool WereTodayOrdersDelieveried()
        {
            var todayOrders = this.GetTodayMenuOrdersList().Entity ?? new List<Order>();
            return (todayOrders.Any() && todayOrders.All(ord => ord.OrderStatusId == (int)OrderStatusesEnum.Completed));
        }

        private string GetOrderCompletedEmailHeader(Order order)
        {
            string header = "AKVELON-LUNCH: Заказ доставлен";
            return header;
        }

        private string GetOrderCompletedEmailBody(Order order)
        {
            string body = "Здравствуйте, " + order.User.FirstName + "!<br/> <b>Ваш заказ №" + order.Id + " доставлен.</b> <br/><br/>Вы заказали:<br/>";
            foreach (var orderItem in order.OrderItems)
            {
                body += orderItem.Dish.Name + "  -  " + orderItem.Count + " шт.<br/>";
            }
            body += "________________<br/>";
            body += "Стоимость заказа: " + order.Cost + " руб.";
            body += "<br/>Текущий баланс: " + order.User.Balance + " руб.";
            body += "<br/><br/><i>Приятного аппетита!</i>";
            return body;
        }

        private void SendEmailsToOrdersUsers(List<Order> orders)
        {
            foreach (Order order in orders)
            {
                string header = this.GetOrderCompletedEmailHeader(order);
                string body = this.GetOrderCompletedEmailBody(order);

                new EmailSender().Send(order.User.Email, header, body);
            }
        }
    }
}
