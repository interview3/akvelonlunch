﻿using System;
using System.Linq;
using AkvelonLunch.Data.Entities;
using System.Collections.Generic;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Repositories.OrderRepository
{
    public interface IOrderRepository
    {
        DalOperationStatus<List<decimal>> GetSumsOfOrdersCostBySomeTimeSpan(DateTime[] dateTimes);
        DalOperationStatus<List<int?>> GetOrderIdsByDishId(int dishId);
        DalOperationStatus<int?> Create(Order order);
        DalOperationStatus<bool> DeleteOrder(int id, int currentUserId);
        DalOperationStatus<Order> GetOrder(int id);
        DalOperationStatus<Order> GetOrderIncludingDishes(int id);
        DalOperationStatus<Order> GetPendingOrderByUserId(int userId);
        DalOperationStatus<IQueryable<Order>> GetOrdersByUserId(int userId);
        DalOperationStatus<Order> GetOrderOfUserByDate(int userId, DateTime date);
        DalOperationStatus<List<Order>> GetOrdersOfUserInPeriod(int userId, DateTime start, DateTime finish);
        DalOperationStatus<IQueryable<Order>> GetOrderList();
        DalOperationStatus<List<Order>> GetTodayMenuOrdersList();
        DalOperationStatus<IQueryable<Order>> GetOrdersListByTimeSpan(DateTime startDate, DateTime finishDate);
        DalOperationStatus<IQueryable<Order>> GetOrdersListByDate(DateTime date);
        DalOperationStatus<IQueryable<OrderItem>> GetTodayOrderItemsOfProvisioner(int provisionerId);
        DalOperationStatus<List<User>> GetUsersWhoMadeOrderByProvisionerId(int provisionerId, DateTime start, DateTime finish);
        DalOperationStatus<bool> NotifyAboutDelivering(DateTime date);
        DalOperationStatus<string> GetOrderStatusString(int statusId);
        DalOperationStatus<List<Order>> GetFilteredOrders(int provisionerId, DateTime start, DateTime finish);
        void RecalculateOrdersCostAfterDishChanged(List<int?> ordersIdList, int ChangedDishId, int adminId, bool ItIsDelete = true);
        bool WereTodayOrdersDelieveried();
    }
}
