﻿using System;
using System.Linq;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Repositories.MenuRepository
{
    public interface IMenuRepository
    {
        DalOperationStatus<int?> Create(Menu menu);
        DalOperationStatus<bool> Delete(int id);
        DalOperationStatus<Menu> GetMenuByDate(DateTime date);
        DalOperationStatus<Menu> GetMenuByDateAndByUserId(DateTime date, int userId);
        DalOperationStatus<Menu> GetMenu(int id);
        DalOperationStatus<IQueryable<Menu>> GetMenuList();
        DalOperationStatus<bool> Update(Menu menu);
        bool MenuIsBlocked();
    }
}
