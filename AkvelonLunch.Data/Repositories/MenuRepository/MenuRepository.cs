﻿using System;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Data.Repositories.MenuRepository
{
    public class MenuRepository : IMenuRepository
    {
        private AkvelonLunchContext _ctx;

        public MenuRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<int?> Create(Menu menu)
        {
            var os = new DalOperationStatus<int?>();
            try
            {
                menu.IsDeleted = false;
                foreach (var dish in menu.Dishes)
                {
                    this._ctx.Dishes.Attach(dish);
                }

                this._ctx.Menus.Add(menu);
                os.Count = this._ctx.SaveChanges();

                os.Entity = menu.Id;

                if (os.Entity != null)
                {
                    os.Message = "Menu created.";
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                }
                else
                {
                    os.Message = "Menu is not created.";
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Count = 0;
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a menu.");
                os.Message = "Unable to save changes when you create a menu.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> Delete(int id)
        {
            var os = new DalOperationStatus<bool>();
            var menu = _ctx.Menus.Find(id);
            if (menu != null)
            {
                var orders = _ctx.Orders.Where(ord => !ord.IsDeleted &&
                                                      ord.MenuId == id);
                if (orders.Any())
                {
                    os.Entity = false;
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu could not be deleted because some orders use it.";
                    return os;
                }

                try
                {
                    menu.IsDeleted = true;
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Menu deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not deleted.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a dish.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Menu not found.";
            }
            return os;
        }

        public DalOperationStatus<Menu> GetMenuByDate(DateTime date)
        {
            var os = new DalOperationStatus<Menu>();
            try
            {
                os.Entity =
                    _ctx.Menus.Include("Dishes.DishType").Include("Dishes.Provisioner")
                        .Where(
                            m =>
                                (m.IsDeleted == false) &&
                                (DbFunctions.TruncateTime(m.DateToShow) == DbFunctions.TruncateTime(date)))
                        .AsNoTracking()
                        .FirstOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Menu successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menu by date.");
                os.Message = "Unable to get menu by date.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Menu> GetMenuByDateAndByUserId(DateTime date, int userId)
        {
            var os = new DalOperationStatus<Menu>();

            User user = new UserRepository.UserRepository(_ctx).GetUserById(userId).Entity;

            if (user != null && user.IsDeleted)
            {
                os.StatusCode = DalOperationStatusCode.UserIsDeletedError;
                os.Message = "Пользователь удалён";
                os.Count = 0;
                return os;
            }

            return this.GetMenuByDate(date);
        }

        public DalOperationStatus<Menu> GetMenu(int id)
        {
            var os = new DalOperationStatus<Menu>();
            try
            {
                os.Entity = _ctx.Menus.Include("Dishes").Where(c => c.Id == id)
                    .AsNoTracking()
                    .SingleOrDefault();
                if (os.Entity != null && os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Menu successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menu by id.");
                os.Message = "Unable to get menu by id.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Menu>> GetMenuList()
        {
            var os = new DalOperationStatus<IQueryable<Menu>>();
            try
            {
                os.Entity = _ctx.Menus.Include("Dishes.DishType").Include("Dishes.Provisioner")
                    .Where(m => m.IsDeleted == false).AsQueryable().AsNoTracking();
                var menusCount = os.Entity.ToList<Menu>().Count;
                if (menusCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of menus successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of menus is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menus list.");
                os.Message = "Unable to get menus list.";
                os.Exception = ex;
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Menu menu)
        {
            var os = new DalOperationStatus<bool>();
            menu.DateToShow = DateTime.Now;
            var original = _ctx.Menus.Include("Dishes.DishType").Include("Dishes.Provisioner").FirstOrDefault(m => m.Id == menu.Id);
            var orderStatuses = _ctx.OrderStatuses;
            if (original != null)
            {
                try
                {
                    var removedDishes = original.Dishes.Where(od => menu.Dishes.All(d => d.Id != od.Id));

                    if (removedDishes.Any())
                    {
                        menu.Orders = _ctx.Orders.Include("OrderItems").Where(o => o.MenuId == menu.Id.Value && o.OrderStatus.Equals(orderStatuses.FirstOrDefault(ors=>ors.Id == 2))).ToList();
                        foreach (var order in menu.Orders)
                        {
                            foreach (var orderItem in order.OrderItems)
                            {
                                if (removedDishes.Any(d => d.Id == orderItem.DishId))
                                {
                                    os.Entity = false;
                                    os.StatusCode = DalOperationStatusCode.Error;
                                    os.Message = "Cannot delete used dishes from menu. Unable to update.";
                                    return os;
                                }
                            }
                        }
                    }


                    foreach (var dish in removedDishes.ToList())
                    {
                        this._ctx.Dishes.Attach(dish);
                        original.Dishes.Remove(dish);
                    }

                    var addedDishes = menu.Dishes.Where(d => original.Dishes.All(od => od.Id != d.Id));
                    foreach (var dish in addedDishes)
                    {
                        this._ctx.Dishes.Attach(dish);
                        original.Dishes.Add(dish);
                    }

                    this._ctx.Entry(original).State = EntityState.Modified;

                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Menu updated.";
                    os.Entity = true;
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not updated.";
                    os.Exception = ex;
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a menu.");
                    os.Entity = false;
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Menu not found.";
                os.Entity = false;
            }
            return os;
        }

        public bool MenuIsBlocked()
        {
            TimeSpan blockingHour = TimeSpan.Parse(new SettingRepository.SettingRepository(_ctx).GetSettingById((int)SettingEnum.BlockingHour).Entity.Value);
            TimeSpan now = DateTime.Now.TimeOfDay;
            if (now < blockingHour)
                return false;
            return true;
        }
    }
}
