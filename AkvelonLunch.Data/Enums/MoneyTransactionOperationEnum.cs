﻿namespace AkvelonLunch.Data.Enums
{
    public enum MoneyTransactionOperationEnum
    {
        Payment = 1,
        CacheWithdrawal,
        CacheIncrease
    }
}
