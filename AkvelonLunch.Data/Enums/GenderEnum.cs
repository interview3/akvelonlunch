﻿namespace AkvelonLunch.Data.Enums
{
    public enum GenderEnum
    {
        Male = 1,
        Female = 2
    }
}
