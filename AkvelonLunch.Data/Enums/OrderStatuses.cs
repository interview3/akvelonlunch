﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonLunch.Data.Enums
{
    public enum OrderStatuses
    {
        Canceled = -1,
        Pending,
        Completed
    }
}
