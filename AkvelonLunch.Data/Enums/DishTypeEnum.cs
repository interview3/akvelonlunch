﻿namespace AkvelonLunch.Data.Enums
{
    public enum DishTypeEnum
    {
        Soup =1,
        SecondCourse,
        Salad,
        Other
    }
}
