﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonLunch.Data.Enums
{
    public enum SettingEnum
    {
        BlockingHour = 1,
        NotificationsEnabled,
        RefillBalanceNotificationTime,
        MinBalanceValueWhenShowRefillBalancePopup,
        MakeOrderNotificationTime
    }
}
