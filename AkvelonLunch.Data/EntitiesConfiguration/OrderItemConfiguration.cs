﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class OrderItemConfiguration : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemConfiguration()
        {
            this.ToTable("OrderItems");

            this.HasKey(oi => oi.Id);
            this.Property(oi => oi.Id).IsRequired();
            this.Property(oi => oi.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(oi => oi.Count).IsRequired();

            this.HasRequired(oi => oi.Dish).WithMany(d => d.OrderItems).HasForeignKey(oi => oi.DishId);

            this.HasRequired(oi => oi.Order).WithMany(o => o.OrderItems).HasForeignKey(oi => oi.OrderId);
        }
    }
}
