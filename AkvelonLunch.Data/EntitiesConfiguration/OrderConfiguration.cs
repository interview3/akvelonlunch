﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            this.ToTable("Orders");

            this.HasKey(o => o.Id);
            this.Property(o => o.Id).IsRequired();
            this.Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(o => o.Cost).IsRequired();

            this.Property(o => o.OrderTime).IsRequired();

            this.Property(o => o.IsDeleted).IsRequired();

            this.HasRequired(o => o.OrderStatus).WithMany(os => os.Orders).HasForeignKey(o => o.OrderStatusId);

            this.HasRequired(o => o.User).WithMany(u => u.Orders).HasForeignKey(o => o.UserId);

            this.HasMany(o => o.OrderItems).WithRequired().HasForeignKey(oi => oi.OrderId);

            this.HasRequired(o => o.Menu).WithMany(m => m.Orders).HasForeignKey(o => o.MenuId).WillCascadeOnDelete(false);
        }
    }
}
