﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class MyDishTypeConfiguration : EntityTypeConfiguration<DishType>
    {
        public MyDishTypeConfiguration()
        {
            this.ToTable("DishTypes");
            
            this.HasKey(d => d.Id);
            
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.Name).IsRequired();
            this.Property(d => d.Name).HasMaxLength(127);

            this.HasMany(t => t.Dishes).WithRequired().HasForeignKey(d => d.DishTypeId);
        }
    }
}
