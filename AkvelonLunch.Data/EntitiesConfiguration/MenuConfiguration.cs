﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class MenuConfiguration: EntityTypeConfiguration<Menu>
    {
        public MenuConfiguration()
        {
            this.ToTable("Menus");

            this.HasKey(m => m.Id);
            this.Property(m => m.Id).IsRequired();
            this.Property(m => m.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(m => m.DateToShow).IsRequired();

            this.HasRequired(m => m.Creator).WithMany(d => d.CreatedMenus).HasForeignKey(d => d.CreatorId);

            this.Property(m => m.IsDeleted).IsRequired();

            this.HasMany(m => m.Dishes).WithMany(d => d.Menus);

            this.HasMany(m => m.Orders).WithRequired().HasForeignKey(o => o.MenuId).WillCascadeOnDelete(false);
        }
    }
}
