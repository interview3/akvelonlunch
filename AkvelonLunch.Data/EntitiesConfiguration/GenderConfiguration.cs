﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class GenderConfiguration : EntityTypeConfiguration<Gender>
    {
        public GenderConfiguration()
        {
            this.ToTable("Genders");

            this.HasKey(d => d.Id);
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.Name).IsRequired();
            this.Property(d => d.Name).HasMaxLength(127);

            this.HasMany(t => t.Users).WithRequired().HasForeignKey(d => d.GenderId);
        }
    }
}
