﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class DiscountConfiguration : EntityTypeConfiguration<Discount>
    {
        public DiscountConfiguration()
        {
            this.ToTable("Discounts");

            this.HasKey(dsc => dsc.ProvisionerId);

            this.Property(dsc => dsc.DiscountSum).IsRequired();

            this.Property(dsc => dsc.NumberOfDishes).IsRequired();

            this.HasRequired(dsc => dsc.Provisioner).WithOptional(p=>p.Discount);
        }
    }
}
