﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Infrastructure.Annotations;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.ToTable("Users");

            this.HasKey(u => u.Id);
            this.Property(u => u.Id).IsRequired();
            this.Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(u => u.Login).IsRequired();
            this.Property(u => u.Login).HasMaxLength(120);
            this.Property(u => u.Login)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_UniqueLogin") {IsUnique = true}));

            this.Property(u => u.FirstName).IsRequired();
            this.Property(u => u.FirstName).HasMaxLength(35);

            this.Property(u => u.SecondName).IsOptional();
            this.Property(u => u.SecondName).HasMaxLength(70);

            this.Property(u => u.LastName).IsRequired();
            this.Property(u => u.LastName).HasMaxLength(70);

            this.Property(u => u.Balance).IsRequired();

            this.Property(u => u.Email).IsOptional();
            this.Property(u => u.Email).HasMaxLength(255);

            this.HasRequired(u => u.Gender).WithMany(g => g.Users).HasForeignKey(u => u.GenderId);

            this.HasRequired(u => u.Role).WithMany(r => r.Users).HasForeignKey(u => u.RoleId);

            this.HasMany(u => u.Orders).WithRequired().HasForeignKey(o => o.UserId);

            this.HasMany(u => u.CreatedMenus).WithRequired().HasForeignKey(m => m.CreatorId);

            this.HasMany(d => d.UserSettings).WithRequired().HasForeignKey(oi => oi.UserId);

            this.Property(o => o.IsDeleted).IsRequired();
        }
    }
}
