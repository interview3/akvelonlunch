﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class SettingConfiguration : EntityTypeConfiguration<Setting>
    {
        public SettingConfiguration()
        {
            this.ToTable("Settings");

            this.HasKey(s => s.Id);
            this.Property(s => s.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(s => s.Id).IsRequired();

            this.Property(s => s.Name).IsRequired();
            this.Property(s => s.Name).HasMaxLength(127);

            this.Property(s => s.Value).IsRequired();
            this.Property(s => s.Value).HasMaxLength(1000);
            
            this.Property(s => s.Description).IsOptional();
            this.Property(s => s.Description).HasMaxLength(1000);

            this.HasMany(d => d.UserSettings).WithRequired().HasForeignKey(oi => oi.SettingId);
        }
    }
}
