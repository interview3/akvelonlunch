﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class DishConfiguration: EntityTypeConfiguration<Dish>
    {
        public DishConfiguration()
        {
            this.ToTable("Dishes");

            this.HasKey(d => d.Id);
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.Name).IsRequired();
            this.Property(d => d.Name).HasMaxLength(127);

            this.Property(d => d.Cost).IsRequired();

            this.Property(d => d.Description).IsOptional();
            this.Property(d => d.Description).HasMaxLength(1000);

            this.Property(d => d.WeightInGramms).IsOptional();

            this.Property(d => d.DishPreviewFileName).IsOptional();

            this.Property(d => d.IsDeleted).IsRequired();

            this.HasRequired(d => d.DishType).WithMany(t => t.Dishes).HasForeignKey(d => d.DishTypeId);

            this.HasRequired(d => d.Provisioner).WithMany(p => p.Dishes).HasForeignKey(d => d.ProvisionerId);

            this.HasMany(d => d.OrderItems).WithRequired().HasForeignKey(oi => oi.DishId);

            this.HasMany(d => d.Menus).WithMany(m => m.Dishes);
        }
    }
}
