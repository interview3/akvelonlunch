﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class UserSettingConfiguration : EntityTypeConfiguration<UserSetting>
    {
        public UserSettingConfiguration()
        {
            this.ToTable("UserSettings");

            this.HasKey(d => d.Id);
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.UserId).IsRequired();

            this.Property(d => d.SettingId).IsRequired();

            this.Property(d => d.Value).IsRequired();
            this.Property(d => d.Value).HasMaxLength(1000);

            this.HasRequired(oi => oi.Setting).WithMany(s => s.UserSettings).HasForeignKey(oi => oi.SettingId);
            this.HasRequired(oi => oi.User).WithMany(s => s.UserSettings).HasForeignKey(oi => oi.UserId);
        }
    }
}
