﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class MoneyTransactionOperationConfiguration : EntityTypeConfiguration<MoneyTransactionOperation>
    {
        public MoneyTransactionOperationConfiguration()
        {
            this.ToTable("MoneyTransactionOperations");

            this.HasKey(d => d.Id);
            this.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(d => d.Id).IsRequired();

            this.Property(d => d.Name).IsRequired();
            this.Property(d => d.Name).HasMaxLength(127);

            this.Property(d => d.Description).IsOptional();
            this.Property(d => d.Description).HasMaxLength(1000);

            this.HasMany(o => o.TransactionLogs).WithRequired().HasForeignKey(oi => oi.OperationId);
        }
    }
}
