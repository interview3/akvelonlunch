﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class ProvisionerConfiguration : EntityTypeConfiguration<Provisioner>
    {
        public ProvisionerConfiguration()
        {
            this.ToTable("Provisioners");

            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.Id).IsRequired();

            this.Property(p => p.SupplierName).IsRequired();
            this.Property(p => p.SupplierName).HasMaxLength(255);

            this.Property(p => p.Address).IsOptional();
            this.Property(p => p.Address).HasMaxLength(255);

            this.Property(p => p.Phone).IsRequired();
            this.Property(p => p.Phone).HasMaxLength(35);

            this.Property(p => p.Email).IsOptional();
            this.Property(p => p.Email).HasMaxLength(255);

            this.Property(p => p.IsDeleted).IsRequired();

            this.Property(p => p.ContactPersonName).IsOptional();
            this.Property(p => p.ContactPersonName).HasMaxLength(127);

            this.HasMany(p => p.Dishes).WithRequired().HasForeignKey(d => d.ProvisionerId);

            this.HasOptional(p => p.Discount);
        }
    }
}
