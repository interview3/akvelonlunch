﻿using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.EntitiesConfiguration
{
    class MoneyTransactionLogConfiguration : EntityTypeConfiguration<MoneyTransactionLog>
    {
        public MoneyTransactionLogConfiguration()
        {
            this.ToTable("MoneyTransactionLogs");

            this.HasKey(tl => tl.TransactionId);
            this.Property(tl => tl.TransactionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(tl => tl.TransactionId).IsRequired();

            this.Property(tl => tl.Comment).IsOptional();
            this.Property(tl => tl.Comment).HasMaxLength(255);

            this.Property(tl => tl.InitiatorUserId).IsOptional();

            this.HasRequired(tl => tl.InitiatorUser).WithMany().HasForeignKey(tl => tl.InitiatorUserId).WillCascadeOnDelete(false); ;

            this.Property(tl => tl.AffectedUserId).IsRequired();

            this.HasRequired(tl => tl.AffectedUser).WithMany().HasForeignKey(tl => tl.AffectedUserId).WillCascadeOnDelete(false); ;

            this.Property(tl => tl.BalanceAfter).IsRequired();

            this.Property(tl => tl.BalanceBefore).IsRequired();

            this.HasRequired(tl => tl.Operation).WithMany(o => o.TransactionLogs).HasForeignKey(tl => tl.OperationId);

            this.Property(tl => tl.TransactionTime).IsRequired();

            this.Property(tl => tl.OrderId).IsOptional();

            this.HasOptional(tl => tl.Order).WithMany(o => o.MoneyTransactionLogs).HasForeignKey(tl => tl.OrderId);
        }
    }
}
