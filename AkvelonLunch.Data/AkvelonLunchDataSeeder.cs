﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data
{
    class AkvelonLunchDataSeeder
    {
        private readonly AkvelonLunchContext _context;

        public AkvelonLunchDataSeeder(AkvelonLunchContext context)
        {
            this._context = context;
        }

        public void Seed()
        {
            if (this._context.Roles.Any())
            {
                return;
            }

            //dictionaries
            _context.Roles.AddRange(RolesSeedData);
            _context.Genders.AddRange(GendersSeedData);
            _context.OrderStatuses.AddRange(OrderStatusesSeedData);
            _context.MyDishTypes.AddRange(MyDishTypeSeedData);
            _context.Settings.AddRange(SettingsSeedData);
            _context.MoneyTransactionOperations.AddRange(MoneyTransactionOperationsSeedData);

            //fakes
            _context.Provisioners.AddRange(ProvisionersSeedData);
            _context.Discounts.AddRange(DiscountsSeedData);
            _context.Users.AddRange(UsersSeedData);
            _context.Dishes.AddRange(DishesSeedData);
            _context.Menus.AddRange(MenuSeedData);
            _context.Orders.AddRange(OrdersSeedData);
            _context.MoneyTransactionLogs.AddRange(MoneyTransactionLogsSeedData);

            this._context.SaveChanges();
        }

        private static readonly IList<Discount> DiscountsSeedData = new List<Discount>
        {
            new Discount() {ProvisionerId = 1, DiscountSum = 20.0m, NumberOfDishes = 3}
        };

        private static readonly IList<Provisioner> ProvisionersSeedData = new List<Provisioner>
        {
            new Provisioner() { Id = 1, Address = "ул. Пушкина дом Колотушкина", ContactPersonName = "Женя", Email = "a@b.com", Phone = "+70000000", SupplierName = "Обеды в офис", Discount = DiscountsSeedData[0]}
        };
        private static readonly IList<DishType> MyDishTypeSeedData = new List<DishType>
        {
            new DishType() { Id=0, Name ="Суп"},
            new DishType() { Id=1, Name ="Второе блюдо"},
            new DishType() { Id=2, Name ="Салат"},
            new DishType() { Id=3, Name ="Дополнительное"}
        };

        private static readonly IList<Dish> DishesSeedData = new List<Dish>
        {
            new Dish {Id= 1, Name = "Брамбова полевка", Cost = 25.0m, DishType = MyDishTypeSeedData[0], Provisioner = ProvisionersSeedData.First(), Description = "густой суп из картофеля и грибов со сметанной заправкой. Подается в хлебной плошке"},
            new Dish {Id = 2, Name  = "Рыбная полевка",Cost = 34.0m, DishType = MyDishTypeSeedData[0], Provisioner = ProvisionersSeedData.First(), Description = "уха из карпа и лисичек"},
            new Dish {Id = 3, Name = "Суп панадель",Cost = 20.0m, DishType = MyDishTypeSeedData[0], Provisioner = ProvisionersSeedData.First(), Description = "сытная похлебка из домашней курочки с блинчиками. Подается со сметаной"},
            new Dish {Id = 4, Name = "Суп кремовый",Cost = 26, DishType = MyDishTypeSeedData[0], Provisioner = ProvisionersSeedData.First()},
            new Dish {Id = 5, Name = "Суп-фрэш холодный из вишни",Cost = 22.0m, DishType = MyDishTypeSeedData[0], Provisioner = ProvisionersSeedData.First(), Description = "со сметаной и белым ромом"},
            new Dish {Id = 6, Name =  "Лобстер по-Гавайски",Cost = 5000.0m, DishType = MyDishTypeSeedData[1], Provisioner = ProvisionersSeedData.First(), Description = "Идеально подходит под пивасик"},
            new Dish {Id = 7, Name = "Ческа Липа",Cost = 28.0m, DishType = MyDishTypeSeedData[2], Provisioner = ProvisionersSeedData.First(), Description = "салат из молодого картофеля с зеленым горошком, зеленым лучком и обжаренным до румяной корочки беконом, приправленный пикантным соусом на основе свежих яиц и ароматного масла с добавление чеснока"},
            new Dish {Id = 8, Name = "Кутна Гора",Cost = 38.0m, DishType = MyDishTypeSeedData[2], Provisioner = ProvisionersSeedData.First()},
            new Dish {Id=9, Name = "Прага",Cost = 33.0m, DishType = MyDishTypeSeedData[2], Provisioner = ProvisionersSeedData.First(), Description = "нежная телятина, обжаренная на гриле с яблоком"},
            new Dish {Id=10, Name = "Миловице",Cost = 35.0m, DishType = MyDishTypeSeedData[2], Provisioner = ProvisionersSeedData.First(), Description = "салат с копченой ветчиной и кальмарами. Подается с сырной заправкой"},
            new Dish {Id=11, Name = "Картошка по-деревенски",Cost = 41.0m, DishType = MyDishTypeSeedData[1], Provisioner = ProvisionersSeedData.First(), Description = "вкусная молодая картошка, обжаренная в ароматном масле"},
            new Dish {Id=12, Name = "Брамборы",Cost = 32.0m, DishType = MyDishTypeSeedData[1], Provisioner = ProvisionersSeedData.First(), Description = "блинчик из картофеля с норвежской семгой и свежими томатами"},
            new Dish {Id=13, Name = "Тропинки",Cost = 15.0m, DishType = MyDishTypeSeedData[3], Provisioner = ProvisionersSeedData.First(), Description = "запеченные с чесноком и сыром гренки из черного хлеба"},
            new Dish {Id=14, Name = "Шаурма",Cost = 57.0m, DishType = MyDishTypeSeedData[3], Provisioner = ProvisionersSeedData.First(), Description = "классическая куриная шаурма"}
        };

        private static readonly IList<User> UsersSeedData = new List<User>
        {
            new User {Id=1, FirstName = "Vasya", SecondName = "Ivanson", LastName = "Pupkin", Balance = 0.0m, GenderId = (int)GenderEnum.Male, Email = "vasyapupkin@yandex.ru", RoleId = (int)RoleEnum.Admin, Login="user1"},
            new User {Id=2, FirstName = "Petr", SecondName = "Vladimirson", LastName = "Sidorov", Balance = 0.0m, GenderId = (int)GenderEnum.Male, Email = "petrsidorov@mail.ru", RoleId = (int)RoleEnum.Client, Login="user2"},
            new User {Id=3, FirstName = "Mariya", SecondName = "Sergsdottir", LastName = "Ivanova", Balance = 0.0m, GenderId = (int)GenderEnum.Female, Email = "mariyaivanova@yandex.ru", RoleId = (int)RoleEnum.Client, Login="user3"}
        };

        private static readonly IList<Menu> MenuSeedData = new List<Menu>
        {
            new Menu { Id = 1, Creator = UsersSeedData.First(), DateToShow = DateTime.Now, Dishes = DishesSeedData.Take(14).ToList()}
        };

        private static readonly IList<OrderItem> OrderItemsSeedData = new List<OrderItem>
        {
            new OrderItem {Id = 1, Dish = DishesSeedData.Take(6).Last(), Count = 1},
            new OrderItem { Id = 2, Dish = DishesSeedData.Take(4).Last(), Count = 3}
        };

        private static readonly IList<Order> OrdersSeedData = new List<Order>
        {
            new Order {Id = 1, MenuId = 1, User = UsersSeedData[0], OrderTime = new DateTime(2016, 7, 13, 14, 25, 14), OrderItems = OrderItemsSeedData.Take(2).ToList(), Cost = 3000, OrderStatusId = (int)OrderStatusesEnum.Pending}
        };

        private static readonly IList<Setting> SettingsSeedData = new List<Setting>
        {
            new Setting() {Id = (int)SettingEnum.BlockingHour, Name = "Blocking hour", Value = MakeDate(21,59,59).ToString(), Description = "Время, по истечению которого заказ блюд невозможен."},
            new Setting() {Id = (int)SettingEnum.NotificationsEnabled, Name = "Notifications enabled", Value = "False", Description = "Уведомления включены."},
            new Setting() {Id = (int)SettingEnum.RefillBalanceNotificationTime, Name = "Refill balance notification time", Value = MakeDate(9,00,00).ToString(), Description = "Время уведомления \"Пополните баланс\"."},
            new Setting() {Id = (int)SettingEnum.MinBalanceValueWhenShowRefillBalancePopup, Name = "Min balance value when show refill balance popup", Value = "130", Description = "Баланс, при котором показывается уведомление \"Пополните баланс\"."},
            new Setting() {Id = (int)SettingEnum.MakeOrderNotificationTime, Name = "Make order notification time", Value = MakeDate(11,00,00).ToString(), Description = "Время уведомления \"Не забудьте сделать заказ\"."}
        };

        private static TimeSpan MakeDate(int h, int m, int s)
        {
            var _day = DateTime.Today;
            DateTime date = new DateTime(_day.Year, _day.Month, _day.Day, h, m, s);
            return date.TimeOfDay;
        }

        private static readonly IList<MoneyTransactionOperation> MoneyTransactionOperationsSeedData = new List<MoneyTransactionOperation>
        {
            new MoneyTransactionOperation() {Id = (int)MoneyTransactionOperationEnum.Payment, Name = "Списание денежных средств", Description = ""},
            new MoneyTransactionOperation() {Id = (int)MoneyTransactionOperationEnum.CacheWithdrawal, Name = "Возврат денежных средств", Description = ""},
            new MoneyTransactionOperation() {Id = (int)MoneyTransactionOperationEnum.CacheIncrease, Name = "Зачисление денежных средств", Description = ""}
        };

        private static readonly IList<MoneyTransactionLog> MoneyTransactionLogsSeedData = new List<MoneyTransactionLog>
        {
            new MoneyTransactionLog() {AffectedUserId = 1, BalanceBefore = 40m, BalanceAfter =440m, OperationId = 2, TransactionTime = new DateTime(2016,07,24,16,47,13),Comment = "Пополнение счета", InitiatorUserId = 3, Operation = MoneyTransactionOperationsSeedData[2]},
            new MoneyTransactionLog() {AffectedUserId = 1, BalanceBefore = 124m, BalanceAfter =10m, OperationId = 0, TransactionTime = new DateTime(2016,07,25,11,23,57),Comment = "Оформлен заказ №1", InitiatorUserId = 1, Operation = MoneyTransactionOperationsSeedData[0], OrderId = 1},
            new MoneyTransactionLog() {AffectedUserId = 1, BalanceBefore = 10m, BalanceAfter =124m, OperationId = 1, TransactionTime = new DateTime(2016,07,25,11,56,12),Comment = "Отмена заказа №1", InitiatorUserId = 1, Operation = MoneyTransactionOperationsSeedData[1], OrderId = 1},
            //new MoneyTransactionLog() {AffectedUserId = 2, BalanceBefore = 73m, BalanceAfter =23m, OperationId = 0, TransactionTime = DateTime.Now,Comment = "Оформлен заказ №2", InitiatorUserId = 2, Operation = MoneyTransactionOperationsSeedData[0], OrderId = 2}
        };

        private static readonly IList<Gender> GendersSeedData = new List<Gender>
        {
            new Gender() {Id = 1, Name = "муж."},
            new Gender() {Id = 2, Name = "жен."}
        };

        private static readonly IList<Role> RolesSeedData = new List<Role>
        {
            new Role() {Id = 1, Name = "Admin"},
            new Role() {Id = 2, Name = "User"}
        };

        private static readonly IList<OrderStatus> OrderStatusesSeedData = new List<OrderStatus>
        {
            new OrderStatus() {Id = 1,Name = "Отменен"},
            new OrderStatus() {Id = 2,Name = "Принят"},
            new OrderStatus() {Id = 3,Name = "Доставлен"}
        };
    }
}

