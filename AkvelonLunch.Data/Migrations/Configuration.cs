namespace AkvelonLunch.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AkvelonLunch.Data.AkvelonLunchContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(AkvelonLunch.Data.AkvelonLunchContext context)
        {
            new AkvelonLunchDataSeeder(context).Seed();
        }
    }
}
