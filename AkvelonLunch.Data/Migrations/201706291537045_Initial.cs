namespace AkvelonLunch.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        ProvisionerId = c.Int(nullable: false),
                        NumberOfDishes = c.Int(nullable: false),
                        DiscountSum = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ProvisionerId)
                .ForeignKey("dbo.Provisioners", t => t.ProvisionerId)
                .Index(t => t.ProvisionerId);
            
            CreateTable(
                "dbo.Provisioners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierName = c.String(nullable: false, maxLength: 255),
                        Address = c.String(maxLength: 255),
                        Phone = c.String(nullable: false, maxLength: 35),
                        Email = c.String(maxLength: 255),
                        ContactPersonName = c.String(maxLength: 127),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dishes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(maxLength: 1000),
                        WeightInGramms = c.Int(),
                        DishPreviewFileName = c.String(),
                        ProvisionerId = c.Int(nullable: false),
                        DishTypeId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DishTypes", t => t.DishTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Provisioners", t => t.ProvisionerId, cascadeDelete: true)
                .Index(t => t.ProvisionerId)
                .Index(t => t.DishTypeId);
            
            CreateTable(
                "dbo.DishTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateToShow = c.DateTime(nullable: false),
                        CreatorId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatorId, cascadeDelete: true)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 120),
                        FirstName = c.String(nullable: false, maxLength: 35),
                        SecondName = c.String(maxLength: 70),
                        LastName = c.String(nullable: false, maxLength: 70),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Email = c.String(maxLength: 255),
                        GenderId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Genders", t => t.GenderId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.Login, unique: true, name: "IX_UniqueLogin")
                .Index(t => t.GenderId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderTime = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                        OrderStatusId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menus", t => t.MenuId)
                .ForeignKey("dbo.OrderStatuses", t => t.OrderStatusId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.MenuId)
                .Index(t => t.OrderStatusId);
            
            CreateTable(
                "dbo.MoneyTransactionLogs",
                c => new
                    {
                        TransactionId = c.Int(nullable: false, identity: true),
                        AffectedUserId = c.Int(nullable: false),
                        InitiatorUserId = c.Int(nullable: false),
                        OperationId = c.Int(nullable: false),
                        TransactionTime = c.DateTime(nullable: false),
                        BalanceBefore = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BalanceAfter = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(maxLength: 255),
                        OrderId = c.Int(),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("dbo.Users", t => t.AffectedUserId)
                .ForeignKey("dbo.Users", t => t.InitiatorUserId)
                .ForeignKey("dbo.MoneyTransactionOperations", t => t.OperationId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .Index(t => t.AffectedUserId)
                .Index(t => t.InitiatorUserId)
                .Index(t => t.OperationId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.MoneyTransactionOperations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DishId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dishes", t => t.DishId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.DishId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.OrderStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        SettingId = c.Int(nullable: false),
                        Value = c.String(nullable: false, maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Settings", t => t.SettingId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SettingId);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 127),
                        Value = c.String(nullable: false, maxLength: 1000),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DishMenus",
                c => new
                    {
                        Dish_Id = c.Int(nullable: false),
                        Menu_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Dish_Id, t.Menu_Id })
                .ForeignKey("dbo.Dishes", t => t.Dish_Id, cascadeDelete: true)
                .ForeignKey("dbo.Menus", t => t.Menu_Id, cascadeDelete: true)
                .Index(t => t.Dish_Id)
                .Index(t => t.Menu_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Discounts", "ProvisionerId", "dbo.Provisioners");
            DropForeignKey("dbo.Dishes", "ProvisionerId", "dbo.Provisioners");
            DropForeignKey("dbo.DishMenus", "Menu_Id", "dbo.Menus");
            DropForeignKey("dbo.DishMenus", "Dish_Id", "dbo.Dishes");
            DropForeignKey("dbo.Menus", "CreatorId", "dbo.Users");
            DropForeignKey("dbo.UserSettings", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserSettings", "SettingId", "dbo.Settings");
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Orders", "UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatuses");
            DropForeignKey("dbo.OrderItems", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderItems", "DishId", "dbo.Dishes");
            DropForeignKey("dbo.MoneyTransactionLogs", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.MoneyTransactionLogs", "OperationId", "dbo.MoneyTransactionOperations");
            DropForeignKey("dbo.MoneyTransactionLogs", "InitiatorUserId", "dbo.Users");
            DropForeignKey("dbo.MoneyTransactionLogs", "AffectedUserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.Users", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.Dishes", "DishTypeId", "dbo.DishTypes");
            DropIndex("dbo.DishMenus", new[] { "Menu_Id" });
            DropIndex("dbo.DishMenus", new[] { "Dish_Id" });
            DropIndex("dbo.UserSettings", new[] { "SettingId" });
            DropIndex("dbo.UserSettings", new[] { "UserId" });
            DropIndex("dbo.OrderItems", new[] { "OrderId" });
            DropIndex("dbo.OrderItems", new[] { "DishId" });
            DropIndex("dbo.MoneyTransactionLogs", new[] { "OrderId" });
            DropIndex("dbo.MoneyTransactionLogs", new[] { "OperationId" });
            DropIndex("dbo.MoneyTransactionLogs", new[] { "InitiatorUserId" });
            DropIndex("dbo.MoneyTransactionLogs", new[] { "AffectedUserId" });
            DropIndex("dbo.Orders", new[] { "OrderStatusId" });
            DropIndex("dbo.Orders", new[] { "MenuId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropIndex("dbo.Users", new[] { "GenderId" });
            DropIndex("dbo.Users", "IX_UniqueLogin");
            DropIndex("dbo.Menus", new[] { "CreatorId" });
            DropIndex("dbo.Dishes", new[] { "DishTypeId" });
            DropIndex("dbo.Dishes", new[] { "ProvisionerId" });
            DropIndex("dbo.Discounts", new[] { "ProvisionerId" });
            DropTable("dbo.DishMenus");
            DropTable("dbo.Settings");
            DropTable("dbo.UserSettings");
            DropTable("dbo.Roles");
            DropTable("dbo.OrderStatuses");
            DropTable("dbo.OrderItems");
            DropTable("dbo.MoneyTransactionOperations");
            DropTable("dbo.MoneyTransactionLogs");
            DropTable("dbo.Orders");
            DropTable("dbo.Genders");
            DropTable("dbo.Users");
            DropTable("dbo.Menus");
            DropTable("dbo.DishTypes");
            DropTable("dbo.Dishes");
            DropTable("dbo.Provisioners");
            DropTable("dbo.Discounts");
        }
    }
}
