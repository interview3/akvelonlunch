﻿using System.Data.Entity;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.EntitiesConfiguration;
using AkvelonLunch.Data.Migrations;

namespace AkvelonLunch.Data
{
    public class AkvelonLunchContext:DbContext
    {
        public AkvelonLunchContext():
            base("AkvelonLunchConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AkvelonLunchContext, Configuration>("AkvelonLunchConnection"));
        }

        public DbSet<Dish> Dishes { get; set;}
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Provisioner> Provisioners { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<MoneyTransactionLog> MoneyTransactionLogs { get; set; }
        public DbSet<DishType> MyDishTypes { get; set; }
        public DbSet<MoneyTransactionOperation> MoneyTransactionOperations { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DishConfiguration());
            modelBuilder.Configurations.Add(new MenuConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
            modelBuilder.Configurations.Add(new OrderConfiguration());
            modelBuilder.Configurations.Add(new ProvisionerConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SettingConfiguration());
            modelBuilder.Configurations.Add(new MoneyTransactionLogConfiguration());
            modelBuilder.Configurations.Add(new MoneyTransactionOperationConfiguration());
            modelBuilder.Configurations.Add(new MyDishTypeConfiguration());
            modelBuilder.Configurations.Add(new GenderConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new OrderStatusConfiguration());
            modelBuilder.Configurations.Add(new DiscountConfiguration());
            modelBuilder.Configurations.Add(new UserSettingConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
