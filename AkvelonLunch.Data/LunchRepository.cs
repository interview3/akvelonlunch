﻿using System;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using EntityFramework.Extensions;
using AkvelonLunch.Utilities;
namespace AkvelonLunch.Data
{
    public class LunchRepository : ILunchRepository
    {
        private AkvelonLunchContext _ctx;
        public LunchRepository(AkvelonLunchContext ctx)
        {
            _ctx = ctx;
        }

        public DalOperationStatus<int?> Create(Order order)
        {
            var os = new DalOperationStatus<int?>();
            _ctx.Orders.Add(order);

            try
            {
                os.Count = _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create an order.");
                os.Message = "Unable to save changes when you create an order.";
                os.StatusCode = DalOperationStatusCode.Error;
            }

            os.Entity = order.Id;

            if (os.Entity != null)
            {
                os.Message = "The order is created.";
                os.StatusCode = DalOperationStatusCode.ChangesSaved;
            }
            else
            {
                os.Message = "The order is not created.";
                os.StatusCode = DalOperationStatusCode.Error;
                os.Count = 0;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(Provisioner provisioner)
        {
            var os = new DalOperationStatus<int?>();
            _ctx.Provisioners.Add(provisioner);

            try
            {
                os.Count = _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a provisioner.");
                os.Message = "Unable to save changes when you create a provisioner.";
                os.StatusCode = DalOperationStatusCode.Error;
            }

            os.Entity = provisioner.Id;
            if (os.Entity != null)
            {
                os.Message = "The provisioner is created.";
                os.StatusCode = DalOperationStatusCode.ChangesSaved;
            }
            else
            {
                os.Message = "The provisioner is not created.";
                os.StatusCode = DalOperationStatusCode.Error;
                os.Count = 0;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(Menu menu)
        {
            var os = new DalOperationStatus<int?>();
            _ctx.Menus.Add(menu);

            try
            {
                os.Count = _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a menu.");
                os.Message = "Unable to save changes when you create a menu.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            os.Entity = menu.Id;

            if (os.Entity != null)
            {
                os.Message = "Menu created.";
                os.StatusCode = DalOperationStatusCode.ChangesSaved;
            }
            else
            {
                os.Message = "Menu is not created.";
                os.StatusCode = DalOperationStatusCode.Error;
                os.Count = 0;
            }
            return os;
        }

        public DalOperationStatus<int?> Create(Dish dish)
        {
            var os = new DalOperationStatus<int?>();
            _ctx.Dishes.Add(dish);

            try
            {
                os.Count = _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you create a dish.");
                os.Message = "Unable to save changes when you create a dish.";
                os.StatusCode = DalOperationStatusCode.Error;
            }

            os.Entity = dish.Id;
            if (os.Entity != null)
            {
                os.Message = "Dish created.";
                os.StatusCode = DalOperationStatusCode.ChangesSaved;
            }
            else
            {
                os.Message = "Dish is not created.";
                os.StatusCode = DalOperationStatusCode.Error;
                os.Count = 0;
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteDish(int id)
        {
            var os = new DalOperationStatus<bool>();
            if (_ctx.Dishes.Find(id) != null)
            {
                try
                {
                    os.Count = _ctx.Dishes.Where(d => d.Id == id).Delete();
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Dish deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not deleted.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a dish.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Dish not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteMenu(int id)
        {
            var os = new DalOperationStatus<bool>();
            if (_ctx.Menus.Find(id) != null)
            {
                try
                {
                    os.Count = _ctx.Menus.Where(m => m.Id == id).Delete();
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Menu deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not deleted.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a dish.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Menu not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteOrder(int id)
        {
            var os = new DalOperationStatus<bool>();
            if (_ctx.Orders.Find(id) != null)
            {
                try
                {
                    os.Count = _ctx.Orders.Where(o => o.Id == id).Delete();
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Order deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not deleted.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete an order.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Order not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> DeleteProvisioner(int id)
        {
            var os = new DalOperationStatus<bool>();
            if (_ctx.Provisioners.Find(id) != null)
            {
                try
                {
                    os.Count = _ctx.Provisioners.Where(p => p.Id == id).Delete();
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Provisioner deleted.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not deleted.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you delete a provisioner.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Provisioner not found.";
            }
            return os;
        }

        public DalOperationStatus<Dish> GetDish(int id)
        {
            var os = new DalOperationStatus<Dish>();
            try
            {
                os.Entity = _ctx.Dishes
                .Where(c => c.Id == id)
                .SingleOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Dish successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get dish by id.");
                os.Message = "Unable to get dish by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Dish>> GetDishList()
        {
            var os = new DalOperationStatus<IQueryable<Dish>>();
            try
            {
                os.Entity = _ctx.Dishes.AsQueryable();
                var dishesCount = os.Entity.ToList<Dish>().Count;
                if (dishesCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Dishes list successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dishes list is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get dishes list.");
                os.Message = "Unable to get dishes list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Menu> GetMenuByDate(DateTime date)
        {
            var os = new DalOperationStatus<Menu>();
            try
            {
                os.Entity = _ctx.Menus.Include("Dishes")
                    .Where(c => DbFunctions.TruncateTime(c.DateToShow) == DbFunctions.TruncateTime(date))
                    .SingleOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Menu successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menu by date.");
                os.Message = "Unable to get menu by date.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Menu> GetMenu(int id)
        {
            var os = new DalOperationStatus<Menu>();
            try
            {
                os.Entity = _ctx.Menus
                .Include("Dishes")
                .Where(c => c.Id == id)
                .SingleOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Menu successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menu by id.");
                os.Message = "Unable to get menu by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Menu>> GetMenuList()
        {
            var os = new DalOperationStatus<IQueryable<Menu>>();
            try
            {
                os.Entity = _ctx.Menus
                .Include("Dishes")
                .AsQueryable();
                var menusCount = os.Entity.ToList<Menu>().Count;
                if (menusCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of menus successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of menus is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get menus list.");
                os.Message = "Unable to get menus list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetOrder(int id)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems")
                .Where(c => c.Id == id)
                .SingleOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by id.");
                os.Message = "Unable to get order by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetPendingOrderByUserId(int userId)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems")
                .Where(c => c.UserId == userId)
                .Where(s => s.Status == OrderStatuses.Pending)
                .FirstOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Pending order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Pending order not found.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get pending order by user's id.");
                os.Message = "Unable to get pending order by user's id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Order> GetOrderOfUserByDate(int userId, DateTime date)
        {
            var os = new DalOperationStatus<Order>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems")
                .Where(c => c.UserId == userId)
                .Where(c => DbFunctions.TruncateTime(c.OrderTime) == DbFunctions.TruncateTime(date))
                .FirstOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Order successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Order not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get order by user's id and date.");
                os.Message = "Unable to get order by user's id and date.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Order>> GetOrderList()
        {
            var os = new DalOperationStatus<IQueryable<Order>>();
            try
            {
                os.Entity = _ctx.Orders
                .Include("OrderItems")
                .AsQueryable();
                var ordersCount = os.Entity.ToList<Order>().Count;
                if (ordersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of orders successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of orders is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get orders list.");
                os.Message = "Unable to get orders list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }
        
        public IQueryable<Order> GetTodayOrdersList()
        {
            DateTime date = DateTime.Now;
            return _ctx.Orders
                .Include("OrderItems")
                .Where(ord => DbFunctions.TruncateTime(ord.OrderTime) == DbFunctions.TruncateTime(date))
                .AsQueryable();
        }

        public DalOperationStatus<Provisioner> GetProvisioner(int id)
        {
            var os = new DalOperationStatus<Provisioner>();
            try
            {
                os.Entity = _ctx.Provisioners
                .Where(c => c.Id == id)
                .FirstOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Provisioner successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioner by id.");
                os.Message = "Unable to get provisioner by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<Provisioner> GetProvisionerWithDishes(int id)
        {
            var os = new DalOperationStatus<Provisioner>();
            try
            {
                os.Entity = _ctx.Provisioners
                .Include("Dishes")
                .Where(c => c.Id == id)
                .SingleOrDefault();
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "Provisioner successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not received.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioner with dishes by id.");
                os.Message = "Unable to get provisioner with dishes by id.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<IQueryable<Provisioner>> GetProvisionerList()
        {
            var os = new DalOperationStatus<IQueryable<Provisioner>>();
            try
            {
                os.Entity = _ctx.Provisioners.AsQueryable();
                var provisionersCount = os.Entity.ToList<Provisioner>().Count;
                if (provisionersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of provisioners successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of provisioners is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get provisioners list.");
                os.Message = "Unable to get provisioners list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public DalOperationStatus<User> GetUserById(int id)
        {
            var os = new DalOperationStatus<User>();
            try
            {
                os.Entity = _ctx.Users.Find(id);
                if (os.Entity.Id > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "User successfully found.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User not found.";
                }
            }
            catch (Exception ex)
            {
                os.StatusCode = DalOperationStatusCode.Error;
                os.Message = "User find error occured.";
                GlobalLogger.GetInstance().Error(ex, "Unable to find user by id.");
            }
            return os;
        }

        public DalOperationStatus<IQueryable<User>> GetUserList()
        {
            var os = new DalOperationStatus<IQueryable<User>>();
            try
            {
                os.Entity = _ctx.Users.AsQueryable();
                var usersCount = os.Entity.ToList<User>().Count;
                if (usersCount > 0)
                {
                    os.StatusCode = DalOperationStatusCode.OperationSuccessful;
                    os.Message = "List of users successfully received.";
                }
                else
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "List of users is empty.";
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to get users list.");
                os.Message = "Unable to get users list.";
                os.StatusCode = DalOperationStatusCode.Error;
            }
            return os;
        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }

        public DalOperationStatus<bool> Update(Provisioner provisioner)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Provisioners.Find(provisioner.Id);
            if (original != null)
            {
                try
                {
                    _ctx.Entry(original).CurrentValues.SetValues(provisioner);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Provisioner updated.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Provisioner not updated.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a provisioner.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Provisioner not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> UpdateUserBalance(int id, decimal balance)
        {
            var os = new DalOperationStatus<bool>();
            var user = _ctx.Users.Find(id);
            if (user != null)
            {
                try
                {
                    User userWithNewBalance = user;
                    userWithNewBalance.Balance = balance;
                    _ctx.Entry(user).CurrentValues.SetValues(userWithNewBalance);
                    _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "User's balance was updated.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "User's balance wasn't updated.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a user's balance.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "User not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Menu menu)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Menus.Find(menu.Id);
            if (original != null)
            {
                try
                {
                    _ctx.Entry(original).CurrentValues.SetValues(menu);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Menu updated.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Menu not updated.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a menu.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Menu not found.";
            }
            return os;
        }

        public DalOperationStatus<bool> Update(Dish dish)
        {
            var os = new DalOperationStatus<bool>();
            var original = _ctx.Dishes.Find(dish.Id);
            if (original != null)
            {
                try
                {
                    _ctx.Entry(original).CurrentValues.SetValues(dish);
                    os.Count = _ctx.SaveChanges();
                    os.StatusCode = DalOperationStatusCode.ChangesSaved;
                    os.Message = "Dish updated.";
                }
                catch (Exception ex)
                {
                    os.StatusCode = DalOperationStatusCode.Error;
                    os.Message = "Dish not updated.";
                    GlobalLogger.GetInstance().Error(ex, "Unable to save changes when you update a dish.");
                }
            }
            else
            {
                os.StatusCode = DalOperationStatusCode.EntityNotFound;
                os.Message = "Dish not found.";
            }
            return os;
        }
    }
}
