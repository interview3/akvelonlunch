﻿namespace AkvelonLunch.Reports
{
    public enum OrderReportColumnNames
    {
        FirstName,
        LastName,
        Cost,
        Date,
        Status
    }
}
