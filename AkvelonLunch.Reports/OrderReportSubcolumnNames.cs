﻿namespace AkvelonLunch.Reports
{
    public enum OrderReportSubcolumnNames
    {
        DishName = 1,
        DishCost,
        Count,
        Total
    }
}
