﻿using System;
using System.Collections.Generic;
using System.Linq;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Entities;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace AkvelonLunch.Reports
{
    public class MoneyTransactionReportsGenerator
    {
        public IWorkbook GenerateReport(DateTime startDate, DateTime finishDate)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var repository = new Data.Repositories.MoneyTransactionLogRepository.MoneyTransactionLogRepository(ctx);
                report = MakeMoneyTransactionReport(repository.GetTransactionsOfTimeSpan(startDate, finishDate).Entity.ToList(), startDate, finishDate);
            }
            return report;
        }

        public IWorkbook GenerateReport(int userId, DateTime startDate, DateTime finishDate)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var repository = new Data.Repositories.MoneyTransactionLogRepository.MoneyTransactionLogRepository(ctx);
                report = MakeMoneyTransactionReport(repository.GetUserTransactionsOfTimeSpan(userId, startDate, finishDate).Entity.ToList(), startDate, finishDate);
            }
            return report;
        }

        public IWorkbook GenerateReport(int userId)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var repository = new Data.Repositories.MoneyTransactionLogRepository.MoneyTransactionLogRepository(ctx);
                report = MakeMoneyTransactionReport(repository.GetUserTransactions(userId).Entity.ToList(), null, null);
            }
            return report;
        }

        private IWorkbook MakeMoneyTransactionReport(List<MoneyTransactionLog> moneyTransactionLogs, DateTime? startDate, DateTime? finishDate)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var row0Font = CreateRow0Font(workbook);
            var row1Font = CreateRow1Font(workbook);

            ISheet sheet = workbook.CreateSheet("AkvelonLunch Money Transactions Report");
            sheet.RowSumsBelow = false;

            CreateDocumentTitleRow(startDate, finishDate, sheet, workbook, row0Font);

            if (moneyTransactionLogs.Count > 0)
            {
                CreateHeadersRow(sheet, workbook, row1Font);

                int activeRowWithDateOfTransactions = 2;
                int activeSubRow = 0;
                string dateOfLastMoneyTransaction = null;
                foreach (var moneyTransaction in moneyTransactionLogs)
                {
                    if (dateOfLastMoneyTransaction == null || dateOfLastMoneyTransaction != moneyTransaction.TransactionTime.ToString("dd/MM/yyyy"))   //if begun transactions of next day
                    {
                        if (dateOfLastMoneyTransaction != null)
                        {
                            sheet.GroupRow(activeRowWithDateOfTransactions, activeRowWithDateOfTransactions + activeSubRow);    //grouping rows of money transaction of past day
                            sheet.SetRowGroupCollapsed(activeRowWithDateOfTransactions, true);
                            activeRowWithDateOfTransactions += activeSubRow;
                            activeSubRow = 0;
                        }
                        dateOfLastMoneyTransaction = moneyTransaction.TransactionTime.ToString("dd/MM/yyyy");
                        IRow dateRow = sheet.CreateRow(activeRowWithDateOfTransactions++);
                        dateRow.CreateCell(0).SetCellValue(dateOfLastMoneyTransaction + ":");

                        var dateRowFont = CreateDateRowFont(workbook);
                        dateRow.GetCell(0).CellStyle = workbook.CreateCellStyle();
                        dateRow.GetCell(0).CellStyle.SetFont(dateRowFont);
                    }

                    IRow row = sheet.CreateRow(activeRowWithDateOfTransactions + activeSubRow);
                    FillMoneyTransactionLogRow(row, moneyTransaction);
                    activeSubRow++;
                }

                sheet.GroupRow(activeRowWithDateOfTransactions, activeRowWithDateOfTransactions + activeSubRow - 1); //grouping money transaction log rows by dates
                sheet.SetRowGroupCollapsed(activeRowWithDateOfTransactions, true); //collaps(hide) money transaction log rows
            }
            else
            {
                CreateLabelOfEmptyDocument(sheet, workbook);
            }
            return workbook;
        }

        /// <summary>
        /// Creating a font for row with title.
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns>Font of row number 0.</returns>
        private IFont CreateRow0Font(IWorkbook workbook)
        {
            IFont row0Font = workbook.CreateFont();
            row0Font.FontHeightInPoints = 18;
            row0Font.FontName = "Times New Roman";
            row0Font.Boldweight = (short)FontBoldWeight.Bold;
            return row0Font;
        }

        /// <summary>
        /// Creating a font for row with headers of columns.
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns>Font of row number 1.</returns>
        private IFont CreateRow1Font(IWorkbook workbook)
        {
            IFont row1Font = workbook.CreateFont();
            row1Font.FontHeightInPoints = 12;
            row1Font.FontName = "Calibri";
            row1Font.Boldweight = (short)FontBoldWeight.Bold;
            return row1Font;
        }

        /// <summary>
        /// Creating a font for row with date of money transactions.
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns>Font.</returns>
        private IFont CreateDateRowFont(IWorkbook workbook)
        {
            IFont dateRowFont = workbook.CreateFont();
            dateRowFont.FontHeightInPoints = 16;
            dateRowFont.FontName = "Times New Roman";
            dateRowFont.Boldweight = (short)FontBoldWeight.Bold;
            return dateRowFont;
        }

        /// <summary>
        /// Create first title row of document
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="finishDate"></param>
        /// <param name="sheet"></param>
        /// <param name="workbook"></param>
        /// <param name="row0Font"></param>
        private void CreateDocumentTitleRow(DateTime? startDate, DateTime? finishDate, ISheet sheet, IWorkbook workbook, IFont row0Font)
        {
            ICell cell = sheet.CreateRow(0).CreateCell(0);
            if (startDate == null && finishDate == null)
            {
                cell.SetCellValue("Все денежные операции пользователя:");
            }
            else if (startDate.Value.Date == finishDate.Value.Date)
            {
                cell.SetCellValue("Денежные операции, сделанные " + startDate.Value.Date.ToString("dd/MM/yyyy") + ":");
            }
            else
            {
                cell.SetCellValue("Денежные операции, сделанные в период с " + startDate.Value.Date.ToString("dd/MM/yyyy") +
                                  " по " +
                                  finishDate.Value.Date.ToString("dd/MM/yyyy") + ":");
            }
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(row0Font);
        }

        /// <summary>
        /// Create row with headers of columns.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="workbook"></param>
        /// <param name="row1Font"></param>
        private void CreateHeadersRow(ISheet sheet, IWorkbook workbook, IFont row1Font)
        {
            var row1 = sheet.CreateRow(1);
            string[] headers =
            {
                "Пользователь:", "Инициатор транзакции:", "Операция:", "Баланс до:", "Баланс после:", "Дата:",
                "Комментарий:"
            };
            for (int i = 0; i < headers.Length; i++)
            {
                row1.CreateCell(i).SetCellValue(headers[i]);
                row1.GetCell(i).CellStyle = workbook.CreateCellStyle();
                row1.GetCell(i).CellStyle.SetFont(row1Font);
            }

            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.AffectedUser, 7000);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.InitiatorUser, 7000);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.OperationName, 9000);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.BalanceBefore, 4000);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.BalanceAfter, 4000);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.TransactionTime, 4500);
            sheet.SetColumnWidth((int)MoneyTransactionColumnNames.Comment, 9000);
        }

        /// <summary>
        /// Fills row with information of money transaction. 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="moneyTransaction"></param>
        private void FillMoneyTransactionLogRow(IRow row, MoneyTransactionLog moneyTransaction)
        {
            row.CreateCell((int)MoneyTransactionColumnNames.AffectedUser)
                .SetCellValue(moneyTransaction.AffectedUser.FirstName + " " + moneyTransaction.AffectedUser.LastName);
            row.CreateCell((int)MoneyTransactionColumnNames.InitiatorUser)
                .SetCellValue(moneyTransaction.InitiatorUser.FirstName + " " + moneyTransaction.InitiatorUser.LastName);
            row.CreateCell((int)MoneyTransactionColumnNames.OperationName).SetCellValue(moneyTransaction.Operation.Name);
            row.CreateCell((int)MoneyTransactionColumnNames.BalanceBefore)
                .SetCellValue((double)moneyTransaction.BalanceBefore + " руб.");
            row.CreateCell((int)MoneyTransactionColumnNames.BalanceAfter)
                .SetCellValue((double)moneyTransaction.BalanceAfter + " руб.");
            row.CreateCell((int)MoneyTransactionColumnNames.TransactionTime)
                .SetCellValue(moneyTransaction.TransactionTime.ToString("HH:mm dd/MM/yyyy"));
            row.CreateCell((int)MoneyTransactionColumnNames.Comment).SetCellValue(moneyTransaction.Comment);
        }

        /// <summary>
        /// Invoked if list of transactions is empty.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="workbook"></param>
        private void CreateLabelOfEmptyDocument(ISheet sheet, IWorkbook workbook)
        {
            var rowOfEmptyDocLabel = sheet.CreateRow(2);
            IFont emptyDocLabelFont = workbook.CreateFont();
            emptyDocLabelFont.FontHeightInPoints = 20;
            emptyDocLabelFont.FontName = "Times New Roman";
            emptyDocLabelFont.Boldweight = (short)FontBoldWeight.Bold;

            rowOfEmptyDocLabel.CreateCell(0).SetCellValue("Денежных операций за указанный период не обнаружено!");
            rowOfEmptyDocLabel.GetCell(0).CellStyle = workbook.CreateCellStyle();
            rowOfEmptyDocLabel.GetCell(0).CellStyle.SetFont(emptyDocLabelFont);
        }
    }
}
