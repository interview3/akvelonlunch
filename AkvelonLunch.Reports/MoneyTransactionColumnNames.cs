﻿namespace AkvelonLunch.Reports
{
    public enum MoneyTransactionColumnNames
    {
        AffectedUser,
        InitiatorUser,
        OperationName,
        BalanceBefore,
        BalanceAfter,
        TransactionTime,
        Comment
    }
}
