﻿using System;
using System.Collections.Generic;
using System.Linq;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Entities;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace AkvelonLunch.Reports
{
    public class OrderReportsGenerator
    {
        public IWorkbook GenerateReport(DateTime startDate, DateTime finishDate)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var orderRepository = new Data.Repositories.OrderRepository.OrderRepository(ctx);
                report = MakeOrderReport(orderRepository.GetOrdersListByTimeSpan(startDate, finishDate).Entity.ToList(), startDate, finishDate);
            }

            return report;
        }

        public IWorkbook GenerateReport(int userId, DateTime startDate, DateTime finishDate)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var orderRepository = new Data.Repositories.OrderRepository.OrderRepository(ctx);
                report = MakeOrderReport(orderRepository.GetOrdersOfUserInPeriod(userId, startDate, finishDate).Entity.ToList(), startDate, finishDate);
            }

            return report;
        }

        public IWorkbook GenerateReport(int userId)
        {
            IWorkbook report;
            using (var ctx = new AkvelonLunchContext())
            {
                var orderRepository = new Data.Repositories.OrderRepository.OrderRepository(ctx);
                report = MakeOrderReport(orderRepository.GetOrdersByUserId(userId).Entity.ToList(), null, null);
            }

            return report;
        }

        private IWorkbook MakeOrderReport(List<Order> orders, DateTime? startDate, DateTime? finishDate)
        {
            IWorkbook workbook = new XSSFWorkbook();

            var row0Font = CreateRow0Font(workbook);
            var row1Font = CreateRow1Font(workbook);

            ISheet sheet = workbook.CreateSheet("AkvelonLunch Orders Report");
            sheet.RowSumsBelow = false;

            var titleCell = CreateDocumentTitleRow(startDate, finishDate, sheet, workbook);
            titleCell.CellStyle.SetFont(row0Font);

            if (orders.Count > 0)
            {
                CreateHeadersRow(sheet, workbook, row1Font);

                int activeRow = 2;
                foreach (var order in orders)
                {
                    IRow row = sheet.CreateRow(activeRow++);
                    FillOrderInformationRow(row, order);

                    int activeSubRow = 0;
                    decimal sumWithoutDiscount = 0;
                    IRow subHeadersRow = sheet.CreateRow(activeRow);
                    CreateOrderItemHeaderRow(subHeadersRow, workbook, row1Font);
                    activeSubRow++;

                    foreach (var orderItem in order.OrderItems)
                    {
                        IRow subRow = sheet.CreateRow(activeRow + activeSubRow++);
                        FillOrderItemRow(subRow, orderItem);
                        sumWithoutDiscount += orderItem.Dish.Cost * orderItem.Count;
                    }

                    IRow discountRow = sheet.CreateRow(activeRow + activeSubRow++);
                    FillDiscountRow(discountRow, sumWithoutDiscount - order.Cost, workbook, row1Font);
                    sheet.GroupRow(activeRow, activeRow + activeSubRow); //grouping subrows
                    sheet.SetRowGroupCollapsed(activeRow, true); //hide subrows
                    activeRow += activeSubRow + 1;
                }
            }
            else
            {
                CreateLabelOfEmptyDocument(sheet, workbook);
            }
            return workbook;
        }

        /// <summary>
        /// Creating a font for row with title.
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns>Font of row number 0.</returns>
        private IFont CreateRow0Font(IWorkbook workbook)
        {
            IFont row0_font = workbook.CreateFont();
            row0_font.FontHeightInPoints = 18;
            row0_font.FontName = "Times New Roman";
            row0_font.Boldweight = (short)FontBoldWeight.Bold;
            return row0_font;
        }

        /// <summary>
        /// Creating a font for row with headers of columns.
        /// </summary>
        /// <param name="workbook"></param>
        /// <returns>Font of row number 1.</returns>
        private IFont CreateRow1Font(IWorkbook workbook)
        {
            IFont row1_font = workbook.CreateFont();
            row1_font.FontHeightInPoints = 12;
            row1_font.FontName = "Calibri";
            row1_font.Boldweight = (short)FontBoldWeight.Bold;
            return row1_font;
        }

        private ICell CreateDocumentTitleRow(DateTime? startDate, DateTime? finishDate, ISheet sheet, IWorkbook workbook)
        {
            ICell cell = sheet.CreateRow(0).CreateCell(0);
            if (startDate == null && finishDate == null)
            {
                cell.SetCellValue("Все заказы пользователя:");
            }
            else if (startDate.Value.Date == finishDate.Value.Date)
            {
                cell.SetCellValue("Заказы, сделанные " + startDate.Value.Date.ToString("dd/MM/yyyy") + ":");
            }
            else
            {
                cell.SetCellValue("Заказы, сделанные в период с " + startDate.Value.Date.ToString("dd/MM/yyyy") + " по " +
                                  finishDate.Value.Date.ToString("dd/MM/yyyy") + ":");
            }
            cell.CellStyle = workbook.CreateCellStyle();
            return cell;
        }

        private void CreateHeadersRow(ISheet sheet, IWorkbook workbook, IFont row1Font)
        {
            var row1 = sheet.CreateRow(1);
            string[] headers = { "Имя:", "Фамилия:", "Стоимость:", "Дата:", "Статус:" };
            for (int i = 0; i < headers.Length; i++)
            {
                row1.CreateCell(i).SetCellValue(headers[i]);
                row1.GetCell(i).CellStyle = workbook.CreateCellStyle();
                row1.GetCell(i).CellStyle.SetFont(row1Font);
            }

            sheet.SetColumnWidth((int)OrderReportColumnNames.FirstName, 5000);
            sheet.SetColumnWidth((int)OrderReportColumnNames.LastName, 7000);
            sheet.SetColumnWidth((int)OrderReportColumnNames.Cost, 3000);
            sheet.SetColumnWidth((int)OrderReportColumnNames.Date, 4500);
            sheet.SetColumnWidth((int)OrderReportColumnNames.Status, 3000);
        }

        private void FillOrderInformationRow(IRow row, Order order)
        {
            row.CreateCell((int)OrderReportColumnNames.FirstName).SetCellValue(order.User.FirstName);
            row.CreateCell((int)OrderReportColumnNames.LastName).SetCellValue(order.User.LastName);
            row.CreateCell((int)OrderReportColumnNames.Cost).SetCellValue((double)order.Cost + " руб.");
            row.CreateCell((int)OrderReportColumnNames.Date).SetCellValue(order.OrderTime.ToString("HH:mm dd/MM/yyyy"));
            row.CreateCell((int)OrderReportColumnNames.Status).SetCellValue(order.OrderStatus.Name);
        }

        private void CreateOrderItemHeaderRow(IRow subHeadersRow, IWorkbook workbook, IFont row1Font)
        {
            subHeadersRow.CreateCell((int)OrderReportSubcolumnNames.DishName).SetCellValue("Название блюда:");
            subHeadersRow.CreateCell((int)OrderReportSubcolumnNames.DishCost).SetCellValue("Цена");
            subHeadersRow.CreateCell((int)OrderReportSubcolumnNames.Count).SetCellValue("Количество");
            subHeadersRow.CreateCell((int)OrderReportSubcolumnNames.Total).SetCellValue("Итого");
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.DishName).CellStyle = workbook.CreateCellStyle();
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.DishName).CellStyle.SetFont(row1Font);
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.DishCost).CellStyle = workbook.CreateCellStyle();
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.DishCost).CellStyle.SetFont(row1Font);
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.Count).CellStyle = workbook.CreateCellStyle();
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.Count).CellStyle.SetFont(row1Font);
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.Total).CellStyle = workbook.CreateCellStyle();
            subHeadersRow.GetCell((int)OrderReportSubcolumnNames.Total).CellStyle.SetFont(row1Font);
        }

        private void FillOrderItemRow(IRow subRow, OrderItem orderItem)
        {
            subRow.CreateCell(1).SetCellValue(orderItem.Dish.Name);
            subRow.CreateCell(2).SetCellValue(orderItem.Dish.Cost + " руб.");
            subRow.CreateCell(3).SetCellValue(orderItem.Count + " шт.");
            subRow.CreateCell(4).SetCellValue(orderItem.Count * orderItem.Dish.Cost + " руб.");
        }

        private void FillDiscountRow(IRow discountRow, decimal discount, IWorkbook workbook, IFont row1Font)
        {
            discountRow.CreateCell(3).SetCellValue("Скидка:");
            discountRow.CreateCell(4).SetCellValue(discount + " руб.");
            discountRow.GetCell(4).CellStyle = workbook.CreateCellStyle();
            discountRow.GetCell(4).CellStyle.SetFont(row1Font);
        }

        private void CreateLabelOfEmptyDocument(ISheet sheet, IWorkbook workbook)
        {
            var rowOfEmptyDocLabel = sheet.CreateRow(2);
            IFont emptyDocLabel_font = workbook.CreateFont();
            emptyDocLabel_font.FontHeightInPoints = 20;
            emptyDocLabel_font.FontName = "Times New Roman";
            emptyDocLabel_font.Boldweight = (short)FontBoldWeight.Bold;

            rowOfEmptyDocLabel.CreateCell(0).SetCellValue("Заказов за указанный период не обнаружено!");
            rowOfEmptyDocLabel.GetCell(0).CellStyle = workbook.CreateCellStyle();
            rowOfEmptyDocLabel.GetCell(0).CellStyle.SetFont(emptyDocLabel_font);
        }
    }
}
