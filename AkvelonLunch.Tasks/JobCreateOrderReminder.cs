﻿using System;
using System.Collections.Generic;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Repositories.MenuRepository;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Notifications;
using Quartz;

namespace AkvelonLunch.Tasks
{
    public class JobCreateOrderReminder : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            List<Data.Entities.User> usersWhoNotCreatedOrder;
            using (var ctx = new AkvelonLunchContext())
            {
                var enabledTodaysMenu = new MenuRepository(ctx).GetMenuByDate(DateTime.Today).Entity;
                if (enabledTodaysMenu == null) return;
                usersWhoNotCreatedOrder = new UserRepository(ctx).GetUsersForCreateOrderReminder().Entity;
            }
            string header;
            string body;
            EmailSender emailSender = new EmailSender();
            if (usersWhoNotCreatedOrder != null)
            {
                foreach (var user in usersWhoNotCreatedOrder)
                {
                    header = "AKVELON-LUNCH: Не забудьте сделать заказ";
                    body = user.FirstName + ", вы сегодня ничего не заказали.<br/>";
                    body += "До окончания приема заказов остался час.<br/>";
                    body += "<br/>Текущий баланс: <b> " + user.Balance + "</b> рублей.";
                    emailSender.Send(user.Email, header, body);
                }
            }

        }
    }
}
