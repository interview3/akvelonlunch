﻿using System;
using System.Collections.Generic;
using System.Linq;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Notifications;
using Quartz;

namespace AkvelonLunch.Tasks
{
    public class JobRefillBalanceReminder : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var emailSender = new EmailSender();
            List<Data.Entities.User> usersWithMinBalance;
            using (var ctx = new AkvelonLunchContext())
            {
                var today = DateTime.Today.Day + "." + DateTime.Today.Month;
                switch (today)
                {
                    case "26.8":
                        {
                            var users = new UserRepository(ctx).GetUserList().Entity.ToList();
                            foreach (var user in users)
                            {
                                var header = "Akvelon Lunch: С юбилеем нас!";
                                var body = "Приложению Akvelon Lunch сегодня исполнилось " + (DateTime.Today.Year - 2016);
                                emailSender.Send(user.Email, header, body);
                            }
                            break;
                        }
                    case "31.12":
                        {
                            var users = new UserRepository(ctx).GetUserList().Entity.ToList();
                            foreach (var user in users)
                            {
                                var header = "Akvelon Lunch: С Новым Годом!";
                                var body = user.FirstName + ", поздравляем Вас с наступающим Новым Годом!<br/>";
                                body += "<i>Пусть будет новый год <br/>Началом всех начал <br/>И сбудется все то, <br/>Что в жизни намечал.</i>";
                                emailSender.Send(user.Email, header, body);
                            }
                            break;
                        }
                }
                usersWithMinBalance = new UserRepository(ctx).GetUsersForBalanceChecker().Entity;
            }
            if (usersWithMinBalance != null)
            {
                foreach (var user in usersWithMinBalance)
                {
                    var header = "AKVELON-LUNCH: Ваш баланс менее 130 рублей";
                    var body = "Доброе утро, <b> " + user.FirstName + "</b>! <br/>";
                    body += "<br/>Ваш баланс приближается к нулю.";
                    body += "<br/>Текущий баланс: <b> " + user.Balance + "</b> рублей.";
                    emailSender.Send(user.Email, header, body);
                }
            }


        }

    }


}
