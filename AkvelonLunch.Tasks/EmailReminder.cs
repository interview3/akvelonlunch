﻿using System;
using System.Linq;
using System.Threading;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Data.Repositories.MenuRepository;
using Quartz;
using Quartz.Impl;

namespace AkvelonLunch.Tasks
{
    public class EmailReminder
    {
        public Timer TimerToSchedule { get; private set; }
        private IScheduler Scheduler;

        public EmailReminder()
        {
            Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter
            {
                Level = Common.Logging.LogLevel.Info
            };

            Scheduler = StdSchedulerFactory.GetDefaultScheduler();
            Scheduler.Start();
        }

        public void ActivateNotifications()
        {
            using (var ctx = new AkvelonLunchContext())
            {
                var settingRefillBalanceNotificationTime = ctx.Settings.FirstOrDefault(s => s.Id == (int)SettingEnum.RefillBalanceNotificationTime);
                if (settingRefillBalanceNotificationTime != null)
                {
                    var refillBalanceNotificationTime = TimeSpan.Parse(settingRefillBalanceNotificationTime.Value);
                    ActivateBalanceChecker(refillBalanceNotificationTime);
                }

                var settingMakeOrderNotificationTime = ctx.Settings.FirstOrDefault(s => s.Id == (int)SettingEnum.MakeOrderNotificationTime);
                if (settingMakeOrderNotificationTime != null)
                {
                    var createOrderNotificationTime = TimeSpan.Parse(settingMakeOrderNotificationTime.Value);
                    ActivateCreateOrderReminder(createOrderNotificationTime);
                }
            }
        }

        public void ActivateBalanceChecker(TimeSpan time)
        {
            Action balanceCheckSchedulerCreateAction = RefillBalanceRemindScheduler;
            SetUpTimer(time, balanceCheckSchedulerCreateAction);
        }

        public void ActivateCreateOrderReminder(TimeSpan time)
        {
            Action remindCreateOrderAction = CreateOrderRemindScheduler;
            SetUpTimer(time, remindCreateOrderAction);
        }

        private void SetUpTimer(TimeSpan alertTime, Action action)
        {
            DateTime current = DateTime.Now;
            TimeSpan timeToGo;
            if (alertTime > current.TimeOfDay)
            {
                timeToGo = alertTime - current.TimeOfDay; //calculate time to start schaduler 
            }
            else
            {
                timeToGo = alertTime - current.TimeOfDay + new TimeSpan(24, 0, 0); //if today time to start schaduler has passed, add 24 hour
            }
            TimerToSchedule = new Timer(x => action(), null, timeToGo, Timeout.InfiniteTimeSpan);
        }

        #region create jobs
        private void RefillBalanceRemindScheduler()
        {
            IJobDetail job = JobBuilder.Create<JobRefillBalanceReminder>()
                .WithIdentity("RefillBalanceReminderJob", "NotificationsGroup")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("RefillBalanceReminderTrigger", "NotificationsGroup")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .Build();

            Scheduler.ScheduleJob(job, trigger);
        }

        private void CreateOrderRemindScheduler()
        {
            IJobDetail job = JobBuilder.Create<JobCreateOrderReminder>()
                .WithIdentity("CreateOrderRemindJob", "NotificationsGroup")
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("CreateOrderRemindTrigger", "NotificationsGroup")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .Build();

            Scheduler.ScheduleJob(job, trigger);
        }
        #endregion
    }
}
