﻿namespace AkvelonLunch.Constants
{
    public static class ControllerMessages
    {
        public const string NotSave = "Could not save to the database.";
        public const string DishNotFound = "Could not find dish.";
        public const string SetMenuError = "Could not set menu.";
        public const string GetMenuError = "Could not get menu.";
        public const string MakeOrderError = "Could not make order.";
        public const string CancelOrderError = "Could not cancel order.";
        public const string GetUserInfoError = "Could not get current user info.";
    }
}