﻿using System.Web.Mvc;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;

namespace AkvelonLunch.Controllers
{
    [AuthorizeMVCFilter(Roles = Roles.Administrator)]
    public class AdminController : Controller
    {
        
        public ActionResult Index()
        {
            return this.View("../Home/Index");
        }
        
        public ActionResult Menu()
        {
            return this.View("../Home/Index");
        }
       
        public ActionResult Orders()
        {
            return this.View("../Home/Index");
        }

        public ActionResult Report()
        {
            return this.View("../Home/Index");
        }
    }
}