﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Repositories.ProvisionerRepository;
using AkvelonLunch.Models;
using AkvelonLunch.Responses;
using AkvelonLunch.Data;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/provisioner")]
    public class ProvisionerController : BaseApiController
    {
        private ProvisionerRepository TheRepository;
        public ProvisionerController() : base()
        {
            TheRepository = new ProvisionerRepository(TheContext);
        }

        [HttpGet]
        [Route("getProvisioner/{id}")]
        [AuthenticationFilter(Roles = Roles.Client)]
        public ProvisionerModel GetProvisionerById(int id)
        {
            var provisioner = TheRepository.GetProvisioner(id).Entity;
            return TheModelFactory.Create(provisioner);
        }

        [Route("create")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage CreateProvisioner([FromBody]ProvisionerModel model)
        {
            Provisioner provisioner = TheModelFactory.Parse(model);
            var os = TheRepository.Create(provisioner);
            var mssg = new CreateResponseMessage<int?>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("delete/{id}")]
        [HttpDelete]

        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DeleteProvisioner(int id)
        {
            var os = TheRepository.DeleteProvisioner(id);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("allprovisioners")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public ICollection<ProvisionerModel> GetListOfProvisioners()
        {
            var allProvisioners = TheRepository.GetProvisionerList().Entity.ToList<Provisioner>();

            return TheModelFactory.CreateListOfProvisioners(allProvisioners);
        }

        [Route("update")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage UpdateProvisioner([FromBody]ProvisionerModel model)
        {
            Provisioner provisioner = TheModelFactory.Parse(model);
            var os = TheRepository.Update(provisioner);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

    }
}