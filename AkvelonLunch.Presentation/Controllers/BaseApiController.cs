﻿using System.Web.Http;
using AkvelonLunch.Data;
using AkvelonLunch.Models;

namespace AkvelonLunch.Controllers
{
    using System.Web;

    using AkvelonLunch.Auth.TicketChecker;

    public class BaseApiController : ApiController
    {
        public int CurrentUserId
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    ITicketChecker ticketChecker = new TikcetChecker();
                    var cookieName = System.Configuration.ConfigurationManager.AppSettings["AuthenticationCookieName"];
                    var httpCookie = HttpContext.Current.Request.Cookies.Get(cookieName);
                    if (httpCookie != null)
                    {
                        return ticketChecker.GetUserId(httpCookie.Value);
                    }
                }
                return -1;
            }
        }

        private ModelFactory modelFactory;
        protected AkvelonLunchContext TheContext { get; }

        public BaseApiController()
        {
            this.TheContext = new AkvelonLunchContext();
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (this.modelFactory == null)
                {
                    this.modelFactory = new ModelFactory();
                }
                return this.modelFactory;
            }
        }
        
    }
}