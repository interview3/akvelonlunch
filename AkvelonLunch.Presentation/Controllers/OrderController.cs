﻿using System;
using System.Web.Http;
using AkvelonLunch.Models;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Responses;
using System.Net.Http;
using System.Net;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Data.Repositories.OrderRepository;
using AkvelonLunch.Reports;

namespace AkvelonLunch.Controllers
{
    using Data.Repositories.UserRepository;

    [AuthenticationFilter(Roles = Roles.Client)]
    [RoutePrefix("api/order")]
    public class OrderController : BaseApiController
    {
        private OrderRepository TheRepository;
        public OrderController() : base()
        {
            TheRepository = new OrderRepository(TheContext);
        }

        [Route("allorder")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public ICollection<OrderModel> GetListOfOrders()
        {
            var allOrders = TheRepository.GetOrderList().Entity.ToList<Order>();
            return TheModelFactory.CreateListOfOrders(allOrders);
        }

        [Route("make")]
        [HttpPost]
        public HttpResponseMessage MakeOrder([FromBody]OrderModel model)
        {
            Order order = TheModelFactory.Parse(model);
            order.UserId = this.CurrentUserId;
            order.OrderTime = DateTime.Now;
            var os = TheRepository.Create(order);
            var mssgForOrder = new CreateResponseMessage<int?>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.UserIsDeletedError:
                    //(new CustomAuthentication(HttpContext.Current)).LogOut();
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, os.Message);
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.Created, mssgForOrder);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssgForOrder);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssgForOrder);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(mssgForOrder);
                default:
                    return null;
            }
        }


        [Route("cancel/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteOrder(int id)
        {
            var os = TheRepository.DeleteOrder(id, this.CurrentUserId);
            var mssgForOrder = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssgForOrder);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssgForOrder);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssgForOrder);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssgForOrder);
                default:
                    return null;
            }
        }

        [HttpGet]
        [Route("byId/{id}")]
        public OrderModel GetOrderById(int id)
        {
            var order = TheRepository.GetOrder(id).Entity;

            return TheModelFactory.Create(order);
        }

        [Route("getOrderByUserId/{userId}")]
        [HttpGet]
        public OrderModel GetPendingOrderByUserId(int userId)
        {
            var order = TheRepository.GetPendingOrderByUserId(userId).Entity;

            return TheModelFactory.Create(order);
        }

        [Route("statistics")]
        [HttpGet]
        public HttpResponseMessage GetStatistics()
        {
            var orders = TheRepository.GetOrderList();
            if (orders.Entity != null)
            {
                decimal sum = (orders.Entity.Any()) ? orders.Entity.Sum(ord => ord.Cost) : 0;
                return Request.CreateResponse(
                    HttpStatusCode.OK,
                    new Models.OrderStatisticModel
                    {
                        TotalDishesCountToday = orders.Entity.Count(),
                        TotalOrderCostSumToday = sum
                    });
            }
            else
            {
                return Request.CreateResponse(
                    HttpStatusCode.InternalServerError,
                    new Models.OrderStatisticModel { TotalDishesCountToday = 0, TotalOrderCostSumToday = 0 });
            }
        }

        [Route("getTodayUserOrder")]
        [HttpGet]
        public HttpResponseMessage GetTodayUserOrder()
        {
            var os = TheRepository.GetOrderOfUserByDate(this.CurrentUserId, DateTime.Today.Date);

            switch (os.StatusCode)
            {
                case DalOperationStatusCode.UserIsDeletedError:
                    //(new CustomAuthentication(HttpContext.Current)).LogOut();
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, os.Message);
                default:
                    return Request.CreateResponse(HttpStatusCode.OK, TheModelFactory.Create(os.Entity));
            }
        }

        [Route("ordersInPeriod")]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        [HttpGet]
        public HttpResponseMessage UserOrdersInPeriod([FromUri] int userId, [FromUri] DateTime start, [FromUri] DateTime finish)
        {
            var orders = TheRepository.GetOrdersOfUserInPeriod(userId, start, finish);

            //var response = new CreateResponseMessage<List<Order>>(orders);
            var response = new CreateResponseMessage<List<OrderModel>>(new DalOperationStatus<List<OrderModel>>()
            {
                Entity = TheModelFactory.CreateListOfOrders(orders.Entity).ToList()
            });

            switch (orders.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, response.OperationStatus.Entity);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, response);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, response);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, response);
                default:
                    return null;
            }
        }

        [Route("today")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage GetTodayMenuOrders()
        {
            var os = TheRepository.GetTodayMenuOrdersList();
            var todayMenuOrders = TheModelFactory.CreateListOfOrders(os.Entity);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, todayMenuOrders);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, os.Message);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, os.Message);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, todayMenuOrders);
                default:
                    return null;
            }
        }

        [Route("OrdersByDate/{date:DateTime?}")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage GetOrdersByDate(DateTime date)
        {
            var os = TheRepository.GetOrdersListByDate(date);
            var mssgForTodayOrders = new CreateResponseMessage<IQueryable<Order>>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssgForTodayOrders);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssgForTodayOrders);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssgForTodayOrders);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssgForTodayOrders);
                default:
                    return null;
            }
        }

        [Route("byProvisionerId/{id}")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage GetTodayOrderItemsOfProvisioner(int id)
        {
            var os = TheRepository.GetTodayOrderItemsOfProvisioner(id);
            var mssgForOrderItems = new CreateResponseMessage<IQueryable<OrderItem>>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, os.Entity);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssgForOrderItems);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssgForOrderItems);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssgForOrderItems);
                default:
                    return null;
            }
        }

        [Route("usersWhoMadeToday")]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        [HttpGet]
        public HttpResponseMessage GetUsersWhoMadeOrderByProvisionerId([FromUri] int provisionerId, [FromUri] DateTime start, [FromUri] DateTime finish)
        {
            var os = TheRepository.GetUsersWhoMadeOrderByProvisionerId(provisionerId, start, finish);
            var mssg = new CreateResponseMessage<List<User>>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, os.Entity);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("todayUserOrder/{userId}")]
        [HttpGet]
        public OrderModel GetTodayOrderByUserId(int userId)
        {
            var order = TheRepository.GetOrderOfUserByDate(userId, DateTime.Now).Entity;
            return TheModelFactory.Create(order);
        }

        [Route("userOrderByDate")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public OrderModel GetUserOrderByDate(int userId, DateTime date)
        {
            var order = TheRepository.GetOrderOfUserByDate(userId, date).Entity;
            return TheModelFactory.Create(order);
        }

        [Route("notifyAboutDelivering")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage NotifyAboutDelivering()
        {
            var os = TheRepository.NotifyAboutDelivering(DateTime.Today);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, os.Entity);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, os.BuildErrorMessageString());
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, os.Message);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, os.Message);
                default:
                    return null;
            }
        }

        [Route("orderstatus/{Id}")]
        [HttpGet]
        public string GetOrderStatusStringByOrderId(int Id)
        {
            var statusString = TheRepository.GetOrderStatusString(Id).Entity;
            return statusString;
        }

        [Route("wereTodayOrdersDelieveried")]
        [HttpGet]
        public bool WereTodayOrdersDelieveried()
        {
            bool wereTodayOrdersDelieveried = TheRepository.WereTodayOrdersDelieveried();
            return wereTodayOrdersDelieveried;
        }

        [Route("CostSumsOfTimeSpans")]
        [HttpGet]
        public List<decimal> GetSumsOfOrdersCostBySomeTimeSpan(DateTime[] dateTimes)
        {
            var statusString = TheRepository.GetSumsOfOrdersCostBySomeTimeSpan(dateTimes).Entity;
            return statusString;
        }

        [Route("filtered")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage GetFilteredOrders([FromUri] int provisionerId, [FromUri] DateTime start, [FromUri] DateTime finish)
        {
            var orders = TheRepository.GetFilteredOrders(provisionerId, start, finish);
            List<OrderRowModel> orderModels = new List<OrderRowModel>();
            for (int i = 0; i < orders.Entity.Count; i++)
            {
                orderModels.Add(TheModelFactory.CreateRow(orders.Entity.ElementAt(i)));
            }
            return Request.CreateResponse(orderModels);
        }

        [Route("download")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(DateTime startDate, DateTime endDate)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new OrderReportsGenerator().GenerateReport(startDate, endDate);
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "From " + startDate.Day + "." + startDate.Month +
                                                                     "." + startDate.Year + " to " + endDate.Day +
                                                                     "." + endDate.Month + "." + endDate.Year +
                                                                     " orders report.xlsx";
                return result;
            }
        }

        [Route("downloadByUserIdAndTimeSpan")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(int userId, DateTime startDate, DateTime endDate)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new OrderReportsGenerator().GenerateReport(userId, startDate, endDate);
            var userRepository = new UserRepository(TheContext);
            var user = userRepository.GetUserById(userId).Entity;
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "From " + startDate.Day + "." + startDate.Month +
                                                                     "." + startDate.Year + " to " + endDate.Day +
                                                                     "." + endDate.Month + "." + endDate.Year +
                                                                     " " + user.FirstName + " " + user.LastName + " orders report.xlsx";
                return result;
            }
        }

        [Route("downloadByUserId")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(int userId)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new OrderReportsGenerator().GenerateReport(userId);
            var userRepository = new UserRepository(TheContext);
            var user = userRepository.GetUserById(userId).Entity;
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = user.FirstName + " " + user.LastName + " orders report.xlsx";
                return result;
            }
        }

    }
}