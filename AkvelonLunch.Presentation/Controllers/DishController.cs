﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Data;
using AkvelonLunch.Models;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Repositories.DishRepository;
using AkvelonLunch.Responses;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/dish")]
    public class DishController : BaseApiController
    {
        private DishRepository TheRepository;
        public DishController() : base()
        {
            TheRepository = new DishRepository(TheContext);
        }

        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public DishModel GetDish(int id)
        {
            var dish = TheRepository.GetDish(id).Entity;

            return TheModelFactory.Create(dish);
        }

        [Route("dishtypes")]
        [HttpGet]
        public IList<DishTypeModel> GetMyDishType()
        {
            var types = TheRepository.GetDishTypes();

            return TheModelFactory.CreateListOfDishTypeModels(types.Entity);
        }

        [Route("alldishes")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public ICollection<DishModel> GetListOfDish()
        {
            var dishes = TheRepository.GetDishList().Entity.ToList<Dish>();
            return TheModelFactory.CreateListOfDishes(dishes);
        }


        [Route("create")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage CreateDish([FromBody]DishModel model)
        {
            Dish dish = TheModelFactory.Parse(model);
            var os = TheRepository.Create(dish);
            var mssg = new CreateResponseMessage<int?>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }

        }

        [Route("delete/{id}")]
        [HttpDelete]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DeleteDish(int id)
        {
            var os = TheRepository.DeleteDish(id, this.CurrentUserId);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("update")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage UpdateDish([FromBody]DishModel model)
        {
            Dish dish = TheModelFactory.Parse(model);
            var os = TheRepository.Update(dish, this.CurrentUserId);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }
    }
}