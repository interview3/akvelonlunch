﻿using System.Web.Mvc;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;

namespace AkvelonLunch.Controllers
{
    public class HomeController : Controller
    {
        [AuthorizeMVCFilter(Roles = Roles.Client)]
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Login()
        {
            return this.View("Index");
        }        
    }
}