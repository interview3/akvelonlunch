﻿using System.Collections.Generic;
using System.Web;
using AkvelonLunch.Data;
using AkvelonLunch.Models;
using System.Web.Http;
using AkvelonLunch.Data.Entities;
using System.Net.Http;
using AkvelonLunch.Data.Repositories.UserRepository;
using System.Net;
using System;
using System.Linq;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Auth.TicketChecker;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Responses;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        private UserRepository TheRepository;
        public UserController() : base()
        {
            TheRepository = new UserRepository(TheContext);
        }

        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public UserModel GetUserById(int id)
        {
            var user = TheRepository.GetUserById(id).Entity;

            return TheModelFactory.Create(user);
        }

        [Route("allusers")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public ICollection<UserModel> GetListOfUsers()
        {
            var allUsers = TheRepository.GetUserList().Entity.ToList<User>();

            return TheModelFactory.CreateListOfUsers(allUsers);
        }

        [Route("updatebalance")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage UpdateBalance(int id, decimal balance)
        {
            var os = TheRepository.UpdateUserBalance(id, balance, CurrentUserId);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("update")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage Update([FromBody]UserModel userModel)
        {
            var user = TheModelFactory.Parse(userModel);
            User admin;
            if (GetCurrentUser().TryGetContentValue(out admin))
            {
                var os = TheRepository.UpdateUser(user, (int)admin.Id);
                var mssg = new CreateResponseMessage<bool>(os);
                switch (os.StatusCode)
                {
                    case DalOperationStatusCode.ChangesSaved:
                    case DalOperationStatusCode.OperationSuccessful:
                        return Request.CreateResponse(HttpStatusCode.OK, mssg);
                    case DalOperationStatusCode.Error:
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                    case DalOperationStatusCode.EntityNotFound:
                        return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                    case DalOperationStatusCode.NoChanges:
                        return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                }
            }
            return Request.CreateResponse(HttpStatusCode.NotAcceptable);
        }

        [Route("delete/{userId}")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DeleteUser(int userId)
        {
            var os = TheRepository.DeleteUser(userId);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }


        [Route("")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public HttpResponseMessage GetCurrentUser()
        {
            try
            {
                ITicketChecker ticketChecker = new TikcetChecker();
                int userId = ticketChecker.GetUserId(HttpContext.Current.Request.Cookies.Get(System.Configuration.ConfigurationManager.AppSettings["AuthenticationCookieName"]).Value);
                User user = TheRepository.GetUserById(userId).Entity;
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, ex);
            }
        }

        [HttpGet]
        [Route("totalcash")]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage GetTotalCash()
        {
            var cash = TheRepository.GetTotalCash();
            if (cash.Entity != null)
                return Request.CreateResponse(HttpStatusCode.OK, cash.Entity);
            return Request.CreateResponse(HttpStatusCode.NotFound, cash.Entity);
        }

        [HttpPost]
        [Route("setUserSettings")]
        [AuthenticationFilter(Roles = Roles.Client)]
        public HttpResponseMessage SetUserSetting([FromBody]UserSettingModel userSettingModel)
        {
            var userSetting = TheModelFactory.Parse(userSettingModel);
            var os = TheRepository.UpdateUserSetting(userSetting);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                default:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
            }
        }

        
        [Route("enabledNotifications")]
        [HttpGet]
        public HttpResponseMessage EnabledNotifications(bool value)
        {
            var userSetting = new UserSetting()
            {
                SettingId = (int)SettingEnum.NotificationsEnabled,
                UserId = CurrentUserId,
                Value = value.ToString()
            };
            var os = TheRepository.UpdateUserSetting(userSetting);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                default:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
            }
        }

        [Route("getEnabledNotificationValue")]
        [HttpGet]
        public bool GetEnabledNotificationsValue()
        {
            return TheRepository.GetEnabledNotificationsValue(CurrentUserId);
        }
    }
}