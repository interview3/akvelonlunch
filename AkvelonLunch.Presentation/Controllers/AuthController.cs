﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.Authentification;
using AkvelonLunch.Data;
using Newtonsoft.Json.Linq;
using AkvelonLunch.Data.Repositories.UserRepository;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApiController
    {
        private IAuthentication _auth;

        private IUserRepository _userRepository;

        public AuthController() : base()
        {
            _userRepository = new UserRepository(TheContext);
            _auth = new CustomAuthentication(HttpContext.Current, _userRepository);
        }

        // GET api/<controller>
        [System.Web.Http.HttpGet]
        [Route("signIn")]
        public HttpResponseMessage SignIn()
        {
            Credentials userCredentials = Credentials.GetCredentialsFromHttpRequestMessage(Request);
            if (userCredentials != null && _auth.Login(userCredentials))
            {
                // HttpContext.Current.User = _auth.CurrentUser;
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        [Route("logOut")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage LogOut()
        {
            CustomAuthentication auth = new CustomAuthentication(HttpContext.Current, null);
            auth.LogOut();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}