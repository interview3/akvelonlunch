﻿using AkvelonLunch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AkvelonLunch.Data.Entities;
using System.Net;
using System.Net.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Responses;
using AkvelonLunch.Data.Repositories.MenuRepository;
using AkvelonLunch.Data;

namespace AkvelonLunch.Controllers
{
    using System.Web;

    using AkvelonLunch.Auth.Authentification;

    [AuthenticationFilter(Roles = Roles.Client)]
    [RoutePrefix("api/menu")]
    public class MenuController : BaseApiController
    {
        private MenuRepository TheRepository;

        public MenuController() : base()
        {
            TheRepository = new MenuRepository(TheContext);
        }

        [Route("create")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage CreateMenu([FromBody] MenuModel model)
        {
            model.DateToShow = model.DateToShow.ToLocalTime();
            Menu menu = TheModelFactory.Parse(model);
            menu.CreatorId = this.CurrentUserId;
            var os = TheRepository.Create(menu);
            var mssg = new CreateResponseMessage<int?>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }


        [Route("")]
        [HttpGet]
        public HttpResponseMessage GetMenu()
        {
            var os = TheRepository.GetMenuByDateAndByUserId(DateTime.Now, this.CurrentUserId);

            switch (os.StatusCode)
            {
                case DalOperationStatusCode.UserIsDeletedError:
                    //(new CustomAuthentication(HttpContext.Current)).LogOut();
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, os.Message);
                default:
                    return Request.CreateResponse(HttpStatusCode.OK, TheModelFactory.Create(os.Entity));
            }
        }

        [HttpGet]
        public MenuModel GetMenu(int id)
        {
            var menu = TheRepository.GetMenu(id).Entity;

            return TheModelFactory.Create(menu);
        }

        [Route("getMenuByDate/{date:DateTime?}")]
        [HttpGet]
        public MenuModel GetMenuByDate(DateTime date)
        {
            var menu = TheRepository.GetMenuByDateAndByUserId(date, this.CurrentUserId).Entity;

            return TheModelFactory.Create(menu);
        }

        [Route("allmenu")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public ICollection<MenuModel> GetListOfMenu()
        {
            var allMenu = TheRepository.GetMenuList().Entity.ToList<Menu>();
            return TheModelFactory.CreateListOfMenu(allMenu);
        }


        [Route("delete/{id}")]
        [HttpDelete]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DeleteMenu(int id)
        {
            var os = TheRepository.Delete(id);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("update")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage UpdateMenu([FromBody] MenuModel model)
        {
            Menu menu = TheModelFactory.Parse(model);
            var os = TheRepository.Update(menu);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }

        [Route("MenuIsBlocked")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public bool MenuIsBlocked()
        {
            return TheRepository.MenuIsBlocked();   //true - menu is blocked, false - menu is opened.
        }
    }
}