﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Web.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Models;
using AkvelonLunch.Data.Repositories.MoneyTransactionLogRepository;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Reports;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/MoneyTransactionLog")]
    [AuthenticationFilter(Roles = Roles.Administrator)]
    public class MoneyTransactionLogController : BaseApiController
    {
        private readonly MoneyTransactionLogRepository TheRepository;
        public MoneyTransactionLogController() : base()
        {
            this.TheRepository = new MoneyTransactionLogRepository(TheContext);
        }

        [HttpGet]
        public MoneyTransactionLogModel GetMoneyTransactionById(int id)
        {
            var transaction = this.TheRepository.GetById(id).Entity;

            return TheModelFactory.Create(transaction);
        }

        [Route("fromPage")]
        [HttpGet]
        public MoneyTransactionListWithCount<MoneyTransactionLogModel> GetMoneyTransactionsFromPage(int transactionsOnThePage, int pageNumber)
        {
            MoneyTransactionListWithCount<MoneyTransactionLogModel> moneyTransactionModelsListWithCount = new MoneyTransactionListWithCount<MoneyTransactionLogModel>();
            MoneyTransactionListWithCount<MoneyTransactionLog> moneyTransactionsListWithCount = this.TheRepository.GetListByCountTransactionsOnPageAndPageNumber(transactionsOnThePage, pageNumber).Entity;
            moneyTransactionModelsListWithCount.LogCount = moneyTransactionsListWithCount.LogCount;
            moneyTransactionModelsListWithCount.LogList = TheModelFactory.CreateListOfMoneyTransactionLogs(moneyTransactionsListWithCount.LogList).ToList();
            return moneyTransactionModelsListWithCount;
        }

        [Route("full")]
        [HttpGet]
        public MoneyTransactionListWithCount<MoneyTransactionLogModel> GetFullMoneyTransactionLog()
        {
            MoneyTransactionListWithCount<MoneyTransactionLogModel> moneyTransactionModelsListWithCount = new MoneyTransactionListWithCount<MoneyTransactionLogModel>();
            MoneyTransactionListWithCount<MoneyTransactionLog> moneyTransactionsListWithCount = this.TheRepository.GetFullMoneyTransactionLog().Entity;
            moneyTransactionModelsListWithCount.LogCount = moneyTransactionsListWithCount.LogCount;
            moneyTransactionModelsListWithCount.LogList = TheModelFactory.CreateListOfMoneyTransactionLogs(moneyTransactionsListWithCount.LogList).ToList();
            return moneyTransactionModelsListWithCount;
        }

        [Route("byDates")]
        [HttpGet]
        public ICollection<MoneyTransactionLogModel> GetMoneyTransactionsByDates(DateTime beginning, DateTime ending)
        {
            var transactions = this.TheRepository.GetTransactionsOfTimeSpan(beginning, ending).Entity;

            return TheModelFactory.CreateListOfMoneyTransactionLogs(transactions.ToList());
        }

        [Route("ofuser")]
        [HttpGet]
        public ICollection<MoneyTransactionLogModel> GetUserTransactions(int userId)
        {
            var transactions = this.TheRepository.GetUserTransactions(userId).Entity;

            return TheModelFactory.CreateListOfMoneyTransactionLogs(transactions.ToList());
        }

        [Route("ofUserByDates")]
        [HttpGet]
        public ICollection<MoneyTransactionLogModel> GetMoneyTransactionsOfUserByDates(int userId, DateTime beginning, DateTime ending)
        {
            var transactions = this.TheRepository.GetUserTransactionsOfTimeSpan(userId, beginning, ending).Entity;

            return TheModelFactory.CreateListOfMoneyTransactionLogs(transactions.ToList());
        }

        [Route("download")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(DateTime startDate, DateTime endDate)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new MoneyTransactionReportsGenerator().GenerateReport(startDate, endDate);
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "From " + startDate.Day + "." + startDate.Month +
                                                                     "." + startDate.Year + " to " + endDate.Day +
                                                                     "." + endDate.Month + "." + endDate.Year +
                                                                     " money transactions report.xlsx";
                return result;
            }
        }

        [Route("downloadByUserIdAndTimeSpan")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(int userId, DateTime startDate, DateTime endDate)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new MoneyTransactionReportsGenerator().GenerateReport(userId, startDate, endDate);
            var userRepository = new UserRepository(TheContext);
            var user = userRepository.GetUserById(userId).Entity;
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "From " + startDate.Day + "." + startDate.Month +
                                                                     "." + startDate.Year + " to " + endDate.Day + "." +
                                                                     endDate.Month + "." + endDate.Year + " " + user.FirstName +
                                                                     " " + user.LastName + " money transactions report.xlsx";
                return result;
            }
        }

        [Route("downloadByUserId")]
        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage DownloadReport(int userId)
        {
            MemoryStream ms = new MemoryStream();
            var workbook = new MoneyTransactionReportsGenerator().GenerateReport(userId);
            var userRepository = new UserRepository(TheContext);
            var user = userRepository.GetUserById(userId).Entity;
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                var byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.GetBuffer());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = user.FirstName + " " + user.LastName + " money transactions report.xlsx";
                return result;
            }
        }

    }
}