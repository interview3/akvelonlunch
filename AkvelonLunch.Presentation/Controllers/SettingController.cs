﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AkvelonLunch.Auth;
using AkvelonLunch.Auth.AtributeFilters;
using AkvelonLunch.Data;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Data.Repositories.SettingRepository;
using AkvelonLunch.Models;
using AkvelonLunch.Responses;

namespace AkvelonLunch.Controllers
{
    [RoutePrefix("api/setting")]
    public class SettingController : BaseApiController
    {
        private SettingRepository TheRepository;

        public SettingController() : base()
        {
            TheRepository = new SettingRepository(TheContext);
        }

        [Route("add")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage AddSetting([FromBody]SettingModel model)
        {
            Setting setting = TheModelFactory.Parse(model);
            var os = TheRepository.Add(setting);
            var mssg = new CreateResponseMessage<int?>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }


        [HttpGet]
        [AuthenticationFilter(Roles = Roles.Client)]
        public SettingModel GetSetting(int id)
        {
            var setting = TheRepository.GetSettingById(id).Entity;

            return TheModelFactory.Create(setting);
        }

        [Route("update")]
        [HttpPost]
        [AuthenticationFilter(Roles = Roles.Administrator)]
        public HttpResponseMessage UpdateSetting([FromBody]SettingModel model)
        {

            Setting setting = TheModelFactory.Parse(model);
            var os = TheRepository.Update(setting);
            var mssg = new CreateResponseMessage<bool>(os);
            switch (os.StatusCode)
            {
                case DalOperationStatusCode.ChangesSaved:
                case DalOperationStatusCode.OperationSuccessful:
                    return Request.CreateResponse(HttpStatusCode.OK, mssg);
                case DalOperationStatusCode.Error:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, mssg);
                case DalOperationStatusCode.EntityNotFound:
                    return Request.CreateResponse(HttpStatusCode.NotFound, mssg);
                case DalOperationStatusCode.NoChanges:
                    return Request.CreateResponse(HttpStatusCode.NotModified, mssg);
                default:
                    return null;
            }
        }
    }
}