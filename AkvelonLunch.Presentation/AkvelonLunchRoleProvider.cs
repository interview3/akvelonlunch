﻿namespace AkvelonLunch
{
    using Presentation.Data;
    using System;
    using System.Configuration.Provider;
    using System.Linq;
    using System.Net;
    using System.Web.Http;
    using System.Web.Security;


    /// <summary>
    /// Custom role provider for using our database.
    /// </summary>
    public sealed class PrismRoleProvider : RoleProvider
    {
        #region Fields

        /// <summary>
        /// Logger instance, used to log events.
        /// Configured by common.logging section in Web.config.
        /// </summary>
//        private static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The storage.
        /// </summary>
        private readonly Storage storage;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PrismRoleProvider"/> class.
        /// </summary>
        /// <exception cref="HttpResponseException">
        /// Http Response Exception.
        /// </exception>
        public PrismRoleProvider()
        {
            try
            {
                this.storage = new Storage();
            }
            catch (Exception exception)
            {
                //Logger.Error("Error creating PrismRoleProvider", exception);
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the application name.
        /// </summary>
        public override string ApplicationName { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add users to roles.
        /// </summary>
        /// <param name="usernames">
        /// The usernames.
        /// </param>
        /// <param name="rolenames">
        /// The role names.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            try
            {
                for (int i = 0; i < usernames.Length; ++i)
                {
                    //this.storage.EmployeesManager.SetRole(this.storage.EmployeesManager.Get(usernames[i]).Id, rolenames[i]);
                }
            }
            catch (Exception exception)
            {
                //Logger.Error("Error adding user to role", exception);
            }
        }

        /// <summary>
        /// The create role.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override void CreateRole(string roleName)
        {
            try
            {
                //this.storage.RolesManager.Add(roleName);
            }
            catch (Exception exception)
            {
                //Logger.Error("Error creating role", exception);
            }
        }

        /// <summary>
        /// The delete role.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <param name="throwOnPopulatedRole">
        /// The throw on populated role.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>  .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            try
            {
                if (this.RoleExists(roleName))
                {
                    throw new ProviderException("Role doesn't exist");
                }

                if (throwOnPopulatedRole && this.GetUsersInRole(roleName).Length > 0)
                {
                    throw new ProviderException("Cannot delete a populated role");
                }

                //this.storage.RolesManager.Delete(this.storage.RolesManager.Get(roleName).Id);
            }
            catch (Exception exception)
            {
                //Logger.Error("Error deleting role", exception);
                return false;
            }

            return true;
        }

        /// <summary>
        /// The find users in role.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <param name="usernameToMatch">
        /// The username to match.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            try
            {
                //return (from b in this.storage.EmployeesManager.Get() where b.Login == usernameToMatch && b.Role == roleName select b.Login).ToArray();
                return new string[] { };
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override string[] GetAllRoles()
        {
            try
            {
                //return (from b in this.storage.EmployeesManager.Get() select b.Login).ToArray();
                return new string[] { };
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        /// <summary>
        /// The get roles for user.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string[] GetRolesForUser(string username)
        {
            try
            {
                using (var tmpStorage = new Storage())
                {
                    //var result = tmpStorage.EmployeesManager.Get(username);
                    //return new[] { result.Role };

                    return new string[] { };
                }
            }
            catch (Exception exception)
            {
                //Logger.Error("Error getting roles for user", exception);
                return new string[] { };
            }
        }

        /// <summary>
        /// The get users in role.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                //      return (from b in this.storage.EmployeesManager.Get() where b.Role == roleName select b.Login).ToArray();
                return new string[] { };
            }
            catch (Exception)
            {
                return new string[] { };
            }
        }

        /// <summary>
        /// The is user in role.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>   .
        /// </returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            try
            {
               // if (this.storage.EmployeesManager.Get(username).Role == roleName)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// The remove users from roles.
        /// </summary>
        /// <param name="usernames">
        /// The usernames.
        /// </param>
        /// <param name="roleNames">
        /// The role names.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                foreach (string t in usernames)
                {
          //          this.storage.EmployeesManager.SetRole(this.storage.EmployeesManager.Get(t).Id, "User");
                }
            }
            catch (Exception exception)
            {
             //   Logger.Error("Error removing user from role", exception);
            }
        }

        /// <summary>
        /// The role exists.
        /// </summary>
        /// <param name="roleName">
        /// The role name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>   .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Http Response Exception.
        /// </exception>
        public override bool RoleExists(string roleName)
        {
            try
            {
            //    this.storage.RolesManager.Get(roleName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}