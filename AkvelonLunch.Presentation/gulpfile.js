/// <binding BeforeBuild='clean' AfterBuild='moveToScripts, ts, bundle' />
var gulp = require('gulp'),
    clean = require('gulp-clean'),
    path = require('path'),
    Builder = require('systemjs-builder'),
    ts = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps');

var tsProject = ts.createProject('./tsconfig.json');

var appDev = 'ScriptsApp'; // where your ts files are, whatever the folder structure in this folder, it will be recreated in the below 'dist/app' folder
var appProd = 'Scripts/app';

gulp.task('moveToScripts', function (done) {
    gulp.src([
     'node_modules/core-js/client/shim.min.js',
     'node_modules/zone.js/dist/zone.min.js',
     'node_modules/reflect-metadata/Reflect.js',
     'node_modules/systemjs/dist/system.src.js'
    ]).pipe(gulp.dest('./Scripts/'));
});


gulp.task('clean', function () {
    gulp.src('./Scripts/app/')
        .pipe(clean());
});
 
//gulp.task('watch', ['watch.ts']);

//gulp.task('watch.ts', ['ts'], function () {
//    return gulp.watch('ScriptsApp/*.ts', ['ts']);
//});

/** first transpile your ts files */
gulp.task('ts', () => {
    return gulp.src(appDev + '/**/*.ts')
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(ts(tsProject))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(appProd));
});

/** then bundle */
gulp.task('bundle', function () {
    // optional constructor options
    // sets the baseURL and loads the configuration file
    var builder = new Builder('', 'Scripts/systemjs.config.js');

    /*
       the parameters of the below buildStatic() method are:
           - your transcompiled application boot file (the one wich would contain the bootstrap(MyApp, [PROVIDERS]) function - in my case 'dist/app/boot.js'
           - the output (file into which it would output the bundled code)
           - options {}
    */
    return builder
        .buildStatic(appProd + '/boot.js', appProd + '/bundle.js', { minify: true, sourceMaps: true })
        .then(function () {
            console.log('Build complete');
        })
        .catch(function (err) {
            console.log('Build error');
            console.log(err);
        });
});

gulp.task('default', ['ts', 'bundle']);