﻿using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Responses
{
    public class UpdateResponseMessage
    {
        public bool UpdateSuccess { get; set; }

        public string Message { get; set; }

        public UpdateResponseMessage(bool updateSuccess, string message)
        {
            this.UpdateSuccess = updateSuccess;
            this.Message = message;
        }

    }
}