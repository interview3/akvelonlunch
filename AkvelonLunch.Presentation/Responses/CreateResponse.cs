﻿using AkvelonLunch.Data;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Responses
{
    public class CreateResponseMessage<T>
    {
        public int? Id { get; set; }

        public string Message { get; set; }

        public DalOperationStatus<T> OperationStatus {get; set;}

        public CreateResponseMessage(DalOperationStatus<T> status)
        {
            this.OperationStatus = status;
        }

        public CreateResponseMessage(int? id, string message)
        {
            this.Id = id;
            this.Message = message;
        }
    }
}