﻿/// <reference path="../typings/globals/core-js/index.d.ts" />
import {bootstrap} from '@angular/platform-browser-dynamic'
import {MainComponent} from './mainApp';

import {MainScreenComponent} from './components/mainScreen/mainScreen.component'
import {DishService} from './services/dish.service';
import {OrderService} from './services/order.service';
import {LoginService} from './services/login.service';
import {HTTP_PROVIDERS} from '@angular/http';
import {appRouterProviders} from './app.routes';
import {NotifyService} from './services/notify.service';
import {InformationService} from './services/information.service';
import Approutes = require("./app.routes");

bootstrap(MainComponent,
    [HTTP_PROVIDERS, Approutes.appRouterProviders, 
        DishService, LoginService, NotifyService, InformationService]);