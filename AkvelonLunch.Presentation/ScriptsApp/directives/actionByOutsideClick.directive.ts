﻿import {Directive, ElementRef, Output, Input, EventEmitter, HostListener} from '@angular/core';

@Directive({
    selector: '[clickOutside]'
})

export class ClickOutsideDirective {
    private _idString:string;
    constructor(private _elementRef: ElementRef) {
    }

    @Output()
    public clickOutside = new EventEmitter();
    
    @Input() set idString(idString: string) {
        this._idString = idString;
    }

    @HostListener('document:click', ['$event.target'])
    public onClick(targetElement) {
 
        if (!targetElement) {
            return;
        }

        //infoLogoObj keeps infoLogo image block for preventing earlier pop-up closing

        let infoLogoObj = document.getElementById(this._idString);

        const clickedInside = infoLogoObj.contains(targetElement);

        if (!clickedInside) {
            this.clickOutside.emit(null);
        }
    }
}