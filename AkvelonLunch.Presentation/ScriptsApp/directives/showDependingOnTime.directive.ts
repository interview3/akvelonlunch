﻿import { Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {SettingsService} from '../services/settings.service';
import {FormatClass} from '../utils/formatClass';

@Directive({
    selector: '[showDependingOnTime]',
    providers: [SettingsService]
})
export class ShowDependingOnTime implements OnInit {
    private el: HTMLElement;
    private _mode: string;
    private _closed: boolean = false;
    private _displayString:string = "block";
    @Input() set showMode(modeString: string) {
        this._mode = modeString;
    }

    @Input() set enableClosed(closed: boolean) {
        this._closed = closed;

        this.updateElement(closed);
    }

    @Input() set displayMode(displayString: string) {
        this._displayString = displayString;
    }

    constructor(el: ElementRef, private settingsService: SettingsService) {
        this.el = el.nativeElement;

        this.updateElement(this._closed);
    }
    ngOnInit() {
        if (this._mode == "show-then-closed") {
            this.el.style.visibility = "hidden";
        } else {
            this.el.removeAttribute("disable");
        }
        this.checkCurrentHour(SettingsService.blockHourCached);
        this.settingsService.getblockHourChangedEmitter()
            .subscribe((blockHour) => {
                this.checkCurrentHour(blockHour);
            });
    }
    private updateElement(closed:boolean) {
        if (!closed) {
            switch (this._mode) {

                case "show-then-closed":
                    this.el.style.visibility = "hidden";
                    break;
                case "disable-then-closed":
                    this.el.removeAttribute("disable");
                    break;
                case "hide-then-closed":
                    this.el.style.visibility = "visible";
                    break;
                case "remove-then-closed":

                    this.el.style.display = this._displayString;
                    break;
            }
        } else {
            switch (this._mode) {
                case "show-then-closed":
                    this.el.style.visibility = "visible";
                    break;
                case "disable-then-closed":
                    this.el.setAttribute("disable", "");
                    break;
                case "hide-then-closed":
                    this.el.style.visibility = "hidden";
                    break;
                case "remove-then-closed":
                    this.el.style.display = "none";
                    break;
            }
        }
    }
    private checkCurrentHour(blockHour: Date) {
        if (blockHour == null)
            return;
        
        let curDate = new Date();
        let closedFlag = this._closed || !this.oneDateLessThanOther(curDate, blockHour)
       
        this.updateElement(closedFlag);
    }
    public oneDateLessThanOther(date1: Date, date2: Date) {
        return (date1.getHours() < date2.getHours()
            || (date1.getHours() == date2.getHours())
            && (date1.getMinutes() < date2.getMinutes()))  
    }
}