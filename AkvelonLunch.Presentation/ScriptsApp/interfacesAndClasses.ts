﻿interface IChart<T> {
    addItem(el: T);
    removeItem(el: T);
    getItemIdx(el: T): number;
    returnAsArray(): Array<Tuple<T, number>>;
}
class Tuple<F, S>{
    public first: F;
    public second: S;
    constructor(f: F, s: S) {
        this.first = f;
        this.second = s;
    }
}