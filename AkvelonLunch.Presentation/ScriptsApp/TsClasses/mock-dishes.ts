﻿import {Dish} from '../components/dish';
import {DishModel} from './JsonPlaceHolderClasses'
import {DishType} from './JsonPlaceHolderClasses'

export var DISHES: DishModel[] = [
    { "Id": 1, "Name": "Soup1", "Description": "Test1", "Cost": 50, "DishTypeId": 0},
    { "Id": 2, "Name": "Soup2", "Description": "Test2", "Cost": 100, "DishTypeId": 2},
    { "Id": 3, "Name": "Soup3", "Description": "Test3", "Cost": 200, "DishTypeId": 0},
    { "Id": 4, "Name": "Soup4", "Description": "Test4", "Cost": 100, "DishTypeId": 1},
    { "Id": 5, "Name": "Soup5", "Description": "Test5", "Cost": 70, "DishTypeId": 0},
];