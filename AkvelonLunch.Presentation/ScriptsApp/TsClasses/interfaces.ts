﻿
export interface ICart<T>{
    addItem(el: T);
    removeItem(el: T);
    getItemIdx(el: T): number;
    clear();
}