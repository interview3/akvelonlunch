﻿
//Enums

export enum DishType {
    Soup,
    SecondCourse,
    Salad,
    Other
}

//Model classes

export class DishModel {
    public Id: number;
    public Name: string;
    public Description: string;
    public Cost: number;
    public DishTypeId: DishType;

    constructor() { }
}

export class MenuModel {
    public Id:number;
    public Dishes : DishModel[];

    constructor() { }
}

export class OrderItemModel {
    public Id:number;
    public Dish: DishModel;
    public Count: number;

    constructor() { }
}

export class OrderModel {
    public Id:number;
    public Cost:number;
    public Item: OrderItemModel[];

    constructor() { }
}

export class UserModel {
    public Id:number;
    public Name: string;
    public Balance: number;

    constructor() { }
}