﻿import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import {CartComponent} from './cart/cart.component';
import {MenuComponent} from './menu/menu.component';
import {OrderHistoryComponent} from './orderHistory/orderHistory.component';
import {DishModel} from '../../ModelClasses/DishModel';
import {InfoPanel} from './infoPanel/infoPanel.component';
import {DishService} from '../../services/dish.service';
import {OrderService} from '../../services/order.service';
import {LoginService} from '../../services/login.service';
import {UserService} from '../../services/user.service';
import {SettingsService} from '../../services/settings.service';
import {Subscription}   from 'rxjs/Subscription';
import {LoginComponent} from '../login/login.component';
import {HTTP_PROVIDERS} from '@angular/http';
import {InformationService} from '../../services/information.service';
import {Router, RouterLink, RouterOutlet, ROUTER_DIRECTIVES} from '@angular/router';
import {ProvisionerService} from '../../services/provisioner.service';
import { AdminService } from '../../services/admin.service';
import {NotifyService} from '../../services/notify.service';
import {InteractionService} from '../../services/interaction.service';
import {NotifyComponent} from '../notification/notification.component';
import {UserSettings} from './userSettings/userSettings.component';
import {PushNotificationComponent} from '../../utils/NotificationsHelper';
import {Help} from './help/help.component'

@Component({
    selector: 'main-screen',
    templateUrl: `./ScriptsApp/components/mainScreen/mainScreen.html`,
    directives: [CartComponent, MenuComponent, InfoPanel, LoginComponent,
        RouterLink, RouterOutlet, ROUTER_DIRECTIVES, NotifyComponent,
        OrderHistoryComponent, UserSettings, PushNotificationComponent, Help],
    providers: [AdminService, ProvisionerService, DishService,
	LoginService, SettingsService, HTTP_PROVIDERS, InteractionService],
    styleUrls: ['./ScriptsApp/components/mainScreen/mainScreen.css']
})

export class MainScreenComponent {   

    constructor(private interactionService: InteractionService,
                private notifyService: NotifyService,
                private infotmationService: InformationService,
                private orderService: OrderService,
                private userService:UserService) {
        this.interactionService.getOrderHistoryShowStateEmitter()
            .subscribe((isVisible) => {
                this.onOrderHistoryShowStateChanged(isVisible);
            });
        interactionService.getUserSettingsEmitter()
            .subscribe((show) => {
                this.onUserSettings(show);
            });
        interactionService.getHelpPopUp()
            .subscribe((show) => {
                this.onHelpShowStateChanged(show);
            });
        this.notifyService.getMessageForPushNotify()
            .subscribe(
            (message) => { this.changeMessagePushNotify(message)})
    }

    changeMessagePushNotify(message) {
         if (message == "balance") {
             this.userService.getuser()
                   .subscribe(
                      (balance) => {
                         localStorage.setItem("PushNotify", "balance");
                         this.notification.body = "Не забудьте пополнить баланс! Ваш текущий баланс " + balance.Balance;
                         this.notifyService.showPushNotify('');   
                 });
          }
         if (message == "notOrder") {
              localStorage.setItem("PushNotify", "notOrder");
              this.notification.body = "Не забудьте сделать Ваш заказ";
              this.notifyService.showPushNotify('');   
          }
          if (message == "orderDelivered") {
              this.orderService.loadTodayUserOrder()
                  .subscribe(
                  (order) => {
                      localStorage.setItem("PushNotify", "orderDelivered");
                      this.notification.body = "Ваш заказ №" + order.Id + " доставлен";
                      this.notifyService.showPushNotify('');
                  });
          }             
    }

	 public notification: any = {
        show: false,
        title: 'Akvelon Lunch',
        icon: './ScriptsApp/components/mainScreen/icon/iconPushNotify.png'
      }
	
    private onUserSettings(show: boolean) {
        if (show) {
            this.onOrderHistoryShowStateChanged(false);
            this.onHelpShowStateChanged(false);
            (<HTMLScriptElement>(document.getElementsByTagName("usersettings")[0])).style.display = "block";
        } else {
            (<HTMLScriptElement>(document.getElementsByTagName("usersettings")[0])).style.display = "none";
        }
     }

    private onOrderHistoryShowStateChanged(isVisible: boolean) {
        if (isVisible) {
            this.onUserSettings(false);
            this.onHelpShowStateChanged(false);
            (<HTMLScriptElement>(document.getElementsByTagName("orderhistory")[0])).style.display = "block";//("display", "block");
        } else {
            (<HTMLScriptElement>(document.getElementsByTagName("orderhistory")[0])).style.display = "none";
        }
    }

    private onHelpShowStateChanged(show: boolean) {
        if (show) {
            this.onUserSettings(false);
            this.onOrderHistoryShowStateChanged(false);
            (<HTMLScriptElement>(document.getElementsByTagName("help")[0])).style.display = "block";
        } else {
            (<HTMLScriptElement>(document.getElementsByTagName("help")[0])).style.display = "none";
        }
    }
}
