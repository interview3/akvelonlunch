﻿import {Component, Output, EventEmitter} from '@angular/core';
import {DishModel} from '../../../ModelClasses/DishModel';
import {DishService} from '../../../services/dish.service';
import {OnInit} from '@angular/core';
import {DishTypeModel} from '../../../ModelClasses/DishTypeModel';
import {MenuModel} from '../../../ModelClasses/MenuModel';
import {OrderItemModel} from '../../../ModelClasses/OrderItemModel';
import {OrderService} from '../../../services/order.service';
import {InformationService} from '../../../services/information.service';
import {UserService} from '../../../services/user.service';
import {ShowDependingOnTime} from '../../../directives/showDependingOnTime.directive';
import {NotifyComponent} from '../../notification/notification.component';
import {NotifyService} from '../../../services/notify.service';

@Component({
    selector: 'menu',
    templateUrl: `./ScriptsApp/components/mainScreen/menu/menu.component.html`,
    styleUrls: ['./ScriptsApp/components/mainScreen/menu/menu.component.css',
        './ScriptsApp/components/mainScreen/mainScreen.css'],
    directives: [ShowDependingOnTime]
})

export class MenuComponent implements OnInit {
    public menuFlag: boolean = false;
    private menuLoadWasPerformed = false;
    public dishes: OrderItemModel[];
    private dishTypeList: string[];

    public set Locked(value: boolean) {
        this.dishService.lockedForEdit = value;
    }
    public get Locked(): boolean {
        return this.dishService.lockedForEdit;
    }

    public dishesOfType(DishTypeId: number): OrderItemModel[] {
        let curDishesArray: OrderItemModel[] = [];
        for (let i = 0; i < this.dishes.length; ++i)
        {
            if (this.dishes[i].Dish.DishType.Name == this.dishTypeList[DishTypeId]) {
                curDishesArray.push(this.dishes[i]);
            }
        }
        return curDishesArray;
    }

    constructor(private dishService: DishService,
        private orderService: OrderService,
        private userService: UserService,
        private informationService: InformationService,
        private notifyService: NotifyService) {
        this.dishes = [];
        dishService.incCountItems()
            .subscribe(dish => {
                this.onIncDishCount(dish)
            });
        dishService.getDishCountChangeEmitter()
            .subscribe((orderItem) => {
                this.onDishCountChange(orderItem);
            });
       this.dishTypeList = [];
        dishService.loadDishTypeList()
            .subscribe((dishTypesList) => {
                for (var i = 0; i < dishTypesList.length; ++i)
                    this.dishTypeList.push(dishTypesList[i].Name);
                
            });
     //   this.dishTypeList = ["Салат", "Суп", "Второе блюдо", "Дополнительное"];
       
    }

    loadMenu() {
        this.dishService.loadMenu().subscribe(
            (result) => { this.onMenuLoad(result) },
            (error) => {
                this.notifyService.showNotify("Can't get menu");
                setTimeout(this.loadMenu.bind(this), 10000);
            }
        );
    }

    loadOrderAndUpdateDishCounts() {
        this.orderService.loadTodayUserOrder()
            .subscribe((userOrder) => {
                if (userOrder != null) {
                    for (let i = 0; i < userOrder.Items.length; ++i) {
                        if (userOrder.Items[i].Count > 0) {
                            for (let j = 0; j < this.dishes.length; ++j) {
                                if (userOrder.Items[i].Dish.Id == this.dishes[j].Dish.Id) {
                                    this.dishes[j].Count = userOrder.Items[i].Count;
                                }
                            }
                        }
                    }
                }
            });
    }
    private onMenuLoad(result: MenuModel) {
       
        try { 
            var menuNotFound = document.getElementById("MenuNotFound");
            if (result == null) {
                this.menuFlag = false;
                menuNotFound.className = menuNotFound.className.replace("", "show");

            } else {
                let dishes = result.Dishes;
                if (dishes.length < 1) {
                    this.menuFlag = false;
                    menuNotFound.className = menuNotFound.className.replace("", "show");
                } else {
                    this.menuFlag = true;
                    menuNotFound.className = menuNotFound.className.replace("show", "");
                    for (let i = 0; i < dishes.length; ++i)
                        this.initDish(dishes[i]);
                    this.loadOrderAndUpdateDishCounts();
                    this.dishService.menuLoaded(result);
                }
            }

        } catch (error) {
            console.log(error);
        }
        this.menuLoadWasPerformed = true;
    }

    initDish(dish: DishModel) {
        this.dishes.push(new OrderItemModel(dish, 0));   
    }

    ngOnInit() {
        this.loadMenu();
    }

    incDishCount(dish: DishModel) {
        if (!this.Locked) {
            this.dishService.addDishToCart(dish);
        }
    }

    changeDishCount(dish: DishModel, newCount: number) {
        if (!this.Locked) {
            this.dishService.setDishCount(dish, newCount);
        }
    }

    onIncDishCount(dish:DishModel) {
        var idx = this.getDishIdxFromArray(dish);
        this.dishes[idx].Count++;
    }

    onDishCountChange(orderItem:OrderItemModel) {
        var idx = this.getDishIdxFromArray(orderItem.Dish);
     
        if (orderItem.Count < 0)
            throw new Error("Count must be greater than zero");
        this.dishes[idx].Count = orderItem.Count;
    }

    private getDishFromArray(dish: DishModel): DishModel {
        for (var i = 0; i < this.dishes.length; ++i) {
            if (this.dishes[i].Dish.Id == dish.Id)
                return this.dishes[i].Dish;
        }
        return null;
    }

    private getDishIdxFromArray(dish: DishModel) {
        for (var i = 0; i < this.dishes.length; ++i) {
            if (this.dishes[i].Dish.Id == dish.Id)
                return i;
        }
        return null;
    }
}