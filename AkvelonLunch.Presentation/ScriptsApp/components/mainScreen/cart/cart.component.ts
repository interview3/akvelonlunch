﻿import {Component, OnInit, Input, Output} from '@angular/core';
import {DishModel} from '../../../ModelClasses/DishModel';
import {MenuModel} from '../../../ModelClasses/MenuModel';
import {DiscountModel} from '../../../ModelClasses/DiscountModel';
import {DishTypeEnum} from '../../../ModelClasses/DishTypeEnum';
import {DishService} from '../../../services/dish.service';
import {OrderService} from '../../../services/order.service';
import {Subscription}   from 'rxjs/Subscription';
import {OrderItemModel} from '../../../ModelClasses/OrderItemModel';
import {OrderModel} from '../../../ModelClasses/OrderModel';
import {InfoPanel} from '../infoPanel/InfoPanel.component'
import {OrderStatuses} from '../../../ModelClasses/OrderStatuses';
import {InformationService} from '../../../services/information.service';
import {SettingsService} from '../../../services/settings.service'; 
import {UserService} from '../../../services/user.service'; 
import {FormatClass} from '../../../utils/formatClass';
import {ShowDependingOnTime} from '../../../directives/showDependingOnTime.directive';
import {Observable} from 'rxjs/Observable';
import {NotifyComponent} from '../../notification/notification.component';
import {NotifyService} from '../../../services/notify.service';
import {ProvisionerService} from '../../../services/provisioner.service';

@Component({
    selector: 'cart',
    templateUrl: `./ScriptsApp/components/mainScreen/cart/cart.component.html`,
    styleUrls: ['./ScriptsApp/components/mainScreen/cart/cart.component.css',
        './ScriptsApp/components/mainScreen/mainScreen.css'],
    directives: [ShowDependingOnTime]
})
export class CartComponent implements OnInit {
    @Input()
    public isAdminPanel: boolean = false;
    @Input()
    public order: OrderModel;
    private init:boolean = false;
    @Input()
    public userName: string;
    @Input()
    public orderSum:number;

    public timer: any;
    public makeOrderError: string;
    public buttonDis: boolean = false;
    public moneyFlag: boolean = true;
    public totalSum: number = 0;
    private discount: number = 0;
    private menuId: number = 0;
    public balance: number;
    private blockHour: Date;
    public orderStatus: OrderStatuses = null;
    public timeForOrderIsValid: boolean;
    private provisionerDiscount: number = 0;
    public set Locked(value: boolean) {
        this.dishService.lockedForEdit = value;
    }
    public get Locked(): boolean {
        return this.dishService.lockedForEdit ||
       !this.timeForOrderIsValid;
    }
    constructor(private dishService: DishService,
                private orderService: OrderService,
                private userService: UserService,
                private settingsService: SettingsService,
                private notifyService: NotifyService,
                private provisionerService: ProvisionerService) {
        notifyService.getEnableButton().subscribe(
            (button) => {
                this.enableMakeOrderButton();
            });
        this.userService.getuser().subscribe((userInfo) => {
            this.balance = userInfo.Balance;
        });  
        dishService.getDishAddToCartEmitter()
            .subscribe(dish => {
                this.onAddDish(dish);           
            });
        dishService.getDishCountChangeEmitter()
            .subscribe((orderItem) => {
                this.onSetDishCount(orderItem.Dish, orderItem.Count);
            });
        dishService.getMenuLoadedEmitter()
            .subscribe((menuModel) => {
                this.onMenuLoaded(menuModel);
            });
        this.order = new OrderModel();
        this.blockHour = SettingsService.blockHourCached;
        this.checkTime(this.blockHour);
        this.settingsService.getblockHourChangedEmitter()
            .subscribe((blockHour) => {
                this.blockHour = blockHour;
                this.checkTime(blockHour);
            });
    }

    private onMenuLoaded(menuModel: MenuModel) {
        this.menuId = menuModel.Id;
    }

    public getHourFromStringHHMMSS(timeString: string): number {
        let pIdx = timeString.indexOf(':');
        return +timeString.substr(0, pIdx);
    }

    ngOnInit() {
        this.provisionerService.getProvisioner(1).subscribe((result) => {
            if (result.Discount != null) {
                this.provisionerDiscount = result.Discount.DiscountSum;
            }
            if (!this.isAdminPanel)
                this.loadTodayUserOrder();
        });            
    }

    public loadTodayUserOrder(forceLoad: boolean = false) {
       
        this.orderService.loadTodayUserOrder(forceLoad)
            .subscribe((userOrder) => {
                this.onLoadTodayUserOrder(userOrder);
            });
    }
    loadOrderCart() {
        this.orderService.loadTodayUserOrder()
            .subscribe((userOrder) => {
                this.order.Id = userOrder.Id;
                for (var i = 0; i < userOrder.Items.length; ++i) {
                    this.addOrderItem(userOrder.Items[i]);
                }
            });
    }

    public getOrderItemByDishId(Id: number): OrderItemModel {
        for (var i = 0; i < this.order.Items.length; ++i) {
            if (this.order.Items[i].Dish.Id == Id) {
                return this.order.Items[i];
            }
        }
        return null;
    }

    public addDish(dish: DishModel) {
        this.dishService.addDishToCart(dish);
    }

    @CalcTotalSum
    public removeDish(dish: DishModel) {
        this.dishService.setDishCount(dish, 0);
    }

    public setDishCount(dish: DishModel, count: number) {
        this.dishService.setDishCount(dish, count);
    }

    @CalcTotalSum
    public addOrderItem(orderItem: OrderItemModel) {
        this.order.Items.push(orderItem);
        this.dishService.addOrderItem(orderItem);
    }

    @CalcTotalSum
    public onAddDish(dish: DishModel) {
        if (this.updateBalance() >= this.getPreviewTotalSumWithDish(dish,1)) {
            this.dishAdd(dish);
            this.order.Items = this.order.Items.sort((a, b) => a.Dish.Name.localeCompare(b.Dish.Name));
        }
        else {
            this.notifyService.showNotify("No money");
        }
    }

    updateBalance():number {
        this.userService.getuser(true).subscribe((userInfo) => {
            this.balance = userInfo.Balance;
        });
        return this.balance;
    }


    dishAdd(dish: DishModel) {
            var orderItem = this.getOrderItemByDishId(dish.Id);
            this.dishService.incCountDish(dish);
   
            if (orderItem != null) {
                orderItem.Count++;
            } else {
                var oi = new OrderItemModel(dish, 1);
                oi.OrderId = this.order.Id;
                this.order.Items.push(oi);
            }
    }

    @CalcTotalSum
    public onSetDishCount(dish: DishModel, count: number) {
        var orderItem = this.getOrderItemByDishId(dish.Id);
        if (orderItem != null) {
            if (count > 0) {
                orderItem.Count = count;
            } else {
                this.deleteOrderItemFromOrder(dish.Id);
            }
            this.order.Items = this.order.Items.sort((a, b) => a.Dish.Name.localeCompare(b.Dish.Name));
        }
    }
    @CalcTotalSum
    public clear() {
        let dishesForRemove: DishModel[] = [];

        for (var i = 0; i < this.order.Items.length; ++i) {
            dishesForRemove.push(this.order.Items[i].Dish);
        }
        for (var i = 0; i < dishesForRemove.length; ++i) {
            this.dishService.setDishCount(dishesForRemove[i], 0);
        }

        this.order.Items = [];
    }

    public makeOrder() {
        if (!this.timeForOrderIsValid)
            return;
        this.order.MenuId = this.menuId;
        if (this.orderStatus == null) {
            this.orderService.makeOrder(this.order)
                .subscribe(
                (createdOrder) => {                    
                    if (createdOrder.OperationStatus.StatusCode == 2) {
                        this.notifyService.showNotify(createdOrder.OperationStatus.Message);
                        console.log(createdOrder.OperationStatus.Message);
                        this.disableMakeOrderButton();
                    } else {
                        this.order.Id = createdOrder.Id;
                        this.loadTodayUserOrder(true);
                        this.Locked = true;
                        this.orderService.changeOrder();
                    }                   
                },
                (error) => {
                    this.disableMakeOrderButton()
                    this.notifyService.showNotify(error);                                   
                });
        } else {
            this.orderService.updateOrder(this.order)
                .subscribe((updateOrder) => {
                   if (updateOrder.OperationStatus.StatusCode == 2) {
                       this.notifyService.showNotify(updateOrder.OperationStatus.Message);
                       console.log(updateOrder.OperationStatus.Message);
                       this.disableMakeOrderButton()
                   } else {
                       this.order.Id = updateOrder.Id;
                       this.loadTodayUserOrder();
                       this.Locked = true;
                       this.orderService.changeOrder();
                    }
                });
        }              
    }


    public deleteOrderItemFromOrder(dishId: number) {
        let orderItemWas: boolean = false;

        for (var i = 0; i < this.order.Items.length - 1; ++i) {
            if (this.order.Items[i].Dish.Id == dishId) {
                orderItemWas = true;
            }
            if (orderItemWas == true)
                this.order.Items[i] = this.order.Items[i + 1];

        }
        this.order.Items.length--;
    }

    public unlock() {
        this.Locked = false;
    }

    public cancel() {
        this.orderService.cancelOrder(this.order.Id).subscribe(() => this.onCancelOrder());
        this.Locked = false;
    }

    public onCancelOrder() {
        this.order.Id = null;
        this.orderStatus = null;
        this.orderService.changeOrder();
    }

    @CalcTotalSum
    public onLoadTodayUserOrder(userOrder: OrderModel) {
        if (userOrder == null) {
            this.orderStatus = null;
            if (this.order.Id != null) {
                this.clear();
            }
            this.Locked = false;

        } else {
            this.orderStatus = userOrder.OrderStatusId;
            
            this.order = userOrder;
            this.order.Items = this.order.Items.sort((a, b) => a.Dish.Name.localeCompare(b.Dish.Name));

            this.Locked = true;
        }
    }

    public checkTime(blockHour: Date){
        let curDate = new Date();
        if (blockHour == null)
            return;

        if (curDate.getHours() < blockHour.getHours()
            || (curDate.getHours() == blockHour.getHours())
        && (curDate.getMinutes() < blockHour.getMinutes())) {
            return this.timeForOrderIsValid = true;
        } else {
            return this.timeForOrderIsValid = false;
        }
    }

    public get OrderStatus(): string {
        if (this.orderStatus == null)
            return "";
        if (this.orderStatus == OrderStatuses.Pending) {
            if (this.Locked)
                return "Заказ №" + this.order.Id + " принят";
        }
        if (this.orderStatus == OrderStatuses.Completed)
            return "Заказ №" + this.order.Id + " доставлен";
        if (this.orderStatus == OrderStatuses.Canceled)
            return "Заказ №" + this.order.Id + " отменен";
    }

    calcDiscount(itemsCount: number): number {       
        //TODO  make provisioners logic
        let discount = 0;
        if ((Math.floor(itemsCount / 3) > 0) && ((this.totalSum - Math.floor(itemsCount / 3) * this.provisionerDiscount) > 0)) {
            discount = Math.floor(itemsCount / 3) * this.provisionerDiscount;
        } else {
            if (Math.floor(itemsCount / 3) == 0) {
                discount = 0;
            };
            if (discount < 0) {
                discount = 0;
            }
        }
        return discount;
    }  

    disableMakeOrderButton(){
         this.buttonDis = true;
         this.timer = setTimeout(this.enableMakeOrderButton.bind(this), 4800);
    }

    enableMakeOrderButton() {
        clearTimeout(this.timer.data.handleId);
        this.buttonDis = false;
    }

    getPreviewTotalSumWithDish(dish: DishModel, count: number): number {       
        let totalSum = 0;
        let itemsCount: number = 0;
        for (var i = 0; i < this.order.Items.length; ++i) {
            this.order.Items[i].Count = Number(this.order.Items[i].Count);
            totalSum += this.order.Items[i].Dish.Cost * this.order.Items[i].Count;
            if (this.isDiscountForTypeExists(this.order.Items[i].Dish.DishType.Id)) {              
                itemsCount += this.order.Items[i].Count;
            }
        }

        totalSum += dish.Cost * count;
        if (this.isDiscountForTypeExists(dish.DishType.Id)) {
            itemsCount += count;
        }

        let discount = this.calcDiscount(itemsCount);
        return totalSum - discount;
    }
    isDiscountForTypeExists(dishTypeId:number) {
        return (dishTypeId == DishTypeEnum.Soup
                || dishTypeId == DishTypeEnum.Salad
                || dishTypeId == DishTypeEnum.SecondCourse);
    }
}


function CalcTotalSum(target, key: string, value: any) {
    return {       
        value: function (...args: any[]) {             
            var result = value.value.apply(this, args);
            this.totalSum = 0;      
            let itemsCount:number = 0;
            for (var i = 0; i < this.order.Items.length; ++i) {
                this.order.Items[i].Count = Number(this.order.Items[i].Count);
                this.totalSum += this.order.Items[i].Dish.Cost * this.order.Items[i].Count;
                if (this.isDiscountForTypeExists(this.order.Items[i].Dish.DishType.Id))                                        
                        itemsCount += this.order.Items[i].Count;                    
            }

            const saladCount: number = this.order.Items.filter(function (item) {
                return item.Dish.DishType.Id == DishTypeEnum.Salad;
            }).reduce(function (sum, item) { return sum + item.Count }, 0);
            const soupCount: number = this.order.Items.filter(function (item) {
                return item.Dish.DishType.Id == DishTypeEnum.Soup;
            }).reduce(function (sum, item) { return sum + item.Count }, 0);
            const secondCourseCount: number = this.order.Items.filter(function (item) {
                return item.Dish.DishType.Id == DishTypeEnum.SecondCourse;
            }).reduce(function (sum, item) { return sum + item.Count }, 0);

            if (saladCount && soupCount && secondCourseCount) {
                this.discount = this.calcDiscount(Math.min(saladCount, soupCount, secondCourseCount) * 3);
            } else {
                this.discount = 0;
            }
        }
    };
}