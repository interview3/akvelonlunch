﻿import {Component} from '@angular/core';
import {OrderService} from '../../../services/order.service';
import {UserService} from '../../../services/user.service';
import {InteractionService} from '../../../services/interaction.service';
import {InformationService} from '../../../services/information.service';
import {DatePicker} from '../../datepicker/datepicker.component';
import {DateFilter} from '../../dateFilter/dateFilter.component';
import {OrderModel} from '../../../ModelClasses/OrderModel';
import {UserModel} from '../../../ModelClasses/UserModel';

@Component({
    selector: 'orderhistory',
    templateUrl: `./ScriptsApp/components/mainScreen/orderHistory/orderHistory.component.html`,
    styleUrls: ['./ScriptsApp/components/mainScreen/orderHistory/orderHistory.component.css',
                './ScriptsApp/components/mainScreen/mainScreen.css'],
    directives: [DatePicker, DateFilter]
})

export class OrderHistoryComponent {
    public blockHour: Date = null;
    public blockHours: number;
    public blockMinutes: number;
    public static showed: boolean = false;
    private order: OrderModel = null;
    public initOrderDate: Date;
    public numberOfIntervalsInChart: number = 2;
    public orderSumArray: number[] = [];

    constructor(private settingsService: OrderService,
        private interactionService: InteractionService,
        private orderService: OrderService,
        private informationService: InformationService,
        private userService: UserService) {
            this.initOrderDate = new Date();
            this.getOrderByDate(this.initOrderDate);
        this.interactionService.getOrderHistoryShowStateEmitter()
            .subscribe((flag) => {
                if (flag)
                    this.getOrderByDate(this.initOrderDate);
            });
    }

    public close() {
        this.interactionService.setOrderHistoryShowState(false);
       
    }

    getOrderByDate(date: Date) {
        this.userService.getuser()
            .subscribe((user) => {
            this.onUpdateCurOrder(date, user);
        });

        //don't know how to do that checking; this way it works (Anatoly)

    }

    onUpdateCurOrder(date: Date, user: UserModel) {
        this.orderService.getUserOrderByDate(user.Id, date)
            .subscribe((order) => {
                this.order = order;
            });
    }

    getOrderSumsInInterval(datePeriod: any) {
        let intervals: [Date, Date][] = [];
       
        let begTime = datePeriod.StartDate.getTime();
        let endTime = datePeriod.FinishDate.getTime();
        let millisecDiff = Math.abs(endTime - begTime);
        let timeStep = millisecDiff / this.numberOfIntervalsInChart;
        let curTime = begTime;
        
        while (curTime < endTime) {
            let dateBeg = new Date(curTime);
            let dateEnd = new Date(curTime + timeStep);
            intervals.push([dateBeg, dateEnd]);

            curTime += timeStep;
        }

        this.orderService.getOrderSumsInIntervals(intervals)
             .subscribe((dateIntervals) => {
                 this.orderSumArray = dateIntervals;
            });
        
    }

    getOrderSumsForLastWeek() {
        
        /*let period = new DateFilterPeriod();

        this.getOrderSumsInInterval()*/
    }

    getOrderSumsForLastMonth() {

    }

    getOrderSumsForLastThree() {

    }

    getOrderSumsForLastYear() {

    }
}
