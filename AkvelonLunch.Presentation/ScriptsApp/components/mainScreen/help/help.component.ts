﻿import {Component} from '@angular/core';
import {InteractionService} from '../../../services/interaction.service';
import {SettingsService} from '../../../services/settings.service';

@Component({
    selector: 'help',
    templateUrl: `./ScriptsApp/components/mainScreen/help/help.component.html`,
    styleUrls: ['./ScriptsApp/components/mainScreen/help/help.component.css']
})

export class Help {
    public blockHours: number;
    public blockMinutes: number;
    public blockMinutesString: string;

    constructor(private interactionService: InteractionService,
                private settingsService: SettingsService) {
        settingsService.getblockHourChangedEmitter().subscribe((blockHour) => {
            if (blockHour != null) {
                this.blockHours = blockHour.getHours();
                this.blockMinutes = blockHour.getMinutes();
                if (Math.floor(this.blockMinutes / 10) == 0) {
                    this.blockMinutesString = this.blockMinutes + '0';
                } else {
                    this.blockMinutesString = this.blockMinutes.toString();
                };
            }
        });
    }
    closeSettings() {
        this.interactionService.showHelpPopUp(false);
    }
}
