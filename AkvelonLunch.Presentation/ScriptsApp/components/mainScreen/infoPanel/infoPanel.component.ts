﻿import {Component, Input, OnInit} from '@angular/core';
import {InformationService} from '../../../services/information.service';
import { Router } from '@angular/router';
import {OrderService} from '../../../services/order.service';
import {UserRole} from '../../../ModelClasses/UserRole';
import {ClickOutsideDirective} from '../../../directives/actionByOutsideClick.directive';
import {NotifyComponent} from '../../notification/notification.component';
import {OrderHistoryComponent} from '../orderHistory/orderHistory.component';
import {NotifyService} from '../../../services/notify.service';
import {InteractionService} from '../../../services/interaction.service';
import {LoginService} from '../../../services/login.service';
import {UserService} from '../../../services/user.service';
import {MainScreenComponent} from '../mainScreen.component';

//@Injectable()
@Component({
    selector: 'infopanel',
    templateUrl: `./ScriptsApp/components/mainScreen/infoPanel/infoPanel.component.html`,
    styleUrls: [
        './ScriptsApp/components/mainScreen/infoPanel/infoPanel.component.css',
        './ScriptsApp/components/mainScreen/mainScreen.css'
    ],
    directives: [ClickOutsideDirective]
})
export class InfoPanel implements OnInit  {
    public userName: string;
    public balance: number;
    public isAdmin: boolean;
    public balanceError: boolean;
    public refreshDisable: boolean = false;

    public TotalDishesCountToday: number;
    public TotalOrderCostSumToday: number;

    constructor(private informationService: InformationService,
        private router: Router,
        private orderService: OrderService,
        private notifyService: NotifyService,
        private interactionService: InteractionService,
        private loginService: LoginService,
        private userService: UserService) {
        userService.getuser().subscribe(
                (userInfo) => {
                    this.getUserSuccess(userInfo);
                },
                error => this.getUserBalanceFailed(error)
        );

        orderService.getOrderChangeEmmiter()
            .subscribe((stat) => {
                    this.updateBalance();
                }
        );
        informationService.getCurrentUserBalanceUpdatedEmitter()
            .subscribe((newBalance) => {
                this.updateBalance();
            }
        );
    }

    ngOnInit() {
        //if (!this.checkCalcSupporting()) {
        //    document.getElementById("user-dropdown").style.right = "6%";
        //}
    }

    refreshBalanceButton() {
        if (!this.refreshDisable) {
            this.refreshDisable = true;
            setTimeout(this.refreshBalanceButton.bind(this), 1000);
            setTimeout(this.updateBalance.bind(this), 2000);
            var refreshBalance = document.getElementById("refreshButton");
            refreshBalance.className = refreshBalance.className.replace("glyphicon glyphicon-refresh refreshButton", "glyphicon glyphicon-refresh refreshButton rotate");
            setTimeout(className => { refreshBalance.className = refreshBalance.className.replace("glyphicon glyphicon-refresh refreshButton rotate", "glyphicon glyphicon-refresh refreshButton") }, 2000);  
        } else {
            this.refreshDisable = false;
        }           
    }

    updateBalance() {
        this.userService.getuser(true)
            .subscribe((userInfo) =>
                {
                    this.balance = userInfo.Balance;
                    this.balanceError = false;
                },
                error => this.getUserBalanceFailed(error)
            );
    }

private getUserSuccess(userInfo) {
        this.balance = userInfo.Balance;
        this.userName = userInfo.FirstName + ' ' + userInfo.SecondName;
        this.isAdmin = new UserRole(userInfo.RoleId).Name == 'Администратор';
        if (this.balance < 150) {
            this.notifyService.changeMessagePushNotify("balance");
        }
    }

    private getUserBalanceFailed(error) {
        this.balanceError = true;     
        this.userService.getuser()
            .subscribe(
            (userInfo) => {
                    this.getUserSuccess(userInfo);
                },
            (error) => {                               
                setTimeout(this.getUserBalanceFailed.bind(this), 10000, error);
                this.notifyService.showNotify("Can't get balance");         
            });
        console.log(error);      
    }

    private getUserFailed(error) {
        console.log(error);
    }

    public logout() {
        this.loginService.logout()
            .subscribe(
                result => this.logoutSuccess(),
                error => this.logoutFailed(error));
    }

    private logoutSuccess() {
        this.router.navigate(['./Home/Login']);
    }

    private logoutFailed(error) {
        console.log(error);
    }

    private getStatisticsSuccess(stat) {
        this.TotalDishesCountToday = stat.TotalDishesCountToday;
        this.TotalOrderCostSumToday = stat.TotalOrderCostSumToday;
    }

    private getStatisticsFailed(error) {
        console.log(error);
    }

    //TODO: this function should be replaced to some special class for formatting
    private getTwoDigitNumberAsString(num: number): string {
        if (num < 10)
            return "0" + String(num);
        else
            return String(num);
    }

    public hideDropDownMenu() {
        document.getElementById("myDropdown").classList.remove("show");
    }

    public redirectTo(path: string) {
        var menu = document.getElementById("account-menu");
        this.router.navigate([path]);
    }

    public dropDownMenu() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    public showOrderHistoryPopUp() {
        this.interactionService.setOrderHistoryShowState(true);
    }
    public showUserSettings() {
        this.interactionService.showUserSettings(true);
    }
    public showHelpPopUp() {
        this.interactionService.showHelpPopUp(true);
    }
}

