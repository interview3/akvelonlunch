﻿import {Component, OnInit} from '@angular/core';
import {PushNotificationComponent} from '../../../utils/NotificationsHelper';
import {InteractionService} from '../../../services/interaction.service';
import {InformationService} from '../../../services/information.service';
import {SettingsService} from '../../../services/settings.service';
import {NotifyService} from '../../../services/notify.service';

@Component({
    selector: 'usersettings',
    templateUrl: `./ScriptsApp/components/mainScreen/userSettings/userSettings.component.html`,
    styleUrls: [`./ScriptsApp/components/mainScreen/userSettings/userSettings.component.css`],
    directives: [PushNotificationComponent]
})
export class UserSettings {
    public pushPermission: string;
    public checkboxChecked: boolean;

    constructor(private interactionService: InteractionService,
        private informationService: InformationService,
        private settingsService: SettingsService,
        private notifyService: NotifyService) {
        try {
            if (PushNotificationComponent.checkCompatibility()) {
                PushNotificationComponent.requestPermission((value) => {
                });
            }
        } catch (e) {
            console.log(e);
        } 
    }

    ngOnInit() {
        this.settingsService.getEnabledNotificationValue().subscribe(result => {
            if (result) {
                this.checkboxChecked = true;
            } else {
                this.checkboxChecked = false;
            }
        });
    }

    closeSettings() {
        this.interactionService.showUserSettings(false);
    }

    saveOptions(letterNotify) {
         if (letterNotify) {              
             this.settingsService.notificationSettings(true).subscribe((result) => {
                 this.notifyService.showNotify("Настройки сохранены"); 
             });                      
         } else {
             this.settingsService.notificationSettings(false).subscribe((result) => {
                 this.notifyService.showNotify("Настройки сохранены");
             });           
        }  
         this.closeSettings();
    }
}