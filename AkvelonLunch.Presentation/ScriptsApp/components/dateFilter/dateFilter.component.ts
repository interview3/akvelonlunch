﻿import {Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import {DatePicker} from '../datepicker/datepicker.component';
import {IDate} from "../datepicker/interfaces/index";

declare var require: any;

@Component({
    selector: 'date-filter',
    templateUrl: `./ScriptsApp/components/datefilter/datefilter.component.html`,
    styleUrls: [`./ScriptsApp/components/datefilter/datefilter.component.css`],
    directives: [DatePicker]
})
export class DateFilter {
    @Input() public period: DateFilterPeriod;
    private startFilterDate: Date;
    private finishFilterDate: Date;

    @Output()
    onPeriodChanged: EventEmitter<DateFilterPeriod> = new EventEmitter<DateFilterPeriod>();

    ngOnInit() {
        if (this.period != undefined) {
            this.startFilterDate = this.period.StartDate;
            this.finishFilterDate = this.period.FinishDate;
        } else {
            this.startFilterDate = this.finishFilterDate = new Date();
        }
    }

    startDateWasChanged(date: Date) {
        this.startFilterDate = date;
        this.onPeriodChanged.emit(new DateFilterPeriod(this.startFilterDate, this.finishFilterDate));
    }

    finishDateWasChanged(date: Date) {
        this.finishFilterDate = date;
        this.onPeriodChanged.emit(new DateFilterPeriod(this.startFilterDate, this.finishFilterDate));
    }

    toIDate(date: Date): IDate {
        if (date == undefined) date = new Date();
        var idate: IDate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
        return idate;
    }
}

export class DateFilterPeriod {
    public StartDate:Date;
    public FinishDate: Date;

    constructor(start: Date, finish: Date) {
        this.StartDate = start;
        this.FinishDate = finish;
    }

    public static Default(): DateFilterPeriod {
        var now = new Date();
        now.setHours(0);
        now.setMinutes(0);
        now.setSeconds(0);
        return new DateFilterPeriod(now, now);
    }
}