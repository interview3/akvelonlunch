﻿import {Component, Input} from '@angular/core';
import {LoginService} from '../../services/login.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Component({
    selector: 'login',
    templateUrl: `./ScriptsApp/components/login/login.html`,
    styleUrls: [`./ScriptsApp/components/login/login.css`]
})

export class LoginComponent {
    public message: string;
    public done: boolean = false;

    constructor(public router: Router, private loginService: LoginService) {       
    }

    public signin(login, password, remember) {
        this.loginService.signin(login, password, remember)
            .subscribe(
                result => this.loginSuccess(),
                error => this.loginFailed(error)
        );
    }

    private loginSuccess() {
        this.router.navigate(['./']);
    }

    private loginFailed(error) {
        this.done = true;
        this.message = 'Неверное имя пользователя или пароль';
        console.log(error);
    }
}
