﻿import {Component, Output, EventEmitter} from '@angular/core';
import {NotifyService} from '../../services/notify.service';
import {OrderStatuses} from '../../ModelClasses/OrderStatuses';
import {SettingsService} from '../../services/settings.service';
import {InformationService} from  '../../services/information.service';
import {OrderService} from '../../services/order.service';
import {DishService} from '../../services/dish.service';
import {OrderModel} from '../../ModelClasses/OrderModel';

@Component({
    selector: 'notify',
    templateUrl: `./ScriptsApp/components/notification/notification.component.html`,
    styleUrls: ['./ScriptsApp/components/notification/notification.component.css']
})
export class NotifyComponent {
    public timeBlock: number;
    public currentTime: number;
    public timer: number;
    public menuFlag: boolean;
    public notifyMessage: string;
    public orderStatus: number = 0;
    public messageError: string;
    public disableButton: boolean = false;
    public errorFlag: boolean;
    public orderId: boolean;
    public popupFlag: boolean = true;
    public order: OrderModel;
    private blockHour: Date;
    public dateString: any;
    private checkOrderTimer: any;

    @Output() pushEvent = new EventEmitter();

    constructor(private notifyService: NotifyService,
                private settingsService: SettingsService,
                private dishService: DishService,
                private orderService: OrderService) {
        notifyService.getPushNotify()
            .subscribe(
            (show) => {         
                this.showPushNotify();
            });    
        notifyService.getErrorMessage()
            .subscribe((message) => {
                this.showPopUpError(message);
            });
        this.blockHour = SettingsService.blockHourCached;
        this.checkTime(this.blockHour);
        settingsService.getblockHourChangedEmitter()
            .subscribe((blockHour) => {
                this.blockHour = blockHour; 
                this.orderService.loadTodayUserOrder(true)
                    .subscribe((userOrder) => {
                        if (userOrder == null) {
                            this.orderStatus = null;
                            this.verifyTime(userOrder);
                        } else {
                            this.orderStatus = userOrder.OrderStatusId;
                            this.orderId = userOrder.Id;
                            this.checkTime(this.blockHour);
                        }
                    });
            });
        dishService.loadMenu().subscribe(
            (result) => {
                if (result == null) {
                    this.menuFlag = false;
                } else {
                    this.menuFlag = true;
                }
            });
                
        this.order = new OrderModel();             
    }

    public checkTime(blockHour: Date) {
        let curDate = new Date();
        if (blockHour == null)
            return;
        if (curDate.getHours() > blockHour.getHours()
            || (curDate.getHours() == blockHour.getHours())
            && (curDate.getMinutes() > blockHour.getMinutes())) {
            this.verifyOrderStatus();                         
        }
    } 

    verifyOrderStatus() {
        this.orderService.loadTodayUserOrder()
            .subscribe((userOrder) => {
                if (userOrder == null) {
                    this.orderStatus = null;
                } else {
                    this.orderStatus = userOrder.OrderStatusId;
                }
            });  
        if (this.orderStatus == 3) {
            this.notifyService.changeMessagePushNotify("orderDelivered");            
        }
    }

    showPopUpError(message: string) {
        var errorMessage = document.getElementById("errorSnackbar");
        errorMessage.className = errorMessage.className.replace("show", "");
        console.log(message);
        switch (message) {
            case "Server error":
                this.messageError = "Ошибка сервера";
                break;
            case "No money":
                this.messageError = "Недостаточно средств на счёте";
                break;
            case "Internal Server Error":
                this.messageError = "Внутренняя ошибка сервера";
                break;
            case "Can't get balance":
                this.messageError = "Не удаётся получить баланс";
                break;
            case "Can't get menu":
                this.messageError = "Не удаётся получить меню";
                break;
            default:
                this.messageError = message;
                break;
        }              
        errorMessage.className = "show";
        setTimeout(test => { errorMessage.className = errorMessage.className.replace("show", ""); }, 4700);  
    }

    closePopUpError() {
        var errorMessage = document.getElementById("errorSnackbar");
        clearTimeout(this.timer);
        errorMessage.className = errorMessage.className.replace("show", "");
        this.notifyService.turnOnButton();
    }

    verifyTime(userOrder) {  // how much time is left until the block hour      
        let curDate = new Date();
        if (this.blockHour == null)
            return;
        this.timeBlock = this.blockHour.getHours() * 60 + this.blockHour.getMinutes();
        this.currentTime = curDate.getHours() * 60 + curDate.getMinutes() ;  
        if (userOrder == null) {
            if ((this.timeBlock - this.currentTime <= 30) && (this.timeBlock - this.currentTime > 0)) {
                this.notifyService.changeMessagePushNotify("notOrder");
            }
        }
    }

    showPushNotify() {
        let curDate = new Date();
        this.dateString = curDate.getDate();
        if ((this.orderId != null) && (localStorage.getItem("PushNotify") == "orderDelivered") && (localStorage.getItem("PushNotifyOrderDelivered") != this.dateString)) {
            this.pushEvent.emit('');
            localStorage.setItem("PushNotifyOrderDelivered", this.dateString);
        }
        if (this.menuFlag && (this.orderId == null) && (localStorage.getItem("PushNotify") == "notOrder") && (localStorage.getItem("PushNotifyNotOrder") != this.dateString)) {            
            this.pushEvent.emit('');
            localStorage.setItem("PushNotifyNotOrder", this.dateString);
        }
        if ((localStorage.getItem("PushNotify") == "balance") && (localStorage.getItem("PushNotifyBalance") != this.dateString)) {
            this.pushEvent.emit('');    
            localStorage.setItem("PushNotifyBalance", this.dateString);
        }
    }
}