﻿import { Component, Output, EventEmitter } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { HTTP_PROVIDERS } from '@angular/http';
import { Router, RouterLink, RouterOutlet, ROUTER_DIRECTIVES } from '@angular/router';
import { SettingsService } from '../../../services/settings.service';
import {NotifyService} from '../../../services/notify.service';
import {NotifyComponent} from '../..//notification/notification.component';

@Component({
    selector: 'admin-settings',
    templateUrl: `./ScriptsApp/components/admin/settings/settings.component.html`,
    styleUrls: ['./ScriptsApp/components/admin/settings/settings.component.css'],
})

export class AdminSettingsComponent {
    public blockHour: Date = null;
    public blockHours: number;
    public blockMinutes: number;
    constructor(private settingsService: SettingsService,
                private notifyService: NotifyService) {

        this.blockHour = SettingsService.blockHourCached;
        if (this.blockHour != null) {
            
            this.onUpdateInputs(this.blockHour);
        }
        this.settingsService.getblockHourChangedEmitter()
            .subscribe((blockHour) => this.onUpdateInputs(blockHour));
        
    }
    private onUpdateInputs(blockHour: Date) {
        this.blockHours = blockHour.getHours();
        this.blockMinutes = blockHour.getMinutes();
        
    }
    public saveSettings() {
        this.settingsService.changeBlockHour(this.blockHours, this.blockMinutes).subscribe(() => {
            this.settingsService.loadBlockHour();
            this.notifyService.showNotify("Сохранено");
            }
        );

    }
}
