﻿import { Component, EventEmitter, Input, Output } from "@angular/core"
import { Control, ControlGroup, FormBuilder, Validators  } from "@angular/common";
import { UserModel } from "../../../../ModelClasses/UserModel";
import { UserRole } from "../../../../ModelClasses/UserRole";
import { UserService } from "../../../../services/user.service";
import {InformationService} from '../../../../services/information.service';
import {NotifyService} from '../../../../services/notify.service';

@Component({
    selector: "user-form",
    templateUrl: `./ScriptsApp/components/admin/users/userForm/userForm.component.html`,
    styleUrls: [`./ScriptsApp/components/admin/users/userForm/userForm.component.css`,
                `./ScriptsApp/components/admin/confirmWindow.css`],
    providers: [FormBuilder],
    inputs: ['User'],
})
export class UserForm {
    @Input()
    public User: UserModel;
    public UserFormIsVisible: boolean;
    private _modalHeader: string = "Редактирование пользователя";

    private _userRoles: UserRole[];
    public _selectedUserRole: string;
    private _isDeleting:boolean=false;

    private _usersForm: ControlGroup;
    private FirstNameControl:Control;
    private LastNameControl:Control;
    private LoginControl:Control;
    private EmailControl:Control;
    private BalanceControl:Control;


    @Output() onFormClosed: EventEmitter<UserModel>;
    @Output() onUserDeleted: EventEmitter<number>;

    ngOnInit() {
        this._selectedUserRole = this.User.Role.Name;

        this.FirstNameControl = new Control(this.User.FirstName, Validators.required);
        this.LastNameControl = new Control(this.User.LastName, Validators.required);
        this.LoginControl = new Control(this.User.Login, Validators.required);
        this.EmailControl = new Control(this.User.Email,
            Validators.compose([
                Validators.required,
                Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-z]{2,4}$")
            ]));
        this.BalanceControl = new Control(this.User.Balance, Validators.required);

        this._usersForm = this._formBuilder.group({
            FirstName: this.FirstNameControl,
            LastName: this.LastNameControl,
            Login: this.LoginControl,
            Email: this.EmailControl,
            Balance: this.BalanceControl
        });
    }

    constructor(private informationService: InformationService,
        private userService: UserService,
        private _formBuilder: FormBuilder,
        private notifyService: NotifyService) {
        this._userRoles = UserRole.getAllRoles();
        this.onFormClosed = new EventEmitter<UserModel>(false);
        this.onUserDeleted = new EventEmitter<number>();
    }

    public updateUser(firstName: string, lastName: string, login: string, email: string, balance: number) {
        this.User.FirstName = firstName;
        this.User.LastName = lastName;
        this.User.Login = login;
        this.User.Email = email;
        this.User.Balance = balance;
        this.User.RoleId = UserRole.getRoleIdByRoleString(this._selectedUserRole);
        this.User.RoleString = new UserRole(this.User.RoleId).Name;

        this.userService.updateUser(this.User)
            .subscribe(
                upd => this.updateUserSuccess(),
                error => this.updateUserFailed(error)
            );
    }

    updateUserSuccess() {
        this.userService.getuser()
            .subscribe(
                userModel => {
                    if (userModel.Id == this.User.Id) {
                        this.informationService.currentUserBalanceUpdated(this.User.Balance);                       
                    }
                    this.notifyService.showNotify("Изменения сохранены");
                }
            );
        this.UserFormIsVisible = false;
        this.onFormClosed.emit(this.User);
    }

    updateUserFailed(error) {
        console.log(error);
    }

    closeForm() {
        this.User.RoleId = this.User.Role.Id;
        this.UserFormIsVisible = false;
        this.onFormClosed.emit(null);
    }

    deleteUser() {
        this.notifyService.showNotify("Пользователь удалён");
        this.UserFormIsVisible = false;
        this.onUserDeleted.emit(this.User.Id);
        this._isDeleting = false;
    }

    getRoleString(roleId: number):string {
        return new UserRole(roleId).Name;
    }

    public TryDelete() {
        this._isDeleting = true;
    }
    public popUpCloseWindow() {
        this._isDeleting = false;
    }
}