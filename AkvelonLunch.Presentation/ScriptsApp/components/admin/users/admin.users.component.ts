﻿import {Component, Output, Input} from '@angular/core';
import { UserModel } from '../../../ModelClasses/UserModel';
import {UserService} from '../../../services/user.service';
import { UsersGrid } from './usersGrid/usersGrid.component';
import { UserForm } from './userForm/userForm.component';

@Component({
    selector: 'admin-users',
    templateUrl: `./ScriptsApp/components/admin/users/admin.users.component.html`,
    //styleUrls: [`./ScriptsApp/components/admin/users/admin.users.component.css`],
    directives: [UsersGrid, UserForm]
})
export class AdminUsersComponent {
    public UsersList: UserModel[];

    constructor(private userService: UserService) {
        this.userService.getallusers()
            .subscribe(
                (us: UserModel[]) => this.getUsersSuccess(us),
                error => this.getUsersFailed(error)
            );

        this.userService.getUserListChangeEmmiterr()
            .subscribe(
                user => this.onUserListUpdate(user)
            );
    }

    private getUsersSuccess(us: UserModel[]) {
        this.UsersList = new Array<UserModel>();
        for (var i = 0; i < us.length; i++) {
            this.UsersList.push(us[i]);
        }
        this.userService.emitUpdateGrid(this.UsersList);
    }

    private getUsersFailed(error) {
        console.log(error);
    }

    private onUserListUpdate(user: UserModel) {
        for (let i = 0; i < this.UsersList.length; i++)
            if (this.UsersList[i].Id === user.Id) {
                this.UsersList[i] = user;
                return;
            }
        this.UsersList.push(user);
        this.userService.emitUpdateGrid(this.UsersList);
    }

    formWasClosed(user: UserModel) {
        if (user != null) {
            this.userService.getUserListChangeEmmiterr()
                .subscribe(
                    user => this.onUserListUpdate(user)
                );

            this.userService.emitUpdateGrid(this.UsersList);
        }
    }

    userWasDeleted(userId: number) {
        this.userService.deleteUser(userId)
            .subscribe(
                res => {
                    var j = this.UsersList.findIndex(us => us.Id == userId);
                    this.UsersList.splice(j, 1);
                    this.userService.emitUpdateGrid(this.UsersList);
                },
                error => console.log(error)
            );
    }
}