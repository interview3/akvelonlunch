﻿import { Component, EventEmitter, Output } from "@angular/core";
import { UserService } from '../../../../services/user.service';
import { GridConfig, ColumnState, Column, Grid } from '../../../../utils/GridHelpers';
import { BaseGrid } from "../../../../BaseGrid/BaseGrid";
import { UserModel } from "../../../../ModelClasses/UserModel";
import { UserRole } from "../../../../ModelClasses/UserRole";
import { UserForm} from "../userForm/userForm.component";

@Component({
    selector: 'users-grid',
    templateUrl: `./ScriptsApp/components/admin/users/usersGrid/usersGrid.component.html`,
    styleUrls: ['./ScriptsApp/BaseGrid/BaseGrid.css'],
    directives: [UserForm],
})
export class UsersGrid extends BaseGrid {
    public IsSelected: boolean = false;
    public SelectedUser: UserModel;
    
    @Output() onFormClosed:EventEmitter<UserModel>;
    @Output() onUserDeleted:EventEmitter<number>;

    constructor(private userService: UserService) {
        super(
            new Array<Column>(
                new Column("Логин", "Login"),
                new Column("Имя пользователя", "FullName"),
                new Column("Адрес электронной почты", "Email"),
                new Column("Баланс", "Balance"),
                new Column("Роль", "RoleString")
            ),
            new Column("FullName", "FullName"),
            new Array<number>(5, 10, 25, 50),
            25);

        this.userService.getGridChangeEmitter()
            .subscribe(usersList => {
                this.setUsersGrid(usersList);
            });

        this.onFormClosed = new EventEmitter<UserModel>(false);
        this.onUserDeleted = new EventEmitter<number>();
    }

    protected onClickRowItem(row: any) {
        let user = new UserModel(row);
        this.IsSelected = true;
        this.SelectedUser = user;
        this.userService.emitUpdateGridRow(user);
    }

    private setUsersGrid(usersList: UserModel[]) {
        this.changeGrid(this.convertArrayUsersToArrayAny(usersList));
    }


    private convertArrayUsersToArrayAny(usersList: UserModel[]) {
        let anyList = new Array<any>();
        usersList.forEach(u => anyList.push(this.convertUserToAny(u)));
        return anyList;
    }

    private convertUserToAny(userItem: UserModel): any {
        var role: UserRole = new UserRole(userItem.RoleId);
        return {
            Id: userItem.Id,
            Login: userItem.Login,
            FullName: userItem.FirstName + " " + userItem.LastName,
            FirstName: userItem.FirstName,
            LastName:userItem.LastName,
            Email: userItem.Email,
            Balance: userItem.Balance,
            Role: role,
            RoleString: role.Name,
        };
    }

    formWasClosed(user: UserModel) {
        this.IsSelected = false;
        if (user != null) {
            this.onFormClosed.emit(user);
        }
    }

    userWasDeleted(userId: number) {
        this.IsSelected = false;
        this.SelectedUser = null;
        this.onUserDeleted.emit(userId);
    }
}