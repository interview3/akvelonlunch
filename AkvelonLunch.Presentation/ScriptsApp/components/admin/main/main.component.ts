﻿import { Component, Output, EventEmitter } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { HTTP_PROVIDERS } from '@angular/http';
import { Router, RouterLink, RouterOutlet, ROUTER_DIRECTIVES } from '@angular/router';
import { InfoPanel } from '../../mainScreen/infoPanel/infoPanel.component';
import {OrderService} from '../../../services/order.service';
import { AdminMenuComponent } from '../menu/admin.menu.component';
import { AdminOrdersComponent } from '../orders/admin.orders.component';
import { AdminUsersComponent } from '../users/admin.users.component';
import { AdminTransactionsComponent } from '../transactionLog/admin.transactionLog.component';
import { ProvisionersComponent } from "../provisioners/provisioners.component";
import { AdminSettingsComponent } from "../settings/settings.component";
import {PushNotificationComponent} from '../../../utils/NotificationsHelper';
import {NotifyComponent} from '../../notification/notification.component';
import { SettingsService } from '../../../services/settings.service';
import { ProvisionerService } from '../../../services/provisioner.service';

@Component({
    selector: 'admin-main',
    templateUrl: `./ScriptsApp/components/admin/main/main.component.html`,
    styleUrls: ['./ScriptsApp/components/admin/main/admin.main.component.css',
        './ScriptsApp/components/mainScreen/mainScreen.css'],
    directives: [ProvisionersComponent, AdminMenuComponent, AdminOrdersComponent,
        AdminSettingsComponent, AdminUsersComponent, AdminTransactionsComponent, InfoPanel, RouterLink,
        NotifyComponent, RouterOutlet, ROUTER_DIRECTIVES, PushNotificationComponent],
    providers: [HTTP_PROVIDERS, SettingsService, ProvisionerService]
})

export class AdminMainScreenComponent {
    private provisionersIsVisible: boolean;
    private menuIsVisible: boolean;
    private orderIsVisible: boolean;
    private settingsAreVisible: boolean;
    private usersAreVisible:boolean;
    private moneyLogIsVisible:boolean;

    private visibleState: any = { 
        menuIsVisible: true,
        orderIsVisible: false,
        provisionersIsVisible: false,
        settingsAreVisible: false,
        usersAreVisible: false,
        moneyLogIsVisible: false
    }

    private resetVisibleState() {
        for (let p in this.visibleState) {
            this.visibleState[p] = false;
        }
    }

    //TODO: Must be changed to simple & single var
    constructor() {
    }

    private showProvisioners() {
        this.resetVisibleState();
        this.visibleState.provisionersIsVisible = true;


    }

    private showMenu() {
        this.resetVisibleState();
        this.visibleState.menuIsVisible = true;
    }

    private showOrder() {
        this.resetVisibleState();
        this.visibleState.orderIsVisible = true;
    }

    private showSettings() {
        this.resetVisibleState();
        this.visibleState.settingsAreVisible = true;
    }

    private showUsers() {
        this.resetVisibleState();
        this.visibleState.usersAreVisible = true;
    }

    public showMoneyLog() {
        this.resetVisibleState();
        this.visibleState.moneyLogIsVisible = true;
    }
	
	public notification: any = {
        show: false,
        title: 'Akvelon Lunch',
        body: 'Уведомления отправлены',
        icon: './ScriptsApp/components/mainScreen/icon/iconPushNotify.png'
    }
}