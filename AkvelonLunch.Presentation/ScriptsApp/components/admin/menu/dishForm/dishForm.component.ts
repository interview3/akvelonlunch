﻿import { Component } from "@angular/core";
import { Control, ControlGroup, FormBuilder, Validators  } from "@angular/common";
import { AdminService } from '../../../../services/admin.service';
import { ProvisionerService } from '../../../../services/provisioner.service';
import { DishModel } from "../../../../ModelClasses/DishModel";
import { DishTypeModel } from "../../../../ModelClasses/DishTypeModel";
import { ProvisionerModel } from '../../../../ModelClasses/ProvisionerModel';

@Component({
    selector: 'dish-form',
    templateUrl: `./ScriptsApp/components/admin/menu/dishForm/dishForm.component.html`,
    styleUrls: [`./ScriptsApp/components/admin/menu/dishForm/dishForm.component.css`,
                 './ScriptsApp/components/mainScreen/mainScreen.css'],
    providers: [ProvisionerService, FormBuilder]
})
export class DishForm {

    private _dish: DishModel;
    private _dishFormIsVisible: boolean;
    private _editedFromGrid: boolean;
    private _modalHeader: string;
    private _dishTypes: DishTypeModel[];
    private _selectedDishType: DishTypeModel;
    private _provisionersList: ProvisionerModel[];
    private _selectProvisioner: ProvisionerModel;
    private _dishForm: ControlGroup;

    private NameControl: Control; 
    private DescriptionControl: Control; 
    private CostControl: Control; 
    
    constructor(private _adminService: AdminService,
        private _provisionerService: ProvisionerService,
        private _formBuilder: FormBuilder) {
        this._dish = null;
        this._dishFormIsVisible = false;
        this._adminService.getDishFromDisplayEmitter()
            .subscribe(dish => this.showForm(dish));
        this._adminService.getDishTypes()
            .subscribe(typeList => this._dishTypes = typeList);
        this._provisionerService.loadProvisioners()
            .subscribe(provisionerList => this._provisionersList = provisionerList);

        this.NameControl = new Control('', Validators.required);
        this.DescriptionControl = new Control('', Validators.maxLength(1000));
        this.CostControl = new Control('',
            Validators.compose(
            [
                Validators.required,
                Validators.pattern("(^[1-9][0-9]*(\.[0-9]{0,2})?$)|(^0[\.\,][0-9]{0,2}$)")
            ]));

        this._dishForm = this._formBuilder.group({
            Name: this.NameControl,
            Description: this.DescriptionControl,
            Cost: this.CostControl
        });
    }

    private onLoadDishTypeList(dishTypeList: DishTypeModel[]) {
        this._dishTypes = dishTypeList;
    }

    private updateControlsForm(dish: DishModel) {
        if (dish) {
            this.NameControl.updateValue(this._dish.Name);
            this.DescriptionControl.updateValue(this._dish.Description);
            this.CostControl.updateValue(this._dish.Cost);
        }
        else {
            this.NameControl.updateValue("");
            this.DescriptionControl.updateValue("");
            this.CostControl.updateValue("");
        }
    }

    private showForm(dish: any) {
        this._editedFromGrid = (dish != null) ? dish.editedFromGrid : false;
        this._modalHeader = dish ? "Редактирование блюда" : "Создание блюда";
        this._dish = dish;
        if (dish) {
            this._dishTypes.forEach( (type) => {
                if (type.Id === this._dish.DishType.Id) {
                    this._selectedDishType = type;
                }
            });
            this._provisionersList.forEach( (provisioner) => {
                if (provisioner.Id === this._dish.Provisioner.Id) {
                    this._selectProvisioner = provisioner;
                }
            });
        }
        else if (this._dishTypes) {
            this._selectedDishType = this._dishTypes[0];
            this._selectProvisioner = this._provisionersList[0];
        }
        this.updateControlsForm(this._dish);
        this._dishFormIsVisible = true;
    }

    public onSelectDishType(dishType: DishTypeModel) {
        this._selectedDishType = dishType;
    }

    public AddDish(Name: string, Description: string, Cost: string) {
        if (this._dishForm.valid) {
            let newDish = new DishModel({
                Name: Name,
                Description: Description,
                Cost: +Cost,
                DishType: this._selectedDishType,
                Provisioner: this._selectProvisioner
            });

            this._adminService.createDish(newDish)
                .subscribe(
                    res => {
                        console.log(res);
                        newDish.Id = res.OperationStatus.Entity;
                        this._adminService.emitUpdateDishList(newDish);
                    },
                    error => console.log(error)
                );
        }
        this._dishFormIsVisible = false;
    }

    public UpdateDish(Id: number, Name: string, Description: string, Cost: string) {
        if (this._dishForm.valid) {
            let newDish = new DishModel({
                Id: Id,
                Name: Name,
                Description: Description,
                Cost: +Cost,
                DishType: this._selectedDishType,
                Provisioner: this._selectProvisioner
            });
            this._adminService.updateDish(newDish)
                .subscribe(
                    res => {
                        if (this._editedFromGrid) {
                            this._adminService.emitUpdateDishListFromGrid(newDish);
                        } else {
                            this._adminService.emitUpdateDishList(newDish);
                        };
                    }
                );
        }
        this._dishFormIsVisible = false;
    }
    
}