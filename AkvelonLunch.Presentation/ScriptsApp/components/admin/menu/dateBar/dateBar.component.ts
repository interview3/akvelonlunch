﻿import { Component } from '@angular/core';
import { AdminService } from '../../../../services/admin.service';

@Component({
    selector: 'date-bar',
    templateUrl: `./ScriptsApp/components/admin/menu/dateBar/dateBar.component.html`,
    styleUrls: ['./ScriptsApp/components/admin/menu/dateBar/dateBar.component.css']
})
export class DateBar {
 
    private currentDate: Date;
    private currnetMonday: Date;
    private weekLength: number = 7;
    private dayLength: number = 24 * 60 * 60 * 1000;
    private weekdaysNameList: string[];
    private dateNow: Date = new Date();

    constructor(private _adminService: AdminService) {
        this.currnetMonday = this.getCurrnetMonday(new Date());
        this.setCurrnetDate(new Date());
        this.weekdaysNameList = new Array<string>("Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс");
    }

    private getDateByShift(date: Date, numbedOfDays: number): Date {
        return new Date(date.getTime() + numbedOfDays * this.dayLength);
    }

    private getCurrnetMonday(date: Date) : Date {
        let day = date.getDay();
        let shiftDate: number;
        if (day === 0) {
            shiftDate = -6;
        }
        else {
            shiftDate = 1 - day;
        }
        return this.getDateByShift(date, shiftDate);
    }

    private setCurrnetDate(date: Date) {
        this.currentDate = date;
        this._adminService.emitDateChange(this.currentDate);
    }

    private getWeekList() : Date[] {
        let dateList = new Array<Date>(this.weekLength);
        let numDate: number;
        for (let i = 0; i < dateList.length; i++) {
            let date = new Date(this.currnetMonday.getTime());
            date.setDate(date.getDate() + i);
            dateList[i] = date;
        }
        
        return dateList;
    }

    private onDateBarClick(date: Date) {
        this.setCurrnetDate(date);
    }

    private onNextWeekClick() {
        this.currnetMonday = this.getDateByShift(this.currnetMonday, this.weekLength);
        this.setCurrnetDate(this.currnetMonday);
    }

    private onLastWeekClick() {
        this.currnetMonday = this.getDateByShift(this.currnetMonday, -this.weekLength);
        this.setCurrnetDate(this.currnetMonday);
    }

    private isToday(dateItem: Date):boolean {
        var now = new Date();
        return (dateItem.getDate() == now.getDate() &&
            dateItem.getMonth() == now.getMonth() &&
            dateItem.getFullYear() == now.getFullYear());
    }

}