﻿import { Component } from "@angular/core"
import { AdminService } from '../../../../services/admin.service';
import { DishModel } from "../../../../ModelClasses/DishModel";
import { DishTypeModel } from "../../../../ModelClasses/DishTypeModel";
import { GridConfig, ColumnState, Column, Grid } from '../../../../utils/GridHelpers';
import { BaseGrid } from "../../../../BaseGrid/BaseGrid"

@Component({
    selector: 'dish-grid',
    templateUrl: './ScriptsApp/BaseGrid/BaseGrid.html',
    styleUrls: ['./ScriptsApp/components/admin/menu/dishGrid/dishGrid.component.css']
})
export class DishGrid extends BaseGrid {
    
    constructor(private _adminService: AdminService) {

        super(
            new Array<Column>(
                new Column("Название", "Name"),
                new Column("Описание", "Description"),
                new Column("Цена", "Cost"),
                new Column("Тип", "DishType"),
                new Column("Поставщик", "SupplierName")),
            new Column("Название", "Name"),
            new Array<number>(5, 10, 25, 50),
            25,
            true);
        this._adminService.getGridChangeEmitter()
            .subscribe(dishList => this.setDishGreed(dishList));
    }

    protected onClickRowItem(row: any) {
        let dish = new DishModel({
            Id: row.Id,
            Name: row.Name,
            Description: row.Description,
            Cost: +row.Cost,
            DishType: new DishTypeModel(row.DishTypeId, row.DishType),
            ProvisionerId: row.Provisioner.Id,
            Provisioner: row.Provisioner
        });
        this._adminService.emitUpdateDishList(dish);
    }

    private setDishGreed(dishList: DishModel[]) {
        this._adminService.getFullDishList()
            .subscribe(
            (list: DishModel[]) => {
                this.changeGridWithExclude(this.convertArrayDishToArrayAny(list),
                    this.convertArrayDishToArrayAny(dishList),
                    (a, b) => a.Id === b.Id);
            },
            error => console.log(error)
            );
    }

    private convertDishToAny(dish: DishModel): any {
        if (!dish.DishType)
            console.log(dish);
        return {
            Id: dish.Id,
            Name: dish.Name,
            Description: dish.Description,
            Cost: dish.Cost,
            DishType: dish.DishType.Name,
            DishTypeId: dish.DishType.Id,
            SupplierName: dish.Provisioner.SupplierName,
            Provisioner: dish.Provisioner
        }
    }

    private convertArrayDishToArrayAny(dishList: DishModel[]) {
        let anyList = new Array<any>();
        dishList.forEach(d => anyList.push(this.convertDishToAny(d)));
        return anyList;
    }
}