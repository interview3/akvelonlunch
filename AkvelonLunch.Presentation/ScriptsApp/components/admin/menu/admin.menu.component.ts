﻿import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/admin.service';
import { DishService } from '../../../services/dish.service';
import { DishModel } from "../../../ModelClasses/DishModel";
import { DishTypeModel } from "../../../ModelClasses/DishTypeModel";
import { MenuModel } from "../../../ModelClasses/MenuModel";
import { EnumHelpers } from '../../../utils/EnumHelpers';
import { DishForm } from './dishForm/dishForm.component';
import { DishGrid } from './dishGrid/dishGrid.component';
import { DateBar } from './dateBar/dateBar.component';
import {NotifyService} from '../../../services/notify.service';

@Component({
    selector: 'admin-menu',
    templateUrl: `./ScriptsApp/components/admin/menu/admin.menu.component.html`,
    styleUrls: ['./ScriptsApp/components/admin/menu/admin.menu.component.css', './ScriptsApp/components/mainScreen/mainScreen.css',
                './ScriptsApp/components/admin/confirmWindow.css'],
    directives: [DishForm, DishGrid, DateBar],
    providers: [AdminService, DishService]
})

export class AdminMenuComponent implements OnInit {

    private _existMenu: MenuModel;
    private _dishList: DishModel[];
    private _IsMenuChange: boolean;
    private _dishTypeList: string[];
    private _currentDate: Date;
    private _isDeleting:boolean=false;

    constructor(private _adminService: AdminService,
        private dishService: DishService,
        private notifyService: NotifyService) {
        this._dishList = new Array<DishModel>();
        this._IsMenuChange = false;
        this._adminService.getDishListChangeEmitter()
            .subscribe(
                dish => this.onDishListUpdate(dish)
        );
        this._adminService.getDishListChangeFromGridEmitter()
            .subscribe(
                dish => this.onDishListUpdateFromGrid(dish)
        );

        this._adminService.getDateBarChangeEmitter()
            .subscribe(
                date => this.onDateChange(date)
        );
        this._dishTypeList = [];
        dishService.loadDishTypeList()
            .subscribe((dishTypesList) => {
                for (var i = 0; i < dishTypesList.length; ++i)
                    this._dishTypeList.push(dishTypesList[i].Name);
            });

    }

    ngOnInit() {
        this.loadMenu();
    }

    private loadMenu() {
        this._adminService.loadMenuByDate(this._currentDate)
            .subscribe(
                (menu: MenuModel) => this.loadMenuSuccess(menu),
                error => this.loadMenuFailed(error)
            );
    }

    private onDateChange(date: Date) {
        this._currentDate = date;
        this.loadMenu();
    }

    private loadMenuSuccess(menu: MenuModel) {
        this._existMenu = menu;
        this._dishList = this._existMenu ? this._existMenu.Dishes : new Array<DishModel>();
        this._adminService.emitUpdateGrid(this._dishList);
    }

    private loadMenuFailed(error) {
        console.log(error);
    }

    private onDishListUpdateFromGrid(dish: DishModel) {
        this._IsMenuChange = true;
        for (let i = 0; i < this._dishList.length; i++)
            if (this._dishList[i].Id === dish.Id) {
                this._dishList[i] = dish;
                return;
            }
        this._adminService.emitUpdateGrid(this._dishList);
    }

    private onDishListUpdate(dish: DishModel) {
        this._IsMenuChange = true;
        for (let i = 0; i < this._dishList.length; i++)
            if (this._dishList[i].Id === dish.Id) {
                this._dishList[i] = dish;
                return;
            }
        this._dishList.push(dish);
        this._adminService.emitUpdateGrid(this._dishList);
    }

    public getDishListOfType(type: string) {
        let dishList = new Array<DishModel>();
        this._dishList.forEach( dish => {
            if (dish.DishType.Name === type)
                dishList.push(dish);
        });
        return dishList;
    }
    
    public CreateNewDish() {
        this._adminService.emitShowDishForm(null);
    }

    public EditDish(dish: DishModel) {
        this._adminService.emitShowDishForm(dish);
    }
    
    public DeleteDishFromList(dish: DishModel) {
        for (let i = 0; i < this._dishList.length; i++) {
            if (this._dishList[i].Id === dish.Id) {
                this._dishList.splice(i, 1);
            }
        }
        this._IsMenuChange = true;
        this._adminService.emitUpdateGrid(this._dishList);
    }
    
    public CreateMenu() {
        if (this._dishList.length != 0) {
            this._adminService.createMenu(this._dishList, 1, this._currentDate)
                .subscribe(
                res => {                      
                        console.log(res);
                        this._dishList = new Array<DishModel>();
                        this.loadMenu();
                        this.notifyService.showNotify("Меню создано");
                    },
                error => {
                    this.notifyService.showNotify("Не удалось создать меню");
                    console.log(error)
                });
        }

        this._IsMenuChange = false;
    }

    public UpdateMenu() {
        this._existMenu.Dishes = this._dishList;
        this._adminService.updateMenu(this._existMenu)
            .subscribe(
                res => {
                    console.log(res);
                    this.notifyService.showNotify("Меню обновлено");
                },
                error => {
                    console.log(error);
                    this.notifyService.showNotify("Не удалось обновить меню");
                }
            );
        this._IsMenuChange = false;
    }

    public CreateOrUpdateMenu() {
        if (!this._existMenu) {
            this.CreateMenu();
        } else {
            this.UpdateMenu();
        }
    }
    public DeleteMenu() {
        if (this._existMenu) {
            this._adminService.deleteMenu(this._existMenu.Id)
                .subscribe(
                    res => {
                        console.log(res);
                        this._existMenu = null;
                        this.notifyService.showNotify("Меню удалено");
                        this._dishList = new Array<DishModel>();
                        this._adminService.emitUpdateGrid(this._dishList);
                        this._isDeleting = false;
                    },
                    error => {
                        console.log(error);
                        this.notifyService.showNotify("Это меню нельзя удалить, потому что по нему уже сделаны заказы");
                        this._isDeleting = false;
                    }
                );
        }
    }

    public dishesOfType(DishTypeId: number): DishModel[] {
        let curDishesArray: DishModel[] = [];
       
        for (let i = 0; i < this._dishList.length; ++i) {
            if (this._dishList[i].DishType.Name == this._dishTypeList[DishTypeId]) {
                curDishesArray.push(this._dishList[i]);
            }
        }
        return curDishesArray;
    }

    public editWasClicked(row: any) {
        row.editedFromGrid = true;
        row.DishType = new DishTypeModel(row.DishTypeId, row.DishType.toString());
        this._adminService.emitShowDishForm(row);
        this._adminService.emitUpdateGrid(this._dishList);
    }

    TryDeleteMenu() {
        this._isDeleting = true;
    }

    popUpCloseWindow() {
        this._isDeleting = false;
    }
}

