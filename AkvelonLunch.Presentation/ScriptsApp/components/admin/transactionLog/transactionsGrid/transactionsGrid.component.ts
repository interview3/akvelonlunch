﻿import { Component, EventEmitter, Output } from "@angular/core";
import { UserService } from '../../../../services/user.service';
import { GridConfig, ColumnState, Column, Grid } from '../../../../utils/GridHelpers';
import { BaseGrid } from "../../../../BaseGrid/BaseGrid";
import {MoneyTransactionModel} from "../../../../ModelClasses/MoneyTransactionModel"
import {TransactionOperationModel} from "../../../../ModelClasses/TransactionOperationModel"
import {MoneyTransactionService} from "../../../../services/moneytransactions.service"

@Component({
    selector: 'transactions-grid',
    templateUrl: `./ScriptsApp/BaseGrid/BaseGrid.html`,
    styleUrls: [ './ScriptsApp/BaseGrid/BaseGrid.css',
                 './ScriptsApp/components/admin/transactionLog/transactionsGrid/transactionsGrid.component.css'],
})

export class TransactionsGrid extends BaseGrid {
    @Output()
    public onRowClicked: EventEmitter<TransactionOperationModel> = new EventEmitter<TransactionOperationModel>();

    constructor(private transactionService: MoneyTransactionService) {
        super(
            new Array<Column>(
                new Column("№", "TransactionId"),
                new Column("Затронутый пользователь", "AffectedUser"),
                new Column("Пользователь-инициатор", "InitiatorUser"),
                new Column("Операция", "Operation"),
                new Column("Баланс до операции", "BalanceBefore"),
                new Column("Баланс после операции", "BalanceAfter"),
                new Column("Комментарий", "Comment"),
                new Column("Дата и время", "TransactionTime")
            ),
            new Column("AffectedUser", "AffectedUser"),
            new Array<number>(10, 20, 25, 50),
            25);

        this.transactionService.getGridChangeEmitter()
            .subscribe(transactionsList => {
                this.setTransactionsGrid(transactionsList);
            });

        this.onColumnClick(new Column("№", "TransactionId"));
        this.onColumnClick(new Column("№", "TransactionId"));
    }

    protected onClickRowItem(row: any) {
        if (row.OrderId != undefined && row.OrderId != null)
            this.onRowClicked.emit(row as TransactionOperationModel);
    }

    private setTransactionsGrid(transactionsList: MoneyTransactionModel[]) {
        this.changeGrid(this.convertArrayTransactionsToArrayAny(transactionsList));
    }


    private convertArrayTransactionsToArrayAny(transactionsList: MoneyTransactionModel[]) {
        let anyList = new Array<any>();
        transactionsList.forEach(tr => anyList.push(this.convertTransactionToAny(tr)));
        return anyList;
    }

    private convertTransactionToAny(transaction: MoneyTransactionModel): any {
        var transactionAny: any = {
            TransactionId: +transaction.TransactionId,
            AffectedUser: transaction.AffectedUser.FirstName + " " + transaction.AffectedUser.LastName,
            InitiatorUser: transaction.InitiatorUser.FirstName + " " + transaction.InitiatorUser.LastName,
            Operation: new TransactionOperationModel(transaction.Operation.Id, transaction.Operation.Name).Name,
            BalanceBefore: transaction.BalanceBefore,
            BalanceAfter: transaction.BalanceAfter,
            Comment: transaction.Comment,
            TransactionTime: this.convertDateToAccurateString(transaction.TransactionTime),
            OrderId: transaction.OrderId
        }
        return transactionAny;
    }

    private convertDateToAccurateString(date: any) {
        var year = date.substr(0, 4);
        var month = date.substr(5, 2);
        var day = date.substr(8, 2);

        var hours = date.substr(11, 2);
        var minutes = date.substr(14, 2);
        var seconds = date.substr(17, 2);
        var str = year + "/" + month + "/" + day + " " +
            hours + ":" + minutes + ":" + seconds;
        return str;
    }
}