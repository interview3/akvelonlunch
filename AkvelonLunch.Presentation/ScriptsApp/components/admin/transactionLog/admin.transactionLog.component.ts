﻿import {Component, Output, Input} from '@angular/core';
import { MoneyTransactionModel } from '../../../ModelClasses/MoneyTransactionModel';
import { OrderModel } from '../../../ModelClasses/OrderModel';
import {MoneyTransactionService} from '../../../services/moneytransactions.service';
import {SettingsService} from '../../../services/settings.service';
import {OrderService} from '../../../services/order.service';
import {ProvisionerService} from '../../../services/provisioner.service';
import { TransactionsGrid } from './transactionsGrid/transactionsGrid.component';
import { DateFilter, DateFilterPeriod } from '../../dateFilter/dateFilter.component';
import { CartComponent} from '../../mainScreen/cart/cart.component';

@Component({
    selector: 'admin-money-log',
    templateUrl: `./ScriptsApp/components/admin/transactionLog/admin.transactionLog.component.html`,
    styleUrls: [`./ScriptsApp/components/admin/transactionLog/admin.transactionLog.component.css`],
    providers: [MoneyTransactionService, SettingsService, ProvisionerService],
    directives: [TransactionsGrid, DateFilter, CartComponent]
})
export class AdminTransactionsComponent {
    public TransactionsList: MoneyTransactionModel[];
    private filterPeriod: DateFilterPeriod = DateFilterPeriod.Default();
    private SelectedOrder: OrderModel;
    private SelectedUserName: string;
    private IsSelected:boolean;

    constructor(private transactionService: MoneyTransactionService,
        private orderService: OrderService) {
        this.transactionService.getTransactions(this.filterPeriod.StartDate, this.filterPeriod.FinishDate)
            .subscribe(
                (transactions: MoneyTransactionModel[]) => this.getTransactionsSuccess(transactions),
                error => this.getTransactionsFailed(error)
            );

        this.transactionService.getTransactionsListChangeEmitter()
            .subscribe(
                transaction => this.onTransactionsListUpdate(transaction),
                err => console.log(err)
            );
    }

    private getTransactionsSuccess(transactions: MoneyTransactionModel[]) {
        this.TransactionsList = new Array<MoneyTransactionModel>();
        for (var i = 0; i < transactions.length; i++) {
            this.TransactionsList.push(transactions[i]);
        }
        this.transactionService.emitUpdateGrid(this.TransactionsList);
    }

    private getTransactionsFailed(error) {
        console.log(error);
    }

    private onTransactionsListUpdate(transaction: MoneyTransactionModel) {
        for (let i = 0; i < this.TransactionsList.length; i++)
            if (this.TransactionsList[i].TransactionId === transaction.TransactionId) {
                this.TransactionsList[i] = transaction;
                return;
            }
        this.TransactionsList.push(transaction);
        this.transactionService.emitUpdateGrid(this.TransactionsList);
    }

    periodWasChanged(period: DateFilterPeriod) {
        this.filterPeriod = period;

        this.transactionService.getTransactions(this.filterPeriod.StartDate, this.filterPeriod.FinishDate)
            .subscribe(
                (transactions: MoneyTransactionModel[]) => this.getTransactionsSuccess(transactions),
                error => this.getTransactionsFailed(error)
            );

        this.transactionService.getTransactionsListChangeEmitter()
            .subscribe(
                transaction => this.onTransactionsListUpdate(transaction),
                err => console.log(err)
            );
    }

    showOrder(transaction: MoneyTransactionModel) {
        this.orderService.getOrderById(transaction.OrderId)
            .subscribe(
                ord => this.loadOrderSuccess(ord),
                err => this.loadOrderFailed(err)
        );

        this.SelectedUserName = transaction.AffectedUser.toString();
    }

    private loadOrderSuccess(order: OrderModel) {
        this.IsSelected = true;
        this.SelectedOrder = order;
    }

    private loadOrderFailed(error) {
        console.log(error);
    }

    closeForm() {
        this.IsSelected = false;
        this.SelectedOrder = undefined;
        this.SelectedUserName = "";
    }

    downloadReport() {
        this.transactionService.downloadReportInPeriod(this.filterPeriod.StartDate, this.filterPeriod.FinishDate);
    }
}