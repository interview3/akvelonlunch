﻿import { Component, Output, EventEmitter } from "@angular/core";
import { Control, ControlGroup, FormBuilder, Validators  } from "@angular/common";
import { ProvisionerService } from '../../../../services/provisioner.service';
import { ProvisionerModel } from '../../../../ModelClasses/ProvisionerModel';
import {NotifyService} from '../../../../services/notify.service';

@Component({
    selector: 'provisioner-form',
    templateUrl: `./ScriptsApp/components/admin/provisioners/provisionerForm/provisionerForm.component.html`,
    styleUrls: [
        `./ScriptsApp/components/admin/provisioners/provisionerForm/provisionerForm.component.css`,
        './ScriptsApp/components/mainScreen/mainScreen.css',
        `./ScriptsApp/components/admin/confirmWindow.css`
    ],
    providers: [ProvisionerService, FormBuilder]
})
export class ProvisionerForm {
    private SupplierNameControl: Control;
    private AddressControl: Control;
    private PhoneControl: Control;
    private EmailControl: Control;
    private ContactPersonNameControl: Control;
    private _provisionerForm: ControlGroup;

    private _provisioner = null;
    private _provisionerFormIsVisible = true;
    public static Provisioner: ProvisionerModel;

    private _isEditing:boolean;
    private _modalHeader: string;
    private _isDeleting:boolean = false;

    @Output()
    public onProvisionerEdited: EventEmitter<ProvisionerModel> = new EventEmitter<ProvisionerModel>();
    @Output()
    public onProvisionerDeleted: EventEmitter<number> = new EventEmitter<number>();

    ngOnInit() {
        if (ProvisionerForm.Provisioner == null || ProvisionerForm.Provisioner == undefined) {
            this._provisioner = new ProvisionerModel({
                SupplierName: '',
                Address: '',
                Phone: '',
                Email: '',
                ContactPersonName: ''
            });
            this._modalHeader = "Создание нового поставщика";
            this._isEditing = false;
        } else {
            this._provisioner = ProvisionerForm.Provisioner;
            this._modalHeader = "Редактирование поставщика";
            this._isEditing = true;
        }

        this.SupplierNameControl = new Control(this._provisioner.SupplierName, Validators.required);
        this.AddressControl = new Control(this._provisioner.Address,
            Validators.compose([Validators.required, Validators.maxLength(1000)]));
        this.PhoneControl = new Control(this._provisioner.Phone,
            Validators.compose([Validators.required, Validators.maxLength(35)]));
        this.EmailControl = new Control(this._provisioner.Email, Validators.required);
        this.ContactPersonNameControl = new Control(this._provisioner.ContactPersonName, Validators.required);

        this._provisionerForm = this._formBuilder.group({
            SupplierName: this.SupplierNameControl,
            Address: this.AddressControl,
            Phone: this.PhoneControl,
            Email: this.EmailControl,
            ContactPersonName: this.ContactPersonNameControl
        });
    }

    constructor(private _formBuilder: FormBuilder,
        private provisionerService: ProvisionerService,
        private notifyService: NotifyService) {
    }

    private CreateProvisioner(SupplierName: string, Address: string, Phone: string, Email: string, ContactPersonName: string) {
        let newProvisioner = new ProvisionerModel({
            SupplierName: SupplierName,
            Address: Address,
            Phone: Phone,
            Email: Email,
            ContactPersonName: ContactPersonName
        });

        this.provisionerService.createProvisioner(newProvisioner)
            .subscribe(
            result => {
                newProvisioner.Id = result.OperationStatus.Entity;
                this.onProvisionerEdited.emit(newProvisioner);
                this._provisionerFormIsVisible = false;
                ProvisionerForm.Provisioner = undefined;
                this._provisioner = undefined;
                this._isEditing = false;
                this.notifyService.showNotify("Поставщик создан");
            },
            error => console.log(error)
        );
    }

    public UpdateProvisioner(Id: number, SupplierName: string, Address: string, Phone: string, Email: string, ContactPersonName: string) {
        let newProvisioner = new ProvisionerModel({
            Id: Id,
            SupplierName: SupplierName,
            Address: Address,
            Phone: Phone,
            Email: Email,
            ContactPersonName: ContactPersonName
        });
        this.provisionerService.updateProvisioner(newProvisioner)
            .subscribe(
            result => {
                ProvisionerForm.Provisioner = undefined;
                this._provisioner = undefined;
                this._isEditing = false;
                this.onProvisionerEdited.emit(newProvisioner);
                this.notifyService.showNotify("Изменения сохранены");
            },
            error => console.log(error)
        );
    }

    public TryDelete() {
        this._isDeleting = true;
    }
    public popUpCloseWindow() {
        this._isDeleting = false;
    }

    public DeleteProvisioner(id: number) {       
        this.provisionerService.deleteProvisioner(id)
            .subscribe(
                res => {
                    ProvisionerForm.Provisioner = undefined;
                    this._provisioner = undefined;
                    this._isEditing = false;
                    this.onProvisionerDeleted.emit(id);            
                },
                error => console.log(error)
        );
        this._isDeleting = false;
    }
}