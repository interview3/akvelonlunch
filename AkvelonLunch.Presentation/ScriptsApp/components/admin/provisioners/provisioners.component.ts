﻿import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { ProvisionerModel } from '../../../ModelClasses/ProvisionerModel';
import { ProvisionerService } from '../../../services/provisioner.service';
import { ProvisionersGrid } from './provisionersGrid/provisionersGrid.component';
import { ProvisionerForm } from './provisionerForm/provisionerForm.component';

@Component({
    selector: 'provisioners',
    templateUrl: './ScriptsApp/components/admin/provisioners/provisioners.component.html',
    styleUrls: [ './ScriptsApp/components/admin/provisioners/provisioners.component.css',
                 './ScriptsApp/components/mainScreen/mainScreen.css'],
    providers: [ProvisionerService],
    directives: [ProvisionersGrid, ProvisionerForm]
})
export class ProvisionersComponent {
    private provisionersList: ProvisionerModel[] = new Array<ProvisionerModel>();
    private _provisioner: ProvisionerModel;
    private _provisionerFormIsVisible: boolean;

    constructor(private provisionerService: ProvisionerService, private router: Router) {
        this.LoadProvisioner();
        this._provisionerFormIsVisible = false;
    }

    private LoadProvisioner() {
        this._provisionerFormIsVisible = false;
        this.provisionerService.loadProvisioners()
            .subscribe(
                result => {
                    result.forEach(r => {
                        this.provisionersList.push(new ProvisionerModel(r));
                        this.provisionerService.emitUpdateGrid(this.provisionersList);
                    });
                },
                error => console.log(error));

        this.provisionerService.getProvisionersListChangeEmitter()
            .subscribe(
                provivioner => this.onProvisionersListUpdate(provivioner),
                err => console.log(err)
            );
    }

    public AddProvisioner() {
        this._provisioner = null;
        this._provisionerFormIsVisible = true;
        ProvisionerForm.Provisioner = undefined;
        this.provisionerService.emitShowProvisionerForm(undefined);
    }

    public EditProvisioner(provisioner: ProvisionerModel) {
        this._provisioner = provisioner;
        this._provisionerFormIsVisible = true;
    }

    private onProvisionersListUpdate(provisioner: ProvisionerModel) {
        for (let i = 0; i < this.provisionersList.length; i++)
            if (this.provisionersList[i].Id == provisioner.Id) {
                this.provisionersList[i] = provisioner;
                return;
            }
        this.provisionersList.push(provisioner);
        this.provisionerService.emitUpdateGrid(this.provisionersList);
    }


    provisionerSelected(provisioner: ProvisionerModel) {
        this._provisionerFormIsVisible = true;
        this._provisioner = provisioner;
        ProvisionerForm.Provisioner = provisioner;
        this.provisionerService.emitShowProvisionerForm(provisioner);
    }

    provisionerEdited(provisioner: ProvisionerModel) {
        if (provisioner != null) {
            this.onProvisionersListUpdate(provisioner);
            this.provisionerService.emitUpdateGrid(this.provisionersList);
        }
        this._provisionerFormIsVisible = false;
    }

    provisionerDeleted(provisionerId: number) {
        var j = this.provisionersList.findIndex(pr => pr.Id == provisionerId);
        this.provisionersList.splice(j, 1);
        this.provisionerService.emitUpdateGrid(this.provisionersList);
        this._provisionerFormIsVisible = false;
    }
}