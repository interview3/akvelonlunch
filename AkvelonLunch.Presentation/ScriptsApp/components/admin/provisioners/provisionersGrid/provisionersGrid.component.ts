﻿import { Component, EventEmitter, Output } from "@angular/core";
import { GridConfig, ColumnState, Column, Grid } from '../../../../utils/GridHelpers';
import { BaseGrid } from "../../../../BaseGrid/BaseGrid";
import { ProvisionerModel } from '../../../../ModelClasses/ProvisionerModel';
import { ProvisionerService } from '../../../../services/provisioner.service';

@Component({
    selector: 'provisioners-grid',
    templateUrl: `./ScriptsApp/BaseGrid/BaseGrid.html`,
    styleUrls: [ './ScriptsApp/BaseGrid/BaseGrid.css',
        './ScriptsApp/components/admin/provisioners/provisionersGrid/provisionersGrid.component.css'],
})

export class ProvisionersGrid extends BaseGrid {
    @Output()
    public onRowClicked: EventEmitter<ProvisionerModel> = new EventEmitter<ProvisionerModel>();

    constructor(private provisionerService: ProvisionerService) {
        super(
            new Array<Column>(
                new Column("№", "Id"),
                new Column("Название фирмы", "SupplierName"),
                new Column("Адрес", "Address"),
                new Column("Телефон", "Phone"),
                new Column("Адрес электронной почты", "Email"),
                new Column("Контактное лицо", "ContactPersonName")
            ),
            new Column("Id", "Id"),
            new Array<number>(1, 2, 5, 10),
            25);

        this.provisionerService.getGridChangeEmitter()
            .subscribe(provisionersList => {
                this.setProvisionersGrid(provisionersList);
            });
    }

    protected onClickRowItem(row: any) {
        this.onRowClicked.emit(row as ProvisionerModel);
    }

    private setProvisionersGrid(provisionersList: ProvisionerModel[]) {
        this.changeGrid(this.convertArrayProvisionerToArrayAny(provisionersList));
    }


    private convertArrayProvisionerToArrayAny(provisionersList: ProvisionerModel[]) {
        let anyList = new Array<any>();
        provisionersList.forEach(pr => anyList.push(this.convertProvisionerToAny(pr)));
        return anyList;
    }

    private convertProvisionerToAny(provisioner: ProvisionerModel): any {
        var provisionerAny: any = {
            Id: provisioner.Id.toString(),
            Address: provisioner.Address,
            SupplierName: provisioner.SupplierName,
            Email: provisioner.Email,
            Phone: provisioner.Phone,
            ContactPersonName: provisioner.ContactPersonName
        }
        return provisionerAny;
    }
}