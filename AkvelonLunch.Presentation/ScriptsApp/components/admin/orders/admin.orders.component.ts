﻿import {Component, Output, Input, EventEmitter } from '@angular/core';
import { provideRouter, RouterConfig, Router } from '@angular/router';
import { ProvisionerModel } from '../../../ModelClasses/ProvisionerModel';
import { OrderItemModel } from '../../../ModelClasses/OrderItemModel';
import { OrderListItemModel } from '../../../ModelClasses/OrderListItemModel';
import { OrderModel } from '../../../ModelClasses/OrderModel';
import { OrderStatuses } from '../../../ModelClasses/OrderStatuses';
import { DishModel } from '../../../ModelClasses/DishModel';
import { UserModel } from '../../../ModelClasses/UserModel';
import { ProvisionerService } from '../../../services/provisioner.service';
import { OrderService } from '../../../services/order.service';
import { UserService } from '../../../services/user.service';
import { DateFilter, DateFilterPeriod } from '../../dateFilter/dateFilter.component';
import { OrdersGrid } from './ordersGrid/ordersGrid.component';
import {NotifyService} from '../../../services/notify.service';
import { OrdersReportComponent } from './ordersReport/ordersReport.component';

@Component({
    selector: 'admin-orders',
    templateUrl: `./ScriptsApp/components/admin/orders/admin.orders.component.html`,
    styleUrls: [`./ScriptsApp/components/admin/orders/admin.orders.component.css`,
        './ScriptsApp/components/mainScreen/mainScreen.css',
        `./ScriptsApp/components/admin/confirmWindow.css`],
    directives: [OrdersGrid, DateFilter, OrdersReportComponent],
    providers: [ProvisionerService]
})
export class AdminOrdersComponent {
    public Provisioners: ProvisionerModel[] = new Array<ProvisionerModel>();
    public Users: Array<UserModel[]> = new Array<UserModel[]>();
    public OneUsers: UserModel[] = new Array<UserModel>();
    public TotalOrder: OrderItemModel[];
    public TotalSum: number = 0;
    public TotalTodaySum: number = 0;
    public TotalCash: number = 0;
    public OrdersList: OrderListItemModel[];
    public displayGrid: string = "none";

    private filterPeriod: DateFilterPeriod = DateFilterPeriod.Default();
    public wasDeliev: boolean = false;
    public mode = "main";
    private performedOrders: number[] = new Array<number>();
    private isConfirmDelieveryWindowVisible:boolean=false;

    @Output() pushNotify = new EventEmitter();

    constructor(private notifyService: NotifyService,
        private provisionerService: ProvisionerService,
        private orderService: OrderService,
        private userService: UserService,
        private router:Router) {
        this.loadProvisioner();

        this.orderService.getOrderListChangeEmmiterr()
            .subscribe(
                order => this.onOrderListUpdate(order)
        );

        this.userService.getTotalCash()
            .subscribe(
            cash => {
                this.TotalCash = cash;
            });

        var res = this.orderService.wereTodayOrdersDelieveried();
        res.subscribe(
            r => {
                this.wasDeliev = r;
            });
    }

    public loadProvisioner() {
        this.initProperties();

        this.provisionerService.loadProvisioners()
            .subscribe(
                (result: ProvisionerModel[]) => this.loadProvisionersSuccess(result),
                error => console.log(error));

        return this.OrdersList;
    }

    private initProperties() {
        this.Provisioners = new Array<ProvisionerModel>();
        this.OrdersList = new Array<OrderListItemModel>();
        this.OneUsers = new Array<UserModel>();
        this.TotalOrder = new Array<OrderItemModel>();
        this.performedOrders = new Array<number>();
        this.TotalSum = 0;
        this.TotalTodaySum = 0;
    }

    public loadProvisionersSuccess(list: ProvisionerModel[]) {
        list.forEach(r => {
            this.Provisioners.push(new ProvisionerModel(r));
            this.getFilteredOrders(r.Id);
        });
    }

    private getFilteredOrders(provisionerId: number) {
        this.orderService.getFilteredOrders(provisionerId, this.filterPeriod.StartDate, this.filterPeriod.FinishDate)
            .subscribe(
                (ord) => {
                    ord.forEach(o => {
                        this.addRowInList(o);
                    });
                    this.displayGrid = (this.OrdersList.length > 0) ? "inline" : "none";
                    this.orderService.emitUpdateGrid(this.OrdersList);
                },
                err => console.log(err)
            );
    }

    private addRowInList(ord) {
        var row: OrderListItemModel = new OrderListItemModel();
        row.Id = ord.Id;
        row.TotalSum.push(ord.Order.Cost);
        row.Order.push(ord.Order);
        row.UserBalance = ord.UserBalance;
        row.UserName = ord.UserName;
        row.OrderDate.push(this.convertDateToString(ord.OrderDate));

        this.TotalSum += ord.Order.Cost;
        if (this.compareSimpleDateToNow(ord.OrderDate) && ord.Order.OrderStatusId == 2) {
            this.TotalTodaySum += ord.Order.Cost;
        }

        for (var i = 0; i < ord.Order.Items.length; i++) {
            this.takeOrderItems(ord.Order.Items[i], row);
        }
        this.OrdersList.push(row);
    }

    private compareSimpleDateToNow(date) {
        var now = new Date();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        var year = now.getFullYear();
        var txtNow = year + "/" + ((month < 10) ? ("0" + month) : month) + "/" + ((day < 10) ? ("0" + day) : day) ;
        return (txtNow == this.convertDateToString(date));
    }
    
    private convertDateToString(date: any) :string {
        var s = date.toString();
        var year = s.substr(0, 4);
        var month = s.substr(5, 2);
        var day = s.substr(8, 2);
        return year + "/" + month + "/" + day;
    }

    private takeOrderItems(it: OrderItemModel, row: OrderListItemModel) {
        if (this.TotalOrder[it.DishId] == undefined ||
            this.TotalOrder[it.DishId] == null) {
            this.TotalOrder[it.DishId] = new OrderItemModel(it.Dish, it.Count);
        } else {
            this.TotalOrder[it.DishId].Count += it.Count;
        }
    }

    public getOrderStatusString(stat, row: OrderListItemModel) {
        this.orderService.getOrderStatusStringByOrderStatusId(stat)
            .subscribe(
                st => {
                    row.StatusString.push(st);
            });
    }

    tryDelieveOrders() {
        this.isConfirmDelieveryWindowVisible = true;
    }

    popUpCloseWindow() {
        this.isConfirmDelieveryWindowVisible = false;
    }

    public giveOrdersDelieveryStatus() {
        this.wasDeliev = true;
        this.isConfirmDelieveryWindowVisible = false;

        this.TotalCash -= this.TotalTodaySum;
        this.orderService.notifyAboutDelivering()
            .subscribe(
                ord => { return ord; }
            );

        //this.notifyService.showPushNotify('');
        this.pushNotify.emit('');
        //localStorage.setItem("PushNotify", "");
        //localStorage.setItem("statusOrder", "open");  
    }

    private onOrderListUpdate(order: OrderListItemModel) {
        for (let i = 0; i < this.OrdersList.length; i++)
            if (this.OrdersList[i].Id === order.Id) {
                this.OrdersList[i] = order;
                return;
            }
        this.OrdersList.push(order);
        this.orderService.emitUpdateGrid(this.OrdersList);
    }

    wereOrders() {
        return this.displayGrid != 'none';
    }

    showReport() {
        this.mode = "report";
        OrdersReportComponent.List = this.OrdersList;
        OrdersReportComponent.sTotalSum = this.TotalSum;
        //this.router.navigateByUrl("/Admin/Report");
    }
    backToAdmin() {
        this.mode = "main";
        this.loadProvisioner();
        this.orderService.getOrderListChangeEmmiterr()
            .subscribe(
            order => this.onOrderListUpdate(order)
            );
    }

    periodWasChanged(period: DateFilterPeriod) {
        this.filterPeriod = period;
        this.loadProvisioner();

        this.userService.getTotalCash()
            .subscribe(
            cash => this.TotalCash = cash);
        this.orderService.emitUpdateGrid(this.OrdersList);
    }

    downloadReport() {
        this.orderService.downloadReportInPeriod(this.filterPeriod.StartDate, this.filterPeriod.FinishDate);
    }
}