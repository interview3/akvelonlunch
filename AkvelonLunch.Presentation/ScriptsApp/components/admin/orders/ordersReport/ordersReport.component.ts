﻿import {Component, Input, Output, EventEmitter} from '@angular/core';
import { provideRouter, RouterConfig, Router } from '@angular/router';
import { OrderListItemModel } from '../../../../ModelClasses/OrderListItemModel';
import { ProvisionerService } from '../../../../services/provisioner.service';
import { OrderService } from '../../../../services/order.service';
import { UserService } from '../../../../services/user.service';
import { OrdersGrid } from '../ordersGrid/ordersGrid.component';
import {AdminOrdersComponent} from "../admin.orders.component";

@Component({
    selector: 'orders-report',
    templateUrl: `./ScriptsApp/components/admin/orders/ordersReport/ordersReport.component.html`,
    styleUrls: [`./ScriptsApp/components/admin/orders/ordersReport/ordersReport.component.css`],
})
export class OrdersReportComponent {
    public OrdersList: OrderListItemModel[];
    public static List: OrderListItemModel[];
    public static sTotalSum:number;
    public TotalSum:number;
    @Output()
    public onClosed: EventEmitter<OrderListItemModel[]> = new EventEmitter<OrderListItemModel[]>();

    public get today(): string {
        var now = new Date();
        var month = now.getMonth() + 1; //months from 1-12
        var day = now.getDate();
        var year = now.getFullYear();

        return (day + "/" + month + "/" + year);
    }
    
    constructor(private router: Router) {
        this.OrdersList = OrdersReportComponent.List;
        this.TotalSum = OrdersReportComponent.sTotalSum;
    }

    private onOrderListUpdate(order: OrderListItemModel) {
        for (let i = 0; i < this.OrdersList.length; i++)
            if (this.OrdersList[i].Id === order.Id) {
                this.OrdersList[i] = order;
                return;
            }
        this.OrdersList.push(order);
        //this.orderService.emitUpdateGrid(this.OrdersList);
    }

    backToAdminPanel() {
        this.onClosed.emit(this.OrdersList);
        //this.router.navigateByUrl("/Admin/Index");
    }

    toSimpleDate(d: any) {
        var date = new Date(d.toString());
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();
        var txtNow = ((day < 10) ? ("0" + day) : day) + "." + ((month < 10) ? ("0" + month) : month) + "." + year;
        return txtNow;
    }
}