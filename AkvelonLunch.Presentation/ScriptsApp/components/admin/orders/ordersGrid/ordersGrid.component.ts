﻿import { Component, OnInit } from "@angular/core";
import { OrderService } from '../../../../services/order.service';
import { InformationService } from '../../../../services/information.service';
import { SettingsService } from '../../../../services/settings.service';
import { NotifyService } from '../../../../services/notify.service';
import { GridConfig, ColumnState, Column, Grid } from '../../../../utils/GridHelpers';
import { BaseGrid } from "../../../../BaseGrid/BaseGrid";
import { OrderListItemModel } from "../../../../ModelClasses/OrderListItemModel";
import { OrderModel } from "../../../../ModelClasses/OrderModel";
import { CartComponent} from '../../../mainScreen/cart/cart.component';

@Component({
    selector: 'orders-grid',
    templateUrl: `./ScriptsApp/components/admin/orders/ordersGrid/ordersGrid.component.html`,
    styleUrls: [
        './ScriptsApp/BaseGrid/BaseGrid.css',
        './ScriptsApp/components/admin/orders/ordersGrid/ordersGrid.component.css'
    ],
    directives: [CartComponent],
    providers:[SettingsService]
})
export class OrdersGrid extends BaseGrid implements OnInit{
    public IsSelected: boolean = false;
    public SelectedOrder: OrderListItemModel = null;

    ngOnInit() {
        this.SelectedOrder = null;
    }
    

    constructor(private _orderService: OrderService,
        private _informationService: InformationService,
        private _settingsService: SettingsService) {
        super(
            new Array<Column>(
                new Column("Имя пользователя", "UserName"),
                new Column("Баланс пользователя", "UserBalance"),
                new Column("Стоимость заказа", "TotalSum"),
                new Column("Дата заказа", "OrderDate")
            ),
            new Column("UserName", "UserName"),
            new Array<number>(5, 10, 25, 50),
            25);

        this._orderService.getGridChangeEmitter()
            .subscribe(ordersList => {
                this.setOrdersGrid(ordersList);
            });
    }

    protected onClickRowItem(row: any) {
        this.IsSelected = true;
        this.SelectedOrder = (row as OrderListItemModel);
    }

    private setOrdersGrid(ordersList: OrderListItemModel[]) {
        this.changeGrid(this.convertArrayOrdersToArrayAny(ordersList));
    }


    private convertArrayOrdersToArrayAny(ordersList: OrderListItemModel[]) {
        let anyList = new Array<any>();
        ordersList.forEach(d => anyList.push(this.convertOrderToAny(d)));
        return anyList;
    }

    private convertOrderToAny(orderItem: OrderListItemModel): any {
        return {
            Id: orderItem.Id,
            UserName: orderItem.UserName,
            UserBalance: orderItem.UserBalance,
            TotalSum: orderItem.TotalSum,
            Status: orderItem.Status,
            StatusString: orderItem.StatusString,
            Order: orderItem.Order,
            OrderDate: orderItem.OrderDate
        };
    }

    closeForm() {
        this.IsSelected = false;
        this.SelectedOrder = null;
    }
}