import {IDayLabels} from "./daylabels.interface";
import {IMonthLabels} from "./monthlabels.interface";
import {IDate} from "./date.interface";

export interface IOptions {
    dayLabels?: IDayLabels;
    monthLabels?: IMonthLabels;
    dateFormat?: string;
    todayBtnTxt?: string;
    firstDayOfWeek?: string;
    sunHighlight?: boolean;
    disabledUntil?: IDate;
    disabledSince?: IDate;
    disableWeekends?: boolean;
    height?: string;
    width?: string;
    inline?: boolean;
}