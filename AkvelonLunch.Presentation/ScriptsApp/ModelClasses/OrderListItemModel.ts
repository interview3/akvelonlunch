﻿import {OrderStatuses} from "./OrderStatuses";
import {OrderModel} from "./OrderModel";

export class OrderListItemModel {
    public Id: number[];
    public UserName: string;
    public UserBalance: number;
    public TotalSum: number[];
    public Status: OrderStatuses[];
    public StatusString: string[];
    public Order: OrderModel[];
    public OrderDate:string[];

    constructor() {
        this.TotalSum = new Array<number>();
        this.Id = new Array<number>();
        this.Status = new Array<OrderStatuses>();
        this.StatusString = new Array<string>();
        this.Order = new Array<OrderModel>();
        this.OrderDate = new Array<string>();
    }
}