﻿export class FilterModel {
    constructor(
        public ProvisionerId: number,
        public StartDate: Date,
        public FinishDate: Date) {
    }
}