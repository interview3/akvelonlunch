﻿import {OrderItemModel} from './OrderItemModel';
import {OrderStatuses} from './OrderStatuses';

export class OrderModel {
    public Id: number;
    public UserId: number;
    public Cost: number;
    public Items: OrderItemModel[];
    public OrderTime: Date;
    public OrderStatusId: OrderStatuses;
    public MenuId: number;
    constructor() {
        this.Items = [];
    }
}
