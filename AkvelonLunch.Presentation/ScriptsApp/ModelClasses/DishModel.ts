﻿import { DishTypeModel } from './DishTypeModel'
import { ProvisionerModel } from './ProvisionerModel'
import {DishTypeEnum} from './DishTypeEnum'

export class DishModel {
    public Id: number;
    public Name: string;
    public Description: string;
    public Cost: number;
    public DishType: DishTypeModel;
    public Provisioner: ProvisionerModel;

    constructor(obj: any) {
        this.Id = obj.Id;
        this.Name = obj.Name;
        this.Description = obj.Description;
        this.Cost = obj.Cost;
        this.DishType = obj.DishType;
        this.Provisioner = obj.Provisioner;
   }
}
