﻿export class UserRole {
    public Name: string;

    constructor(public Id: number) {
        switch (Id) {
        case 1:
            this.Name = 'Администратор';
            break;
        case 2:
            this.Name = 'Клиент';
            break;
        }
    }

    public static getAllRoles() {
        return [
            new UserRole(1),
            new UserRole(2)
        ];
    }

    public static getRoleIdByRoleString(name: string): number {
        var roles = UserRole.getAllRoles();
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].Name == name)
                return (i + 1);
        }
        return -1;
    }
}