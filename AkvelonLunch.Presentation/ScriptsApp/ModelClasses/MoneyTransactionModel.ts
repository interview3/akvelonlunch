﻿import {UserModel} from './UserModel';
import {TransactionOperationModel} from './TransactionOperationModel';

export class MoneyTransactionModel {
    public TransactionId: number;
    public AffectedUser: UserModel;
    public InitiatorUser: UserModel;
    public Operation: TransactionOperationModel;
    public BalanceAfter: number;
    public BalanceBefore: number;
    public TransactionTime: Date;
    public Comment: string;
    public OrderId: number;
}