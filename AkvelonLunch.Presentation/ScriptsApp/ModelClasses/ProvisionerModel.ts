﻿export class ProvisionerModel {

    public Id: number;
    public SupplierName: string;
    public Address: string;
    public Phone: string;
    public Email: string;
    public ContactPersonName: string;

    constructor(obj: any) {
        this.Id = obj.Id;
        this.SupplierName = obj.SupplierName;
        this.Address = obj.Address;
        this.Phone = obj.Phone;
        this.Email = obj.Email;
        this.ContactPersonName = obj.ContactPersonName;

    }
}