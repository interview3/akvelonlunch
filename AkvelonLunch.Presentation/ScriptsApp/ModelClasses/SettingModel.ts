﻿export class SettingModel {
    public Id: number;
    public Name: string;
    public Value: string;
    public Description: string;
}