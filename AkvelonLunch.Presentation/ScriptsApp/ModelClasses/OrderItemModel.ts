﻿import {DishModel} from './DishModel'

export class OrderItemModel {
    public Id: number;
    public DishId: number;
    public Dish: DishModel;
    public Count: number;
    public OrderId: number;

    constructor(dish: DishModel, count: number) {
        this.Dish = dish;
        this.DishId = this.Dish.Id;
        this.Count = count;
    }
}