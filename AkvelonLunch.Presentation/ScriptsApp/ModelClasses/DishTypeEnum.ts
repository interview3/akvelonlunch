﻿export enum DishTypeEnum {
    Soup = 1,
    SecondCourse = 2,
    Salad = 3,
    Other = 4
}
