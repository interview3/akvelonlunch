﻿import { DishModel } from './DishModel'

export class MenuModel {
    public Id: number;
    constructor(public Dishes: DishModel[], public CreatorId: number, public DateToShow: Date) { }
}