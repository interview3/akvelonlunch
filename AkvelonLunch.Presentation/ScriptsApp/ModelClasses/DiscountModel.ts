﻿import { DishTypeModel } from './DishTypeModel'
import { ProvisionerModel } from './ProvisionerModel'
export class DiscountModel {
    public ProvisionerId: number;
    public Provisioner: ProvisionerModel;
    public NumberOfDishes: number;
    public DiscountSum: number;
    constructor(obj: any) {
        this.ProvisionerId = obj.ProvisionerId;
        this.Provisioner = obj.Provisioner;
        this.NumberOfDishes = obj.NumberOfDishes;
        this.DiscountSum = obj.DiscountSum;
    }
}