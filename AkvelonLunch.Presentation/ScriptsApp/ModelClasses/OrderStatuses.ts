﻿export enum OrderStatuses {
    Canceled = 1,
    Pending = 2,
    Completed = 3
}