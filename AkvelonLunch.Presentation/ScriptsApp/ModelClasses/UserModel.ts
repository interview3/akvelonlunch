﻿import {UserRole} from "./UserRole";

export class UserModel {
    public Id: number;
    public Login: string;
    public FirstName: string;
    public LastName: string;
    public Email: string;
    public Balance: number;
    public RoleString:string;
    public Role: UserRole;
    public RoleId: number;

    constructor(row: any) {
        this.Id = row.Id,
            this.Login = row.Login,
            this.FirstName = row.FirstName,
            this.LastName = row.LastName,
            this.Email = row.Email,
            this.Balance = row.Balance,
            this.Role = row.Role,
            this.RoleString = row.RoleString
    }
}