﻿import {Dish} from './components/dish';

export var DISHES: Dish[] = [
    { "id": 1, "name": "Soup1" },
    { "id": 2, "name": "Soup2" },
    { "id": 3, "name": "Soup3" },
    { "id": 4, "name": "Soup4" }
];