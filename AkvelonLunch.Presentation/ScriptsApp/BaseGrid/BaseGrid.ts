﻿import {Component, Output, Input, EventEmitter } from '@angular/core';
import { GridConfig, ColumnState, Column, Grid } from '../utils/GridHelpers';

export abstract  class BaseGrid {

    public static templateURL: './ScriptsApp/BaseGrid/BaseGrid.html';

    protected numberOfPage: number = 0;
    protected initialCountOfItemsOnPage: number = 10;
    protected paginationLength: number = 6;
    
    private greed: Grid = null;
    private filterString: string = "";

    @Output()
    public editClick:EventEmitter<any> = new EventEmitter<any>();

    protected abstract onClickRowItem(row: any);

    constructor(private columns: Column[],
        private columnFilter: Column,
        private listCountOfItemsOnPage: number[],
        private filterInputLength: number,
        private isEditable=false) {
        this.greed = new Grid(this.columns, this.columnFilter, new Array<any>(), this.initialCountOfItemsOnPage);
    }

    protected changeGrid(itemList: any[]) {
        this.greed.setItemsList(itemList);
    }

    protected changeGridWithExclude(itemList: any[], excludeList: any[], cmp: (a: any, b: any) => boolean) {
        this.greed.setItemsListWithExclude(itemList, excludeList, cmp);
    }

    private resetFilterString() {
        this.filterString = "";
        this.onChangeFilterString();
    }
    
    private currentPage() {
        return this.greed.getPage(this.numberOfPage);
    }

    
    private getNumberOfPageList() {
        var buf = new Array<number>();
        if (this.greed.CountOfPages <= this.paginationLength) {
            buf = new Array<number>(this.greed.CountOfPages);
            for (let i = 0; i < buf.length; i++)
                buf[i] = i;
        } else {
            var len = this.paginationLength / 2;
            var start = Math.max(this.numberOfPage - len, 0);
            var finish = Math.min(this.numberOfPage + len, this.greed.CountOfPages);
            if (finish - start < this.paginationLength) {
                if (start == 0)
                    finish = this.paginationLength;
                if (finish == this.greed.CountOfPages)
                    start = finish - this.paginationLength;
            }
            for (var i = start; i < finish; i++) {
                buf.push(i);
            }
        }
        return buf;
    }

    private onChangeFilterString() {
        this.numberOfPage = 0;
        this.greed.onFilterStringChange(this.filterString);
    }

    public onColumnClick(column: Column) {
        this.greed.onColumnClick(column);
    }

    private onPageChange(numberOfPage: number) {
        this.numberOfPage = numberOfPage;
    }

    private onDecrementPage() {
        if (this.numberOfPage - 1 >= 0) {
            this.numberOfPage--;
        }
    }

    private onIncrementPage() {
        if (this.numberOfPage + 1 < this.greed.CountOfPages) {
            this.numberOfPage++;
        }
    }

    private onSetFirtPage() {
        this.numberOfPage = 0;
    }

    private onSetLastPage() {
        if (this.greed.CountOfPages !== 0) {
            this.numberOfPage = this.greed.CountOfPages - 1;
        }
    }

    private onChangeCountOfItemsOnPage(count: string) {
        this.numberOfPage = 0;
        this.greed.PageSize = +count;
    }

    public onEditClick(row) {
        this.editClick.emit(row);
    }

    private getColumnState(column: Column): ColumnState {
        return this.greed.getStateColumn(column);
    }

    public isArray(x: any):boolean {
        return (Array.isArray(x));
    }

    public trySplitArray(x) {
        return x;
    }
}