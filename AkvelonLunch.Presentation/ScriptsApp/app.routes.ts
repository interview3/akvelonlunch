﻿import { provideRouter, RouterConfig } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {MainScreenComponent} from './components/mainScreen/mainScreen.component';
import {AdminMainScreenComponent} from './components/admin/main/main.component';
import {OrdersReportComponent} from './components/admin/orders/ordersReport/ordersReport.component';

const routes: RouterConfig = [    
    { path: '', component: MainScreenComponent },
    { path: 'Home/Login', component: LoginComponent },
    { path: 'Home/Index', component: MainScreenComponent },
    { path: 'Admin/Index', component: AdminMainScreenComponent },
    { path: 'Admin/Report', component: OrdersReportComponent}
];


export const appRouterProviders = [
    provideRouter(routes)
];