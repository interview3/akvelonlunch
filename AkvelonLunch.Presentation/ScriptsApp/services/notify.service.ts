﻿import {EventEmitter, Injectable} from '@angular/core';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Http} from '@angular/http';

@Injectable()
export class NotifyService extends HttpHelpers {
    showNotifyErrorMessage: EventEmitter<string> = new EventEmitter<string>();
    enableButton: EventEmitter<boolean> = new EventEmitter<boolean>();
    notifyFillUpBalance: EventEmitter<number> = new EventEmitter<number>();
    notifyMenuSaveOpResult: EventEmitter<string> = new EventEmitter<string>();
    showPushNotifications: EventEmitter<any> = new EventEmitter<any>();
    changePushNotifyMessage: EventEmitter<string> = new EventEmitter<string>();

    changeMessagePushNotify(message: string) {
        this.changePushNotifyMessage.emit(message);
    }
    getMessageForPushNotify() {
        return this.changePushNotifyMessage;
    }

    showPushNotify(value: any) {
        this.showPushNotifications.emit(value);
    }
    getPushNotify() {
        return this.showPushNotifications;
    }

    showNotify(error: any) {
        this.showNotifyErrorMessage.emit(error);
    }
    getErrorMessage() {
        return this.showNotifyErrorMessage;
    }

    turnOnButton() {
        this.enableButton.emit(true);
    }
    getEnableButton() {
        return this.enableButton;
    }

    showPopUpBalance(balance: number) {
        this.notifyFillUpBalance.emit(balance);
    }
    getBalanceNotify() {
        return this.notifyFillUpBalance;
    }
    getMenuSaveOpResultNotify() {  
        return this.notifyMenuSaveOpResult;
    }
    showMenuSaveOpResult(message: string) {
        this.notifyMenuSaveOpResult.emit(message);
    }
}