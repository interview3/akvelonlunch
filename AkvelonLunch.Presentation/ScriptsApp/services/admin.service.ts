﻿import { EventEmitter, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { MenuModel } from "../ModelClasses/MenuModel";
import { DishModel } from "../ModelClasses/DishModel";
import { DishTypeModel } from "../ModelClasses/DishTypeModel";
import { HttpHelpers } from '../utils/HttpHelpers';
import { EnumHelpers } from '../utils/EnumHelpers';

@Injectable()
export class AdminService extends HttpHelpers {

    dishFromDisplayEmitter: EventEmitter<DishModel> = new EventEmitter<DishModel>();
    dishListChangeEmitter: EventEmitter<DishModel> = new EventEmitter<DishModel>();
    dishListChangeFromGridEmmiter: EventEmitter<DishModel> = new EventEmitter<DishModel>();
    gridChangeEmitter: EventEmitter<DishModel[]> = new EventEmitter<DishModel[]>();
    dateBarChangeEmitter: EventEmitter<Date> = new EventEmitter<Date>();

    constructor(private http: Http) {
        super(http);
    }

    public emitDateChange(date: Date) {
        this.dateBarChangeEmitter.emit(date);
    }

    public emitShowDishForm(dish: any) {
        this.dishFromDisplayEmitter.emit(dish);
    }

    public emitUpdateDishList(dish: DishModel) {
        this.dishListChangeEmitter.emit(dish);
    }

    public emitUpdateDishListFromGrid(dish: DishModel) {
        this.dishListChangeFromGridEmmiter.emit(dish);
    }
    
    public emitUpdateGrid(dishList: DishModel[]) {
        this.gridChangeEmitter.emit(dishList);
    }

    public getDateBarChangeEmitter() {
        return this.dateBarChangeEmitter;
    }

    public getDishFromDisplayEmitter() {
        return this.dishFromDisplayEmitter;
    }

    public getDishListChangeEmitter() {
        return this.dishListChangeEmitter;
    }

    public getDishListChangeFromGridEmitter() {
        return this.dishListChangeFromGridEmmiter;
    }

    public getGridChangeEmitter() {
        return this.gridChangeEmitter;
    }

    public createDish(dish: DishModel) {
        return this.postaction("/api/dish/create", dish);
    }

    public updateDish(dish: DishModel) {
        return this.postaction("/api/dish/update", dish);
    }

    public deleteDish(Id: number) {
        return this.deleteaction("/api/dish/delete/" + Id);
    }

    public getDishTypes() {
        return this.getaction("/api/dish/dishtypes");
    }

    public getFullDishList() {
        return this.getaction("/api/dish/alldishes");
    }

    public loadMenuByDate(date: Date) {
        
        return this.getaction("/api/menu/getMenuByDate" + "?date=" + date.toUTCString());
    }

    public loadMenu() {
        return this.getaction("/api/menu");
    }

    public createMenu(dishList: DishModel[], UserId: number, date: Date) {
        
        let menu = new MenuModel(dishList, UserId, date);
        return this.postaction("/api/menu/create", menu);
    }

    public updateMenu(menu: MenuModel) {
        return this.postaction("/api/menu/update", menu);
    }

    public deleteMenu(Id: number) {
        return this.deleteaction("/api/menu/delete/" + Id);
    }
}