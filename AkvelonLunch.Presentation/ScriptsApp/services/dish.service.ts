﻿import {EventEmitter, Injectable} from '@angular/core';
import {DishModel} from '../ModelClasses/DishModel';
import {MenuModel} from '../ModelClasses/MenuModel';
import {OrderItemModel} from '../ModelClasses/OrderItemModel';
import {OrderModel} from '../ModelClasses/OrderModel';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class DishService extends HttpHelpers {
    public lockedForEdit: boolean;

    dishAdded: EventEmitter<DishModel> = new EventEmitter<DishModel>();
    menuLoadedEmitter: EventEmitter<MenuModel> = new EventEmitter<MenuModel>();
    dishAddedIncCount: EventEmitter<DishModel> = new EventEmitter<DishModel>();
    dishCountChanged: EventEmitter<OrderItemModel> = new EventEmitter<OrderItemModel>();
    menuUrl: string = "api/menu/";
    dishTypesListUrl: string = "api/dish/dishtypes";

    constructor(private http: Http) {
        super(http);
    }

    addDishToCart(dish: DishModel) {
        this.dishAdded.emit(dish);
    }
    public getIncDishCountEmmiter() {
        return this.dishAdded;
    }
    getDishAddToCartEmitter() {
        return this.dishAdded;
    }

    getMenuLoadedEmitter() {
        return this.menuLoadedEmitter;
    }

    menuLoaded(menuModel: MenuModel) {
        this.menuLoadedEmitter.emit(menuModel);
    }

    incCountDish(dish: DishModel) {
        this.dishAddedIncCount.emit(dish);
    }
    incCountItems() {
        return this.dishAddedIncCount;
    }  

    setDishCount(dish: DishModel, count: number) {
        this.dishCountChanged.emit(new OrderItemModel(dish, count));
    }
    addOrderItem(orderItem: OrderItemModel) {
        this.dishCountChanged.emit(orderItem);
    }
    getDishCountChangeEmitter() {
        return this.dishCountChanged;
    }
    loadMenu() {
        return this.getaction(this.menuUrl);
    }
    loadDishTypeList() {
        return this.getaction(this.dishTypesListUrl);
    }
}