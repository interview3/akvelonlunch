﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import {UserService} from "./user.service";
import {SettingsService} from "./settings.service";
import 'rxjs/Rx';

@Injectable()
export class LoginService extends HttpHelpers {
    constructor(private http: Http,
        private userService: UserService) {

        super(http);
    }

    _logoutUrl: string = "api/auth/logout";
    _getUserUrl: string = "api/user/";
    
    signin(username: string, password: string, remember: boolean) {
        return this.authaction(username, password, remember);
    }

    logout() {
        SettingsService.stopBlockHourPing();
        return this.logoutaction(this._logoutUrl);
    }

    getuser() {
        return this.userService.getuser(true);
    }
}