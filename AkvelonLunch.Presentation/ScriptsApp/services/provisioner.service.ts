﻿import { EventEmitter, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpHelpers } from '../utils/HttpHelpers';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { ProvisionerModel } from '../ModelClasses/ProvisionerModel';

@Injectable()
export class ProvisionerService extends HttpHelpers {
    gridChangeEmitter: EventEmitter<ProvisionerModel[]> = new EventEmitter<ProvisionerModel[]>();
    provisionersListChangeEmitter: EventEmitter<ProvisionerModel> = new EventEmitter<ProvisionerModel>();
    provisionerFormDisplayEmitter: EventEmitter<ProvisionerModel> = new EventEmitter<ProvisionerModel>();

    constructor(private http: Http) {
        super(http);
    } 

    getProvisioner(id: number) {
        return this.getaction("/api/provisioner/getProvisioner/" + id);
    }

    loadProvisioners() {
        return this.getaction("/api/provisioner/allprovisioners");
    }

    createProvisioner(provisioner: ProvisionerModel) {
        return this.postaction("/api/provisioner/create", provisioner);
    }

    deleteProvisioner(id: number) {
        return this.deleteaction("/api/provisioner/delete/" + id);
    }

    updateProvisioner(provisioner: ProvisionerModel) {
        return this.postaction("/api/provisioner/update/", provisioner);
    }

    getGridChangeEmitter() {
        return this.gridChangeEmitter;
    }

    getProvisionersListChangeEmitter() {
        return this.provisionersListChangeEmitter;
    }

    emitUpdateGrid(provisionersList: ProvisionerModel[]) {
        this.gridChangeEmitter.emit(provisionersList);
    }

    public emitShowProvisionerForm(provisioner: any) {
        this.provisionerFormDisplayEmitter.emit(provisioner);
    }

    public getProvisionerFormDisplayEmitter() {
        return this.provisionerFormDisplayEmitter;
    }
}