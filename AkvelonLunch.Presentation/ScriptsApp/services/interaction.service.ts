﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

//Provides interaction between different components (for example, infopanel and orders pop-up)
@Injectable()
export class InteractionService extends HttpHelpers  {
    orderHistoryShowStateEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
    userSettingsEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
    helpShowEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private http: Http) {
        super(http);
    }

    showHelpPopUp(show: boolean) {
        this.helpShowEmitter.emit(show);
    }

    getHelpPopUp() {
        return this.helpShowEmitter;
    }

    getOrderHistoryShowStateEmitter() {
        return this.orderHistoryShowStateEmitter;
    }

    setOrderHistoryShowState(isVisible: boolean) {
        this.orderHistoryShowStateEmitter.emit(isVisible);
    }

    showUserSettings(show: boolean) {
        this.userSettingsEmitter.emit(show);
    }
    getUserSettingsEmitter() {
        return this.userSettingsEmitter;
    }
}