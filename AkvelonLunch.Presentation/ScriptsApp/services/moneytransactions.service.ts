﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {MoneyTransactionModel} from '../ModelClasses/MoneyTransactionModel';

@Injectable()
export class MoneyTransactionService extends HttpHelpers {
    gridChangeEmitter: EventEmitter<MoneyTransactionModel[]> = new EventEmitter<MoneyTransactionModel[]>();
    transactionsListChangeEmitter: EventEmitter<MoneyTransactionModel> = new EventEmitter<MoneyTransactionModel>();

    private _filteredByDatesTransactionsUrl: string ="api/MoneyTransactionLog/byDates";
    private _downloadReportInPeriodUrl: string = "/api/MoneyTransactionLog/download";

    constructor(private http: Http) {
        super(http);
    }
   
    getGridChangeEmitter() {
        return this.gridChangeEmitter;
    }

    getTransactionsListChangeEmitter() {
        return this.transactionsListChangeEmitter;
    }

    emitUpdateGrid(transactionsList: MoneyTransactionModel[]) {
        this.gridChangeEmitter.emit(transactionsList);
    }

    emitUpdateGridRow(transaction: MoneyTransactionModel) {
        this.transactionsListChangeEmitter.emit(transaction);
    }

    getTransactions(beginning: Date, ending: Date) {
        return this.getaction(this._filteredByDatesTransactionsUrl +
            "?beginning=" + beginning.toDateString() +
            "&ending=" + ending.toDateString());
    }

    downloadReportInPeriod(start: Date, finish: Date) {
        var url = this._downloadReportInPeriodUrl +
            "?startDate=" + start.toUTCString() +
            "&endDate=" + finish.toUTCString();
        window.location.href = url;
    }
}