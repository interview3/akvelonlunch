﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {UserModel} from '../ModelClasses/UserModel';

@Injectable()
export class InformationService extends HttpHelpers {

    constructor(private http: Http) {
        super(http);
    }
    currentUserBalanceUpdatedEmitter: EventEmitter<number> = new EventEmitter<number>();

    _logoutUrl: string = "api/auth/logout";
    _statisticUrl: string = "api/order/statistics";

    logout() {
        return this.logoutaction(this._logoutUrl);
    }

    getCurrentUserBalanceUpdatedEmitter() {
        return this.currentUserBalanceUpdatedEmitter;
    }

    currentUserBalanceUpdated(newBalance: number) {
        this.currentUserBalanceUpdatedEmitter.emit(newBalance);
    }

    getstatistics() {
        return this.getaction(this._statisticUrl);
    }
}