﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {SettingModel} from '../ModelClasses/SettingModel';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {FormatClass} from '../utils/formatClass';

@Injectable()
export class SettingsService extends HttpHelpers{
    private settingUrl: string = "api/setting";
    private updateSettingUrl: string = "api/setting/update";
    private blockHourSettingID: number = 1;
    private letterNotification: string = "api/user/getEnabledNotificationValue";
    private static blockHourTimerId: any = null;

    public static blockHourCached: Date = null;
    private static initialized: boolean = false; 
    //flag for one call setInterval
    private static blockHourChanged: EventEmitter<Date> = new EventEmitter<Date>();
    private static queryForBlockHourSent:boolean = false;
    constructor(private http: Http) {
        super(http);
        if (!SettingsService.queryForBlockHourSent) {
            this.loadBlockHour();
        }
        if (!SettingsService.initialized) {
            SettingsService.initialized = true;
            var thisContext = this;
            SettingsService.blockHourTimerId = setInterval(function () {
                thisContext.loadBlockHour();
            }, 10000);
        }
    }

    public getEnabledNotificationValue() {
        return this.getaction(this.letterNotification);
    }

    public static stopBlockHourPing() {
        if (SettingsService.blockHourTimerId != null) {
            clearInterval(SettingsService.blockHourTimerId);
        }
    }
    public notificationSettings(value: boolean) {
        return this.getaction("api/user/enabledNotifications?value=" + value);
    }

    public loadBlockHour() {
        
        SettingsService.queryForBlockHourSent = true;
        this.getaction(this.settingUrl + "/" + String(this.blockHourSettingID))
            .subscribe((blockHourSetting) => {
                console.log(blockHourSetting);
                let curBlockHour: Date = FormatClass.getDateFromStringHHMMSS(blockHourSetting.Value);

                if (SettingsService.blockHourCached != curBlockHour || SettingsService.blockHourCached == null) {
                    SettingsService.blockHourChanged.emit(curBlockHour);
                    SettingsService.blockHourCached = curBlockHour;
                }
    
            });
    }
    public getblockHourChangedEmitter() {
        return SettingsService.blockHourChanged;
    }
    public changeBlockHour(hours:number, minutes:number) {
        let settingModel = new SettingModel();
        settingModel.Id = 1;
        settingModel.Value = String(hours) + ":" + String(minutes) + ":00";
        return this.postaction(this.updateSettingUrl, settingModel);
    }

}