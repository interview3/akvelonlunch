﻿import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {UserModel} from '../ModelClasses/UserModel';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserService extends HttpHelpers {
    private _allUsersUrl: string = "api/user/allusers";
    private _updateUserUrl: string = "api/user/update";
    private _deleteUserUrl: string = "api/user/delete/";
    private _userRolesUrl:string = "api/user/roles";
    private _usersTotalCashUrl: string = "api/user/totalcash";
    private _getUserUrl: string = "api/user/";
    userFromDisplayEmmiter: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    gridChangeEmitter: EventEmitter<UserModel[]> = new EventEmitter<UserModel[]>();
    userListChangeEmmiter: EventEmitter<UserModel> = new EventEmitter<UserModel>();
    
    private userCached: UserModel = null;
    private userQuerySent: boolean = false;
    private userLoaded: EventEmitter<UserModel> = new EventEmitter<UserModel>();

    constructor(private http: Http){
        super(http);

        this.userLoaded.subscribe((user) => {
            this.onUserLoad(user);
        });
    }

    public getuser(forceLoad: boolean = false): any {
        if (forceLoad || (this.userCached == null)) {
            if (this.userQuerySent == false) {
                this.userQuerySent = true;
                let result = this.getaction(this._getUserUrl);
                result.subscribe((user) => {
                    this.userLoaded.emit(user);
                    this.userQuerySent = false;
                });
            }
            return this.userLoaded;

        } else {
            return Observable.create(observer => {
                observer.next(this.userCached);
                observer.complete();
            });
        }
    }

    private onUserLoad(user: UserModel) {
        this.userCached = user;
    }

    updateUser(user: UserModel) {
        return this.postaction(this._updateUserUrl, user);
    }

    getallusers() {
        return this.getaction(this._allUsersUrl);
    }

    getGridChangeEmitter() {
        return this.gridChangeEmitter;
    }

    getUserListChangeEmmiterr() {
        return this.userListChangeEmmiter;
    }

    emitUpdateGrid(usersList: UserModel[]) {
        this.gridChangeEmitter.emit(usersList);
    }

    emitUpdateGridRow(user: UserModel) {
        this.userListChangeEmmiter.emit(user);
    }

    emitShowUserForm(user: UserModel) {
        this.userFromDisplayEmmiter.emit(user);
    }

    getUserFromDisplayEmmiter() {
        return this.userFromDisplayEmmiter;
    }

    getUserRoles() {
        return this.getaction(this._userRolesUrl);
    }

    getTotalCash() {
        return this.getaction(this._usersTotalCashUrl);
    }

    deleteUser(userId: number) {
        return this.getaction(this._deleteUserUrl + userId);
    }
}