﻿import {EventEmitter, Injectable} from '@angular/core';
import {DishModel} from '../ModelClasses/DishModel';
import {MenuModel} from '../ModelClasses/MenuModel';
import {OrderItemModel} from '../ModelClasses/OrderItemModel';
import {OrderListItemModel} from "../ModelClasses/OrderListItemModel";
import {OrderModel} from '../ModelClasses/OrderModel';
import {FilterModel} from '../ModelClasses/FilterModel';
import {OrderStatuses} from '../ModelClasses/OrderStatuses';
import {Http} from '@angular/http';
import {HttpHelpers} from '../utils/HttpHelpers';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class OrderService extends HttpHelpers {
    orderChanged: EventEmitter<any> = new EventEmitter<any>();
    gridChangeEmitter: EventEmitter<OrderListItemModel[]> = new EventEmitter<OrderListItemModel[]>();
    dishListChangeEmitter: EventEmitter<OrderListItemModel> = new EventEmitter<OrderListItemModel>();

    private orderCached: any = null;
    private orderQuerySent: boolean = false;
    private orderLoaded: EventEmitter<OrderModel> = new EventEmitter<OrderModel>();

    private _orderMakeUrl: string = "api/order/make";
    private _orderUpdateUrl: string = "api/order/update";
    private _orderCancelUrl: string = "api/order/cancel";
    private _getTodayUserOrderUrl: string = "api/order/getTodayUserOrder";
    private _provisionerOrdersUrl: string = "/api/order/byProvisionerId/";
    private _getDishNameUrl: string = "/api/dish/";
    private _usersMadeOrdersUrl: string = "/api/order/usersWhoMadeToday";
    private _getUserOrderUrl: string = "/api/order/todayUserOrder/";
    private _getStatusStringUrl: string = "/api/order/orderstatus/";
     private _getUserOrdersInPeriodUrl: string = "/api/order/ordersInPeriod";
    private _getUserOrderByDate: string = "api/order/userOrderByDate";
    private _getOrderSumsInIntervals: string = "api/order/CostSumsOfTimeSpans";
    private _wereTodayOrdersDelieveriedUrl: string = "/api/order/wereTodayOrdersDelieveried";
    private _getFilteredOrdersUrl: string = "/api/order/filtered";
    private _getOrderByIdUrl: string = "/api/order/byId/";
    private _downloadReportInPeriodUrl: string = "/api/order/download";
    private _notifyAboutDeliveringUrl: string = "/api/order/notifyAboutDelivering";

    constructor(private http: Http) {
        super(http);

        this.orderLoaded.subscribe((order) => {
            this.onOrderLoad(order);
        });
    }

    public getOrder(forceLoad: boolean): any {      
        if (forceLoad || (this.orderCached == null)) {
            if (this.orderQuerySent == false) {
                this.orderQuerySent = true;
                let result = this.getaction(this._getTodayUserOrderUrl);
                result.subscribe((user) => {
                    this.orderLoaded.emit(user);
                    this.orderQuerySent = false;
                });
            }
            return this.orderLoaded;

        } else {
            return Observable.create(observer => {
                observer.next(this.orderCached);
                observer.complete();
            });
        }
    }

    private onOrderLoad(order: OrderModel) {
        this.orderCached = order;
    }

    getOrderChangeEmmiter() {
        return this.orderChanged;
    }
    //TODO send empty event object
    changeOrder() {
        this.orderChanged.emit('');
    }

    makeOrder(order: OrderModel) {
        return this.postaction(this._orderMakeUrl, order);
    }

    updateOrder(order: OrderModel) {
        return this.postaction(this._orderUpdateUrl, order);
    }

    cancelOrder(orderId: number) {
        return this.deleteaction(this._orderCancelUrl + "/" + String(orderId));
    }

    loadTodayUserOrder(forceLoad:boolean = false) {
        return this.getOrder(forceLoad);
    }

    getDishById(dishId: number) {
        return this.getaction(this._getDishNameUrl + dishId);
    }

    getUsersMadeOrdersTodayByProvisionerId(provisionerId: number, start: Date, finish: Date) {
        return this.getaction(this._usersMadeOrdersUrl +
            "?provisionerId=" + provisionerId +
            "&start=" + start.toUTCString() +
            "&finish=" + finish.toUTCString());
    }

    getUserOrdersInPeriod(userId: number, start: Date, finish: Date) {
        return this.getaction(this._getUserOrdersInPeriodUrl +
            "?userId=" + userId +
            "&start=" + start.toUTCString() +
            "&finish=" + finish.toUTCString());
    }

    getUserOrder(userId: number) {
        return this.getaction(this._getUserOrderUrl + userId);
    }

    getUserOrderByDate(userId: number, date:Date) {
        return this.getaction(this._getUserOrderByDate + "?userId=" + userId + "&date=" + date.toUTCString());
    }

    notifyAboutDelivering() {
        return this.getaction(this._notifyAboutDeliveringUrl);
    }
    
    getGridChangeEmitter() {
        return this.gridChangeEmitter;
    }

    emitUpdateGrid(ordersList: OrderListItemModel[]) {
        this.gridChangeEmitter.emit(ordersList);
    }

    getOrderListChangeEmmiterr() {
        return this.dishListChangeEmitter;
    }

    getOrderStatusStringByOrderStatusId(id) {
        return this.getaction(this._getStatusStringUrl + id);
    }

    getOrderSumsInIntervals(dateIntervals: [Date, Date][]) {
        let dateArray: Date[] = [];
    
        for (let i = 0; i < dateIntervals.length; ++i) {
            dateArray.push(dateIntervals[i][0]);
            dateArray.push(dateIntervals[i][1]);
        }
        
        return this.postaction(this._getOrderSumsInIntervals,dateArray);
    }

    wereTodayOrdersDelieveried() {
        return this.getaction(this._wereTodayOrdersDelieveriedUrl);
    }

    getFilteredOrders(provisionerId: number, start: Date, finish: Date) {
        return this.getaction(this._getFilteredOrdersUrl +
            "?provisionerId=" + provisionerId +
            "&start=" + start.toUTCString() +
            "&finish=" + finish.toUTCString());
    }

    getOrderById(orderId: number) {
        return this.getaction(this._getOrderByIdUrl + orderId);
    }

    downloadReportInPeriod(start: Date, finish: Date) {
        var url = this._downloadReportInPeriodUrl +
            "?startDate=" + start.toUTCString() +
            "&endDate=" + finish.toUTCString();
        window.location.href = url;
    }
}