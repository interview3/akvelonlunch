﻿import {Component, Output, EventEmitter} from '@angular/core';
import {CartComponent} from './components/mainScreen/cart/cart.component';
import {MenuComponent} from './components/mainScreen/menu/menu.component';
import {DishModel} from './ModelClasses/DishModel';
import {InfoPanel} from './components/mainScreen/infoPanel/infoPanel.component';
import { DishService } from './services/dish.service';
import { LoginService } from './services/login.service';
import { Subscription }   from 'rxjs/Subscription';
import {LoginComponent} from './components/login/login.component';
import {HTTP_PROVIDERS} from '@angular/http';
import {InteractionService} from './services/interaction.service';
import {InformationService} from './services/information.service';
import {OrderService} from './services/order.service';
import {UserService} from './services/user.service';
import {Router, RouterLink, RouterOutlet, ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector: 'lunch-app',
    templateUrl: './ScriptsApp/mainApp.html',
    directives: [CartComponent, MenuComponent, InfoPanel, LoginComponent, RouterLink,
                RouterOutlet, ROUTER_DIRECTIVES],
    providers: [InteractionService, DishService, LoginService, HTTP_PROVIDERS, InformationService,UserService, OrderService]
})

export class MainComponent {
    subscription: Subscription;

    constructor(private dishService: DishService, private loginService: LoginService, private interactionService: InteractionService) {
        
    }

    ngOnInit() {
        this.subscription = this.dishService.getDishAddToCartEmitter()
            .subscribe(
            dish => {
                console.log(dish);
            });
    }
}