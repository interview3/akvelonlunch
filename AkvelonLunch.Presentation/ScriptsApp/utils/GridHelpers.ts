﻿export class Column {
    constructor(public name: string, public field: string) {}
}

export enum ColumnState {
    Descending = -1,
    Clear,
    Ascending
}

export class GridConfig {

    private numberOfColumn: number = 0;
    private columnState: ColumnState = ColumnState.Clear;
    private filterString: string = "";
    
    private nextState() {

        switch (this.columnState) {
            case ColumnState.Clear:
                return ColumnState.Ascending;
            case ColumnState.Ascending:
                return ColumnState.Descending;
            case ColumnState.Descending:
                return ColumnState.Clear;
            default:
                throw Error("Some error in Column State Switch!");
        }
    }

    public get ActiveColumnState() { return this.columnState;  }

    public set NumberOfActiveColumn(numberOfPress: number) {

        if (this.numberOfColumn === numberOfPress) {
            this.columnState = this.nextState();
        }
        else {
            this.numberOfColumn = numberOfPress;
            this.columnState = ColumnState.Ascending;
        }
    }

    public get NumberOfActiveColumn() { return this.numberOfColumn; }

    public set FilterString(filterStr: string) {
        this.filterString = filterStr.toLowerCase();
    }

    public get FilterString() { return this.filterString; }

    public getStateColumn(numberOfColumn: number) {
        if (numberOfColumn !== this.numberOfColumn)
            return ColumnState.Clear;
        return this.columnState;
    }
}

export class Grid {
    private resultItemsList = new Array<any>();
    private greedConfig: GridConfig = new GridConfig();

    constructor(private columns: Column[],
        private columToFiltering: Column,
        private itemsList: any[],
        private pageSize: number) { }

    private getDataFild(gridItem: any, column: Column)  {
        return gridItem[column.field];
    }

    private getFilterValue(gridItem: any): number {
        return this.getDataFild(gridItem, this.columToFiltering).toLowerCase().indexOf(this.greedConfig.FilterString);
    }
    
    private sortResultItemsList() {
        if (this.greedConfig.ActiveColumnState === ColumnState.Ascending) {
            this.resultItemsList.sort((a, b) => {
                let strA = this.getDataFild(a, this.columns[this.greedConfig.NumberOfActiveColumn]);
                let strB = this.getDataFild(b, this.columns[this.greedConfig.NumberOfActiveColumn]);
                if (strA === strB)
                    return 0;
                return strA < strB ? -1 : 1;
            });
        }
        if (this.greedConfig.ActiveColumnState === ColumnState.Descending) {
            this.resultItemsList.sort((a, b) => {
                let strA = this.getDataFild(a, this.columns[this.greedConfig.NumberOfActiveColumn]);
                let strB = this.getDataFild(b, this.columns[this.greedConfig.NumberOfActiveColumn]);
                if (strA === strB)
                    return 0;
                return strA < strB ? 1 : -1;
            });
        }
    }

    private filterItemsList() {
        this.itemsList.sort((a, b) => {
            let valueA = this.getFilterValue(a);
            let valueB = this.getFilterValue(b);
            if (valueA === valueB) {
                let strA = this.getDataFild(a, this.columToFiltering);
                let strB = this.getDataFild(b, this.columToFiltering);
                if (strA == strB)
                    return 0;
                return strA < strB ? -1 : 1;
            }
            if (valueA === -1)
                return 1;
            if (valueB === -1)
                return -1;
            return valueA < valueB ? -1 : 1;
        });
        this.resultItemsList = new Array<any>();
        for (let i = 0; i < this.itemsList.length; i++) {
            if (this.getFilterValue(this.itemsList[i]) !== -1) {
                this.resultItemsList.push(this.itemsList[i]);
            } else {
                break;
            }
        }
    }

    public onColumnClick(column: Column) {
        for (let i = 0; i < this.columns.length; i++) {
            if (this.columns[i].field === column.field && this.columns[i].name === column.name) {
                this.greedConfig.NumberOfActiveColumn = i;
                this.filterItemsList();
                this.sortResultItemsList();
                break;
            }
        }
    }

    public onFilterStringChange(filterStr: string) {
        this.greedConfig.FilterString = filterStr;
        this.filterItemsList();
        this.sortResultItemsList();
    }

    public get CountOfPages() {
        return Math.ceil(this.resultItemsList.length / this.pageSize);
    }

    public getPage(numberOfPage: number): any[] {
        let pageList = new Array<any>();
        for (let i = numberOfPage * this.pageSize;
            i < (numberOfPage + 1) * this.pageSize && i < this.resultItemsList.length;
            i++) {
            pageList.push(this.resultItemsList[i]);
        }
        return pageList;
    }

    public setItemsListWithExclude(itemsList: any[], excludeList: any[], cmp: (a: any, b: any) => boolean) {
        this.itemsList = new Array<any>();
        itemsList.forEach( (d) => {
            for (let i = 0; i < excludeList.length; i++) {
                if (cmp(d, excludeList[i])) {
                    return;
                }
            }
            this.itemsList.push(d);
        });
        this.filterItemsList();
        this.sortResultItemsList();
    }

    public setItemsList(itemsList: any[]) {
        this.itemsList = itemsList;
        this.filterItemsList();
        this.sortResultItemsList();
    }

    public set PageSize(pageSize: number) {
        this.pageSize = pageSize;
    }

    public getStateColumn(column: Column): ColumnState {
        for (let i = 0; i < this.columns.length; i++) {
            if (this.columns[i].field === column.field && this.columns[i])
              return this.greedConfig.getStateColumn(i);
        }
        return ColumnState.Clear;
    }
}