﻿export class EnumHelpers {

    public static getEnumString(enumName: any, key: any): string
    {
        return enumName[enumName[key]];
    }

    public static getFullName(enumName: any) : string[] {

        let names: string[] = [];

        for (var n in enumName) {
            if (typeof enumName[n] === 'number') names.push(n);
        }

        return names;
    }
}