﻿
export class FormatClass{
    public static getDateFromStringHHMMSS(timeString: string): Date {
        let pIdx = timeString.indexOf(':');
        
        
        let hours = +timeString.substr(0, pIdx);
        let sIdx = timeString.indexOf(':', pIdx + 1);
        
        let minutes = +timeString.substr(pIdx + 1, sIdx - pIdx - 1);
        let date = new Date(2016, 2, 2, hours, minutes);

        return date;
    }
}