﻿using AkvelonLunch.Data.Entities;
using System.Linq;
using System.Collections.Generic;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Models
{
    public class ModelFactory
    {
        #region create/parse user
        public UserModel Create(User client)
        {
            if (client != null)
            {
                return new UserModel()
                {
                    Id = client.Id,
                    Balance = client.Balance,
                    Login = client.Login,
                    FirstName = client.FirstName,
                    SecondName = client.SecondName,
                    Email = client.Email,
                    LastName = client.LastName,
                    GenderId = client.GenderId,
                    RoleId = client.RoleId
                };
            }
            else
            {
                return null;
            }
        }

        public User Parse(UserModel model)
        {
            if (model != null)
            {
                return new User()
                {
                    Balance = model.Balance,
                    Id = model.Id,
                    Login = model.Login,
                    FirstName = model.FirstName,
                    SecondName = model.SecondName,
                    Email = model.Email,
                    LastName = model.LastName,
                    GenderId = model.GenderId,
                    RoleId = model.RoleId
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse dish type

        public DishTypeModel Create(DishType dishType)
        {
            if (dishType != null)
            {
                return new DishTypeModel()
                {
                    Id = dishType.Id,
                    Name = dishType.Name
                };
            }
            return null;
        }

        public DishType Parse(DishTypeModel dishTypeModel)
        {
            if (dishTypeModel != null)
            {
                return new DishType()
                {
                    Id = dishTypeModel.Id,
                    Name = dishTypeModel.Name
                };
            }
            return null;
        }

        #endregion

        #region create/parse dish
        public DishModel Create(Dish dish)
        {
            if (dish != null)
            {
                return new DishModel
                {
                    Id = dish.Id,
                    Name = dish.Name,
                    Description = dish.Description,
                    Cost = dish.Cost,
                    DishType = Create(dish.DishType),
                    Provisioner = Create(dish.Provisioner)
                };
            }
            else
            {
                return null;
            }
        }

        public Dish Parse(DishModel model)
        {
            if (model != null)
            {
                return new Dish()
                {
                    Name = model.Name,
                    Description = model.Description,
                    Id = model.Id,
                    Cost = model.Cost,
                    DishTypeId = model.DishType.Id,
                    DishType = null,
                    Provisioner = null,
                    ProvisionerId = model.Provisioner.Id
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse menu
        public MenuModel Create(Menu menu)
        {
            if (menu != null)
            {
                var menuModel = new MenuModel() { Id = menu.Id, CreatorId = menu.CreatorId, DateToShow = menu.DateToShow };
                if (menu.Dishes != null)
                {
                    List<Dish> dishes = menu.Dishes.ToList<Dish>();
                    foreach (Dish dish in dishes)
                        menuModel.Dishes.Add(Create(dish));
                }
                return menuModel;
            }
            else
            {
                return null;
            }
        }

        public Menu Parse(MenuModel model)
        {
            if (model != null)
            {
                var menu = new Menu() { Id = model.Id, CreatorId = model.CreatorId, DateToShow = model.DateToShow };
                menu.Dishes = new List<Dish>();
                if (model.Dishes != null)
                {
                    List<DishModel> dishModelList = model.Dishes.ToList<DishModel>();
                    foreach (DishModel dishModel in dishModelList)
                        menu.Dishes.Add(Parse(dishModel));
                }
                return menu;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse order
        public OrderModel Create(Order order)
        {
            if (order != null)
            {
                var orderModel = new OrderModel()
                {
                    Id = order.Id,
                    Cost = order.Cost,
                    OrderStatusId = order.OrderStatusId,
                    OrderTime = order.OrderTime,
                    UserId = order.UserId,
                    User = Create(order.User),
                    MenuId = order.MenuId,
                };
                if (order.OrderItems != null)
                {
                    List<OrderItem> itemList = order.OrderItems.ToList<OrderItem>();
                    foreach (OrderItem item in itemList)
                        orderModel.Items.Add(Create(item));
                }
                return orderModel;
            }
            else
            {
                return null;
            }
        }

        public Order Parse(OrderModel model)
        {
            if (model != null)
            {
                var order = new Order();
                order.Id = model.Id;
                order.UserId = model.UserId;
                order.Cost = model.Cost;
                order.OrderStatusId = model.OrderStatusId;
                order.OrderTime = model.OrderTime;
                order.MenuId = model.MenuId;

                if (model.Items != null)
                {
                    List<OrderItemModel> itemModelList = model.Items.ToList<OrderItemModel>();
                    order.OrderItems = new List<OrderItem>();
                    foreach (OrderItemModel itemModel in itemModelList)
                    {
                        var orderItem = Parse(itemModel);
                        order.OrderItems.Add(orderItem);
                    }
                }
                return order;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse orderItem
        public OrderItemModel Create(OrderItem item)
        {
            if (item != null)
            {
                return new OrderItemModel()
                {
                    Id = item.Id,
                    OrderId = item.OrderId,
                    Count = item.Count,
                    Dish = Create(item.Dish),
                    DishId = item.DishId
                };
            }
            else
            {
                return null;
            }
        }

        public OrderItem Parse(OrderItemModel model)
        {
            if (model != null)
            {
                return new OrderItem()
                {
                    Id = model.Id,
                    OrderId = model.OrderId,
                    Count = model.Count,
                    Dish = Parse(model.Dish),
                    DishId = model.DishId
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse Provisioner
        public ProvisionerModel Create(Provisioner provisioner)
        {
            if (provisioner != null)
            {
                var provisionerModel = new ProvisionerModel()
                {
                    Address = provisioner.Address,
                    ContactPersonName = provisioner.ContactPersonName,
                    Email = provisioner.Email,
                    Id = provisioner.Id,
                    Phone = provisioner.Phone,
                    SupplierName = provisioner.SupplierName,
                    Discount = Create(provisioner.Discount)
                };
                // This code is cause of StackOverflow exceprion. Dish -> Provisioner -> Dish -> ...
                /*
                if (provisioner.Dishes != null)
                {
                    List<Dish> dishes = provisioner.Dishes.ToList<Dish>();
                    foreach (Dish dish in dishes)
                        provisionerModel.Dishes.Add(Create(dish));
                }
                */
                return provisionerModel;
            }
            else
            {
                return null;
            }
        }

        public Provisioner Parse(ProvisionerModel model)
        {
            if (model != null)
            {
                var provisioner = new Provisioner()
                {
                    Address = model.Address,
                    ContactPersonName = model.ContactPersonName,
                    Email = model.Email,
                    Id = model.Id,
                    Phone = model.Phone,
                    SupplierName = model.SupplierName,
                    Discount = Parse(model.Discount)
                };
                if (model.Dishes != null)
                {
                    List<DishModel> dishModels = model.Dishes.ToList<DishModel>();
                    foreach (DishModel dishModel in dishModels)
                        provisioner.Dishes.Add(Parse(dishModel));
                }
                return provisioner;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create lists

        public IList<DishTypeModel> CreateListOfDishTypeModels(IList<DishType> types)
        {
            if (types != null)
            {
                IList<DishTypeModel> list = new List<DishTypeModel>();
                foreach (var t in types)
                {
                    list.Add(Create(t));
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public ICollection<DishModel> CreateListOfDishes(ICollection<Dish> dishes)
        {
            if (dishes != null)
            {
                var dishModel = new List<DishModel>();
                foreach (Dish dish in dishes)
                {
                    dishModel.Add(Create(dish));
                }
                return dishModel;
            }
            else
            {
                return null;
            }
        }

        public ICollection<MenuModel> CreateListOfMenu(ICollection<Menu> allMenu)
        {
            if (allMenu != null)
            {
                var menuModel = new List<MenuModel>();
                foreach (Menu oneOfMenu in allMenu)
                {
                    menuModel.Add(Create(oneOfMenu));
                }
                return menuModel;
            }
            else
            {
                return null;
            }
        }

        public ICollection<OrderModel> CreateListOfOrders(ICollection<Order> allOrders)
        {
            if (allOrders != null)
            {
                var orderModels = new List<OrderModel>();
                foreach (Order order in allOrders)
                {
                    orderModels.Add(Create(order));
                }
                return orderModels;
            }
            else
            {
                return null;
            }
        }

        public ICollection<ProvisionerModel> CreateListOfProvisioners(ICollection<Provisioner> allProvisioners)
        {
            if (allProvisioners != null)
            {
                var provisionersModelList = new List<ProvisionerModel>();
                foreach (Provisioner provisioner in allProvisioners)
                {
                    provisionersModelList.Add(Create(provisioner));
                }
                return provisionersModelList;
            }
            else
            {
                return null;
            }
        }

        public ICollection<UserModel> CreateListOfUsers(ICollection<User> allUsers)
        {
            if (allUsers != null)
            {
                var usersModelList = new List<UserModel>();
                foreach (User user in allUsers)
                {
                    usersModelList.Add(Create(user));
                }
                return usersModelList;
            }
            else
            {
                return null;
            }
        }

        public ICollection<MoneyTransactionLogModel> CreateListOfMoneyTransactionLogs(
            ICollection<MoneyTransactionLog> logs)
        {
            if (logs != null)
            {
                var modelsList = new List<MoneyTransactionLogModel>();
                foreach (var log in logs)
                {
                    modelsList.Add(Create(log));
                }
                return modelsList;
            }
            else
            {
                return null;
            }
        }

        public ICollection<UserSettingModel> CreateListOfUserSettings(
            ICollection<UserSetting> settings)
        {
            if (settings != null)
            {
                var modelsList = new List<UserSettingModel>();
                foreach (var setting in settings)
                {
                    modelsList.Add(Create(setting));
                }
                return modelsList;
            }
            else
            {
                return null;
            }
        }

        public ICollection<UserSetting> ParseListOfUserSettingModels(
            ICollection<UserSettingModel> settingModels)
        {
            if (settingModels != null)
            {
                var settingList = new List<UserSetting>();
                foreach (var setting in settingModels)
                {
                    settingList.Add(Parse(setting));
                }
                return settingList;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse setting
        public SettingModel Create(Setting setting)
        {
            if (setting != null)
            {
                return new SettingModel()
                {
                    Id = setting.Id,
                    Name = setting.Name,
                    Value = setting.Value,
                    Description = setting.Description,
                };
            }
            else
            {
                return null;
            }
        }

        public Setting Parse(SettingModel model)
        {
            if (model != null)
            {
                return new Setting()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Value = model.Value,
                    Description = model.Description,
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse MoneyTransactionOperation
        public MoneyTransactionOperationModel Create(MoneyTransactionOperation operation)
        {
            if (operation != null)
            {
                return new MoneyTransactionOperationModel()
                {
                    Id = operation.Id,
                    Description = operation.Description,
                    Name = operation.Name
                };
            }
            else
            {
                return null;
            }
        }

        public MoneyTransactionOperation Parse(MoneyTransactionOperationModel model)
        {
            if (model != null)
            {
                return new MoneyTransactionOperation()
                {
                    Id = model.Id,
                    Description = model.Description,
                    Name = model.Name
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse MoneyTransactionLog
        public MoneyTransactionLogModel Create(MoneyTransactionLog log)
        {
            if (log != null)
            {
                return new MoneyTransactionLogModel()
                {
                    TransactionId = log.TransactionId,
                    Operation = Create(log.Operation),
                    OperationId = log.OperationId,
                    TransactionTime = log.TransactionTime,
                    InitiatorUserId = log.InitiatorUserId,
                    InitiatorUser = Create(log.InitiatorUser),
                    Comment = log.Comment,
                    AffectedUserId = log.AffectedUserId,
                    AffectedUser = Create(log.AffectedUser),
                    BalanceAfter = log.BalanceAfter,
                    BalanceBefore = log.BalanceBefore,
                    OrderId = log.OrderId
                };
            }
            else
            {
                return null;
            }
        }

        public MoneyTransactionLog Parse(MoneyTransactionLogModel model)
        {
            if (model != null)
            {
                return new MoneyTransactionLog()
                {
                    TransactionId = model.TransactionId,
                    Operation = Parse(model.Operation),
                    OperationId = model.OperationId,
                    TransactionTime = model.TransactionTime,
                    InitiatorUserId = model.InitiatorUserId,
                    InitiatorUser = Parse(model.InitiatorUser),
                    Comment = model.Comment,
                    AffectedUserId = model.AffectedUserId,
                    AffectedUser = Parse(model.AffectedUser),
                    BalanceAfter = model.BalanceAfter,
                    BalanceBefore = model.BalanceBefore,
                    OrderId = model.OrderId
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse UserSetting
        public UserSettingModel Create(UserSetting setting)
        {
            if (setting != null)
            {
                return new UserSettingModel()
                {
                    Id = setting.Id,
                    SettingId = setting.SettingId,
                    UserId = setting.UserId,
                    Value = setting.Value
                };
            }
            else
            {
                return null;
            }
        }

        public UserSetting Parse(UserSettingModel model)
        {
            if (model != null)
            {
                return new UserSetting()
                {
                    Id = model.Id,
                    SettingId = model.SettingId,
                    UserId = model.UserId,
                    Value = model.Value
                };
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region create/parse Discount
        public DiscountModel Create(Discount discount)
        {
            if (discount != null)
            {
                return new DiscountModel()
                {
                    DiscountSum = discount.DiscountSum,
                    ProvisionerId = discount.ProvisionerId,
                    NumberOfDishes = discount.NumberOfDishes
                };
            }
            return null;
        }

        public Discount Parse(DiscountModel model)
        {
            if (model != null)
            {
                return new Discount()
                {
                    DiscountSum = model.DiscountSum,
                    ProvisionerId = model.ProvisionerId,
                    NumberOfDishes = model.NumberOfDishes
                };
            }
            return null;
        }

        #endregion

        #region create OrderRow

        public OrderRowModel CreateRow(Order order)
        {
            OrderRowModel model = new OrderRowModel();
            model.Id = order.Id;
            model.Order = Create(order);
            model.OrderDate = order.OrderTime;
            model.UserBalance = order.User.Balance;
            model.UserName = order.User.FirstName + " " + order.User.LastName;
            return model;
        }
        #endregion
    }
}