﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AkvelonLunch.Models
{
    public class FilterModel
    {
        public int ProvisionerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
    }
}