﻿using System;

namespace AkvelonLunch.Models
{
    public class MoneyTransactionLogModel
    {
        public int? TransactionId { get; set; }

        public int AffectedUserId { get; set; }

        public UserModel AffectedUser { get; set; }

        public int InitiatorUserId { get; set; }

        public UserModel InitiatorUser { get; set; }

        public int OperationId { get; set; }

        public MoneyTransactionOperationModel Operation { get; set; }

        public DateTime TransactionTime { get; set; }

        public decimal BalanceBefore { get; set; }

        public decimal BalanceAfter { get; set; }

        public string Comment { get; set; }

        public int? OrderId { get; set; }
    }
}