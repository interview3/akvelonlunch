﻿using System;
using System.Collections.Generic;

namespace AkvelonLunch.Models
{
    public class MenuModel
    {
        public MenuModel()
        {
            Dishes = new List<DishModel>();
        }
        public int? Id { get; set; }
        public int CreatorId { get; set; }
        public ICollection<DishModel> Dishes { get; set; }
        public DateTime DateToShow { get; set; }
    }
}