﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AkvelonLunch.Models
{
    public class OrderRowModel
    {
        public int? Id { get; set; }
        public string UserName { get; set; }
        public decimal UserBalance { get; set; }
        public OrderModel Order { get; set; }
        public DateTime OrderDate { get; set; }
    }
}