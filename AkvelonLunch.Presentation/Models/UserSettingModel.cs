﻿namespace AkvelonLunch.Models
{
    public class UserSettingModel
    {
        public int? Id { get; set; }

        public int UserId { get; set; }

        public int SettingId { get; set; }

        public string Value { get; set; }
    }
}