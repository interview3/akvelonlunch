﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AkvelonLunch.Models
{
    public class DishTypeModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}