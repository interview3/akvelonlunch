﻿using System;
using System.Collections.Generic;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Models
{
    public class OrderModel
    {
        public OrderModel()
        {
            Items = new List<OrderItemModel>();
        }
        public int? Id { get; set; }
        public decimal Cost { get; set; }
        public ICollection<OrderItemModel> Items { get; set; }
        public DateTime OrderTime { get; set; }
        public int UserId { get; set; }
        public UserModel User { get; set; }
        public int OrderStatusId { get; set; }
        public int MenuId { get; set; }
    }
}