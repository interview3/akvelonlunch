﻿using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Models
{
    public class DishModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public ProvisionerModel Provisioner { get; set; }
        public DishTypeModel DishType { get; set; }
       
    }
}