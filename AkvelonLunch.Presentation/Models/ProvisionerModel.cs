﻿using System.Collections.Generic;

namespace AkvelonLunch.Models
{
    public class ProvisionerModel
    {
        public ProvisionerModel()
        {
            Dishes = new List<DishModel>();
        }
        public int? Id { get; set; }
        public string SupplierName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPersonName { get; set; }
        public ICollection<DishModel> Dishes { get; set; }
        public DiscountModel Discount { get; set; }
    }
}