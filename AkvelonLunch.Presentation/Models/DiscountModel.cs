﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AkvelonLunch.Models
{
    public class DiscountModel
    {
        public int? ProvisionerId { get; set; }
        public int NumberOfDishes { get; set; }
        public decimal DiscountSum { get; set; }
    }
}