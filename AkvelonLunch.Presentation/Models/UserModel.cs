﻿namespace AkvelonLunch.Models
{
    public class UserModel
    {
        public int? Id { get; set; }
        public string Login { get; set; }
        public string FullName => string.Concat(FirstName, " ", LastName);
        public decimal Balance { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; } 
        public string LastName { get; set; }
        public string Email { get; set; }
        public int GenderId { get; set; }
        public int RoleId { get; set; }
    }
}