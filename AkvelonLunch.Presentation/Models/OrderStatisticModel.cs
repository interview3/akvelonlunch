﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AkvelonLunch.Models
{
    public class OrderStatisticModel
    {
        public int TotalDishesCountToday { get; set; }
        public decimal TotalOrderCostSumToday { get; set; }
    }
}
