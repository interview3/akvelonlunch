﻿namespace AkvelonLunch.Models
{
    public class OrderItemModel
    {
        public int? Id { get; set; }
        public DishModel Dish { get; set; }
        public int DishId { get; set; }
        public int? OrderId { get; set; }
        public int Count { get; set; }
    }
}