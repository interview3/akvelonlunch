﻿using System;
using AkvelonLunch.Tasks;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AkvelonLunch.Utilities;

namespace AkvelonLunch
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            try
            {
                new Thread(() => (new EmailReminder()).ActivateNotifications())
                {
                    IsBackground = true
                }.Start();
            } catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex.ToString());
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_PostAuthorizeRequest()
        {
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            Exception exc = Server.GetLastError();

            GlobalLogger.GetInstance().Error(exc.ToString());

            // Clear the error from the server
            Server.ClearError();
        }
    }
}
