﻿using System;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;

namespace AkvelonLunch.Auth
{
    public class Credentials
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public bool Persistent { get; set; }

        public static Credentials GetCredentialsFromHttpRequestMessage(HttpRequestMessage actionContext)
        {
            string creds = null;
            var auth = actionContext.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
            {
                creds = auth.Parameter;
            }
            if (string.IsNullOrEmpty(creds))
            {
                return null;
            }
            try
            {
                byte[] s1 = Convert.FromBase64String(creds);
                string s2 = Encoding.Default.GetString(s1);
                JObject jo = JObject.Parse(s2);

                string un = jo["username"].ToObject<string>();
                string pd = jo["password"].ToObject<string>();
                bool pt = jo["persistent"].ToObject<bool>();
                
                return new Credentials { Login = un, Password = pd, Persistent = pt };
            }
            // TODO Rewite Exp handler
            catch (Exception)
            {
                return null;
            }
        }
    }
}