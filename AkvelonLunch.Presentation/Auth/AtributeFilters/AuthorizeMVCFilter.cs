﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using AkvelonLunch.Auth.TicketChecker;

namespace AkvelonLunch.Auth.AtributeFilters
{
    public class AuthorizeMVCFilter : AuthorizeAttribute
    {
        private readonly ITicketChecker _cookieChecker;

        private string[] GetUserRolesFromContext(HttpCookie cookie)
        {
            if (cookie != null)
            {
                return _cookieChecker.GetUserRoles(cookie.Value);
            }
            return null;
        }


        private bool CheckUserRoles(string[] userRoles)
        {
            var fileterRoles = Roles.Split(',');

            bool key;

            foreach (var fileterRole in fileterRoles)
            {
                key = false;
                foreach (var userRole in userRoles)
                {
                    if (userRole == fileterRole)
                    {
                        key = true;
                    }
                }
                if (!key)
                    return false;
            }
            return true;
        }

        public AuthorizeMVCFilter(params string[] roles)
        {
            Roles = string.Join(",", roles);

            _cookieChecker = new TikcetChecker();
        }

        private HttpCookie GetCookieFromAuthorizationContext(AuthorizationContext filterContext)
        {
            var cookies = filterContext.HttpContext.Request.Cookies;

            var cookieName = ConfigurationManager.AppSettings["AuthenticationCookieName"];

            return cookies[cookieName];
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var UserRoles = this.GetUserRolesFromContext(GetCookieFromAuthorizationContext(filterContext));

            if (UserRoles == null)
            {
                HandleUnauthorizedRequest(filterContext);
            }
            else if (!this.CheckUserRoles(UserRoles))
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("~/Home/Login");
        }
    }
}