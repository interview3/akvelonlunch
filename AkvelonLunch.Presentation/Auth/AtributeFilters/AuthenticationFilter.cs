﻿using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using AkvelonLunch.Auth.TicketChecker;

namespace AkvelonLunch.Auth.AtributeFilters
{
    /// <summary>
    /// Filter for signing in. Login information is stored in the Authorization HTTP header.
    /// </summary>
    public class AuthenticationFilter : AuthorizeAttribute
    {
        private readonly ITicketChecker _cookieChecker;

        private string[] GetUserRolesFromContext(HttpActionContext actionContext)
        {
            var headers = actionContext.Request.Headers;

            var cookieName = ConfigurationManager.AppSettings["AuthenticationCookieName"];

            var cookie = headers.GetCookies().Select(c => c[cookieName]).FirstOrDefault();

            if (cookie != null)
            {
                return _cookieChecker.GetUserRoles(cookie.Value);
            }
            return null;
        }

        private bool CheckUserRoles(string[] userRoles)
        {
            var fileterRoles = Roles.Split(',');

            bool key;

            foreach (var fileterRole in fileterRoles)
            {
                key = false;
                foreach (var userRole in userRoles)
                {
                    if (userRole == fileterRole)
                    {
                        key = true;
                    }
                }
                if (!key)
                    return false;
            }
            return true;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var UserRoles = this.GetUserRolesFromContext(actionContext);

            if (UserRoles == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                this.HandleUnauthorizedRequest(actionContext);
            }
            else if (!this.CheckUserRoles(UserRoles))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                this.HandleUnauthorizedRequest(actionContext);
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            
        }

        public AuthenticationFilter(params string[] roles)
        {
            Roles = string.Join(",", roles);

            _cookieChecker = new TikcetChecker();
        }
    }
}
