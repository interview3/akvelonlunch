﻿namespace AkvelonLunch.Auth
{
    public static class Roles
    {
        public const string Guest = "None";
        public const string Client = "Client";
        public const string Supplier = "Supplier";
        public const string Administrator = "Administrator";
    }
}