﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Data.Repositories.UserRepository;

namespace AkvelonLunch.Auth.Authentification
{
    public class CustomAuthentication : IAuthentication
    {
        public HttpContext HttpContext { get; set; }

        private IUserRepository _userRepository;

        private readonly string _cookieName = ConfigurationManager.AppSettings["AuthenticationCookieName"];

        private readonly string _domain = ConfigurationManager.AppSettings["DomainName"];


        public CustomAuthentication(HttpContext httpContext)
        {
            HttpContext = httpContext;
        }

        public CustomAuthentication(HttpContext httpContext, IUserRepository userRepository): this(httpContext)
        {
            _userRepository = userRepository;
        }

        private static bool VerifyIfUserDomainCredentialsIsCorrect(Credentials creds, string domain)
        {
            if (creds != null)
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
                {
                    return pc.ValidateCredentials(creds.Login, creds.Password);
                }
            }
            return false;
        }

        private User GetUserFromDomain(Credentials creds, string domain)
        {
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
            {
                UserPrincipal domainUser = UserPrincipal.FindByIdentity(pc, creds.Login);
                return new User
                {
                    FirstName = domainUser.GivenName,
                    SecondName = domainUser.MiddleName ?? String.Empty,
                    LastName = domainUser.Surname,
                    Balance = 0.0m,
                    GenderId = (int)GenderEnum.Male,
                    Email = domainUser.EmailAddress,
                    RoleId = (int)RoleEnum.Client,
                    Login = domainUser.SamAccountName ?? domainUser.UserPrincipalName.Split('@')[0]
                };
            }
        }

        public bool Login(Credentials credentials)
        {
            if (!VerifyIfUserDomainCredentialsIsCorrect(credentials, _domain))
            {
                return false;
            }

            User domainUser = GetUserFromDomain(credentials, _domain);
            if (domainUser == null)
            {
                return false;
            }

            credentials.Login = domainUser.Login; //use well formatted user login

            User dbUser = _userRepository.GetUserByLogin(domainUser.Login).Entity;
            if (dbUser == null)
            {
                _userRepository.Create(domainUser);
                dbUser = domainUser;
            }

            if (dbUser.IsDeleted)
            {
                return false;
            }

            CreateCookie(credentials, dbUser);

            return true;
        }

        public void LogOut()
        {
            var httpCookie = HttpContext.Response.Cookies[_cookieName];
            if (httpCookie != null)
            {
                HttpContext.Response.Cookies[_cookieName].Value = string.Empty;
            }
        }

        private void CreateCookie(Credentials credentials, User user)
        {
            string userRoles = String.Empty; //TODO implement Roles and UserRoles database tables 

            switch (user.RoleId)
            {
                case (int)RoleEnum.Admin:
                    userRoles = "Administrator,Client";
                    break;
                case (int)RoleEnum.Client:
                    userRoles = "Client";
                    break;
            }

            string userData = "?userRoles=" + userRoles + "&userId=" + user.Id.ToString();
            var ticket = new FormsAuthenticationTicket(
                  1,
                  credentials.Login,
                  DateTime.Now,
                  DateTime.Now.Add((credentials.Persistent) ? new TimeSpan(14, 0, 0, 0) : FormsAuthentication.Timeout),
                  credentials.Persistent,
                  userData,
                  FormsAuthentication.FormsCookiePath);
            
            // Encrypt the ticket.
            var encTicket = FormsAuthentication.Encrypt(ticket);
            
            // Create the cookie.
            var AuthCookie = new HttpCookie(_cookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add((credentials.Persistent) ? new TimeSpan(14, 0, 0, 0) : FormsAuthentication.Timeout)
            };
            HttpContext.Response.Cookies.Set(AuthCookie);
        }
    }
}