﻿using System.Web;

namespace AkvelonLunch.Auth.Authentification
{
    interface IAuthentication
    {
        HttpContext HttpContext { get; set; }

        bool Login(Credentials credentials);

        void LogOut();

        //IPrincipal CurrentUser { get;  }
    }
}
