﻿using System.Web.Security;

namespace AkvelonLunch.Auth.TicketChecker
{
    interface ITicketChecker
    {
        string GetTicketName(string cookieValue);

        string GetTicketUserData(string cookieValue);

        int GetUserId(string cookieValue);
        string[] GetUserRoles(string cookieValue);

        FormsAuthenticationTicket GeTicket(string cookieValue);
    }
}
