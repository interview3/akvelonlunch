﻿using System;
using System.Linq.Expressions;
using System.Web.Security;

namespace AkvelonLunch.Auth.TicketChecker
{
    public class TikcetChecker : ITicketChecker
    {
        public FormsAuthenticationTicket GeTicket(string cookieValue)
        {
            string value = cookieValue.Split('&')[0];
            try
            {
                var ticket = FormsAuthentication.Decrypt(value);
                return ticket;
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        public string GetTicketName(string cookieValue)
        {
            var ticket = GeTicket(cookieValue);
            return ticket != null ? ticket.Name : String.Empty;
        }

        public string GetTicketUserData(string cookieValue)
        {
            var ticket = GeTicket(cookieValue);
            return ticket != null ? ticket.UserData : String.Empty;
        }

        public int GetUserId(string cookieValue)
        {
            var values = GetTicketUserData(cookieValue);
            var key = "userId=";
            string[] userData = values.Split(new string[] {"?", "&"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string data in userData)
            {
                if (data.StartsWith(key))
                {
                    var userId = data.Substring(key.Length);
                    int id = -1;
                    if (Int32.TryParse(userId, out id))
                        return id;
                }
            }
            return -1;
        }

        public string[] GetUserRoles(string cookieValue)
        {
            var values = GetTicketUserData(cookieValue);
            var key = "userRoles=";
            string[] userData = values.Split(new string[] {"?", "&"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string data in userData)
            {
                if (data.StartsWith(key))
                {
                    var userRoles = data.Substring(key.Length);
                    return userRoles.Split(',');
                }
            }
            return new string[] {};
        }
    }
}