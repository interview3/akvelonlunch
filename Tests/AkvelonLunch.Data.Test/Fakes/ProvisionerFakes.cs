﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class ProvisionerFakes
    {
        public static Provisioner GetProvisioner()
        {
            return new Provisioner
            {
                Id = null,
                Address = "ул.Пушкина",
                SupplierName = "ООО Пирожки",
                Phone = "+71005008888",
                Email = "pirozhki@mail.ru",
                ContactPersonName = "Василий Пупкин",
                IsDeleted = false,
                Discount = new Discount()
            };
        }
    }
}
