﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class MenuFakes
    {
        public static Menu GetMenu()
        {
            return new Menu {Id = null, DateToShow = DateTime.Now, Dishes = new List<Dish>()};
        }
        
    }
}
