﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class UserFakes
    {
        public static User GetUser(int index)
        {
            return new List<User>
            {
                new User {Id = null, Login = Guid.NewGuid().ToString(), FirstName = "another string", SecondName = "one more string", LastName = "and another", Balance = 200.0m, Email = "dsfb@akvelon.com",
                    GenderId = (int)GenderEnum.Female, RoleId = (int)RoleEnum.Admin, IsDeleted = false},
                new User
                {
                    Id = null, Login  = Guid.NewGuid().ToString(), FirstName = "Vadim", SecondName = "Ivanov", LastName = "pfff", Balance = 88.0m, Email = "herr@akvelon.com",
                    GenderId = (int)GenderEnum.Male, RoleId = (int)RoleEnum.Admin, IsDeleted = false},
                new User
                {
                    Id = null, Login  = Guid.NewGuid().ToString(), FirstName = "Ivan", SecondName = "Durak", LastName = "rrrr", Balance = 200.0m, Email = "herr@akvelon.com",
                    GenderId = (int)GenderEnum.Male, RoleId = (int)RoleEnum.Admin, IsDeleted = false},
                new User
                {
                    Id = null, Login  = Guid.NewGuid().ToString(), FirstName = "Peter", SecondName = "Anon", LastName = "kkkkf", Balance = 200.0m, Email = "herr2@akvelon.com",
                    GenderId = (int)GenderEnum.Male, RoleId = (int)RoleEnum.Admin, IsDeleted = false}
            }[index];
        } 
    }
}
