﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class OrderFakes
    {
        
        public static Order GetOrder()
        {
            return new Order { Id = null, OrderTime = DateTime.Now, UserId = 3, OrderItems = new List<OrderItem>(), OrderStatusId = (int)OrderStatusesEnum.Pending, IsDeleted = false};
        }
        
    }
}
