﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class DiscountFakes
    {
        public static Discount GetDiscount(int index)
        {
            return new List<Discount>
            {
                new Discount {DiscountSum = 20.0m, NumberOfDishes = 3},
                new Discount {NumberOfDishes = 0, DiscountSum = 0.0m}
            }[index - 1];
        }
    }
}
