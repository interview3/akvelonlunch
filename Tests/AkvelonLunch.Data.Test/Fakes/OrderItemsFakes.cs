﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class OrderItemsFakes
    {
        public static OrderItem GetOrderItem(int id)
        {
            return new List<OrderItem>
        {
            new OrderItem {Id = null, DishId = 10, OrderId=0, Count=2 },
            new OrderItem {Id= null,  DishId = 2, OrderId = 2, Count = 2}
        }[id - 1];
        }

    }
}
