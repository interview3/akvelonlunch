﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using NUnit.Framework;

namespace AkvelonLunch.Data.Test.Fakes
{
    internal class DishFakes
    {
        public static Dish GetDish(int index)
        {
            return new List<Dish>
            {
           new Dish { Id = null, Name = "Чоткая баланда", Cost = 0.1m, Description = "ням-ням", WeightInGramms = 150, DishPreviewFileName = "нуль", DishTypeId = (int)DishTypeEnum.Soup},
           new Dish { Id = null, Name = "Супец по-пацански1", Cost = 1.2m, Description = "ом3213номном", WeightInGramms = 150, DishPreviewFileName = "один", DishTypeId = (int)DishTypeEnum.Soup },
           new Dish { Id = null, Name = "Супец по-ersf", Cost = 50m, Description = "омномнw4qerом", WeightInGramms = 1150, DishPreviewFileName = "один", DishTypeId = (int)DishTypeEnum.Soup },
           new Dish { Id = null, Name = "fsdf по-dsf", Cost =50m, Description = "омноtsdtygмном", WeightInGramms = 1250, DishPreviewFileName = "один", DishTypeId = (int)DishTypeEnum.Soup },
           new Dish { Id = null, Name = "jhty по-fghj", Cost =50m, Description = "омноrserмном", WeightInGramms = 1540, DishPreviewFileName = "один", DishTypeId = (int)DishTypeEnum.Soup },
            }[index];
        }
    }
}
