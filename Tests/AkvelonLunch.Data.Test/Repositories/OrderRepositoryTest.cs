﻿using System;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Enums;
using AkvelonLunch.Data.Repositories.DishRepository;
using AkvelonLunch.Data.Repositories.MenuRepository;
using AkvelonLunch.Data.Repositories.OrderRepository;
using AkvelonLunch.Data.Repositories.ProvisionerRepository;
using AkvelonLunch.Data.Repositories.SettingRepository;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Data.Test.Fakes;
using NUnit.Framework;

namespace AkvelonLunch.Data.Test.Repositories
{
    [TestFixture]
    public class OrderRepositoryTest
    {

        [Test]
        public void CreateOrderBeforeBlockingHour()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu!=null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                var createdProvisionerId = provisionerRepository.Create(provisioner).Entity.Value;


                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = createdProvisionerId;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                order.OrderItems.Add(new OrderItem
                {
                    DishId = dish.Id.Value,
                    Count = 2,
                });

                //Act
                if (!MoveBlockingHourForward())
                    throw new Exception("Did not updated blocking hour (+1 hour)");
                var order_os = orderRepository.Create(order);
                ResetBlockingHourToDefault();

                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, order_os.StatusCode, order_os.BuildErrorMessageString());
            }
        }

        [Test]
        public void CreateOrderAfterBlockingHour()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                var createdProvisionerId = provisionerRepository.Create(provisioner).Entity;

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = createdProvisionerId;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                order.OrderItems.Add(new OrderItem
                {
                    DishId = dish.Id.Value,
                    Count = 2,
                });

                //Act
                if (!this.MoveBlockingHourAgo())
                    throw new Exception("Did not updated blocking hour (+1 hour)");
                var orderOs = orderRepository.Create(order);
                ResetBlockingHourToDefault();

                //Assert
                Assert.AreEqual(DalOperationStatusCode.Error, orderOs.StatusCode, orderOs.BuildErrorMessageString());
            }
        }


        private bool MoveBlockingHourAgo()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                var settingsRepo = new SettingRepository(akvelonLunchContext);

                var blockSetting = settingsRepo.GetSettingById(1).Entity;
                var today = DateTime.Now;
                var newBlockingHour = new TimeSpan(today.Hour - 1, today.Minute, today.Second);
                blockSetting.Value = newBlockingHour.ToString();
                if (settingsRepo.Update(blockSetting).Entity)
                    return true;
                return false;
            }
        }

        private bool MoveBlockingHourForward()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                var settingRepository = new SettingRepository(akvelonLunchContext);
                var blockSetting = settingRepository.GetSettingById(1).Entity;

                var today = DateTime.Now;
                var newBlockingHour = new TimeSpan(today.Hour + 1, today.Minute, today.Second);
                blockSetting.Value = newBlockingHour.ToString();
                if (settingRepository.Update(blockSetting).Entity)
                    return true;
                return false;
            }
        }

        private bool ResetBlockingHourToDefault()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                var settingRepository = new SettingRepository(akvelonLunchContext);
                var blockSetting = settingRepository.GetSettingById(1).Entity;

                var normalBlockingHour = new TimeSpan(19, 59, 59);
                blockSetting.Value = normalBlockingHour.ToString();
                if (settingRepository.Update(blockSetting).Entity) return true;
                return false;
            }
        }

        [Test]
        public void CreateInvalidOrder()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                var createdProvisionerId = provisionerRepository.Create(provisioner).Entity.Value;

                var dish1 = DishFakes.GetDish(1);
                dish1.ProvisionerId = createdProvisionerId;
                dishRepository.Create(dish1);

                var dish2 = DishFakes.GetDish(2);
                dish2.ProvisionerId = createdProvisionerId;
                dishRepository.Create(dish2);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish1);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                order.OrderItems.Add(new OrderItem { DishId = dish2.Id.Value, Count = 1, });
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;

                //Act
                var orderOs = orderRepository.Create(order);

                //Assert
                Assert.AreNotEqual(DalOperationStatusCode.ChangesSaved, orderOs.StatusCode, orderOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void CreateOrderNotEnoughMoney()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                var createdProvisionerId = provisionerRepository.Create(provisioner).Entity.Value;

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = createdProvisionerId;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                order.OrderItems.Add(new OrderItem { DishId = dish.Id.Value, Count = 1000, });
                order.MenuId = menu.Id.Value;
                order.UserId = user.Id.Value;
                //Act
                var orderOs = orderRepository.Create(order);
                //Assert
                Assert.AreNotEqual(DalOperationStatusCode.ChangesSaved, orderOs.StatusCode, orderOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void CreateValidOrderNoDiscount()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                provisionerRepository.Create(provisioner);

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = provisioner.Id.Value;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                var orderItem = new OrderItem { DishId = dish.Id.Value, Count = 2, };
                order.OrderItems.Add(orderItem);
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                //Act
                var order_os = orderRepository.Create(order);
                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, order_os.StatusCode, order_os.BuildErrorMessageString());
                Assert.AreEqual(2.4m - 0.0m, order.Cost, order_os.BuildErrorMessageString()); // costOfOrder - DiscountForOrder, Discount calculated by CalculateDiscountForOrder from OrderBusinessLogic class
                Assert.AreEqual(200.0m - 2.4m, user.Balance,  order_os.BuildErrorMessageString());
            }
        }

        [Test]
        public void CreateValidOrderDiscount()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                var createdProvisionerId =  provisionerRepository.Create(provisioner).Entity;


                var dish1 = DishFakes.GetDish(2);
                dish1.ProvisionerId = createdProvisionerId;
                var dish2 = DishFakes.GetDish(3);
                dish2.ProvisionerId = createdProvisionerId;
                var dish3 = DishFakes.GetDish(4);
                dish3.ProvisionerId = createdProvisionerId;

                dishRepository.Create(dish1);
                dishRepository.Create(dish2);
                dishRepository.Create(dish3);

                var men = MenuFakes.GetMenu();
                men.CreatorId = user.Id.Value;
                men.Dishes.Add(dish1);
                men.Dishes.Add(dish2);
                men.Dishes.Add(dish3);
                menuRepository.Create(men);

                var order = OrderFakes.GetOrder();
                order.UserId = user.Id.Value;
                var ordrItm1 = new OrderItem { DishId = dish1.Id.Value, Count = 1, };
                order.OrderItems.Add(ordrItm1);
                var ordrItm2 = new OrderItem { DishId = dish2.Id.Value, Count = 1, };
                order.OrderItems.Add(ordrItm2);
                var ordrItm3 = new OrderItem { DishId = dish3.Id.Value, Count = 1, };
                order.OrderItems.Add(ordrItm3);
                order.MenuId = men.Id.Value;

                //Act
                var order_os = orderRepository.Create(order);
                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, order_os.StatusCode,  order_os.BuildErrorMessageString());
                Assert.AreEqual(130.0m, order.Cost, order_os.BuildErrorMessageString()); // costOfOrder - DiscountForOrder, Discount calculated by CalculateDiscountForOrder from OrderBusinessLogic class
                Assert.AreEqual(70.0m, user.Balance, order_os.BuildErrorMessageString());
            }
        }

        [Test]
        public void DeleteOrderPendingYesBlockingHourNo()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);
                var oldUserBalance = user.Balance;

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                provisionerRepository.Create(provisioner);

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = provisioner.Id.Value;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                var orderItem = new OrderItem { DishId = dish.Id.Value, Count = 2, };
                order.OrderItems.Add(orderItem);
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                orderRepository.Create(order);
                MoveBlockingHourForward();
                //Act
                var orderOs = orderRepository.DeleteOrder(order.Id.Value, order.UserId);

                if (!ResetBlockingHourToDefault())
                    throw new Exception("Не удалось вернуть дефолтное значение blocking hour");
                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, orderOs.StatusCode, orderOs.BuildErrorMessageString());
                Assert.AreEqual(oldUserBalance, user.Balance, orderOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void DeleteOrderPendingNoBlockingHourYes()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                provisionerRepository.Create(provisioner);

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = provisioner.Id.Value;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                var orderItem = new OrderItem { DishId = dish.Id.Value, Count = 2, };
                order.OrderItems.Add(orderItem);
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                orderRepository.Create(order);
                orderRepository.NotifyAboutDelivering(DateTime.Now);
                MoveBlockingHourAgo();
                //Act
                var orderOs = orderRepository.DeleteOrder(order.Id.Value, order.UserId);
                ResetBlockingHourToDefault();
                //Assert
                Assert.AreNotEqual(DalOperationStatusCode.ChangesSaved, orderOs.StatusCode, orderOs.BuildErrorMessageString());
                Assert.AreEqual(200.0m - 2.4m, user.Balance, orderOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void DeletePendingOrderForSecondTime()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                provisionerRepository.Create(provisioner);

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = provisioner.Id.Value;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                var orderItem = new OrderItem { DishId = dish.Id.Value, Count = 2, };
                order.OrderItems.Add(orderItem);
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                orderRepository.Create(order);
                var orderOs = orderRepository.DeleteOrder(order.Id.Value, order.UserId);
                //Act
                orderOs = orderRepository.DeleteOrder(order.Id.Value, order.UserId);
                ResetBlockingHourToDefault();
                //Assert
                Assert.AreEqual(200.0m, user.Balance, orderOs.BuildErrorMessageString());
                Assert.AreNotEqual(200.0m + order.Cost, user.Balance, orderOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void CreateSecondOrderForADay() //Edit when you Immplement SingleMenu verification
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var menuRepository = new MenuRepository(akvelonLunchContext);
                var userRepository = new UserRepository(akvelonLunchContext);
                var provisionerRepository = new ProvisionerRepository(akvelonLunchContext);
                var dishRepository = new DishRepository(akvelonLunchContext);
                var orderRepository = new OrderRepository(akvelonLunchContext);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = akvelonLunchContext.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.OrderStatusId = (int)OrderStatusesEnum.Pending;
                            akvelonLunchContext.Entry(ord).State = EntityState.Modified;
                            akvelonLunchContext.SaveChanges();
                            orderRepository.DeleteOrder(ord.Id.Value, ord.UserId);
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var user = UserFakes.GetUser(0);
                userRepository.Create(user);

                var provisioner = ProvisionerFakes.GetProvisioner();
                provisioner.Discount = DiscountFakes.GetDiscount(1);
                provisionerRepository.Create(provisioner);

                var dish = DishFakes.GetDish(1);
                dish.ProvisionerId = provisioner.Id.Value;
                dishRepository.Create(dish);

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = user.Id.Value;
                menu.Dishes.Add(dish);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                var orderItem = new OrderItem { DishId = dish.Id.Value, Count = 2, };
                order.OrderItems.Add(orderItem);
                order.UserId = user.Id.Value;
                order.MenuId = menu.Id.Value;
                var order_os1 = orderRepository.Create(order);

                //Act
                var order_os2 = orderRepository.Create(order);
                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, order_os1.StatusCode, order_os1.BuildErrorMessageString());
                Assert.AreEqual(2.4m - 0.0m, order.Cost, order_os1.BuildErrorMessageString()); // costOfOrder - DiscountForOrder, Discount calculated by CalculateDiscountForOrder from OrderBusinessLogic class
                Assert.AreEqual(200.0m - 2.4m, user.Balance, order_os1.BuildErrorMessageString());
                Assert.AreNotEqual(DalOperationStatusCode.ChangesSaved, order_os2.StatusCode, order_os2.BuildErrorMessageString());
            }
        }
    }
}
