﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Repositories.DishRepository;
using AkvelonLunch.Data.Repositories.MenuRepository;
using AkvelonLunch.Data.Repositories.OrderRepository;
using AkvelonLunch.Data.Repositories.ProvisionerRepository;
using AkvelonLunch.Data.Repositories.UserRepository;
using NUnit.Framework;
using AkvelonLunch.Data.Test.Fakes;

namespace AkvelonLunch.Data.Test.LunchRepository
{
    [TestFixture]
    class MenuRepositoryTest
    {
        [Test]
        public void DeleteUsedMenu()
        {

            using (var ctx = new AkvelonLunchContext())
            {
                //Arrange
                var provisionerRepository = new ProvisionerRepository(ctx);
                var dishRepo = new DishRepository(ctx);
                var menuRepository = new MenuRepository(ctx);
                var userRepository = new UserRepository(ctx);
                var orderRepository = new OrderRepository(ctx);

                var provis = ProvisionerFakes.GetProvisioner();
                provis.Discount = DiscountFakes.GetDiscount(2);
                var createdProvisionerId = provisionerRepository.Create(provis).Entity.Value;

                var user = UserFakes.GetUser(0);
                var createdUserId = userRepository.Create(user).Entity.Value;

                var dsh = DishFakes.GetDish(1);
                dsh.ProvisionerId = createdProvisionerId;
                dishRepo.Create(dsh);

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = ctx.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.IsDeleted = true;
                            ctx.Entry(ord).State = EntityState.Modified;
                            ctx.SaveChanges();
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = createdUserId;
                menu.Dishes.Add(dsh);
                menuRepository.Create(menu);

                var order = OrderFakes.GetOrder();
                order.UserId = createdUserId;
                order.MenuId = menu.Id.Value;
                order.OrderItems.Add(new OrderItem
                {
                    DishId = dsh.Id.Value,
                    Count = 1,
                });
                orderRepository.Create(order);

                //Act
                var menuOs = menuRepository.Delete(menu.Id.Value);

                //Assert
                Assert.AreEqual(DalOperationStatusCode.Error, menuOs.StatusCode, menuOs.BuildErrorMessageString());
                Assert.AreEqual(false, menu.IsDeleted, menuOs.BuildErrorMessageString());
            }
                
        }
    }
}