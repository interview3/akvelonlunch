﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using NUnit.Framework;
using AkvelonLunch.Data;
using NUnit;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Repositories.DishRepository;
using AkvelonLunch.Data.Repositories.MenuRepository;
using AkvelonLunch.Data.Repositories.OrderRepository;
using AkvelonLunch.Data.Repositories.ProvisionerRepository;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Data.Test.Fakes;


namespace AkvelonLunch.Data.Test.LunchRepository
{
   [TestFixture]
    public class DishRepositoryTest
    {
        [Test]
        public void CreateDish()
        {
            using (var ctx = new AkvelonLunchContext())
            {
                //Arrange
                var provisionerRepository = new ProvisionerRepository(ctx);
                var dishRepo = new DishRepository(ctx);

                var provis = ProvisionerFakes.GetProvisioner();
                provis.Discount = DiscountFakes.GetDiscount(2);
                var createdProvisionerId = provisionerRepository.Create(provis).Entity;
                var dsh = DishFakes.GetDish(1);
                dsh.ProvisionerId = createdProvisionerId;
                
                //Act
                dishRepo.Create(dsh);

                //Assert
                Assert.AreNotEqual(dsh, null);
                Assert.AreNotEqual(dsh.Id, null);
            }
        }

        [Test]
        public void UpdateDishFromOrder()
        {
            using (var ctx = new AkvelonLunchContext())
            {
                //Arrange
                var provisionerRepository = new ProvisionerRepository(ctx);
                var dishRepo = new DishRepository(ctx);
                var menuRepository = new MenuRepository(ctx);
                var userRepository = new UserRepository(ctx);
                var orderRepository = new OrderRepository(ctx);

                var provis = ProvisionerFakes.GetProvisioner();
                provis.Discount = DiscountFakes.GetDiscount(2);
                var createdProvisionerId = provisionerRepository.Create(provis).Entity.Value;

                var dsh = DishFakes.GetDish(1);
                dsh.ProvisionerId = createdProvisionerId;
                dishRepo.Create(dsh);

                var user = UserFakes.GetUser(0);
                var createdUserId = userRepository.Create(user).Entity.Value;

                Menu todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                if (todaysMenu != null)
                {
                    todaysMenu.Orders = ctx.Orders.Where(o => o.MenuId == todaysMenu.Id).ToList();
                }
                while (todaysMenu != null)                                          
                {
                    if (todaysMenu.Orders != null && todaysMenu.Orders.Any())
                    {
                        foreach (var ord in todaysMenu.Orders)
                        {
                            ord.IsDeleted = true;
                            ctx.Entry(ord).State = EntityState.Modified;
                            ctx.SaveChanges();
                        }
                    }
                    menuRepository.Delete(todaysMenu.Id.Value);
                    todaysMenu = menuRepository.GetMenuByDate(DateTime.Now).Entity;
                }

                var menu = MenuFakes.GetMenu();
                menu.CreatorId = createdUserId;
                menu.Dishes.Add(dsh);
                var createdMenuId = menuRepository.Create(menu).Entity.Value;

                var order = OrderFakes.GetOrder();
                order.UserId = createdUserId;
                order.MenuId = createdMenuId;
                order.OrderItems.Add(new OrderItem
                {
                    DishId = dsh.Id.Value,
                    Count = 1,
                });
                orderRepository.Create(order);
                var dshCostBefore = dsh.Cost;
                dsh.Cost = 30.0m;

                //Act
                var dishOs = dishRepo.Update(dsh, createdUserId);

                //Assert
                Assert.AreEqual(DalOperationStatusCode.NoChanges, dishOs.StatusCode, dishOs.BuildErrorMessageString());
                Assert.AreNotEqual(dshCostBefore, dsh.Cost, dishOs.BuildErrorMessageString());
            }
        }
    }
}
