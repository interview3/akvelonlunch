﻿using System.Linq;
using AkvelonLunch.Data.Repositories.ProvisionerRepository;
using NUnit.Framework;
using AkvelonLunch.Data.Test.Fakes;

namespace AkvelonLunch.Data.Test.LunchRepository
{
    [TestFixture]
    class ProvisionerRepositoryTest
    {
        [Test]
        public void CreateProvisioner()
        {
            using (var ctx = new AkvelonLunchContext())
            {
                //Arrange
                var provRepo = new ProvisionerRepository(ctx);
                var prov = ProvisionerFakes.GetProvisioner();
                prov.Discount = DiscountFakes.GetDiscount(1);
                
                // Act
                
                provRepo.Create(prov);

                //Assert
                Assert.AreNotEqual(prov, null, "Provisioner entity null");
                Assert.AreNotEqual(prov.Id, null, "Provisioner id null");
            }
        }
    }
}
