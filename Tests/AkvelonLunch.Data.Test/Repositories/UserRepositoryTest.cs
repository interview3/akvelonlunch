﻿using System.Linq;
using AkvelonLunch.Data.Entities;
using AkvelonLunch.Data.Repositories.UserRepository;
using AkvelonLunch.Data.Repositories.MoneyTransactionLogRepository;
using AkvelonLunch.Data.Test.Fakes;
using NUnit.Framework;

namespace AkvelonLunch.Data.Test.Repositories
{
    [TestFixture]
    public class UserRepositoryTest
    {
        [Test]
        public void UpdateUserBalanceFromAdmin()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                //Arrange
                var userRepository = new UserRepository(akvelonLunchContext);
                var mtlRepository = new MoneyTransactionLogRepository(akvelonLunchContext);

                var userClient = UserFakes.GetUser(0);
                userRepository.Create(userClient);
                var userAdmin = UserFakes.GetUser(1);
                userRepository.Create(userAdmin);
                var balanceBefore = userClient.Balance;

                //Act
                var userOs = userRepository.UpdateUserBalance(userClient.Id.Value, 300.0m, userAdmin.Id.Value);
                var mtl = akvelonLunchContext.MoneyTransactionLogs.Local.LastOrDefault(); // last record in log
                //Assert
                Assert.AreEqual(DalOperationStatusCode.ChangesSaved, userOs.StatusCode, userOs.BuildErrorMessageString());
                Assert.AreEqual(balanceBefore + 100.0m, userClient.Balance, userOs.BuildErrorMessageString());
                Assert.AreEqual(userClient.Id, mtl.AffectedUserId, userOs.BuildErrorMessageString());
                Assert.AreEqual(balanceBefore, mtl.BalanceBefore, userOs.BuildErrorMessageString());
                Assert.AreEqual(300.0m, mtl.BalanceAfter, userOs.BuildErrorMessageString());
            }
        }

        [Test]
        public void CheckCashInCashbox()
        {
            using (var akvelonLunchContext = new AkvelonLunchContext())
            {
                ////Arrange
                //var userRepository = new UserRepository(akvelonLunchContext);

                //var userClient = UserFakes.GetUser(0);
                //userRepository.Create(userClient);
                //var userAdmin = UserFakes.GetUser(1);
                //userRepository.Create(userAdmin);
                //var balanceBefore = userClient.Balance;

                ////Act
                //var userOs = userRepository.UpdateUserBalance(userClient.Id.Value, 300.0m, (int)userAdmin.Id);
                //var mtl = akvelonLunchContext.MoneyTransactionLogs.Local.LastOrDefault(); // last record in log
                ////Assert
                //Assert.AreEqual(DalOperationStatusCode.ChangesSaved, userOs.StatusCode, userOs.BuildErrorMessageString());
                //Assert.AreEqual(balanceBefore + 100.0m, userClient.Balance, userOs.BuildErrorMessageString());
                //Assert.AreEqual(userClient.Id, mtl.AffectedUserId, userOs.BuildErrorMessageString());
                //Assert.AreEqual(balanceBefore, mtl.BalanceBefore, userOs.BuildErrorMessageString());
                //Assert.AreEqual(300.0m, mtl.BalanceAfter, userOs.BuildErrorMessageString());
                Assert.AreEqual(111,111);
            }
        }
    }
}
