﻿using System;
using System.Configuration;
using System.Net.Mail;
using AkvelonLunch.Utilities;

namespace AkvelonLunch.Notifications
{
    public class EmailSender
    {
        public void Send(string mail, string header, string body)
        {
            try
            {
                MailMessage message = new MailMessage("lunch@akvelon.com", mail, header, body)
                {
                    IsBodyHtml = true
                };
                var hostName = ConfigurationManager.AppSettings["EmailHostName"];
                var portNumber = int.Parse(ConfigurationManager.AppSettings["EmailPortNumber"]);
                SmtpClient client = new SmtpClient(hostName, portNumber);
                client.UseDefaultCredentials = true;

                client.Send(message);
            }
            catch (Exception ex)
            {
                GlobalLogger.GetInstance().Error(ex, "Unable to send email to " + mail);
            }
        }
    }
}
