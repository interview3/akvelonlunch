﻿using NLog;
namespace AkvelonLunch.Utilities
{
    public static class GlobalLogger
    {
        private static readonly Logger Instance = LogManager.GetLogger("Global logger");

        public static Logger GetInstance()
        {
            return Instance;
        }
    }
}
