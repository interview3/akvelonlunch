﻿namespace lunch
{
	public class CustomWebResponse<T>
	{
		public string message;
		public T responseObject;
		public bool ok;
		public CustomWebResponse(T Response, string message="", bool ok=true )
		{
			this.message = message;
			responseObject = Response;
			this.ok = ok;
		}
	}
}

