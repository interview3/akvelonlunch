﻿namespace lunch
{
	public class SettingModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
		public string Description { get; set; }
		public SettingModel()
		{
			Id = 1;
			Value = Settings.closingTime;
		}

	}
}

