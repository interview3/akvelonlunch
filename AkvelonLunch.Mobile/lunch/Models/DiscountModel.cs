﻿namespace lunch
{
	public class DiscountModel
	{
		public int ProvisionerId { get; set; }
		public int NumberOfDishes { get; set; }
		public decimal DiscountSum { get; set; }

		public DiscountModel()
		{
			ProvisionerId = 1;
			NumberOfDishes = 3;
			DiscountSum = 20;
		}
	}
}

