﻿namespace lunch
{
	public class DishModel
	{
		public string Name { get; set; }
		public int Id { get; set; }
		public decimal Cost { get; set; }
		public ProvisionerModel Provisioner {get; set; }	
		public DishType DishType {get; set; }
		public string Description { get; set; }
		public bool IsInCart { get;  set; }
		public bool DishIsInMenu { get; set; }
	
		public DishModel(string title, string description, decimal price, DishType dishtype, ProvisionerModel prov, int id=0)
		{
			Provisioner = prov;
			DishType = dishtype;
			Name = title;
			Id = id;
			Cost = price;
			Description = description;
		}
	}
}


