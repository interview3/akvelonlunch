﻿namespace lunch
{
	public class OrderItemModel
	{
		public DishModel Dish { get; set; }
		public int Count { get; set; }
		public int Id { get; set; }
		public int DishId { get; set;}
		public int OrderId { get; set; }

		public OrderItemModel(DishModel dish, int amount = 1)
		{
			Id = dish.Id;
			Dish = dish;
			Count = amount;
			DishId = Dish.Id;
		}
	}
}

