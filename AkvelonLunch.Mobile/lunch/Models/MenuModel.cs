﻿using System.Collections.Generic;
using System;

namespace lunch
{
	public class MenuModel
	{
		public int Id;
		public IList<DishModel> Dishes;
		public DateTime DateToShow;

	    public MenuModel()
	    {
            Dishes = new List<DishModel>();
	        DateToShow = DateTime.Now;
	    }
    }
}

