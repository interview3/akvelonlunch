﻿using System;
using System.Collections.Generic;

namespace lunch
{
	public class OrderModel
	{
		public IList<OrderItemModel> Items { get; set; }
		public decimal Cost { get; set; }
		public int Id { get; set; }
		public DateTime OrderTime { get; set; }
		public int UserId { get; set; }
		public int OrderStatusId { get; set; }
		public int MenuId { get; set; }
		public UserModel User { get; set; }

	public OrderModel(IList<OrderItemModel> items)
		{
			MenuId = MenuComponent.MenuId;
			Id = 0;
			Items = items;
			OrderTime = DateTime.Now;
			UserId = 0;
            Cost = 0;
			User = AuthorisationComponent.GetUser().responseObject;
            foreach (var item in Items)
            {
                Cost += item.Count * item.Dish.Cost;
            }

        }
    }
}

