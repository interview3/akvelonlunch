﻿namespace lunch
{
	public class ProvisionerModel
	{
		public int Id { get; set; }
		public string SupplierName { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		public string ContactPersonName { get; set; }
		public DiscountModel Discount { get; set; }
	}
}

