﻿using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class AdminUserComponent
    {
        private static IList<UserModel> _users;
        private static int _editingdUserId;

        public static CustomWebResponse<IList<UserModel>> GetAllUsers(bool refresh = false)
        {
            if (_users != null && !refresh) return new CustomWebResponse<IList<UserModel>>(_users);
            var response = UserDataService.GetAllUsers();
            if (response.ok)
            {
                _users = response.responseObject;
            }
            else
            {
                return new CustomWebResponse<IList<UserModel>>(null, response.message, response.ok);
            }
            return new CustomWebResponse<IList<UserModel>>(_users);
        }

        public static CustomWebResponse<IList<UserModel>> GetAllUsersByKey(string key)
        {
            if (_users == null)
            {
                var response = GetAllUsers();
                if (!response.ok)
                {
                    return new CustomWebResponse<IList<UserModel>>(null, response.message, response.ok);
                }
            }
            var filteredUsers =
                _users.Where(
                    i =>
                        i.FirstName.ToLower().Contains(key.ToLower()) ||
                        i.LastName.ToLower().Contains(key.ToLower()) || i.SecondName.ToLower().Contains(key.ToLower()));
            return new CustomWebResponse<IList<UserModel>>(filteredUsers.ToList());
        }

        public static UserModel EditingUser()
        {
            return _users?.FirstOrDefault(u => u.Id == _editingdUserId);
        }

        public static CustomWebResponse<bool> Edit(UserModel user)
        {
            return UserDataService.EditUserBalance(user.Id, user.Balance);
        }

        public static int EditingUserId
        {
            get { return _editingdUserId; }
            set { _editingdUserId = value; }
        }
    }
}

