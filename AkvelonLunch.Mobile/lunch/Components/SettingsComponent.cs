﻿namespace lunch
{
	public static class SettingsComponent
	{
		public static bool closed;
		public static CustomWebResponse<SettingModel> GetClosingHour()
		{
			var setting = SettingsDataService.GetSettings(1);
			if (!setting.ok || setting.responseObject == null)
				setting = new CustomWebResponse<SettingModel>(new SettingModel());
			else Settings.closingTime = setting.responseObject.Value;

			return setting;
		}

		public static CustomWebResponse<bool> SetClosingHour(int hour, int minute)
		{
			Settings.closingTime = hour + ":" + minute + ":00";
			var setting = new SettingModel();
			var response = SettingsDataService.SetSetting(setting);
			return response;
		}

		public static CustomWebResponse<bool> IsServiceClosed()
		{
			return SettingsDataService.IsServiceClosed();
		}
	}
}

