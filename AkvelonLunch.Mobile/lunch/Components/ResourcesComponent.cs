﻿namespace lunch
{
	public static class ResourcesComponent
	{
		public static string GetStringFor(string res)
		{
			if (IsDebugging() && (res=="server" || res=="authCookie"))
				return Resources.ResourceManager.GetString("debug" + "_" + res);
			var result = Resources.ResourceManager.GetString(res + "_" + Settings.language);
		    if (result != null) return result;
		    result = Resources.ResourceManager.GetString(res);
		    return result ?? "";
		}

	    private static bool IsDebugging()
	    {
	        return Resources.ResourceManager.GetString("DEBUG_MODE") == "true";
	    }
	}
}


