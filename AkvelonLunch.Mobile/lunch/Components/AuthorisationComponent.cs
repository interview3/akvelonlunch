﻿using System;

namespace lunch
{
    public static class AuthorisationComponent
    {
        public static event Action LogoutEvent;
        public static event Action LoginEvent;
        public static event Action GetUserEvent;

        private static UserModel _user;

        public static CustomWebResponse<UserModel> GetUser()
        {
            if (_user != null) return new CustomWebResponse<UserModel>(_user);
            if (string.IsNullOrEmpty(Settings.ticket))
            {
                return new CustomWebResponse<UserModel>(null, "Not authorized", false);
            }
            var response = AuthorisationDataService.GetUser();
            if (!response.ok || response.responseObject == null) return response;
            _user = response.responseObject;
            GetUserEvent?.Invoke();
            return response;
        }

        public static CustomWebResponse<decimal> GetUserBalance()
        {
            var response = AuthorisationDataService.GetUser();
            decimal balance = 0;
            if (!response.ok || response.responseObject == null)
                return new CustomWebResponse<decimal>(balance, response.message, response.ok);
            balance = response.responseObject.Balance;
            _user = response.responseObject;
            GetUserEvent?.Invoke();
            return new CustomWebResponse<decimal>(balance, response.message, response.ok);
        }


        public static CustomWebResponse<string> GetUserFirstName()
        {
            var firstName = "User";
            if (_user == null)
            {
                var response = GetUser();
                if (_user != null)
                {
                    firstName = _user.FirstName;
                }
                else
                {
                    return new CustomWebResponse<string>(firstName, response.message, response.ok);
                }
            }
            else
            {
                firstName = _user.FirstName;
            }
            return new CustomWebResponse<string>(firstName);
        }

        public static CustomWebResponse<bool> Login(string username, string password, bool remember)
        {
            var loginResponse = AuthorisationDataService.Login(username, password, remember);
            if (!loginResponse.ok || loginResponse.responseObject == null)
                return new CustomWebResponse<bool>(false, loginResponse.message, loginResponse.ok);
            Settings.ticket = loginResponse.responseObject;
            LoginEvent?.Invoke();
            return new CustomWebResponse<bool>(true);
        }

        public static CustomWebResponse<bool> Logout()
        {
            var logoutResponse = AuthorisationDataService.Logout();
            if (!logoutResponse.ok) return logoutResponse;
            Settings.ticket = string.Empty;
            _user = null;
            LogoutEvent?.Invoke();
            return logoutResponse;
        }
    }
}

