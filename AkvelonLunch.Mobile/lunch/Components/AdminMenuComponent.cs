﻿using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class AdminMenuComponent
    {
        private static MenuModel _menu;
        private static bool _isMenuExist = false;

        public static CustomWebResponse<bool> UpdateAdminMenu()
        {
            var responseMenu = MenuComponent.GetMenu();
            if (!responseMenu.ok) return new CustomWebResponse<bool>(false, responseMenu.message, responseMenu.ok);
            _isMenuExist = responseMenu.responseObject != null;
            _menu = responseMenu.responseObject ?? new MenuModel();
            return new CustomWebResponse<bool>(true);
        }

        public static CustomWebResponse<Dictionary<string, List<DishModel>>> GetMenuDictionary()
        {
            if (_menu == null)
            {
                var response = UpdateAdminMenu();
                if (!response.ok || !response.responseObject)
                {
                    return new CustomWebResponse<Dictionary<string, List<DishModel>>>(null, response.message,
                        response.ok);
                }
            }
            DishComponent.MarkDishes();
            return
                new CustomWebResponse<Dictionary<string, List<DishModel>>>(
                    MenuComponent.ConvertMenuToMenuDictionary(_menu));
        }

        public static CustomWebResponse<MenuModel> GetMenu()
        {
            if (_menu != null) return new CustomWebResponse<MenuModel>(_menu);
            var response = UpdateAdminMenu();
            if (!response.ok || !response.responseObject)
            {
                return new CustomWebResponse<MenuModel>(null, response.message, response.ok);
            }
            return new CustomWebResponse<MenuModel>(_menu);
        }

        public static void AddMenuItem(DishModel dish)
        {
            if (_menu == null)
                return;
            var existDish = _menu.Dishes.FirstOrDefault(d => d.Id == dish.Id);
            if (existDish != null) return;
            _menu.Dishes.Add(dish);
            DishComponent.MarkDishes();
        }

        public static void RemoveItem(int dishId)
        {
            var existDish = _menu?.Dishes.FirstOrDefault(d => d.Id == dishId);
            if (existDish == null) return;
            _menu.Dishes.Remove(existDish);
            DishComponent.MarkDishes();
        }
        
        public static bool ToggleMenuItem(DishModel dish)
        {
            var added = false;
            if (_menu == null)
                return added;
            var existDish = _menu.Dishes.FirstOrDefault(d => d.Id == dish.Id);
            if (existDish != null)
            {
                _menu.Dishes.Remove(existDish);
            }
            else
            {
                _menu.Dishes.Add(dish);
                added = true;
            }
            DishComponent.MarkDishes();
            return added;
        }

        public static CustomWebResponse<bool> SaveMenu()
        {
            if (!_isMenuExist)
            {
                var response = MenuDataService.Create(_menu);
                if (response.ok && response.responseObject != -1)
                {
                    _isMenuExist = true;
                    _menu.Id = response.responseObject;
                    return new CustomWebResponse<bool>(true);
                }
                return new CustomWebResponse<bool>(false, response.message, response.ok);
            }
            return MenuDataService.Update(_menu);
        }
    }
}