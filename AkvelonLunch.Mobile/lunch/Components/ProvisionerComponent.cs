﻿using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class ProvisionerComponent
    {
        static IList<ProvisionerModel> _provisioners;

        public static CustomWebResponse<IList<ProvisionerModel>> GetAllProvisioners()
        {
            if (_provisioners != null) return new CustomWebResponse<IList<ProvisionerModel>>(_provisioners);
            var response = ProvisionerDataService.GetAllProvisioners();
            _provisioners = response.responseObject;
            return new CustomWebResponse<IList<ProvisionerModel>>(_provisioners);
        }
     
        public static ProvisionerModel GetProvisionerByNumber(int id)
        {
			if (_provisioners == null)
				GetAllProvisioners();
            return _provisioners?[id];
        }

        public static ProvisionerModel GetProvisionerById(int id)
        {
            if (_provisioners == null)
                GetAllProvisioners();
            return _provisioners?.First(i => i.Id == id);
        }
    }
}

