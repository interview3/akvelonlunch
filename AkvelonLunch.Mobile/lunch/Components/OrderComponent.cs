﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class OrderComponent
    {
        public static event Action MakeOrderEvent;
        public static event Action CancelOrderEvent;
        public static event Action UpdateOrderEvent;

        private static int OurProvisionerId = 1;

        private static OrderModel _order;
        private static bool _isMade;
        private static bool _isArrived;

        private static bool _orderIsNotEmpty
        {
            get
            {
                return _order != null && _order.Items.Count > 0;
            }
        }

        private static bool _userHasEnoughMoney
        {
            get
            {
                var response = AuthorisationComponent.GetUserBalance();
                if (response.ok)
                {
                    return response.responseObject >= TotalSumm;
                }

                return true;
            }
        }
        
		private static void _updateCost()
        {
            if (_order == null)
                return;
            _order.Cost = 0;
            foreach (var item in _order.Items)
            {
                _order.Cost += item.Count * item.Dish.Cost;
            }
        }

        private static void _setOrderMade()
        {
            _isMade = true;
            MakeOrderEvent?.Invoke();
        }

        private static void _setOrderCanceled()
        {
            _isMade = false;
            CancelOrderEvent?.Invoke();
        }

        private static void _setOrderArrived()
        {
            _isMade = true;
            _isArrived = true;
        }

        private static void _setOrder(OrderModel order)
        {
            _order = order;
            if (_order != null)
            {
                // TODO Magic numbers
                switch (_order.OrderStatusId)
                {
                    case 3:
                        _setOrderArrived();
                        break;
                    case 2:
                        _setOrderMade();
                        break;
                    default:
                        _setOrderCanceled();
                        break;
                }
            }
            else
            {
                _order = new OrderModel(new List<OrderItemModel>());
                _setOrderCanceled();
            }
            _updateCost();
            MenuComponent.CheckIsInCard();
        }

        public static void Subscribe()
        {
            AuthorisationComponent.GetUserEvent += () => { UpdateOrder(); };
            AuthorisationComponent.LoginEvent += () => { UpdateOrder(); };
            AuthorisationComponent.LogoutEvent += ResetOrder;
        }

        public static bool UpdateOrder()
        {
            var response = OrderDataService.GetOrder();
            if (!response.ok) return false;
            if (_order != null)
            {
                if ((response.responseObject != null || !isMade) &&
                    (response.responseObject == null || _order.Id == response.responseObject.Id)) return false;
                _setOrder(response.responseObject);
                MenuComponent.CheckIsInCard();
                UpdateOrderEvent?.Invoke();
                return true;
            }
            _setOrder(response.responseObject);
            if (response.responseObject == null) return false;
            MenuComponent.CheckIsInCard();
            UpdateOrderEvent?.Invoke();
            return true;
        }

        public static CustomWebResponse<OrderModel> GetOrder()
        {
            if (_order != null)
            {
				return new CustomWebResponse<OrderModel>(_order);
            }
            var response = OrderDataService.GetOrder();
            _setOrder(response.responseObject);
            return response;
        }

        public static CustomWebResponse<bool> CancelOrder()
        {
            var response = OrderDataService.CancelOrder(_order.Id);
            if (response.ok)
            {
                _setOrderCanceled();
            }
            return response;
        }
        
        public static CustomWebResponse<bool> OrderArrived()
        {
            if (_isMade)
            {
                var response = OrderDataService.IfDelivered();
                _isArrived = response.ok && response.responseObject;
                return response;
            }
            return new CustomWebResponse<bool>(false);
        }

        public static CustomWebResponse<int> MakeOrder()
        {
            _order.MenuId = MenuComponent.MenuId;
            if (!_orderIsNotEmpty)
            {
                return new CustomWebResponse<int>(-1, ResourcesComponent.GetStringFor("emptyOrder"), false);
            }

            if (!_userHasEnoughMoney)
            {
				return new CustomWebResponse<int>(-1, ResourcesComponent.GetStringFor("notEnoughMoney"), false);
            }
            var response = OrderDataService.SendOrder(_order);
			if (response.ok)
			{
				_order.Id = response.responseObject;
				_setOrderMade();
			}
			else
                MenuComponent.UpdateMenu();
            return response;
        }
       
        public static IList<OrderItemModel> GetCart()
        {
            return _order != null ? _order.Items : new List<OrderItemModel>();
        }

        public static void IncrementOrderItemCount(int orderItemId)
        {
            var orderItem = _order?.Items.FirstOrDefault(i => i.Id == orderItemId);
            if (orderItem == null) return;
            orderItem.Count++;
            _updateCost();
        }

        public static void DecrementOrderItemCount(int orderItemId)
        {
            var orderItem = _order?.Items.FirstOrDefault(i => i.Id == orderItemId);
            if (orderItem == null) return;
            if (orderItem.Count == 1)
            {
                _order.Items.Remove(orderItem);
                MenuComponent.CheckIsInCard();
            }
            else
            {
                orderItem.Count--;
            }
            _updateCost();
        }        

        public static void ToggleOrderItem(DishModel dish)
        {
            if (_order == null)
                return;
            var orderItem = _order.Items.FirstOrDefault(i => dish.Id == i.Dish.Id);
            if (orderItem != null)
            {
                _order.Items.Remove(orderItem);               
            }
            else
            {
                _order.Items.Add(new OrderItemModel(dish));
            }
			_updateCost();
            MenuComponent.CheckIsInCard();
        }

        public static void ClearOrder()
        {
            if (_order == null)
                return;
            _order.Items.Clear();
            _updateCost();
            MenuComponent.CheckIsInCard();
        }

        public static void ResetOrder()
        {
            _order = null;
            _updateCost();
            MenuComponent.CheckIsInCard();
        }

        public static int OrderId
        {
            get
            {
                if (_order != null)
                {
                    return _order.Id;
                }
                return -1;
            }
        }

        public static decimal TotalWithoutDiscountSumm
        {
            get
            {
				_updateCost();
				return _order?.Cost ?? 0;
            }
        }

		public static int NumberOfItems
		{
			get
			{
				var count = 0;
			    if (_order == null) return 0;
			    foreach (var item in _order.Items)
			        count += item.Count;
			    return count;
			}
		}

        public static int NumberOfItemsForDiscount
        {
            get
            {
                var count = 0;
                if (_order != null)
                {
                    foreach (var item in _order.Items)
                        if ((item.Dish.DishType.Id == 1 || item.Dish.DishType.Id == 2 || item.Dish.DishType.Id == 3) &&
                            item.Dish.Provisioner.Id == OurProvisionerId)
                            count += item.Count;
                    return count;
                }
                return 0;
            }
        }

        public static decimal TotalSumm
        {
            get
            {
				var provisioner = ProvisionerComponent.GetProvisionerById(OurProvisionerId);
				var discount = new DiscountModel();
				if (provisioner != null && provisioner.Discount !=null)
					discount = provisioner.Discount;
				return TotalWithoutDiscountSumm - discount.DiscountSum * (NumberOfItemsForDiscount / discount.NumberOfDishes);
            }
        }        

        public static bool IsInOrder(DishModel Dish)
        {
            var orderItem = _order?.Items.FirstOrDefault(i => Dish.Id == i.Dish.Id);
            return orderItem != null;
        }

        public static bool isMade
        {
            get { return _isMade; }
        }

        public static bool isArrived
        {
            get
            {
                return _isArrived;
            }
        }
    }
}
