﻿using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class MenuComponent
    {
        private static MenuModel _menu;

        private static bool _isDishEquals(DishModel a, DishModel b)
        {
            return a.Name == b.Name && a.Cost == b.Cost && a.DishType.Id == b.DishType.Id &&
                   a.Description == b.Description;
        }

        private static bool _IsMenuEquals(MenuModel Menu)
        {
            if (_menu.Dishes.Count != Menu.Dishes.Count)
                return false;
            foreach (var dish in _menu.Dishes)
            {
                var dishWithSameId = Menu.Dishes.FirstOrDefault(d => d.Id == dish.Id);
                if (dishWithSameId == null || !_isDishEquals(dish, dishWithSameId))
                    return false;
            }
            foreach (var dish in Menu.Dishes)
            {
                var dishWithSameId = _menu.Dishes.FirstOrDefault(d => d.Id == dish.Id);
                if (dishWithSameId == null || !_isDishEquals(dish, dishWithSameId))
                    return false;
            }
            return true;
        }

        public static int MenuId
        {
            get
            {
                if (_menu != null)
                {
                    return _menu.Id;
                }
                return -1;
            }
        }

        public static bool UpdateMenu()
        {
            var response = MenuDataService.GetMenu();
            if (_menu != null)
            {
                if (!response.ok) return false;
                if (response.responseObject == null || _menu.Id != response.responseObject.Id)
                {
                    _menu = response.responseObject;
                    return true;
                }
                if (_IsMenuEquals(response.responseObject)) return false;
                _menu = response.responseObject;
                return true;
            }
            if (!response.ok || response.responseObject == null) return false;
            _menu = response.responseObject;
            return true;
        }

        public static CustomWebResponse<MenuModel> GetMenu()
        {
            if (_menu != null)
            {
                _menu.Dishes = _menu.Dishes.OrderBy(i => i.DishType.Id).ToList();
                return new CustomWebResponse<MenuModel>(_menu);
            }
            var response = MenuDataService.GetMenu();
            _menu = response.responseObject;
            if (_menu == null) return response;
            {
                _menu.Dishes = _menu.Dishes.OrderBy(i => i.DishType.Id).ToList();
                CheckIsInCard();
            }
            return response;
        }

        public static Dictionary<string, List<DishModel>> ConvertMenuToMenuDictionary(MenuModel menu)
        {
            menu.Dishes = menu.Dishes.OrderBy(i => i.DishType.Id).ToList();
            var menuDictionary = new Dictionary<string, List<DishModel>>();

            foreach (var dish in menu.Dishes)
            {
                var dishType = dish.DishType.Name;
                if (!menuDictionary.ContainsKey(dishType))
                {
                    menuDictionary.Add(dishType, new List<DishModel>());
                }
                menuDictionary[dishType].Add(dish);
            }
            return menuDictionary;
        }

        public static CustomWebResponse<Dictionary<string, List<DishModel>>> GetMenuDictionary()
        {
            if (_menu != null)
                return new CustomWebResponse<Dictionary<string, List<DishModel>>>(ConvertMenuToMenuDictionary(_menu));
            var response = MenuDataService.GetMenu();
            if (response.ok && response.responseObject != null)
            {
                _menu = response.responseObject;
                CheckIsInCard();
            }
            else
            {
                return new CustomWebResponse<Dictionary<string, List<DishModel>>>(null, response.message, response.ok);
            }
            return new CustomWebResponse<Dictionary<string, List<DishModel>>>(ConvertMenuToMenuDictionary(_menu));
        }

        public static void CheckIsInCard()
        {
            if (_menu == null)
                return;
            foreach (var dish in _menu.Dishes)
            {
                dish.IsInCart = OrderComponent.IsInOrder(dish);
            }
        }
    }
}
