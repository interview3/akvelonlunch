﻿using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public static class DishComponent
    {
        static IList<DishType> _dishTypes;
        private static IList<DishModel> _dishes;
        
        public static void MarkDishes()
        {
            var response = AdminMenuComponent.GetMenu();
            if (_dishes == null || !response.ok || response.responseObject == null)
                return;
            var menu = response.responseObject;
            foreach (var dish in _dishes)
            {
                dish.DishIsInMenu = menu.Dishes.FirstOrDefault(d => d.Id == dish.Id) != null;
            }
        }

        public static CustomWebResponse<IList<DishType>> GetAllDishTypes()
        {
            if (_dishTypes != null) return new CustomWebResponse<IList<DishType>>(_dishTypes);
            var response = DishDataService.GetAllDishTypes();
            _dishTypes = response.responseObject;
            return new CustomWebResponse<IList<DishType>>(_dishTypes);
        }

        public static DishType GetDishTypeById(int id)
        {
            return _dishTypes?[id];
        }

        public static CustomWebResponse<IList<DishModel>> GetAllDishes()
        {
            if (_dishes == null)
            {
                var response = MenuDataService.GetDishes();
                if (response.ok)
                {
                    _dishes = response.responseObject ?? new List<DishModel>();
                }
                else
                {
                    return new CustomWebResponse<IList<DishModel>>(null, response.message, response.ok);
                }
            }
			_dishes = _dishes.OrderBy(i => i.Name).ToList();
            MarkDishes();
            return new CustomWebResponse<IList<DishModel>>(_dishes);
        }


        public static CustomWebResponse<IList<DishModel>> GetAllDishesByName(string dishName)
        {
            var response = GetAllDishes();
            if (!response.ok) return new CustomWebResponse<IList<DishModel>>(null, response.message, response.ok);
            var filteredDishes =
                response.responseObject.Where(d => d.Name.ToLower().Contains(dishName.ToLower())).ToList();
            return new CustomWebResponse<IList<DishModel>>(filteredDishes);
        }

        public static CustomWebResponse<int> Create(DishModel dish)
        {
            var response = DishDataService.Create(dish);
            if (response.ok && response.responseObject != -1)
            {
                dish.Id = response.responseObject;
            }
            _dishes.Add(dish);
            AdminMenuComponent.AddMenuItem(dish);
            return response;
        }
    }
}

