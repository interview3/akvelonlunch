﻿using System;
using System.Collections.Generic;

namespace lunch
{
    public static class AdminOrderComponent
    {
        private static IList<OrderModel> _orders;

        public static CustomWebResponse<IList<OrderModel>> GetAllOrders(bool refresh)
        {
            if (_orders != null && !refresh) return new CustomWebResponse<IList<OrderModel>>(_orders);
            var response = OrderDataService.GetAllOrders();
            _orders = response.responseObject;
            return response;
        }

        public static Dictionary<string, int> GetOrderedDishes(bool refresh = false)
        {
            var dict = new Dictionary<string, int>();
            var response = GetAllOrders(refresh);
            if (!response.ok) return dict;
            if (response.responseObject == null) return dict;
            foreach (var order in response.responseObject)
            {
                foreach (var orderItem in order.Items)
                {
                    var dish = orderItem.Dish;
                    if (dict.ContainsKey(dish.Name))
                    {
                        dict[dish.Name] += orderItem.Count;
                    }
                    else
                    {
                        dict.Add(dish.Name, orderItem.Count);
                    }
                }
            }

            return dict;
        }

        public static decimal GetCostOfAllOrders(bool refresh = false)
        {
            decimal totalCost = 0;
            var response = GetAllOrders(refresh);
            if (!response.ok) return totalCost;
            if (response.responseObject == null) return totalCost;
            foreach (var order in response.responseObject)
            {
                totalCost += order.Cost;
            }
            return totalCost;
        }

        public static CustomWebResponse<bool> DeliverOrders()
        {
            return OrderDataService.DeliverOrders();
        }

        public static bool IfDelivered()
        {
            try
            {
                var isDeliev = OrderDataService.IfDelivered();
                if (isDeliev != null && isDeliev.ok)
                {
                    return isDeliev.responseObject;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}