﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;

namespace lunch
{
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;
		private const string SwitcherKey = "switcher_key";
		private static readonly bool SwitcherDefault = true;
		private const string RemindingHourKey = "rem_hour_key";
		private static readonly int RemindingHourDefault = 11;
		private const string RemindingMinuteKey = "rem_min_key";
		private static readonly int RemindingMinuteDefault = 30;
		private const string ticketKey = "ticket_key";
		private static readonly string ticketDefault = "-1";
		private const string langKey = "lang_key";
		private static readonly string langDefault = "ru";
		private const string ClosingHourKey = "closing_hour_key";
		private static readonly string ClosingHourDefault = "12:00:00";
		private static readonly bool remBalanceDefault = true;
		private const string remBalanceKey = "rem_balance_key";
		private static readonly int remBalanceValDefault = 130;
		private const string remBalanceValKey = "rem_bal_val_key";
		private const string CloseMenuHourKey = "close_menu_hour_key";
        private static readonly int CloseMenuHourDefault = 23;
        private const string CloseMenuMinuteKey = "close_menu_minute_key";
        private static readonly int CloseMenuMinuteDefault = 59;

		#endregion


		public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

		public static string ticket 
		{
			get
			{
				return AppSettings.GetValueOrDefault(ticketKey, ticketDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(ticketKey, value);
			}
		}

		public static string closingTime
		{
			get
			{
				return AppSettings.GetValueOrDefault(ClosingHourKey, ClosingHourDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(ClosingHourKey, value);
			}
		}


		public static string language
		{
			get
			{
				return AppSettings.GetValueOrDefault(langKey, langDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(langKey, value);
			}
		}

		public static bool Switcher
		{
			get
			{
				return AppSettings.GetValueOrDefault(SwitcherKey, SwitcherDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SwitcherKey, value);
			}
		}


		public static bool remBalance
		{
			get
			{
				return AppSettings.GetValueOrDefault(remBalanceKey, remBalanceDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(remBalanceKey, value);
			}
		}

		public static int remBalanceVal
		{
			get
			{
				return AppSettings.GetValueOrDefault(remBalanceValKey, remBalanceValDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(remBalanceValKey, value);
			}
		}

		public static int RemHour
		{
			get
			{
				return AppSettings.GetValueOrDefault(RemindingHourKey, RemindingHourDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(RemindingHourKey, value);
			}
		}

		public static int RemMinute
		{
			get
			{
				return AppSettings.GetValueOrDefault(RemindingMinuteKey, RemindingMinuteDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(RemindingMinuteKey, value);
			}
		}

		public static int CloseMenuHour
        {
            get
            {
                return AppSettings.GetValueOrDefault(CloseMenuHourKey, CloseMenuHourDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CloseMenuHourKey, value);
            }
        }

        public static int CloseMenuMinute
        {
            get
            {
                return AppSettings.GetValueOrDefault(CloseMenuMinuteKey, CloseMenuMinuteDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CloseMenuMinuteKey, value);
            }
        }
	}
}

