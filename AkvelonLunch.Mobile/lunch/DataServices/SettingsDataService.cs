﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace lunch
{
    public static class SettingsDataService
    {
        private const string GetSettingsConnectionString = "api/setting/";
        private const string SetSettingsConnectionString = "api/setting/update/";
        private const string CheckClosingHourConnectionString = "api/menu/MenuIsBlocked/";

        public static CustomWebResponse<SettingModel> GetSettings(int id)
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetSettingsConnectionString + id;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<SettingModel>(null, response.ReasonPhrase, false);
                var json = response.Content.ReadAsStringAsync().Result;
                return new CustomWebResponse<SettingModel>(JsonConvert.DeserializeObject<SettingModel>(json));
            }
            catch
            {
                return new CustomWebResponse<SettingModel>(null, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> IsServiceClosed()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + CheckClosingHourConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
                var json = response.Content.ReadAsStringAsync().Result;
                return new CustomWebResponse<bool>(JsonConvert.DeserializeObject<bool>(json));
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> SetSetting(SettingModel setting)
        {
            try
            {
                var sendingObject = JsonConvert.SerializeObject(setting);
                var uri = ResourcesComponent.GetStringFor("server") + SetSettingsConnectionString;
                var response = BasicDataService.CreatePostDataService(uri, sendingObject);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return new CustomWebResponse<bool>(true);
                }
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }
    }
}