﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace lunch
{
    public enum OrderStatusesEnum
    {
        Canceled = 1,
        Pending = 2,
        Completed = 3
    }

    public static class OrderDataService
    {
        private const string MakeOrderConnectionString = "api/order/make";
        private const string GetOrderConnectionString = "api/order/getTodayUserOrder";
        private const string CancelOrderConnectionString = "api/order/cancel/";
        private const string GetStatusConnectionString = "api/order/wereTodayOrdersDelieveried/";
        private const string GetOrdersConnectionString = "api/order/today";
        private const string DeliverOrdersConnectionString = "api/order/notifyAboutDelivering/";

        public static CustomWebResponse<bool> CancelOrder(int id)
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + CancelOrderConnectionString + id;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Delete);
                if (response.StatusCode == HttpStatusCode.OK)
                    return new CustomWebResponse<bool>(true);
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<int> SendOrder(OrderModel order)
        {
            try
            {
                var sendingObject = JsonConvert.SerializeObject(order);
                var uri = ResourcesComponent.GetStringFor("server") + MakeOrderConnectionString;
                var response = BasicDataService.CreatePostDataService(uri, sendingObject);
                if (response.StatusCode != HttpStatusCode.Created)
                    return new CustomWebResponse<int>(-1, response.ReasonPhrase, false);
                var responseString = response.Content.ReadAsStringAsync().Result;
                var answer = JsonConvert.DeserializeObject<CustomServerResponse>(responseString);
                return new CustomWebResponse<int>(answer.OperationStatus.Entity);
            }
            catch
            {
                return new CustomWebResponse<int>(-1, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<OrderModel> GetOrder()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetOrderConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<OrderModel>(null, response.ReasonPhrase, false);
                var order = response.Content.ReadAsStringAsync().Result;
                return new CustomWebResponse<OrderModel>(JsonConvert.DeserializeObject<OrderModel>(order));
            }
            catch
            {
                return new CustomWebResponse<OrderModel>(null, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> DeliverOrders()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + DeliverOrdersConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode == HttpStatusCode.OK)
                    return new CustomWebResponse<bool>(true);
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> IfDelivered()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetStatusConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
                var json = response.Content.ReadAsStringAsync().Result;
                return new CustomWebResponse<bool>(Convert.ToBoolean(json));
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<IList<OrderModel>> GetAllOrders()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetOrdersConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<IList<OrderModel>>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return
                        new CustomWebResponse<IList<OrderModel>>(
                            JsonConvert.DeserializeObject<IList<OrderModel>>(jsonString));
                }
                return new CustomWebResponse<IList<OrderModel>>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<IList<OrderModel>>(null, ResourcesComponent.GetStringFor("noConnection"),
                    false);
            }
        }
    }
}