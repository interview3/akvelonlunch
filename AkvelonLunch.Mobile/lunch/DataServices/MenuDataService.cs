﻿using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace lunch
{
    public static class MenuDataService
    {
        private const string MenuConnectionString = "api/menu/";
        private const string MenuCreateConnectionString = "api/menu/create";
        private const string MenuUpdateConnectionString = "api/menu/update";
        private const string MenuDishesConnectionString = "api/dish/alldishes";

        public static CustomWebResponse<MenuModel> GetMenu()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + MenuConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<MenuModel>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return new CustomWebResponse<MenuModel>(JsonConvert.DeserializeObject<MenuModel>(jsonString));
                }
                return new CustomWebResponse<MenuModel>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<MenuModel>(null, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<IList<DishModel>> GetDishes()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + MenuDishesConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<IList<DishModel>>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return new CustomWebResponse<IList<DishModel>>(
                        JsonConvert.DeserializeObject<IList<DishModel>>(jsonString));
                }
                return new CustomWebResponse<IList<DishModel>>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<IList<DishModel>>(null, ResourcesComponent.GetStringFor("noConnection"),
                    false);
            }
        }

        public static CustomWebResponse<int> Create(MenuModel menu)
        {
            try
            {
                var sendingObject = JsonConvert.SerializeObject(menu);
                var uri = ResourcesComponent.GetStringFor("server") + MenuCreateConnectionString;
                var response = BasicDataService.CreatePostDataService(uri, sendingObject);
                if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK)
                {
                    var jsonString = response.Content.ReadAsStringAsync().Result;
                    var answer = JsonConvert.DeserializeObject<CustomServerResponse>(jsonString);
                    return new CustomWebResponse<int>(answer.OperationStatus.Entity);
                }
                return new CustomWebResponse<int>(-1, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<int>(-1, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> Update(MenuModel menu)
        {
            try
            {
                var sendingObject = JsonConvert.SerializeObject(menu);
                var uri = ResourcesComponent.GetStringFor("server") + MenuUpdateConnectionString;
                var response = BasicDataService.CreatePostDataService(uri, sendingObject);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return new CustomWebResponse<bool>(true);
                }
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }
    }
}