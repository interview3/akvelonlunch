﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace lunch
{
    public static class UserDataService
    {
        private const string GetUsersConnectionString = "api/user/allusers";
        private const string UpdateBalanceConnectionString = "api/user/updatebalance";

        public static CustomWebResponse<bool> EditUserBalance(int uid, decimal ubalance)
        {
            try
            {
                int num;
                int.TryParse(ubalance.ToString(), out num);
                var uri = ResourcesComponent.GetStringFor("server") + UpdateBalanceConnectionString + "?id=" + uid +
                          "&balance=" + num;
                var response = BasicDataService.CreatePostDataService(uri, "");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return new CustomWebResponse<bool>(true);
                }
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<IList<UserModel>> GetAllUsers()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetUsersConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<IList<UserModel>>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return
                        new CustomWebResponse<IList<UserModel>>(
                            JsonConvert.DeserializeObject<IList<UserModel>>(jsonString));
                }
                return new CustomWebResponse<IList<UserModel>>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<IList<UserModel>>(null, ResourcesComponent.GetStringFor("noConnection"),
                    false);
            }
        }
    }
}