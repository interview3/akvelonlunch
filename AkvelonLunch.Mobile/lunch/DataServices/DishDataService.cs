﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace lunch
{
    public static class DishDataService
    {
        private const string GetDishTypeConnectionString = "api/dish/dishtypes/";
        private const string CreateDishConnectionString = "api/dish/create/";

        public static CustomWebResponse<IList<DishType>> GetAllDishTypes()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetDishTypeConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<IList<DishType>>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return new CustomWebResponse<IList<DishType>>(JsonConvert.DeserializeObject<IList<DishType>>(jsonString));
                }
                return new CustomWebResponse<IList<DishType>>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<IList<DishType>>(null, ResourcesComponent.GetStringFor("noConnection"),
                    false);
            }
        }

        public static CustomWebResponse<int> Create(DishModel dish)
        {
            try
            {
                var sendingObject = JsonConvert.SerializeObject(dish);
                var uri = ResourcesComponent.GetStringFor("server") + CreateDishConnectionString;
                var response = BasicDataService.CreatePostDataService(uri, sendingObject);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<int>(-1, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                var answer = JsonConvert.DeserializeObject<CustomServerResponse>(jsonString);
                return new CustomWebResponse<int>(answer.OperationStatus.Entity);
            }
            catch
            {
                return new CustomWebResponse<int>(-1, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }
    }
}