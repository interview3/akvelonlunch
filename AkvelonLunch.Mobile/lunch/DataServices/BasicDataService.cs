﻿using System.Net;
using System.Net.Http;

namespace lunch
{
    public static class BasicDataService
    {
        public static HttpResponseMessage CreatePostDataService(string uri, string httpContent)
        {
            var handler = new HttpClientHandler
            {
                CookieContainer = new CookieContainer(),
                UseCookies = false
            };
            var client = new HttpClient(handler);
            HttpContent content = new StringContent(httpContent);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            client.DefaultRequestHeaders.Add("Cookie",
                ResourcesComponent.GetStringFor("authCookie") + "=" + Settings.ticket);
            return client.PostAsync(uri, content).Result;
        }

        public static HttpResponseMessage CreateCustomDataService(string uri, HttpMethod method)
        {
            var handler = new HttpClientHandler
            {
                CookieContainer = new CookieContainer(),
                UseCookies = false
            };
            var webRequest = new HttpRequestMessage(method, uri);
            var client = new HttpClient(handler);
            client.DefaultRequestHeaders.Add("Cookie",
                ResourcesComponent.GetStringFor("authCookie") + "=" + Settings.ticket);
            return client.SendAsync(webRequest).Result;
        }
    }
}