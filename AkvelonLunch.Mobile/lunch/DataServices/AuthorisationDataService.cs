﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Linq;

namespace lunch
{
    public static class AuthorisationDataService
    {
        private class Creds
        {
            public string username { get; set; }
            public string password { get; set; }
            public bool persistent { get; set; }
        }

        private const string AuthConnectionString = "api/auth/signin/";
        private const string LogoutConnectionString = "api/auth/logOut/";
        private const string GetUserConnectionString = "api/user";

        public static CustomWebResponse<string> Login(string username, string psw, bool remember)
        {
            var cookies = new CookieContainer();
            var handler = new HttpClientHandler {CookieContainer = cookies};
            var uri = ResourcesComponent.GetStringFor("server") + AuthConnectionString;
            var webRequest = new HttpRequestMessage(HttpMethod.Get, uri);
            var creds = new Creds
            {
                username = username.ToLower(),
                password = psw,
                persistent = remember
            };

            var credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(creds)));
            webRequest.Headers.Add("Authorization", "Basic " + credentials);
            var client = new HttpClient(handler);
            try
            {
                var response = client.SendAsync(webRequest).Result;
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<string>(null, response.ReasonPhrase, false);
                var responseCookies = cookies.GetCookies(new Uri(uri)).Cast<Cookie>();
                foreach (var cookie in responseCookies)
                    if (cookie.Name == ResourcesComponent.GetStringFor("authCookie"))
                    {
                        return new CustomWebResponse<string>(cookie.Value);
                    }
                return new CustomWebResponse<string>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<string>(null, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<bool> Logout()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + LogoutConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode == HttpStatusCode.OK)
                    return new CustomWebResponse<bool>(true);
                return new CustomWebResponse<bool>(false, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<bool>(false, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }

        public static CustomWebResponse<UserModel> GetUser()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetUserConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<UserModel>(null, response.ReasonPhrase, false);
                var json = response.Content.ReadAsStringAsync().Result;
                return new CustomWebResponse<UserModel>(JsonConvert.DeserializeObject<UserModel>(json));
            }
            catch
            {
                return new CustomWebResponse<UserModel>(null, ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }
    }
}