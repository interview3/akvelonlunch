﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace lunch
{
    public static class ProvisionerDataService
    {
        private const string GetProvisionerConnectionString = "api/provisioner/allprovisioners/";

        public static CustomWebResponse<IList<ProvisionerModel>> GetAllProvisioners()
        {
            try
            {
                var uri = ResourcesComponent.GetStringFor("server") + GetProvisionerConnectionString;
                var response = BasicDataService.CreateCustomDataService(uri, HttpMethod.Get);
                if (response.StatusCode != HttpStatusCode.OK)
                    return new CustomWebResponse<IList<ProvisionerModel>>(null, response.ReasonPhrase, false);
                var jsonString = response.Content.ReadAsStringAsync().Result;
                if (jsonString.Length > 0)
                {
                    return
                        new CustomWebResponse<IList<ProvisionerModel>>(
                            JsonConvert.DeserializeObject<IList<ProvisionerModel>>(jsonString));
                }
                return new CustomWebResponse<IList<ProvisionerModel>>(null, response.ReasonPhrase, false);
            }
            catch
            {
                return new CustomWebResponse<IList<ProvisionerModel>>(null,
                    ResourcesComponent.GetStringFor("noConnection"), false);
            }
        }
    }
}