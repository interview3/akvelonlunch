using Android.Graphics;

namespace lunch.Droid
{
    static class ViewPreferences
    {
        public static Color getButtonColor()
        {
            return OrderComponent.isMade ? new Color(255, 102, 102) : new Color(31, 196, 136);
        }

        public static Color getTextColor()
        {
            return (OrderComponent.isMade || SettingsComponent.closed) ? new Color(204, 204, 204) : new Color(0, 0, 0);
        }
    }
}