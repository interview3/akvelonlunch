using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Controller;

namespace lunch.Droid.Services
{
    [Service]
    class OrderMakeService : Service
    {
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            NotificationController.CreateOrderMakeNotification();
            return StartCommandResult.NotSticky;
        }


        public override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}