using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Controller;
using Timer = System.Timers.Timer;
namespace lunch.Droid.Services
{
    [Service]
    class MainService : Service
    {
        public static event Action BlockingHourEvent;
        public static event Action OrderArrivedEvent;
        public static event Action MenuUpdateEvent;
        public static event Action OrderUpdateEvent;

        private Thread notificationThread = null;

        private int threadSleep = 5000;
        private static long OrderArrivedTimerPeriod = 60000;
        private static long BlockingHourTimerPeriod = 60000;
        private static long MenuUpdateTimerPeriod = 120000;
        private static long OrderUpdateTimerPeriod = 120000;

        private Timer OrderArrivedTimer = null;
        private Timer BlockingHourTimer = null;
        private Timer MenuUpdateTimer = null;
        private Timer OrderUpdateTimer = null;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            AuthorisationComponent.LoginEvent += OnLogin;
            AuthorisationComponent.LogoutEvent += OnLogout;
            OrderComponent.MakeOrderEvent += OnMakeOrder;
            OrderComponent.CancelOrderEvent += OnCancelOrder;
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            SetThread();
            return StartCommandResult.Sticky;
        }

        private void ClearResources()
        {
            if (notificationThread != null)
            {
                notificationThread.Abort();
            }
            if (OrderArrivedTimer != null)
            {
                OrderArrivedTimer.Dispose();
                OrderArrivedTimer = null;
            }
            if (BlockingHourTimer != null)
            {
                BlockingHourTimer.Dispose();
                BlockingHourTimer = null;
            }
            if (MenuUpdateTimer != null)
            {
                MenuUpdateTimer.Dispose();
                MenuUpdateTimer = null;
            }
            if (OrderUpdateTimer != null)
            {
                OrderUpdateTimer.Dispose();
                OrderUpdateTimer = null;
            }
        }

        private void OnLogin()
        {
            SetThread();
        }

        private void OnLogout()
        {
            ClearResources();
        }

        private void OnMakeOrder()
        {
            NotificationController.ClearOrderMakeAlarm();
            if (OrderArrivedTimer == null)
            {
                SetOrderArrivedTimer();
            }
            OrderArrivedTimer.Start();
        }

        private void OnCancelOrder()
        {
            if (OrderArrivedTimer != null)
            {
                OrderArrivedTimer.Stop();
            }
        }

        private void SetThread()
        {
            ClearResources();
            notificationThread = new Thread(() =>
            {
                var userResponce = AuthorisationComponent.GetUser();
                while (!(userResponce.ok && userResponce.responseObject != null))
                {
                    Thread.Sleep(threadSleep);
                    userResponce = AuthorisationComponent.GetUser();
                }
                NotificationController.SetAlarms();
                SetBlockingHourTimer();
                SetOrderArrivedTimer();
                SetMenuUpdateTime();
                SetOrderUpdateTime();
            });
            notificationThread.Start();
        }

        private void SetOrderUpdateTime()
        {
            OrderUpdateTimer = new Timer(OrderUpdateTimerPeriod);
            OrderUpdateTimer.Elapsed += CheckOrderUpdate;
            OrderUpdateTimer.Start();
        }

        private void SetMenuUpdateTime()
        {
            MenuUpdateTimer = new Timer(MenuUpdateTimerPeriod);
            MenuUpdateTimer.Elapsed += CheckMenuUpdate;
            MenuUpdateTimer.Start();
        }

        private void SetBlockingHourTimer()
        {
            BlockingHourTimer = new Timer(BlockingHourTimerPeriod);
            BlockingHourTimer.Elapsed += CheckBlockingHour;
            CheckBlockingHour(null, null);
            BlockingHourTimer.Start();
        }

        private void SetOrderArrivedTimer()
        {
            OrderArrivedTimer = new Timer(OrderArrivedTimerPeriod);
            OrderArrivedTimer.Elapsed += CheckOrderArrived;
        }

        private void CheckOrderUpdate(object sender, EventArgs e)
        {
            bool isUpdated = OrderComponent.UpdateOrder();
            if (isUpdated)
            {
                OrderUpdateEvent?.Invoke();
            }
        }

        private void CheckMenuUpdate(object sender, EventArgs e)
        {
            bool isUpdated = MenuComponent.UpdateMenu();
            if (isUpdated)
            {
                NotificationController.CreateMenuUpdateNotification();
                MenuUpdateEvent?.Invoke();
            }
        }

        private void CheckBlockingHour(object sender, EventArgs e)
        {
            var SettingsResponse = SettingsComponent.IsServiceClosed();
            if (SettingsResponse.ok)
            {
                var Blocked = SettingsResponse.responseObject;
                if (Blocked ^ SettingsComponent.closed)
                {
                    SettingsComponent.closed = Blocked;
                    BlockingHourEvent?.Invoke();
                }
            }
        }

        private void CheckOrderArrived(object sender, EventArgs e)
        {
            OrderComponent.OrderArrived();
            if (OrderComponent.isArrived)
            {
                OrderArrivedTimer.Stop();
                NotificationController.CreateOrderArriveNotification();
                OrderArrivedEvent?.Invoke();
            }
        }
        
        public override void OnDestroy()
        {
            ClearResources();
            base.OnDestroy();
        }

    }
}