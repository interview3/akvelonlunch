using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Adapter;
using lunch.Droid.Adapter.AdminAdapter;
using lunch.Droid.Controller;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
using Fragment = Android.Support.V4.App.Fragment;
namespace lunch.Droid.Activities.AdminActivities
{
    [Activity(ScreenOrientation = ScreenOrientation.Portrait)]
    public class AdminDishListActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;

        private ListView DishSearchList;
        private TextView DishSearchFilter;
        private Button AddDishButton;
        private Button BackButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminDishList);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.AdminMenuName), Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList<string>());
            GetViewItems();
            SubscribeView();
            AdminDishScreenAdapter.DishClickEvent += OnDishClick;
        }

        private void GetViewItems()
        {
            AddDishButton = FindViewById<Button>(Resource.Id.AddDishButton);
            BackButton = FindViewById<Button>(Resource.Id.AdminDishBackButton);
            DishSearchList = FindViewById<ListView>(Resource.Id.DishSearchList);
            DishSearchFilter = FindViewById<TextView>(Resource.Id.DishSearchFilter);
        }

        private void SubscribeView()
        {
            AddDishButton.Click += OnAddDishButtonClick;
            BackButton.Click += OnBackButtonClick;
            DishSearchFilter.AfterTextChanged += OnSearchFilterChange;
        }

        protected override void OnDestroy()
        {
            AdminDishScreenAdapter.DishClickEvent -= OnDishClick;
            base.OnDestroy();
        }

        protected override void OnStart()
        {
            SetListView();
            base.OnStart();
        }

        private void OnAddDishButtonClick(object sender, EventArgs e)
        {
            StartActivity(typeof(AddDishActivity));
        }

        private void OnBackButtonClick(object sender, EventArgs e)
        {
            Finish();
        }

        private async void SetListView()
        {
            CustomWebResponse<IList<DishModel>> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = DishComponent.GetAllDishes();
            });
            if (Response != null && Response.ok && Response.responseObject != null)
            {
                DishSearchList.Adapter = new AdminDishScreenAdapter(this,
                    Response.responseObject);
            }
        }

        private async void OnSearchFilterChange(object sened, EventArgs e)
        {
            CustomWebResponse<IList<DishModel>> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = DishComponent.GetAllDishesByName(DishSearchFilter.Text);
            });
            if (Response.ok && Response.responseObject != null)
            {
                DishSearchList.Adapter = new AdminDishScreenAdapter(this, Response.responseObject);
            }
        }

        private void OnDishClick(DishModel dish)
        {
            AdminMenuComponent.ToggleMenuItem(dish);
        }
    }
}