﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Adapter.AdminAdapter;
using lunch.Droid.Controller;
using lunch.Droid.Services;

namespace lunch.Droid.Activities.AdminActivities
{
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AdminMenuActivity : ActionBarActivity
    {
        public ListView AdminMenuList;
        private TextView AdminMenuMessage;
        private Button AdminSaveMenu;
        private Button AdminToDishList;
        private TextView AdminToolbar;

        public LeftMenuController leftMenuController;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminMenu);
            
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.AdminMenuName), Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList<string>());
            GetViewItems();
            SubscribeViewItems();
        }

        protected override void OnStart()
        {
            SetListView();
            MainService.MenuUpdateEvent += OnMenuUpdate;
            base.OnStart();
        }

        protected override void OnStop()
        {
            MainService.MenuUpdateEvent -= OnMenuUpdate;
            base.OnStop();
        }

        protected void GetViewItems()
        {
            AdminMenuMessage = FindViewById<TextView>(Resource.Id.AdminMenuMessage);
            AdminMenuList = FindViewById<ListView>(Resource.Id.AdminMenuListView);
            AdminSaveMenu = FindViewById<Button>(Resource.Id.AdminSaveMenuButton);
            AdminToolbar = FindViewById<TextView>(Resource.Id.adminToolbarNameViewText);
            AdminToolbar.Text = GetString(Resource.String.AdminMenuName);
            AdminToDishList = FindViewById<Button>(Resource.Id.AdminToDishListButton);
        }

        protected void SubscribeViewItems()
        {
            AdminSaveMenu.Click += OnSaveMenuClick;
            AdminToDishList.Click += OnToDishListButton;
        }

        private async void SetListView()
        {
            CustomWebResponse<Dictionary<string, List<DishModel>>> MenuDictionaryResponse = null;
            await Task.Factory.StartNew(() =>
            {
                MenuDictionaryResponse = AdminMenuComponent.GetMenuDictionary();
            });
            if (MenuDictionaryResponse != null && MenuDictionaryResponse.ok &&
                MenuDictionaryResponse.responseObject != null)
            {
                AdminMenuMessage.Visibility = ViewStates.Gone;
                AdminMenuList.Adapter = new AdminMenuAdapter(this, MenuDictionaryResponse.responseObject);
            }
            else
            {
                AdminMenuList.Adapter = null;
                AdminMenuMessage.Visibility = ViewStates.Visible;
            }
        }

        private void OnMenuUpdate()
        {
            AdminMenuComponent.UpdateAdminMenu();
            RunOnUiThread(SetListView);
        }


        public void RemoveMenuItem(DishModel dish)
        {
            AdminMenuComponent.RemoveItem(dish.Id);
            SetListView();
        }

        private async void OnSaveMenuClick(object sender, EventArgs e)
        {
            CustomWebResponse<bool> response = null;
            await Task.Factory.StartNew(() =>
            {
                response = AdminMenuComponent.SaveMenu();
            });
            if (response!= null && response.ok && response.responseObject)
            {
                MenuComponent.UpdateMenu();
                Toast.MakeText(this, GetString(Resource.String.MenuSaveSuccess), ToastLength.Long).Show();
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.MenuSaveError), ToastLength.Long).Show();
            }
        }

        private void OnToDishListButton(object sender, EventArgs e)
        {
            StartActivity(typeof(AdminDishListActivity));
        }
    }
}