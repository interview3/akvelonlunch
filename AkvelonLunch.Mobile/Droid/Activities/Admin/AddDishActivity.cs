﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Android.Support.V7.App;
using lunch.Droid.Controller;

namespace lunch.Droid.Activities.AdminActivities
{ 
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AddDishActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;
        private Spinner TypeSpinner;
        private Spinner ProvivionersSpinner;
        private Button SaveDishButton;
        private Button BackButton;
        private EditText DishName;
        private EditText DishDescriprion;
        private EditText DishPrice;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddDishScreen);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.dishName),
                Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList());
            GetViewItems();
            SubscribeView();
        }

        protected override void OnStart()
        {
            SetView();
            base.OnStart();
        }

        private void GetViewItems()
        {
            TypeSpinner = FindViewById<Spinner>(Resource.Id.TypeSpinner);
            ProvivionersSpinner = FindViewById<Spinner>(Resource.Id.ProvivionersSpinner);
            SaveDishButton = FindViewById<Button>(Resource.Id.AddDishSaveButton);
            BackButton = FindViewById<Button>(Resource.Id.AddDishBackButton);
            DishName = FindViewById<EditText>(Resource.Id.DishNameEdit);
            DishDescriprion = FindViewById<EditText>(Resource.Id.DishDescriptionEdit);
            DishPrice = FindViewById<EditText>(Resource.Id.DishPriceEdit);
        }

        private CustomWebResponse<IList<string>> GetAllProvisionersNames()
        {
            var resp = ProvisionerComponent.GetAllProvisioners();
            IList<string> names = new List<string>();
            if (!resp.ok || resp.responseObject == null) return new CustomWebResponse<IList<string>>(names);
            foreach (var prov in resp.responseObject)
            {
                names.Add(prov.ContactPersonName);
            }
            return new CustomWebResponse<IList<string>>(names);
        }

        private async void SetProvivionersSpinner()
        {
            CustomWebResponse<IList<string>> response = null;
            await Task.Factory.StartNew(() =>
            {
                response = GetAllProvisionersNames();
            });
            if (response != null && response.ok && response.responseObject != null)
            {
                ProvivionersSpinner.Adapter = new ArrayAdapter<string>(this,
                    Android.Resource.Layout.SimpleListItem1, response.responseObject);
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.GettingProvisionersError), ToastLength.Long)
                    .Show();
            }

        }

        private static CustomWebResponse<IList<string>> GetAllDishTypesNames()
        {
            var resp = DishComponent.GetAllDishTypes();
            IList<string> names = new List<string>();
            if (!resp.ok || resp.responseObject == null) return new CustomWebResponse<IList<string>>(names);
            foreach (var dishtype in resp.responseObject)
            {
                names.Add(dishtype.Name);
            }
            return new CustomWebResponse<IList<string>>(names);
        }


        private async void SetDishTypeSpiner()
        {
            CustomWebResponse<IList<string>> response = null;
            await Task.Factory.StartNew(() =>
            {
                response = GetAllDishTypesNames();
            });
            if (response != null && response.ok && response.responseObject != null)
            {
                TypeSpinner.Adapter = new ArrayAdapter<String>(this,
                    Android.Resource.Layout.SimpleListItem1, response.responseObject);
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.GettingDishTypesError), ToastLength.Long)
                    .Show();
            }

        }

        private void SetView()
        {
            SetDishTypeSpiner();
            SetProvivionersSpinner();
        }


        private void SubscribeView()
        {
            SaveDishButton.Click += OnSaveDishButtonClick;
            BackButton.Click += OnBackButtonClick;
        }

        private void OnBackButtonClick(object sender, EventArgs e)
        {
            Finish();
        }

        private DishModel CreateDishModel()
        {
            if (string.IsNullOrEmpty(DishName.Text))
            {
                RunOnUiThread(
                    () =>
                    {
                        Toast.MakeText(this, GetString(Resource.String.DishNameEmptyError), ToastLength.Long).Show();
                    });
                return null;
            }
            decimal price;
            if (!decimal.TryParse(DishPrice.Text, out price))
            {
                RunOnUiThread(
                    () => { Toast.MakeText(this, GetString(Resource.String.DishPriseError), ToastLength.Long).Show(); });
                return null;
            }
            var provisionerNumber = ProvivionersSpinner.SelectedItemId;
            var provisioner = ProvisionerComponent.GetProvisionerByNumber((int) provisionerNumber);
            if (provisioner == null)
            {
                RunOnUiThread(
                    () => { Toast.MakeText(this, GetString(Resource.String.noProvModel), ToastLength.Long).Show(); });
                return null;
            }
            var dishTypeNumber = TypeSpinner.SelectedItemId;
            var dishType = DishComponent.GetDishTypeById((int) dishTypeNumber);
            if (dishType == null)
            {
                RunOnUiThread(
                    () => { Toast.MakeText(this, GetString(Resource.String.noDishType), ToastLength.Long).Show(); });
                return null;
            }
            var dish = new DishModel(DishName.Text, DishDescriprion.Text, price, dishType, provisioner, 0);
            return dish;
        }

        private async void OnSaveDishButtonClick(object sender, EventArgs e)
        {
            CustomWebResponse<int> createResponse = null;
            await Task.Factory.StartNew(() =>
            {
                var dish = CreateDishModel();
                if (dish == null) return;
                createResponse = DishComponent.Create(dish);
            });
            if (createResponse != null && createResponse.ok && createResponse.responseObject != -1)
            {
                Toast.MakeText(this, GetString(Resource.String.Success), ToastLength.Long).Show();
                StartActivity(typeof (AdminDishListActivity));
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.dishNotCreated) + createResponse.message,
                    ToastLength.Long).Show();
            }
        }
    }
}