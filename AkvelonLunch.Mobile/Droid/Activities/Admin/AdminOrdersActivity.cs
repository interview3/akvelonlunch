﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4;
using lunch.Droid.Activities.UserActivities;
using lunch.Droid.AdminAdapter;
using lunch.Droid.Services;
using Timer = System.Timers.Timer;
namespace lunch.Droid.Activities.AdminActivities
{
    using Android.Support.V4.Widget;
    using Controller;
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    class AdminOrdersActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;

        private TextView TotalSummOfOrdersLabel;
        private ListView AdminOrdersList;
        private Button DeliverOrdersButton;

      

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(lunch.Droid.Resource.Layout.AdminOrders);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.AdmOrdersName), Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList<string>());
            GetViewItems();
            SubscribeViews();             
        }

        protected override void OnStart()
        {
           base.OnStart();
        }

        protected override void OnStop()
        {          
            base.OnStop();
        }

        protected override void OnResume()
        {
            SetListView();
            SetView();
            base.OnResume();
        }

        private async void SetListView()
        {
            Dictionary<string, int> Response = new Dictionary<string, int>();
            await Task.Factory.StartNew(() =>
            {
                Response = AdminOrderComponent.GetOrderedDishes(true);
            });
            AdminOrdersList.Adapter = new AdminOrdersScreenAdapter(this, Response);
        }

        private void GetViewItems()
        {
             TotalSummOfOrdersLabel = FindViewById<TextView>(Resource.Id.TotalSummOfOrdersLabel);
             AdminOrdersList = FindViewById<ListView>(Resource.Id.AdminOrdersView);
             DeliverOrdersButton = FindViewById<Button>(Resource.Id.DeliverOrdersButton);
        }

        private void SubscribeViews()
        {
            DeliverOrdersButton.Click += OnDeliverOrdersButtonClick;            
        }


        private async void SetView()
        {
            string cost = "0";
            await Task.Factory.StartNew(() =>
            {
                cost = AdminOrderComponent.GetCostOfAllOrders(true).ToString();
            });
            TotalSummOfOrdersLabel.Text = GetString(Resource.String.TotalSummOfOrdersLabel) + cost;

            if (AdminOrderComponent.IfDelivered())
            {
                DeliverOrdersButton.Enabled = false;
                DeliverOrdersButton.Text = GetString(Resource.String.AdminOrdersArriveSuccessMessage);
            }
            else
            {
                DeliverOrdersButton.Enabled = true;
                DeliverOrdersButton.Text = GetString(Resource.String.DeliverOrdersLabel);
            }
        }
        
        private void OnDeliverOrdersButtonClick(object sender, EventArgs e)
        {
            var dialog = new Android.Support.V7.App.AlertDialog.Builder(this)
                .SetPositiveButton(Resource.String.AdminOrdersArrivedDialogYes, async (_sender, args) =>
                {
                    CustomWebResponse<bool> Response = null;
                    await Task.Factory.StartNew(() =>
                    {
                        Response = AdminOrderComponent.DeliverOrders();
                    });
                    if (Response != null && Response.ok)
                    {
                        Toast.MakeText(this, GetString(Resource.String.OrdersDelivered), ToastLength.Long)
                            .Show();
                    }
                    else
                    {
                        Toast.MakeText(this, Response.message, ToastLength.Long).Show();
                    }
                    SetListView();
                    SetView();
                })
                .SetNegativeButton(Resource.String.AdminOrdersArrivedDialogNo, (_sender, args) =>
                {
                    
                })
                .SetMessage(Resource.String.AdminOrdersArrivedDialogMessage)
                .SetTitle(Resource.String.AdminOrdersArrivedDialogTitle)
                .Show();
        }

        private void OnLogout()
        {
            StartActivityForResult(typeof(LoginActivity), 0);
        }
    }
}