using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Controller;

namespace lunch.Droid.Activities.AdminActivities
{
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class UserEditActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;

        private TextView UserEditName;
        private EditText UserEditBalance;
        private Button EditUserSaveButton;
        private Button EditUserBackButton;
        private UserModel User;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            SetContentView(Resource.Layout.UserEdit);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.UserEditName),
                Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList());
            User = AdminUserComponent.EditingUser();
            GetViewItems();
            SubscribeView();
            SetView();
            base.OnCreate(savedInstanceState);
        }

        private void SetView()
        {
            UserEditName.Text = User.FirstName + " " + User.LastName;
            UserEditBalance.Text = Convert.ToInt32(User.Balance).ToString();
        }

        private void SubscribeView()
        {
            UserEditBalance.AfterTextChanged += OnBalanceChanged;
            EditUserSaveButton.Click += OnEditUserSaveButtonClick;
            EditUserBackButton.Click += OnEditUserBackButtonClick;
        }

        private void GetViewItems()
        {
            UserEditName = FindViewById<TextView>(Resource.Id.UserEditName);
            UserEditBalance = FindViewById<EditText>(Resource.Id.UserEditBalance);
            EditUserSaveButton = FindViewById<Button>(Resource.Id.EditUserSaveButton);
            EditUserBackButton = FindViewById<Button>(Resource.Id.EditUserBackButton);
        }

        private void OnEditUserBackButtonClick(object sender, EventArgs e)
        {
            Finish();
        }

        private  void OnBalanceChanged(object sender, EventArgs e)
        {
            var balance = Convert.ToInt32(User.Balance);
            if (int.TryParse(UserEditBalance.Text, out balance))
            {
                User.Balance = balance;
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.BalanceChangeError), ToastLength.Long).Show();
            }
        }

        private async void OnEditUserSaveButtonClick(object sender, EventArgs e)
        {
            CustomWebResponse<bool> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = AdminUserComponent.Edit(User);
            });
            if (Response.ok && Response.responseObject)
            {
                Toast.MakeText(this, GetString(Resource.String.UserEditSuccess),
                    ToastLength.Long).Show();
                Finish();
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.UserEditError), ToastLength.Long).Show();
            }
        }
    }
}