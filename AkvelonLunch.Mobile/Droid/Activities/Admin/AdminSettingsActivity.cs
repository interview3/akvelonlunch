using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Controller;
using Android.Support.V7.App;
using Boolean = Java.Lang.Boolean;
using Android.Content.PM;

namespace lunch.Droid.Activities.AdminActivities
{
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AdminSettingsActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;
        private TimePicker CloseMenuTimePicker;
        private Button AdminCloseMenuSaveButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminSettings);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.AdminSettingsName), Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList<string>());
            GetViewItems();
            SubscribeViewItems();
        }

        protected override void OnStart()
        {
            base.OnStart();
            SetView();
        }

        private void GetViewItems()
        {
            CloseMenuTimePicker = FindViewById<TimePicker>(Resource.Id.AdminCloseMenuTimePicker);
            AdminCloseMenuSaveButton = FindViewById<Button>(Resource.Id.AdminCloseMenuSaveButton);
        }

        private async void SetView()
        {
            CloseMenuTimePicker.SetIs24HourView(Boolean.True);
            CustomWebResponse<SettingModel> blockingHourSettingResponse = null;
            await Task.Factory.StartNew(() =>
            {
                blockingHourSettingResponse = SettingsComponent.GetClosingHour();
            });
            if (blockingHourSettingResponse != null &&
                blockingHourSettingResponse.ok &&
                blockingHourSettingResponse.responseObject != null)
            {
                var blockingHourString = blockingHourSettingResponse.responseObject.Value;
                DateTime blockingHour = DateTime.ParseExact(blockingHourString, "H:m:s",
                    CultureInfo.InvariantCulture);
                CloseMenuTimePicker.CurrentHour = (Java.Lang.Integer) blockingHour.Hour;
                CloseMenuTimePicker.CurrentMinute = (Java.Lang.Integer) blockingHour.Minute;
            }
            else
            {
                CloseMenuTimePicker.CurrentHour = (Java.Lang.Integer) Settings.CloseMenuHour;
                CloseMenuTimePicker.CurrentMinute = (Java.Lang.Integer) Settings.CloseMenuMinute;
            }
        }

        private void SubscribeViewItems()
        {
            CloseMenuTimePicker.TimeChanged += OnCloseMenuTimeChanged;
            AdminCloseMenuSaveButton.Click += OnAdminCloseMenuSaveButtonClick;
        }

        private void OnCloseMenuTimeChanged(object sender, EventArgs e)
        {
            var timeArg = (TimePicker.TimeChangedEventArgs)e;
            Settings.CloseMenuHour = timeArg.HourOfDay;
            Settings.CloseMenuMinute = timeArg.Minute;
        }

        private async void OnAdminCloseMenuSaveButtonClick(object sender, EventArgs e)
        {
            CustomWebResponse<bool> response = null;
            await Task.Factory.StartNew(() =>
            {
                response = SettingsComponent.SetClosingHour(Settings.CloseMenuHour,
                    Settings.CloseMenuMinute);
            });
            if (response.ok && response.responseObject)
            {
                Toast.MakeText(this, GetString(Resource.String.AdminCloseMenuSuccess), ToastLength.Long).Show();
            }
            else
            {
                Toast.MakeText(this, GetString(Resource.String.AdminCloseMenuError), ToastLength.Long).Show();
            }
        }
    }
}