﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4;
using lunch.Droid.Adapter;
using lunch.Droid.Adapter.AdminAdapter;
using lunch.Droid.Controller;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
using Fragment = Android.Support.V4.App.Fragment;
namespace lunch.Droid.Activities.AdminActivities
{
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AdminUserActivity : ActionBarActivity
    {
        public LeftMenuController leftMenuController;

        private ListView UserSearchList;
        private TextView UserSearchFilter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminUser);
            leftMenuController = new LeftMenuController(this, GetString(Resource.String.AdminUserName), Resources.GetStringArray(Resource.Array.LeftMenuListItems).ToList<string>());
            GetViewItems();
            SubscribeView();
        }

        private void GetViewItems()
        {
            UserSearchList = FindViewById<ListView>(Resource.Id.UserSearchList);
            UserSearchFilter = FindViewById<TextView>(Resource.Id.UserSearchFilter);
        }

        private void SubscribeView()
        {
            UserSearchFilter.AfterTextChanged += OnSearchFilterChange;
        }
        
        protected override void OnDestroy()
        {
            AdminUserScreenAdapter.SelectUser -= OnSelectUser;
            base.OnDestroy();
        }

        protected override void OnStart()
        {
            SetListView();
            AdminUserScreenAdapter.SelectUser += OnSelectUser;
            base.OnStart();
        }

        private async void SetListView()
        {
            CustomWebResponse<IList<UserModel>> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = AdminUserComponent.GetAllUsers();
            });
            if (Response != null && Response.ok && Response.responseObject != null)
            {
                UserSearchList.Adapter = new AdminUserScreenAdapter(this,
                    Response.responseObject);
            }
        }

        private async void OnSearchFilterChange(object sened, EventArgs e)
        {
            CustomWebResponse<IList<UserModel>> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = AdminUserComponent.GetAllUsersByKey(UserSearchFilter.Text);
            });
            if (Response != null && Response.ok && Response.responseObject != null)
            {
                UserSearchList.Adapter = new AdminUserScreenAdapter(this, Response.responseObject);
            }
        }

        private void OnSelectUser()
        {
            StartActivity(typeof(UserEditActivity));
        }
        
    }
}