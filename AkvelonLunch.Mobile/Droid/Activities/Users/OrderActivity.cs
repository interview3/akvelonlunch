﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4;
using lunch.Droid.Services;
using Timer = System.Timers.Timer;
using System.Threading;
using lunch.Droid.UserAdapter;

namespace lunch.Droid.Activities.UserActivities
{
    using Controller;
    using System.Threading.Tasks;

    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    class OrderActivity : ActionBarActivity
    {
        public ToolbarController toolbarController;

        private TextView OrderStatusLabel;
        private ListView OrderList;
        private Button MakelOrderButton;
        private Button CancelOrderButton;
        private Button ClearOrderButton;
        private TextView ToolbarBalance;
        private TextView ToolbarOrderCost;
        private ProgressBar ActionIndicator;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(lunch.Droid.Resource.Layout.Order);
            toolbarController = new ToolbarController(this, GetString(Resource.String.OrderName));
            GetViewItems();
            SubscribeViews();
        }

        private async void CheckUser()
        {
            await Task.Factory.StartNew(() =>
            {
                var userResponse = AuthorisationComponent.GetUser();
                if (!userResponse.ok || userResponse.responseObject == null)
                {
                    StartActivityForResult(typeof(LoginActivity), 0);
                }
                else
                {
                    AuthorisationComponent.LogoutEvent += OnLogout;
                    MainService.OrderArrivedEvent += OnOrderArrived;
                    MainService.BlockingHourEvent += OnBlockingHour;
                    MainService.MenuUpdateEvent += OnMenuUpdate;
                    MainService.OrderUpdateEvent += OnOrderUpdate;
                }
            });
        }

        protected override void OnStart()
        {
            base.OnStart();
            StopSpinner();
            CheckUser();
        }

        protected override void OnStop()
        {
            AuthorisationComponent.LogoutEvent -= OnLogout;
            MainService.OrderArrivedEvent -= OnOrderArrived;
            MainService.BlockingHourEvent -= OnBlockingHour;
            MainService.MenuUpdateEvent -= OnMenuUpdate;
            MainService.OrderUpdateEvent -= OnOrderUpdate;
            base.OnStop();
        }

        protected override void OnResume()
        {
            SetListView();
            SetView();
            base.OnResume();
        }

        private async void SetListView()
        {
            CustomWebResponse<OrderModel> Response = null;
            await Task.Factory.StartNew(() =>
            {
                Response = OrderComponent.GetOrder();
            });
            if (Response != null && Response.ok && Response.responseObject != null)
            {
                OrderList.Adapter = new OrderScreenAdapter(this, Response.responseObject.Items);
            }
            else
            {
                OrderList.Adapter = null;
            }
        }

        private void GetViewItems()
        {
            OrderStatusLabel = FindViewById<TextView>(Resource.Id.OrderStatusLabel);
            OrderList = FindViewById<ListView>(Resource.Id.ListOrderView);
            CancelOrderButton = FindViewById<Button>(Resource.Id.CancelOrderButton);
            MakelOrderButton = FindViewById<Button>(Resource.Id.MakeOrderButton);
            ClearOrderButton = FindViewById<Button>(Resource.Id.ClearOrderButton);
            ActionIndicator = FindViewById<ProgressBar>(Resource.Id.OrderActivityIndicator);
        }

        private void SubscribeViews()
        {
            MakelOrderButton.Click += OnMakeOrderButtonClick;
            CancelOrderButton.Click += OnCancelOrderButtonClick;
            ClearOrderButton.Click += OnClearOrderButtonClick;
        }

        private void SetButtons()
        {
            if (OrderComponent.isArrived || SettingsComponent.closed)
            {
                ClearOrderButton.Visibility = ViewStates.Gone;
                CancelOrderButton.Visibility = ViewStates.Gone;
                MakelOrderButton.Visibility = ViewStates.Gone;
            }
            else
            {
                if (OrderComponent.isMade)
                {
                    ClearOrderButton.Visibility = ViewStates.Gone;
                    MakelOrderButton.Visibility = ViewStates.Gone;
                    CancelOrderButton.Visibility = ViewStates.Visible;

                }
                else
                {
                    ClearOrderButton.Visibility = ViewStates.Visible;
                    MakelOrderButton.Visibility = ViewStates.Visible;
                    CancelOrderButton.Visibility = ViewStates.Gone;
                }
            }
        }

        private void SetMessageText()
        {
            if (OrderComponent.isArrived)
            {
                OrderStatusLabel.Text = GetString(Resource.String.OrderName) + " №" + OrderComponent.OrderId + " " +
                                        GetString(Resource.String.ArrivedMessage);
            }
            else if (OrderComponent.isMade)
            {
                OrderStatusLabel.Text = GetString(Resource.String.OrderName) + " №" + OrderComponent.OrderId + " " +
                                  GetString(Resource.String.SentMessage);
            }
            else
            {
                OrderStatusLabel.Text = string.Empty;
            }
        }
        
        private void SetView()
        {
            toolbarController.UpdateToolbar();
            SetButtons();
            SetMessageText();
        }

        private void OnOrderUpdate()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnMenuUpdate()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnBlockingHour()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }
        

        private void OnOrderArrived()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }
       
        private async void OnCancelOrderButtonClick(object sender, EventArgs e)
        {
            StartSpinner();
            CustomWebResponse<bool> cancelResponse = null;
            await Task.Factory.StartNew(() =>
            {
                cancelResponse = OrderComponent.CancelOrder();
            });
            if (cancelResponse != null && cancelResponse.ok)
            {
                SetView();
                SetListView();
            }
            else
            {
                Toast.MakeText(this, cancelResponse.message, ToastLength.Long).Show();
            }
            StopSpinner();
        }
       
        private async void OnMakeOrderButtonClick(object sender, EventArgs e)
        { 
            StartSpinner();
            CustomWebResponse<int> makeResponse = null;
            await  Task.Factory.StartNew(() =>
            {
                makeResponse = OrderComponent.MakeOrder();
            });
            if (makeResponse != null && makeResponse.ok)
            {
                SetView();
                SetListView();
            }
            else
            {
                Toast.MakeText(this, makeResponse.message, ToastLength.Long).Show();
            }
            StopSpinner();
        }

        void StartSpinner()
        {
            ActionIndicator.Visibility = ViewStates.Visible;
        }
        void StopSpinner()
        {
            ActionIndicator.Visibility = ViewStates.Gone;
        }

        private void OnClearOrderButtonClick(object sender, EventArgs e)
        {
            OrderComponent.ClearOrder();
            SetView();
            SetListView();
        }

        private void OnLogout()
        {
            StartActivityForResult(typeof(LoginActivity), 0);
        }
    }
}