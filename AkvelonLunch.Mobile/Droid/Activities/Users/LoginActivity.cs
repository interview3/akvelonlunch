﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using lunch.Droid;
using lunch.Droid.Controller;
using System.Threading.Tasks;

namespace lunch.Droid.Activities.UserActivities
{
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    class LoginActivity : Activity
    {
        private Button EnterButton;
        private EditText Username;
        private EditText Password;     
        private CheckBox RememberCheckBox;
        private ProgressBar ActionIndicator;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Login);
            EnterButton = FindViewById<Button>(Resource.Id.EnterButtonLoginView);
            Username = FindViewById<EditText>(Resource.Id.UsernameLoginView);
            Password = FindViewById<EditText>(Resource.Id.PasswordLoginView);
            RememberCheckBox = FindViewById<CheckBox>(Resource.Id.RememberCheckBox);
            ActionIndicator = FindViewById<ProgressBar>(Resource.Id.LoginActivityIndicator);
            EnterButton.Click += onEnterButtonClick;
            StopSpinner();
        }
        
        private async void onEnterButtonClick(object sender, EventArgs e)
        {
            CustomWebResponse<bool> loginResponse = null;
            StartSpinner();
            string user = Username.Text;
            string pswd = Password.Text;
            bool rem = RememberCheckBox.Checked;
            await Task.Factory.StartNew(() =>
            {
                loginResponse = AuthorisationComponent.Login(user, pswd, rem);
            });
            
            if (loginResponse != null && loginResponse.ok && loginResponse.responseObject)
            {
                SetResult(Result.Ok);
                Finish();
            }
            else
            {
                Toast.MakeText(this, loginResponse.message, ToastLength.Long).Show();
            }
            StopSpinner();
        }

        public override void OnBackPressed()
        {
            Intent startMain = new Intent(Intent.ActionMain);
            startMain.AddCategory(Intent.CategoryHome);
            startMain.SetFlags(ActivityFlags.ClearTop);
            StartActivity(startMain);
            base.OnBackPressed();
        }

        void StartSpinner()
        {
            Username.Enabled = false;
            Password.Enabled = false;
            RememberCheckBox.Enabled = false;
            EnterButton.Enabled = false;
            ActionIndicator.Visibility = ViewStates.Visible;
        }
        void StopSpinner()
        {
            Username.Enabled = true;
            Password.Enabled = true;
            RememberCheckBox.Enabled = true;
            EnterButton.Enabled = true;
            ActionIndicator.Visibility = ViewStates.Gone;
        }
    }
}