﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities.AdminActivities;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using DialogFragment = Android.Support.V4.App.DialogFragment;

namespace lunch.Droid.Activities.UserActivities
{
    public class AccountDialogFragment : DialogFragment
    {
        public event Action LogoutEvent;

        private ListView AccountDialogListView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var view = Activity.LayoutInflater.Inflate(Resource.Layout.AccountDialogFragment, container, false);
            return view;

        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var builder = new AlertDialog.Builder(Activity);
            View view = Activity.LayoutInflater.Inflate(Resource.Layout.AccountDialogFragment, null);
            builder.SetView(view);
            var dialog = builder.Create();
            AccountDialogListView = view.FindViewById<ListView>(Resource.Id.AccountDialogListView);           

            ArrayAdapter<String> ListAdapter = new ArrayAdapter<String>(this.Context, Android.Resource.Layout.SimpleListItem1, Resources.GetStringArray(Resource.Array.AccountDialogListItems));
            if (AuthorisationComponent.GetUser().responseObject.RoleId == 1)
            {
                ListAdapter = new ArrayAdapter<String>(this.Context, Android.Resource.Layout.SimpleListItem1, Resources.GetStringArray(Resource.Array.AccountDialogListItemsPlusAdmin));
                AccountDialogListView.ItemClick += OnAdminListItemClick;              
            }
            else
            {
                AccountDialogListView.ItemClick += OnListItemClick;
            }
            AccountDialogListView.Adapter = ListAdapter;
            return dialog;
        }

        private void OnListItemClick(object sender, EventArgs ev)
        {
            var clickArg = (AdapterView.ItemClickEventArgs) ev;
            switch (clickArg.Id)
            {
                case 0:
                    OnSettingsClick();
                    break;
                case 1:
                    OnLogoutClick();
                    break;
            }
            Activity.SupportFragmentManager.BeginTransaction().Remove(this).Commit();
        }

        private void OnAdminListItemClick(object sender, EventArgs ev)
        {
            var clickArg = (AdapterView.ItemClickEventArgs)ev;
            switch (clickArg.Id)
            {
                case 0:
                    OnAdminPanelClick();
                    break;
                case 1:
                    OnSettingsClick();
                    break;
                case 2:
                    OnLogoutClick();
                    break;
            }
            Activity.SupportFragmentManager.BeginTransaction().Remove(this).Commit();
        }

        private void OnAdminPanelClick()
        {
           Activity.StartActivity(typeof(AdminOrdersActivity));
        }

        private void OnSettingsClick()
        {
            Activity.StartActivity(typeof(SettingActivity));
        }

        private async void OnLogoutClick()
        {
            await Task.Factory.StartNew(() => {
                AuthorisationComponent.Logout();
            });
        }
    }

}