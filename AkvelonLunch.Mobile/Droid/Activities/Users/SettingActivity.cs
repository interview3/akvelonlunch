using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Boolean = Java.Lang.Boolean;

namespace lunch.Droid.Activities.UserActivities
{
    using Controller;
    [Activity(Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class SettingActivity : ActionBarActivity
    {
        private TimePicker SettingsTimePicker;
        private CheckBox SettingsMakeOrderNotification;
        private CheckBox SettingsBalanceNotificationCheckbox;
        private ToolbarController toolbarController;
        private EditText SettingsMinBalance;
        private int minBalance;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Settings);
            toolbarController = new ToolbarController(this, GetString(Resource.String.SettingsName));
            minBalance = Settings.remBalanceVal;
            GetViewItems();
            SetView();
            SubscribeView();
        }

        private async void CheckUser()
        {
            await Task.Factory.StartNew(() =>
            {
                var userResponse = AuthorisationComponent.GetUser();
                if (!userResponse.ok || userResponse.responseObject == null)
                {
                    StartActivityForResult(typeof(LoginActivity), 0);
                }
                else
                {
                    AuthorisationComponent.LogoutEvent += OnLogout;
                }
            });
        }

        protected override void OnStart()
        {
            CheckUser();
            base.OnStart();
        }
        
        protected override void OnStop()
        {
            AuthorisationComponent.LogoutEvent -= OnLogout;
            base.OnStop();
        }

        private void GetViewItems()
        {
            SettingsTimePicker = FindViewById<TimePicker>(Resource.Id.SettingsTimePicker);
            SettingsMakeOrderNotification =
                FindViewById<CheckBox>(Resource.Id.SettingsMakeOrderNotificationCheckbox);
            SettingsBalanceNotificationCheckbox = FindViewById<CheckBox>(Resource.Id.SettingsBalanceNotificationCheckbox);
            SettingsMinBalance = FindViewById<EditText>(Resource.Id.SettingsMinBalance);
        }

        private void SetView()
        {
            SettingsTimePicker.SetIs24HourView(Boolean.True);
            SettingsTimePicker.CurrentHour = (Java.Lang.Integer)Settings.RemHour;
            SettingsTimePicker.CurrentMinute = (Java.Lang.Integer)Settings.RemMinute;
            SettingsMinBalance.Text = minBalance.ToString();
            SettingsBalanceNotificationCheckbox.Checked = Settings.remBalance;
            SettingsMakeOrderNotification.Checked = Settings.Switcher;
            SettingsMinBalance.Enabled = SettingsBalanceNotificationCheckbox.Checked;
            SettingsTimePicker.Enabled = SettingsMakeOrderNotification.Checked;
            toolbarController.UpdateToolbar();
        }

        private void SubscribeView()
        {
            SettingsMakeOrderNotification.Click += OnSettingsMakeOrderNotificationClick;
            SettingsTimePicker.TimeChanged += OnSettingsTimePickerTimeChanged;
            SettingsBalanceNotificationCheckbox.Click += OnSettingsBalanceNotificationCheckboxClick;
            SettingsMinBalance.AfterTextChanged += OnSettingsMinBalanceAfterTextChanged;
        }

        private async void OnSettingsBalanceNotificationCheckboxClick(object sender, EventArgs e)
        {
            Settings.remBalance = SettingsBalanceNotificationCheckbox.Checked;
            SettingsMinBalance.Enabled = SettingsBalanceNotificationCheckbox.Checked;
            await Task.Factory.StartNew(() =>
            {
                if (SettingsMinBalance.Enabled)
                {
                    NotificationController.SetBalanceAlarm();
                }
                else
                {
                    NotificationController.ClearBalanceAlarm();
                }
            });
        }

        private async void OnSettingsMinBalanceAfterTextChanged(object sender, EventArgs e)
        {
            await Task.Factory.StartNew(() =>
            {
                if (int.TryParse(SettingsMinBalance.Text, out minBalance))
                {
                    Settings.remBalanceVal = minBalance;
                    NotificationController.SetBalanceAlarm();
                }
            });
        }

        private async void OnSettingsMakeOrderNotificationClick(object sender, EventArgs e)
        {
            await Task.Factory.StartNew(() =>
            {
                Settings.Switcher = SettingsMakeOrderNotification.Checked;
            });
            SettingsTimePicker.Enabled = SettingsMakeOrderNotification.Checked;
            if (SettingsMinBalance.Enabled)
            {
                NotificationController.SetOrderMakeAlarm();
            }
            else
            {
                NotificationController.ClearOrderMakeAlarm();
            }
        }

        private void OnSettingsTimePickerTimeChanged(object sender, EventArgs e)
        {
            var timeArg = (TimePicker.TimeChangedEventArgs)e;
            Settings.RemHour = timeArg.HourOfDay;
            Settings.RemMinute = timeArg.Minute;
            NotificationController.SetOrderMakeAlarm();
        }

        private void OnLogout()
        {
            StartActivityForResult(typeof(LoginActivity), 0);
        }
    }
}