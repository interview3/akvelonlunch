﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content.Res;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Java.Util;
using lunch.Droid.Adapter.UserAdapter;
using lunch.Droid.Services;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Timer = System.Timers.Timer;
namespace lunch.Droid.Activities.UserActivities
{
    using Controller;
    [Activity(MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MenuActivity : ActionBarActivity
    {
        private ListView MenuList;
        private Button ToolbarBack;
        private FrameLayout MenuFrameLayout;
        private Button OrderEnterButton;
        private TextView MenuMessage;
        private bool? MenuBlocked = null;
        public ToolbarController toolbarController;
        private ProgressBar ActionIndicator;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Menu);
            OrderComponent.Subscribe();
            toolbarController = new ToolbarController(this, GetString(Resource.String.MenuName));
            NotificationController.SetNotificationController(this, AlarmService);
            StartMainService();
            GetViewItems();
            SubscribeViews();
        }
       

        private void StartMainService()
        {
            var service = new Intent(this, typeof(MainService));
            StartService(service);
        }

        protected virtual void GetViewItems()
        {
            MenuList = FindViewById<ListView>(Resource.Id.ListMenuView);
            ToolbarBack = FindViewById<Button>(Resource.Id.ToolbarBackButton);
            MenuFrameLayout = FindViewById<FrameLayout>(Resource.Id.MenuFrameLayout);
            OrderEnterButton = FindViewById<Button>(Resource.Id.OrderEnterButton);
            MenuMessage = FindViewById<TextView>(Resource.Id.MenuMessage);
            ActionIndicator = FindViewById<ProgressBar>(Resource.Id.MenuActivityIndicator);
        }

        private void SubscribeViews()
        {
            OrderEnterButton.Click += OnOrderEnterButtonClick;
        }

        protected async void SetListView()
        {
            StartSpinner();
            CustomWebResponse<Dictionary<string, List<DishModel>>> MenuResponse = null;
            await Task.Factory.StartNew(() =>
            {
                MenuResponse = MenuComponent.GetMenuDictionary();
            });
            if (MenuResponse != null && MenuResponse.ok && MenuResponse.responseObject != null)
            {
                MenuMessage.Visibility = ViewStates.Gone;
                MenuList.Adapter = new MenuScreenAdapter(this, MenuResponse.responseObject);
                //toolbarController.UpdateToolbar();
                if (SettingsComponent.closed)
                {
                    MenuFrameLayout.Foreground = ResourcesCompat.GetDrawable(Resources, Resource.Drawable.closed,
                        null);
                }
                else
                {
                    MenuFrameLayout.Foreground = ResourcesCompat.GetDrawable(Resources,
                        Android.Resource.Color.Transparent, null);
                }
            }
            else
            {
                MenuList.Adapter = null;
                MenuMessage.Visibility = ViewStates.Visible;
                MenuFrameLayout.Foreground = ResourcesCompat.GetDrawable(Resources, Resource.Drawable.closed,
                    null);
            }
            StopSpinner();
        }

        private void SetView()
        {
            ToolbarBack.Visibility = ViewStates.Invisible;
            toolbarController.UpdateToolbar();
        }

        private void OnOrderUpdate()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnMenuUpdate()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnBlockingHour()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnOrderArrived()
        {
            RunOnUiThread(() =>
            {
                SetView();
                SetListView();
            });
        }

        private void OnOrderEnterButtonClick(object sender, EventArgs e)
        {
           StartActivity(typeof(OrderActivity));
        }

        private async void CheckUser()
        {
            await Task.Factory.StartNew(() =>
            {
                var userResponse = AuthorisationComponent.GetUser();
                if (!userResponse.ok || userResponse.responseObject == null)
                {
                    StartActivityForResult(typeof (LoginActivity), 0);
                }
                else
                {
                    AuthorisationComponent.LogoutEvent += OnLogout;
                    MainService.OrderArrivedEvent += OnOrderArrived;
                    MainService.BlockingHourEvent += OnBlockingHour;
                    MainService.MenuUpdateEvent += OnMenuUpdate;
                    MainService.OrderUpdateEvent += OnOrderUpdate;
                }
            });
        }

        protected override void OnStart()
        {
            base.OnStart();
            CheckUser();
            StopSpinner();
        }

        protected override void OnStop()
        {
            AuthorisationComponent.LogoutEvent -= OnLogout;
            MainService.OrderArrivedEvent -= OnOrderArrived;
            MainService.BlockingHourEvent -= OnBlockingHour;
            MainService.MenuUpdateEvent -= OnMenuUpdate;
            MainService.OrderUpdateEvent -= OnOrderUpdate;
            base.OnStop();
        }

        protected override void OnResume()
        {
            SetView();
            SetListView();
            base.OnResume();
        }
        
        private void OnLogout()
        {
            StartActivityForResult(typeof(LoginActivity), 0);
        }

        void StartSpinner()
        {
            MenuMessage.Visibility = ViewStates.Gone;
            ActionIndicator.Visibility = ViewStates.Visible;
        }
        void StopSpinner()
        {
            ActionIndicator.Visibility = ViewStates.Gone;
        }
    }
}