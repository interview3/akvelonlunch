using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities.UserActivities;

namespace lunch.Droid.Adapter.UserAdapter
{
    class MenuScreenAdapter : BaseAdapter<DishModel>
    {
        Dictionary<string, List<DishModel>> groupsItems;
        ListView MenuList;
        Activity context;
        public MenuScreenAdapter(Activity context, Dictionary<string, List<DishModel>> groupsItems)
        {
            this.context = context;           
            this.groupsItems = groupsItems;
            this.MenuList = context.FindViewById<ListView>(lunch.Droid.Resource.Id.ListMenuView);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override DishModel this[int position]
        {
            get
            {
                foreach (var group in groupsItems.Values)
                {
                    int size = group.Count + 1;
                    if (position == 0)
                        return null;
                    if (position < size)
                        return group[position - 1];
                    position -= size;
                }
                return null;
            }
        }

        public override int Count
        {
            get
            {
                int count = 0;
                foreach (var group in groupsItems.Values)
                {
                    count += group.Count + 1;
                }
                return count;
            }
        }

        public override int GetItemViewType(int position)
        {
            foreach (var group in groupsItems.Values)
            {
                int size = group.Count + 1;
                if (position == 0)
                    return 0;
                if (position < size)
                    return 1;
                position -= size;
            }
            throw new NullReferenceException();
        }

        protected string GetGroupName(int position)
        {
            foreach (var key in groupsItems.Keys)
            {
                var group = groupsItems[key];
                int size = group.Count + 1;
                if (position < size)
                    return key;
                position -= size;
            }
            throw new NullReferenceException();
        }

        protected virtual void setMenuSectionHeader(View view, int position)
        {
            var MenuSectionHeaderText = view.FindViewById<TextView>(lunch.Droid.Resource.Id.MenuSectionHeaderName);
            MenuSectionHeaderText.Text = GetGroupName(position).ToUpper();
        }
        
        protected virtual void setMenuItem(View view, int position)
        {
            var item = this[position];
            var MenuItemName = view.FindViewById<TextView>(Resource.Id.MenuItemName);
            MenuItemName.Text = item.Name;
            MenuItemName.SetTextColor(ViewPreferences.getTextColor());
            var MenuItemDescription = view.FindViewById<TextView>(Resource.Id.MenuItemDescription);
            MenuItemDescription.Text = item.Description;
            MenuItemDescription.SetTextColor(ViewPreferences.getTextColor());
            var MenuItemCost = view.FindViewById<TextView>(Resource.Id.MenuItemCost);
            MenuItemCost.Text = item.Cost.ToString();
            MenuItemCost.SetTextColor(ViewPreferences.getTextColor());
            var MenuItemAddButton = view.FindViewById<ImageButton>(Resource.Id.MenuItemAddButton);
            EventHandler OnClick = (sender, e) =>
            {
                OrderComponent.ToggleOrderItem(item);
                MenuList.InvalidateViews();
                (context as MenuActivity).toolbarController.UpdateOrderCost();
                (context as MenuActivity).toolbarController.UpdateCountOrderItems();
            };
            MenuItemAddButton.Click += OnClick;
            view.Click += OnClick;
            if (item.IsInCart)
            {
                MenuItemAddButton.SetImageResource(OrderComponent.isArrived || OrderComponent.isMade || SettingsComponent.closed ? Resource.Drawable.cart_plus_dis : Resource.Drawable.cart_plus);
            }
            else
            {
                MenuItemAddButton.SetImageResource(OrderComponent.isArrived || OrderComponent.isMade || SettingsComponent.closed ? Resource.Drawable.cart_empty_dis : Resource.Drawable.cart_empty);
            }
            
            MenuItemAddButton.Enabled = !(OrderComponent.isArrived || OrderComponent.isMade || SettingsComponent.closed);
            view.Enabled = !(OrderComponent.isArrived || OrderComponent.isMade || SettingsComponent.closed);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var type = GetItemViewType(position);
            View view = convertView;
            if (type == 0)
            {
                view = context.LayoutInflater.Inflate(lunch.Droid.Resource.Layout.MenuSectionHeader, null);
                setMenuSectionHeader(view, position);
            }
            else
            {
                view = context.LayoutInflater.Inflate(lunch.Droid.Resource.Layout.MenuItem, null);
                setMenuItem(view, position);
            }
            return view;
        }
    }
}