using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities.UserActivities;

namespace lunch.Droid.UserAdapter
{
    class OrderScreenAdapter : BaseAdapter<OrderItemModel>
    {
        IList<OrderItemModel> items;
        OrderActivity context;
        ListView OrderList;
        public OrderScreenAdapter(OrderActivity context, IList<OrderItemModel> items)
        {
            this.context = context;
            this.items = items;
            this.OrderList = context.FindViewById<ListView>(lunch.Droid.Resource.Id.ListOrderView);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override OrderItemModel this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = context.LayoutInflater.Inflate(Resource.Layout.OrderItem, null);
            var IncrementOrderCount = view.FindViewById<Button>(lunch.Droid.Resource.Id.IncrementOrderCount);
            int orderItemId = item.Id;
   
            IncrementOrderCount.Click += (sender, e) =>
            {
                OrderComponent.IncrementOrderItemCount(orderItemId);
                OrderList.InvalidateViews();
                context.toolbarController.UpdateOrderCost();
                context.toolbarController.UpdateCountOrderItems();
            };
            var DecrementOrderCount = view.FindViewById<Button>(lunch.Droid.Resource.Id.DecrementOrderCount);
            DecrementOrderCount.Click += (sender, e) =>
            {
                OrderComponent.DecrementOrderItemCount(orderItemId);
                OrderList.InvalidateViews();
                context.toolbarController.UpdateOrderCost();
                context.toolbarController.UpdateCountOrderItems();
            };


            IncrementOrderCount.Enabled = !(OrderComponent.isMade || SettingsComponent.closed);
            DecrementOrderCount.Enabled = !(OrderComponent.isMade || SettingsComponent.closed);

            if (OrderComponent.isArrived)
            {
                IncrementOrderCount.Visibility = ViewStates.Invisible;
                DecrementOrderCount.Visibility = ViewStates.Invisible;
            }
            var OrderItemName = view.FindViewById<TextView>(lunch.Droid.Resource.Id.OrderItemName);
            OrderItemName.Text = item.Dish.Name;
            OrderItemName.SetTextColor(ViewPreferences.getTextColor());
            var OrderItemCount = view.FindViewById<TextView>(lunch.Droid.Resource.Id.OrderItemCount);
            OrderItemCount.Text = item.Count.ToString();
           OrderItemCount.SetTextColor(ViewPreferences.getTextColor());
            return view;
        }
    }
}