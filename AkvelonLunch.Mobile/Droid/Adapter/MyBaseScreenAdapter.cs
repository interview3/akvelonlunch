﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace lunch.Droid.Adapter
{
    public class MyBaseScreenAdapter<T> : BaseAdapter<T>
    {
        protected IList<T> items;
        protected Activity context;
        
        public MyBaseScreenAdapter(Activity context, IList<T> items)
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override T this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            return null;
        }
    }
}