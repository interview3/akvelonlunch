using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities;

namespace lunch.Droid.Adapter.AdminAdapter
{
    class AdminDishScreenAdapter: MyBaseScreenAdapter<DishModel>
    {
        public static event Action<DishModel> DishClickEvent;
      

        public AdminDishScreenAdapter(Activity context, IList<DishModel> items) : base(context, items)
        {

        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            var view = context.LayoutInflater.Inflate(lunch.Droid.Resource.Layout.AdminMenuItem, null);
            var MenuItemName = view.FindViewById<TextView>(Resource.Id.AdminMenuItemName);
            MenuItemName.Text = item.Name;
            MenuItemName.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemDescription = view.FindViewById<TextView>(Resource.Id.AdminMenuItemDescription);
            MenuItemDescription.Text = item.Description;
            MenuItemDescription.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemCost = view.FindViewById<TextView>(Resource.Id.AdminMenuItemCost);
            MenuItemCost.Text = item.Cost.ToString();
            MenuItemCost.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemRemove = view.FindViewById<ImageButton>(Resource.Id.AdminMenuItemRemoveButton);
            MenuItemRemove.SetImageResource(item.DishIsInMenu ? Resource.Drawable.dishAdded : Resource.Drawable.addDish);
            EventHandler OnClick = (sender, e) =>
            {
                if (item.DishIsInMenu)
                {
                    MenuItemRemove.SetImageResource(Resource.Drawable.addDish);
                }
                else
                {
                    MenuItemRemove.SetImageResource(Resource.Drawable.dishAdded);
                }
                if (DishClickEvent != null)
                {
                    DishClickEvent(item);
                }
            };

            MenuItemRemove.Click += OnClick;
            view.Click += OnClick;
            return view;
        }
    }
}