using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace lunch.Droid.Adapter.AdminAdapter
{
    public class AdminUserScreenAdapter : MyBaseScreenAdapter<UserModel>
    {
        public static event Action SelectUser;

        private string UserBalanceText;

       
        public AdminUserScreenAdapter(Activity context, IList<UserModel> items) : base(context, items)
        {

        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = context.LayoutInflater.Inflate(Resource.Layout.UserItem, null);
            SetUserName(view, item);
            SetUserBalance(view, item);
            view.Click += (sender, args) =>
            {
                if (SelectUser != null)
                {
                    AdminUserComponent.EditingUserId = item.Id;
                    SelectUser();
                }
            };
            return view;
        }

        private void SetUserName(View view, UserModel item)
        {
            var UserItemName = view.FindViewById<TextView>(Resource.Id.UserItemName);
            UserItemName.Text = item.FirstName + " " + item.LastName;
        }

        private void SetUserBalance(View view, UserModel item)
        {
            var UserItemBalace = view.FindViewById<TextView>(Resource.Id.UserItemBalance);
            UserItemBalace.Text = UserBalanceText + " " + item.Balance.ToString();
        }
    }
}