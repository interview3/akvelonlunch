﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities.AdminActivities;

namespace lunch.Droid.AdminAdapter
{
    class AdminOrdersScreenAdapter : BaseAdapter<string>
    {
        Dictionary<string, int> items;
        AdminOrdersActivity context;
        ListView AdminOrdersList;
        public AdminOrdersScreenAdapter(AdminOrdersActivity context, Dictionary<string, int> items)
        {
            this.context = context;
            this.items = items;
            this.AdminOrdersList = context.FindViewById<ListView>(lunch.Droid.Resource.Id.AdminOrdersView);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override string this[int position]
        {
            get { return items.Keys.ElementAt(position); }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items.Keys.ElementAt(position);
            var itemCount = items.Values.ElementAt(position);
            View view = convertView;
            view = context.LayoutInflater.Inflate(Resource.Layout.AdminOrdersItem, null);            

            var AdminDishItemName = view.FindViewById<TextView>(lunch.Droid.Resource.Id.AdminDishItemName);
            AdminDishItemName.Text = item;
            var AdminDishItemCount = view.FindViewById<TextView>(lunch.Droid.Resource.Id.AdminDishItemCount);
            AdminDishItemCount.Text = itemCount.ToString();
            return view;
        }
    }
}