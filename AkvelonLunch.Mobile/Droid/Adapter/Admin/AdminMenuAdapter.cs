using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities.AdminActivities;
using lunch.Droid.Adapter.UserAdapter;

namespace lunch.Droid.Adapter.AdminAdapter
{
    class AdminMenuAdapter: MenuScreenAdapter
    {
        Dictionary<string, List<DishModel>> groupsItems;
        ListView MenuList;
        AdminMenuActivity context;

        public AdminMenuAdapter(Activity context, Dictionary<string, List<DishModel>> groupsItems)
            : base(context, groupsItems)
        {
            this.context = context as AdminMenuActivity;
            this.groupsItems = groupsItems;
            this.MenuList = context.FindViewById<ListView>(lunch.Droid.Resource.Id.AdminMenuListView);
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var type = GetItemViewType(position);
            View view = convertView;
            if (type == 0)
            {
                view = context.LayoutInflater.Inflate(lunch.Droid.Resource.Layout.MenuSectionHeader, null);
                setMenuSectionHeader(view, position);
            }
            else
            {
                view = context.LayoutInflater.Inflate(lunch.Droid.Resource.Layout.AdminMenuItem, null);
                setMenuItem(view, position);
            }
            return view;
        }

        protected override void setMenuSectionHeader(View view, int position)
        {
            var MenuSectionHeaderText = view.FindViewById<TextView>(lunch.Droid.Resource.Id.MenuSectionHeaderName);
            MenuSectionHeaderText.Text = GetGroupName(position).ToUpper();
        }

        protected override void setMenuItem(View view, int position)
        {
            var item = this[position];

            var MenuItemName = view.FindViewById<TextView>(Resource.Id.AdminMenuItemName);
            MenuItemName.Text = item.Name;
            MenuItemName.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemDescription = view.FindViewById<TextView>(Resource.Id.AdminMenuItemDescription);
            MenuItemDescription.Text = item.Description;
            MenuItemDescription.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemCost = view.FindViewById<TextView>(Resource.Id.AdminMenuItemCost);
            MenuItemCost.Text = item.Cost.ToString();
            MenuItemCost.SetTextColor(ViewPreferences.getTextColor());

            var MenuItemRemove = view.FindViewById<ImageButton>(Resource.Id.AdminMenuItemRemoveButton);
            EventHandler OnClick = async (sender, e) =>
            {
                context.RemoveMenuItem(item);
                MenuList.InvalidateViews();
            };
            MenuItemRemove.Click += OnClick;
        }
    }
}