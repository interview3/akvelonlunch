using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities;
using Android.Support.V4.App;
using lunch.Droid.Activities.UserActivities;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
namespace lunch.Droid.Controller
{
    public class ToolbarController
    {
        private ActionBarActivity context;
        private ImageButton ToolbarCart;
        private ImageButton ToolbarAccount;
        private TextView ToolbarOrderCost;
        private TextView ToolbarBalance;
        private Toolbar MenuToolbar;
        private Button ToolbarBack;
        private LinearLayout ToolbarCartItemLinearLayout;
        private LinearLayout ToolbarAccountLinearLayout;
        private TextView ToolbarNameView;
        private TextView ToolbarCountOrderItems;
        private string NameView;
        private decimal DefaultBalance;
        private decimal DefaulOrserCost;
        public ToolbarController(ActionBarActivity context, string NameView)
        {
            this.context = context;
            this.NameView = NameView;
            DefaultBalance = 0;
            DefaulOrserCost = 0;
            GetViewItems();
            SetView();
            SubscriveViews();
            OrderComponent.UpdateOrderEvent += () => { context.RunOnUiThread(UpdateToolbar); };
        }

        private void GetViewItems()
        {
            ToolbarCart = context.FindViewById<ImageButton>(Resource.Id.ToolbarCartImageButton);
            ToolbarCartItemLinearLayout = context.FindViewById<LinearLayout>(Resource.Id.ToolbarCartItemLinearLayout);
            ToolbarOrderCost = context.FindViewById<TextView>(Resource.Id.ToolbarOrderCostText);
            ToolbarBalance = context.FindViewById<TextView>(Resource.Id.ToolbarBalanceText);
            MenuToolbar = context.FindViewById<Toolbar>(Resource.Id.Toolbar);
            ToolbarAccount = context.FindViewById<ImageButton>(Resource.Id.ToolbarAccountImageButton);
            ToolbarBack = context.FindViewById<Button>(Resource.Id.ToolbarBackButton);
            ToolbarNameView = context.FindViewById<TextView>(Resource.Id.ToolbarNameViewText);
            ToolbarCountOrderItems = context.FindViewById<TextView>(Resource.Id.ToolbarCountOrderItemsViewText);
            ToolbarAccountLinearLayout = context.FindViewById<LinearLayout>(Resource.Id.ToolbarAccountLinearLayout);
        }

        private void SetView()
        {
            context.SetSupportActionBar(MenuToolbar);
            MenuToolbar.SetContentInsetsAbsolute(0, 0);            
            context.SupportActionBar.SetDisplayShowTitleEnabled(false);
            context.SupportActionBar.SetDisplayShowHomeEnabled(false);
            context.SupportActionBar.SetDisplayShowCustomEnabled(false);
            context.SupportActionBar.SetDisplayUseLogoEnabled(false);
            ToolbarNameView.Text = NameView;
        }

        private void SubscriveViews()
        {
            ToolbarAccount.Click += OnToolbarAccountClick;
            ToolbarAccountLinearLayout.Click += OnToolbarAccountClick;
            ToolbarBack.Click += OnToolbarBackClick;
            if (context.GetType() != typeof(OrderActivity))
            {
                ToolbarCart.Click += OnToolbarCartClick;
                ToolbarCartItemLinearLayout.Click += OnToolbarCartClick;
            }
        }

        private async void OnToolbarCartClick(object sender, EventArgs e)
        {
            context.StartActivity(typeof(OrderActivity));
        }

        private void OnToolbarBackClick(object sender, EventArgs e)
        {
            context.Finish();
        }

        private void OnToolbarAccountClick(object sender, EventArgs e)
        {
            FragmentTransaction ft = context.SupportFragmentManager.BeginTransaction();
            AccountDialogFragment AccountDialog;
            var prev = context.SupportFragmentManager.FindFragmentByTag("AccountDialog");
            if (prev != null)
            {
                ft.Remove(prev);
            }
            ft.AddToBackStack(null);
            AccountDialog = new AccountDialogFragment();
            AccountDialog.Show(ft, "AccountDialog");
        }

        public void UpdateCartImage()
        {
            if (OrderComponent.isArrived)
            {
                ToolbarCart.SetImageResource(Resource.Drawable.delivered_cart);
            }
            else
            {
                if (OrderComponent.isMade)
                {
                    ToolbarCart.SetImageResource(Resource.Drawable.cart);
                }
                else
                {
                    ToolbarCart.SetImageResource(Resource.Drawable.disabled_cart);
                }
            }
        }

        public async void UpdateBalance()
        {
            CustomWebResponse<decimal> BalanceRespose = null;
            ToolbarBalance.Text =
                Convert.ToInt32(DefaultBalance).ToString() + "P";
            await Task.Factory.StartNew(() =>
            {
                BalanceRespose = AuthorisationComponent.GetUserBalance();
            });
            if (BalanceRespose != null && BalanceRespose.ok)
            {
                DefaultBalance = BalanceRespose.responseObject;
                ToolbarBalance.Text =
                    Convert.ToInt32(DefaultBalance).ToString() + "P";
            }
        }

        public void UpdateCountOrderItems()
        {
            int count = OrderComponent.NumberOfItems;
            ToolbarCountOrderItems.Text = count.ToString();
            ToolbarCountOrderItems.Visibility = count == 0 ? ViewStates.Invisible : ViewStates.Visible;
        }

        public async void UpdateOrderCost()
        {
            ToolbarOrderCost.Text = Convert.ToInt32(DefaulOrserCost).ToString() + "P";
            await Task.Factory.StartNew(() =>
            {
                DefaulOrserCost = OrderComponent.TotalSumm;
            });
            ToolbarOrderCost.Text = Convert.ToInt32(DefaulOrserCost).ToString() + "P";
        }


        public void UpdateToolbar()
        {
            UpdateCartImage();
            UpdateBalance();
            UpdateCountOrderItems();
            UpdateOrderCost();
        }
    }
}