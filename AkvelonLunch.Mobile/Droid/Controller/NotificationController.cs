﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Java.Lang;
using Java.Util;
using lunch.Droid.Activities;
using lunch.Droid.Activities.UserActivities;
using Builder = Android.Support.V4.App.NotificationCompat.Builder;
namespace lunch.Droid.Controller
{
    static class NotificationController
    {
        public enum TypeOfAlarm
        {
            OrderMake = 1,
            Balance
        }

        static public string NameOfExtraField = "TypeOfAlarm";

        static private int HourMiliseconds = 3600000;
        static private int MinuteMiliseconds = 60000;

        static private Activity context;
        static private Builder builder;
        static private NotificationManager notificationManager;

        static private int balanceAlarmHour = 9;
        static private int balanceAlarmMinute = 0;

        private static int balanceyNotificationId = 0;
        static int balancePendingIntentId = 1;

        private static int orderArriveyNotificationId = 2;
        static int orderArrivePendingIntentId = 3;

    
        private static int orderMakeNotificationId = 4;
        static int orderMakePendingIntentId = 5;

        private static int menuUpdateNotificationId = 6;
        static int menuUpdatePendingIntentId = 7;

        static PendingIntent orderMakeAlarmPendingIntent = null;
        static PendingIntent balanceAlarmPendingIntent = null;
        
        static string AlarmServiceName;

        public static void SetNotificationController(Activity _context, string AlarmServiceName)
        {
            context = _context;
            builder = new Builder(context);
            notificationManager =
               context.GetSystemService(Context.NotificationService) as NotificationManager;
            NotificationController.AlarmServiceName = AlarmServiceName;
            var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
        }

        public static void ClearAlarm()
        {
            var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
            if (orderMakeAlarmPendingIntent != null)
            {
                alarmManager.Cancel(orderMakeAlarmPendingIntent);
                orderMakeAlarmPendingIntent = null;
            }
            if (balanceAlarmPendingIntent != null)
            {
                alarmManager.Cancel(balanceAlarmPendingIntent);
                balanceAlarmPendingIntent = null;
            }
        }

        public static void ClearOrderMakeAlarm()
        {
            var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
            if (orderMakeAlarmPendingIntent != null)
            {
                alarmManager.Cancel(orderMakeAlarmPendingIntent);
                orderMakeAlarmPendingIntent = null;
            }
        }

        public static void ClearBalanceAlarm()
        {
            var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
            if (balanceAlarmPendingIntent != null)
            {
                alarmManager.Cancel(balanceAlarmPendingIntent);
                balanceAlarmPendingIntent = null;
            }
        }


        public static void SetAlarms()
        {
            SetOrderMakeAlarm();
            SetBalanceAlarm();
        }

        public static void SetOrderMakeAlarm()
        {
            if (OrderComponent.isMade)
                return;
            ClearOrderMakeAlarm();
            if (Settings.Switcher)
            {
                var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
                Intent intent = new Intent(context, typeof(NotificationReceiver));
                intent.PutExtra(NameOfExtraField, (int)TypeOfAlarm.OrderMake);
                orderMakeAlarmPendingIntent = PendingIntent.GetBroadcast(context, 1, intent,
                    PendingIntentFlags.UpdateCurrent);
                
                var currentDate = DateTime.Now;
                long millisecondsToAlarm = (Settings.RemHour - currentDate.Hour) * HourMiliseconds + (Settings.RemMinute - currentDate.Minute) * MinuteMiliseconds;
                if (millisecondsToAlarm < 0)
                    millisecondsToAlarm += AlarmManager.IntervalDay;
                alarmManager.SetRepeating(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + millisecondsToAlarm, AlarmManager.IntervalDay, orderMakeAlarmPendingIntent);
            }
        }

        public static void SetBalanceAlarm()
        {
            ClearBalanceAlarm();
            UserModel user = AuthorisationComponent.GetUser().responseObject;
            if (user == null)
                return;
            if (Settings.remBalance && user.Balance <= Settings.remBalanceVal)
            {
                var alarmManager = (AlarmManager)context.GetSystemService(AlarmServiceName);
                Intent intent = new Intent(context, typeof(NotificationReceiver));
                intent.PutExtra(NameOfExtraField, (int)TypeOfAlarm.Balance);
                balanceAlarmPendingIntent = PendingIntent.GetBroadcast(context, 2, intent,
                    PendingIntentFlags.OneShot);
                var currentDate = DateTime.Now;
                long millisecondsToAlarm = (balanceAlarmHour - currentDate.Hour) * HourMiliseconds + (balanceAlarmMinute - currentDate.Minute) * MinuteMiliseconds;
                if (millisecondsToAlarm < 0)
                    millisecondsToAlarm += AlarmManager.IntervalDay;
                alarmManager.SetRepeating(AlarmType.ElapsedRealtimeWakeup, SystemClock.ElapsedRealtime() + millisecondsToAlarm, AlarmManager.IntervalDay, balanceAlarmPendingIntent);
            }
        }

        public static void CreateBalanceNotification()
        {
            var balanceIntent = new Intent(context, typeof(MenuActivity));
            var balancePendingIntent =
                PendingIntent.GetActivity(context, balancePendingIntentId, balanceIntent, PendingIntentFlags.CancelCurrent);
            builder.SetSmallIcon(Resource.Drawable.mainScreen)
                   .SetContentIntent(balancePendingIntent)
                   .SetContentTitle("Проблемы с балансом.")
                   .SetContentText("Не забудьте положить денег на счет, чтобы заказать обед.")
                   .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
                   .SetAutoCancel(true)
                   .SetPriority((int)NotificationPriority.Max);
            var balanceNotification = builder.Build();
            notificationManager.Notify(balanceyNotificationId, balanceNotification);
        }

        public static void CreateOrderArriveNotification()
        {
            var orderArriveIntent = new Intent(context, typeof(MenuActivity));
            var orderArrivePendingIntent =
                PendingIntent.GetActivity(context, orderArrivePendingIntentId, orderArriveIntent, PendingIntentFlags.CancelCurrent);
            builder.SetSmallIcon(Resource.Drawable.mainScreen)
               .SetContentIntent(orderArrivePendingIntent)
               .SetContentTitle("Заказы доставлены.")
               .SetContentText("Пора обедать.")
               .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
               .SetAutoCancel(true)
               .SetPriority((int)NotificationPriority.Max); ;
            var orderArriveNotification = builder.Build();
            notificationManager.Notify(orderArriveyNotificationId, orderArriveNotification);
        }

        public static void CreateOrderMakeNotification()
        {
            var orderMakeIntent = new Intent(context, typeof(MenuActivity));
            var orderMakePendingIntent =
                PendingIntent.GetActivity(context, orderMakePendingIntentId, orderMakeIntent, PendingIntentFlags.CancelCurrent);
            builder.SetSmallIcon(Resource.Drawable.mainScreen)
               .SetContentIntent(orderMakePendingIntent)
               .SetContentTitle("Скоро обед.")
               .SetContentText("Не забудьте сделать заказ!")
               .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
               .SetAutoCancel(true)
               .SetPriority((int)NotificationPriority.Max); ;
            var orderMakeNotification = builder.Build();
            notificationManager.Notify(orderMakeNotificationId, orderMakeNotification);
        }

        public static void CreateMenuUpdateNotification()
        {
            var menuUpdateIntent = new Intent(context, typeof(MenuActivity));
            var menuUpdatePendingIntent =
                PendingIntent.GetActivity(context, menuUpdatePendingIntentId, menuUpdateIntent, PendingIntentFlags.CancelCurrent);
            builder.SetSmallIcon(Resource.Drawable.mainScreen)
               .SetContentIntent(menuUpdatePendingIntent)
               .SetContentTitle("Доступно новое меню.")
               .SetContentText("Обновление списка блюд в меню.")
               .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
               .SetAutoCancel(true)
               .SetPriority((int)NotificationPriority.Max); ;
            var orderMakeNotification = builder.Build();
            notificationManager.Notify(menuUpdateNotificationId, orderMakeNotification);
        }
    }
}