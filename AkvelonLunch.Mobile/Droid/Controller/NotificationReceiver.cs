using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using lunch.Droid.Services;

namespace lunch.Droid.Controller
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "lunch.BroadcastReceiver" })]
    class NotificationReceiver : BroadcastReceiver
    {
        private int count;
        
        public override void OnReceive(Context context, Intent intent)
        {
            var type = intent.GetIntExtra(NotificationController.NameOfExtraField, 0);
            Intent service = null;
            switch ((NotificationController.TypeOfAlarm)type)
            {
                case NotificationController.TypeOfAlarm.OrderMake:
                    service = new Intent(context, typeof(OrderMakeService));
                    break;
                case NotificationController.TypeOfAlarm.Balance:
                    service = new Intent(context, typeof(BalanceService));
                    break;

            }
            if (service != null)
            {
                context.StartService(service);
                
            }
        }
    }
}