﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using lunch.Droid.Activities;
using Android.Support.V4.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
using Android.Support.V4.Widget;
using lunch.Droid.Activities.AdminActivities;
using lunch.Droid.Activities.UserActivities;


namespace lunch.Droid.Controller
{
    public class LeftMenuController
    {
        private ActionBarActivity context;
        private string NameView;
        private Toolbar mToolbar;
        private MyActionBarDrawerToggle mDrawerToggle;
        private Android.Support.V4.Widget.DrawerLayout mDrawerLayout;
        private ListView mLeftDrawer;
        private TextView mTitle;
        private Button menuBtn;
        private ArrayAdapter mLeftAdapter;
        private ImageButton AdminToolbarHomeButton;

        private List<string> mLeftDataSet;

        public LeftMenuController(ActionBarActivity context, string NameView, List<string> items)
        {
            this.context = context;
            this.NameView = NameView;
            mLeftDataSet = items;
            GetViewItems();
            SetView();
            SubscribeView();
        }

        private void GetViewItems()
        {
            mToolbar = context.FindViewById<Toolbar>(Resource.Id.toolbar);
            menuBtn = mToolbar.FindViewById<Button>(Resource.Id.toolbarMenuButton);
            mTitle = mToolbar.FindViewById<TextView>(Resource.Id.adminToolbarNameViewText);
            mDrawerLayout = context.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            mLeftDrawer = context.FindViewById<ListView>(Resource.Id.left_drawer);
            AdminToolbarHomeButton = context.FindViewById<ImageButton>(Resource.Id.AdminToolbarHomeButton);
        }

        private void SubscribeView()
        {
            mLeftDrawer.ItemClick += MenuListView_ItemClick;
            menuBtn.Click += MenuBtn_Click;
            AdminToolbarHomeButton.Click += OnAdminToolbarHomeButtonClick;
        }

        private void MenuBtn_Click(object sender, EventArgs e)
        {
            if (mDrawerLayout.IsDrawerOpen(mLeftDrawer))
            {
                mDrawerLayout.CloseDrawer(mLeftDrawer);
            }
            else
            {
                mDrawerLayout.OpenDrawer(mLeftDrawer);
            }
        }

        private void SetView()
        {
            context.SetSupportActionBar(mToolbar);
            mDrawerLayout.SetDrawerListener(mDrawerToggle);
            context.SupportActionBar.SetHomeButtonEnabled(true);
            context.SupportActionBar.SetDisplayShowTitleEnabled(true);
            context.SupportActionBar.SetDisplayShowCustomEnabled(true);
            mDrawerToggle = new MyActionBarDrawerToggle(
                context, //Host Activity
                mDrawerLayout, //DrawerLayout
                Resource.String.openDrawer, //Opened Message
                Resource.String.AdmOrdersName //Closed Message
                );
            mDrawerToggle.SyncState();
            context.SupportActionBar.SetTitle(Resource.String.AdmOrdersName);
            mTitle.Text = NameView;
            mLeftDrawer.Tag = 0;
            ArrayAdapter<String> ListAdapter = new ArrayAdapter<String>(context, Android.Resource.Layout.SimpleListItem1,
                mLeftDataSet);
            mLeftDrawer.Adapter = ListAdapter;
        }

        private void OnAdminToolbarHomeButtonClick(object sender, EventArgs e)
        {
            Intent inetnt = new Intent(context, typeof (MenuActivity));
            inetnt.SetFlags(ActivityFlags.ClearTop);
            context.StartActivity(inetnt);
        }

        void MenuListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            switch (e.Id)
            {
                case 0:
                    OnMenuEditClick();
                    break;
                case 1:
                    OnOrdersClick();
                    break;
                case 2:
                    OnUsersClick();
                    break;
                case 3:
                    OnSettingsClick();
                    break;
                case 4:
                    OnBackClick();
                    break;
            }

            //SupportFragmentManager.BeginTransaction().Replace(Resource.Id.main, fragment).Commit();
            mDrawerLayout.CloseDrawers();
            mDrawerToggle.SyncState();
        }

        private void OnMenuEditClick()
        {
            context.StartActivity(typeof(AdminMenuActivity));
        }

        private void OnOrdersClick()
        {
            context.StartActivity(typeof(AdminOrdersActivity));
        }

        private void OnUsersClick()
        {
            context.StartActivity(typeof(AdminUserActivity));
        }
        private void OnSettingsClick()
        {
            context.StartActivity(typeof(AdminSettingsActivity));
        }
        private void OnBackClick()
        {
            Intent inetnt = new Intent(context, typeof(MenuActivity));
            inetnt.SetFlags(ActivityFlags.ClearTop);
            context.StartActivity(inetnt);
        }
    }
}