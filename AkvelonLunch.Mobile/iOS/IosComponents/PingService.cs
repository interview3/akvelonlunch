﻿using System;
using lunch.iOS.Controllers;
using Xamarin.Forms;

namespace lunch.iOS.IosComponents
{
    public static class PingService
    {
        private static bool _stopped = true;
        private static bool _updStopped = true;
        private static bool _clCheckStopped = true;
        private static bool _orderCheckStopped = true;
        private static bool _ordersAdmStopped = true;
        private static bool _moneyCheckStopped = true;

        public static void StartCheckingMenu()
        {
            if (!_stopped) return;
            _stopped = false;
            Forms.Init();
            var seconds = TimeSpan.FromSeconds(ViewPreferences.SmallUpdatingPeriod);
            Device.StartTimer(seconds, CheckMenu);
        }

        private static bool CheckMenu()
        {
            if (_stopped)
                return false;
            var response = MenuComponent.GetMenu();
            if (!response.ok) return true;
            if (response.responseObject == null) return true;
            MenuController.ReloadMenu();
            AdminEditMenuController.ReloadMenuData();
            NotificationService.NotifyAboutMenuUpdate();
            return false;
        }

        public static void StartCheckingOrder()
        {
            if (!_orderCheckStopped) return;
            _orderCheckStopped = false;
            Forms.Init();
            var minutes = TimeSpan.FromMinutes(ViewPreferences.SmallUpdatingPeriod);
            Device.StartTimer(minutes, CheckOrder);
        }

        private static bool CheckOrder()
        {
            if (_orderCheckStopped)
                return false;
            if (!OrderComponent.UpdateOrder()) return true;
            MenuController.ReloadCart();
            AdminEditMenuController.ReloadMenuData();
            return false;
        }

        public static void StartCheckingClosingHour()
        {
            if (!_clCheckStopped) return;
            _clCheckStopped = false;
            Forms.Init();
            var minutes = TimeSpan.FromMinutes(ViewPreferences.LargeUpdatingPeriod);
            Device.StartTimer(minutes, CheckClosingHour);
        }

        private static bool CheckClosingHour()
        {
            if (_clCheckStopped)
                return false;
            var response = SettingsComponent.IsServiceClosed();
            if (!response.ok) return true;
            if (!response.responseObject) return true;
            MenuController.CloseService();
            return false;
        }

        public static void StartCheckingMenuUpdate()
        {
            if (!_updStopped) return;
            _updStopped = false;
            Forms.Init();
            var minutes = TimeSpan.FromMinutes(ViewPreferences.LargeUpdatingPeriod);
            Device.StartTimer(minutes, CheckMenuUpdate);
        }

        private static bool CheckMenuUpdate()
        {
            if (_updStopped)
                return false;
            if (!MenuComponent.UpdateMenu()) return true;
            NotificationService.NotifyAboutMenuUpdate();
            MenuController.ReloadMenu();
            AdminEditMenuController.ReloadMenuData();
            MenuController.ReloadCart();
            return true;
        }

        public static void StartCheckingOrdersAdmin()
        {
            if (!_ordersAdmStopped) return;
            _ordersAdmStopped = false;
            Forms.Init();
            var minutes = TimeSpan.FromMinutes(ViewPreferences.LargeUpdatingPeriod);
            Device.StartTimer(minutes, CheckOrders);
        }

        private static bool CheckOrders()
        {
            if (_ordersAdmStopped)
                return false;
            AdminOrdersController.Refresh();
            return true;
        }

        public static void StartCheckingMoney()
        {
            if (!_moneyCheckStopped) return;
            _moneyCheckStopped = false;
            Forms.Init();
            var minutes = TimeSpan.FromMinutes(ViewPreferences.LargeUpdatingPeriod);
            Device.StartTimer(minutes, CheckMoney);
        }

        private static bool CheckMoney()
        {
            if (_moneyCheckStopped)
                return false;
            AuthorisationComponent.GetUserBalance();
            MenuController.ReloadCart();
            return true;
        }

        public static void StopChecking()
        {
            _stopped = true;
            _updStopped = true;
            _clCheckStopped = true;
            _orderCheckStopped = true;
            _ordersAdmStopped = true;
            _moneyCheckStopped = true;
            CheckOrders();
        }
    }
}


