﻿using UIKit;

namespace lunch.iOS.IosComponents
{
	public static class AlertService
	{
		public delegate void Del();
		public static UIAlertController GetAlert(string message)
		{
			var alert = UIAlertController.Create(ResourcesComponent.GetStringFor("attentionLabel"), message, UIAlertControllerStyle.Alert);
			alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Cancel, null));
			return alert;
		}

		public static UIAlertController GetConfirmingAlert(Del confirm)
		{
			var alert = UIAlertController.Create(ResourcesComponent.GetStringFor("attentionLabel"), ResourcesComponent.GetStringFor("areYouSureName"), UIAlertControllerStyle.Alert);
			alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("yes"), UIAlertActionStyle.Default, action => confirm()));
			alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("no"), UIAlertActionStyle.Cancel, null));
			return alert;
		}
	}
}

