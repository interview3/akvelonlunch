﻿using System;
using Foundation;
using lunch.iOS.Controllers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace lunch.iOS.IosComponents
{
    public static class NotificationService
    {
        static bool _chOrderStopped = true;

        public static void SetNotifications(bool unset = true)
        {
            if (unset)
            {
                RemoveNotificantions();
            }

            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            var components = gregCalendar.Components(NSCalendarUnit.Day | NSCalendarUnit.Year | NSCalendarUnit.Month,
                DateTime.Now.ToNSDate());
            components.Day = DateTime.Now.Day;
            components.Month = DateTime.Now.Month;
            components.Year = DateTime.Now.Year;
            var referenceDate = gregCalendar.DateFromComponents(components);
            var componentsForFireDate =
                gregCalendar.Components(NSCalendarUnit.Year | NSCalendarUnit.Hour | NSCalendarUnit.Minute, referenceDate);
            componentsForFireDate.Year = components.Year;
            componentsForFireDate.Month = components.Month;
            componentsForFireDate.Day = components.Day;
            componentsForFireDate.Hour = Settings.RemHour;
            componentsForFireDate.Minute = Settings.RemMinute;
            var fireDateOfNotification = gregCalendar.DateFromComponents(componentsForFireDate);

            var localNotification = new UILocalNotification
            {
                FireDate = fireDateOfNotification,
                TimeZone = NSTimeZone.LocalTimeZone,
                AlertBody = ResourcesComponent.GetStringFor("dontForget"),
                AlertAction = "View",
                RepeatCalendar = NSCalendar.CurrentCalendar,
                RepeatInterval = NSCalendarUnit.Weekday,
                ApplicationIconBadgeNumber = 1
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(localNotification);
        }

        public static void RemoveNotificantions()
        {
            UIApplication.SharedApplication.CancelAllLocalNotifications();
            SetBalanceNotifications(false);
        }

        public static void RemoveBalanceNotificantions(bool unset = true)
        {
            UIApplication.SharedApplication.CancelAllLocalNotifications();
            SetNotifications(false);
        }

        public static void SetBalanceNotifications(bool unset = true)
        {
            if (unset)
            {
                RemoveBalanceNotificantions(false);
            }

            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            var components = gregCalendar.Components(NSCalendarUnit.Day | NSCalendarUnit.Year | NSCalendarUnit.Month,
                DateTime.Now.ToNSDate());
            components.Day = DateTime.Now.Day;
            components.Month = DateTime.Now.Month;
            components.Year = DateTime.Now.Year;
            var referenceDate = gregCalendar.DateFromComponents(components);
            var componentsForFireDate =
                gregCalendar.Components(NSCalendarUnit.Year | NSCalendarUnit.Hour | NSCalendarUnit.Minute, referenceDate);
            componentsForFireDate.Year = components.Year;
            componentsForFireDate.Month = components.Month;
            componentsForFireDate.Day = components.Day;
            componentsForFireDate.Hour = 9;
            componentsForFireDate.Minute = 00;
            var fireDateOfNotification = gregCalendar.DateFromComponents(componentsForFireDate);

            var localNotification = new UILocalNotification
            {
                FireDate = fireDateOfNotification,
                TimeZone = NSTimeZone.LocalTimeZone,
                AlertBody = ResourcesComponent.GetStringFor("dontForgetBalance"),
                AlertAction = "View",
                RepeatCalendar = NSCalendar.CurrentCalendar,
                RepeatInterval = NSCalendarUnit.Weekday,
                ApplicationIconBadgeNumber = 1
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(localNotification);
        }

        public static bool NotifCanceller()
        {
            CheckNotifications();
            Forms.Init();
            var hours = TimeSpan.FromHours(24);
            Device.StartTimer(hours, CheckNotifications);
            return false;
        }
        
        public static bool CheckNotifications()
        {
            MenuComponent.UpdateMenu();
            if (MenuComponent.GetMenu().responseObject == null)
            {
                RemoveNotificantions();
            }
            else SetNotifications();
            return true;
        }

        public static bool BalanceNotifCanceller()
        {
            CheckBalanceNotifications();
            Forms.Init();
            var hours = TimeSpan.FromHours(24);
            Device.StartTimer(hours, CheckBalanceNotifications);
            return false;
        }

        public static bool CheckBalanceNotifications()
        {
            var response = AuthorisationComponent.GetUserBalance();
            if (!response.ok) return true;
            if (response.responseObject > Settings.remBalanceVal)
                RemoveBalanceNotificantions();
            else SetBalanceNotifications();
            return true;
        }


        public static bool CheckIfOrderArrived()
        {
            if (_chOrderStopped)
                return false;
            if (!OrderComponent.isMade) return false;
            if (!OrderComponent.OrderArrived().responseObject) return true;
			MenuController.ReloadCart();
			MenuController.ReloadMenu();
			MenuController.CartButton.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("delCartImage")),
                UIControlState.Normal);
            MenuController.CountItemsBtn.BackgroundColor = ViewPreferences.GreenColor;
            MenuController.OrderStatus.Text = ResourcesComponent.GetStringFor("orderLabel") + " " +
                                              OrderComponent.OrderId + " " +
                                              ResourcesComponent.GetStringFor("orderArrived");
            MenuController.OrderStatus.Font = ViewPreferences.StatusBoldFont;
            MenuController.ClearButton.Hidden = true;
            var notification = new UILocalNotification
            {
                FireDate = NSDate.Now,
                AlertAction = "View Alert",
                AlertBody = ResourcesComponent.GetStringFor("lunchArrived"),
                ApplicationIconBadgeNumber = 1
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            SetNotifications(false);
            return false;
        }

        public static bool StartCheckingIfOrderArrived()
        {
            _chOrderStopped = false;
            Forms.Init();
            var seconds = TimeSpan.FromSeconds(ViewPreferences.SmallUpdatingPeriod);
            Device.StartTimer(seconds, CheckIfOrderArrived);
            return false;
        }

        public static void StopServices()
        {
            RemoveNotificantions();
            RemoveBalanceNotificantions();
            _chOrderStopped = true;
        }

        public static void NotifyThatOrderArrived()
        {
            var notification = new UILocalNotification
            {
                FireDate = NSDate.Now,
                AlertAction = "View Alert",
                AlertBody = ResourcesComponent.GetStringFor("lunchArrived"),
                ApplicationIconBadgeNumber = 1
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }

        public static void NotifyAboutMenuUpdate()
        {
            var notification = new UILocalNotification
            {
                FireDate = NSDate.Now,
                AlertAction = "View Alert",
                AlertBody = ResourcesComponent.GetStringFor("menuUdpated"),
                ApplicationIconBadgeNumber = 1
            };
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }
    }
}

