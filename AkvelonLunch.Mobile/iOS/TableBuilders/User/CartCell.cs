﻿using System;
using System.Globalization;
using CoreGraphics;
using lunch.iOS.Controllers;
using UIKit;

namespace lunch.iOS.TableBuilders.User
{
	public sealed class CartCell : UITableViewCell
	{
	    public UITableViewCellStyle Style { get; }
	    private readonly UILabel _headingLabel;
	    private readonly UIStepper _stepper;
	    private readonly UITextField _numText;
	    private OrderItemModel _orderItem;

		public CartCell(UITableViewCellStyle style, string cellId) : base(UITableViewCellStyle.Default, cellId)
		{
		    Style = style;
		    _headingLabel = new UILabel
			{
				Font = ViewPreferences.BigFont,
				BackgroundColor = UIColor.Clear
			};

		    _stepper = new UIStepper
            {
                Value = 1,
                TintColor = ViewPreferences.OrangeColor
            };
		    _stepper.TouchUpInside += ChangeAmount;
		    _numText = new UITextField
		    {
		        Font = ViewPreferences.BigFont,
		        TextAlignment = UITextAlignment.Center,
		        KeyboardType = UIKeyboardType.NumberPad,
		        UserInteractionEnabled = false
		    };
		    ContentView.AddSubviews(_headingLabel, _stepper, _numText);
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();
			_stepper.Frame = new CGRect(ContentView.Frame.Width - 100, 23, 70, 35);
			_numText.Frame = new CGRect(ContentView.Frame.Width - 135, 20, 35, 35);
			_headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 150, 35);
		}

		private void ChangeAmount(object sender, EventArgs e)
		{
			if (Convert.ToInt32(_numText.Text) - _stepper.Value > 0)
				RemoveFromCart();
			else AddToCart();
			_numText.Text = _stepper.Value.ToString(CultureInfo.InvariantCulture);
		}

		private void AddToCart()
		{
			OrderComponent.IncrementOrderItemCount(_orderItem.Id);
			var newAmount = _orderItem.Count;
			_numText.Text = newAmount.ToString();
			UpdateItems();
		}

		private void RemoveFromCart()
		{
			var newAmount = _orderItem.Count;
			OrderComponent.DecrementOrderItemCount(_orderItem.Id);
			MenuController.ReloadMenu();
			UpdateItems();
			_numText.Text = newAmount.ToString();
		}

		public void UpdateCell(OrderItemModel oi)
		{
			_orderItem = oi;
			_stepper.UserInteractionEnabled = !(OrderComponent.isMade || SettingsComponent.closed);
			_numText.Text = _orderItem.Count.ToString();
			_stepper.Value = _orderItem.Count;
			_headingLabel.Text = _orderItem.Dish.Name;
		}
		 
		private void UpdateItems()
		{
			MenuController.ReloadCart();
		}
	}
}



