﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using lunch.iOS.Controllers;
using UIKit;

namespace lunch.iOS.TableBuilders.User
{
    public class MenuTableSource : UITableViewSource
    {
        private readonly Dictionary<string, List<DishModel>> _items;
        private const string CellIdentifier = "TableCell";

        public MenuTableSource(Dictionary<string, List<DishModel>> items)
        {
            _items = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (MenuFoodCell) tableView.DequeueReusableCell(CellIdentifier);

            var key = _items.Keys.ElementAt(indexPath.Section);
            if (cell == null)
            {
				cell = new MenuFoodCell(UITableViewCellStyle.Default, CellIdentifier, indexPath.Row, key, _items, indexPath);
            }
            var uiImg = UIImage.FromFile(ResourcesComponent.GetStringFor("cartEmptyImage"));
            if (_items[key][indexPath.Row].IsInCart)
            {
                uiImg = UIImage.FromFile(ResourcesComponent.GetStringFor("cartPlusImage"));
            }
            var dish = _items[key][indexPath.Row];
            cell.UpdateCell(dish, uiImg);
            return cell;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return _items == null ? "" : _items.Keys.ElementAt((int) section);
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _items?.Keys.Count ?? 0;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _items?[_items.Keys.ElementAt((int) section)].Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
			return IsExpandedRow(indexPath) ? ViewPreferences.ExpandedRowHeight:ViewPreferences.RowHeight;
        }

		private bool IsExpandedRow(NSIndexPath path)
		{
			var key = _items.Keys.ElementAt(path.Section);
			var dish = _items[key][path.Row];
			return (MenuController.CellExpanded && MenuController.DishId == dish.Id);
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            if (section == 0)
                return ViewPreferences.HeaderHeight;
            return ViewPreferences.HeaderHeight/2;
        }
    }
}

