﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS.TableBuilders.User
{
    public class CartTableSource : UITableViewSource
    {
        public IList<OrderItemModel> Items;
        private const string CellIdentifier = "TableCell";

        public CartTableSource()
        {
            Items = OrderComponent.GetCart();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (CartCell) tableView.DequeueReusableCell(CellIdentifier);
            var orderItem = Items.ElementAt(indexPath.Row);
            if (cell == null)
            {
                cell = new CartCell(UITableViewCellStyle.Default, CellIdentifier);
            }
            cell.UpdateCell(orderItem);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items?.Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return ViewPreferences.RowHeight;
        }
    }
}

