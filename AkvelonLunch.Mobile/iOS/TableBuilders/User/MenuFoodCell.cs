﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using lunch.iOS.Controllers;
using UIKit;

namespace lunch.iOS.TableBuilders.User
{
    public sealed class MenuFoodCell : UITableViewCell
    {
        private DishModel _dish;
        private readonly UILabel _headingLabel;
        private readonly UILabel _subheadingLabel;
        private readonly UILabel _priceLabel;
        private readonly UIButton _cartBtn;
        private readonly UIButton _arrow;
        public UITableViewCellStyle Style { get; }
        public int R { get; }
        public string K { get; }
        public Dictionary<string, List<DishModel>> I { get; }
        private readonly NSIndexPath _indexPath;

        public MenuFoodCell(UITableViewCellStyle style, string cellId, int r, string k,
		                    Dictionary<string, List<DishModel>> i, NSIndexPath index) : base(UITableViewCellStyle.Default, cellId)
        {
            Style = style;
            R = r;
            K = k;
            I = i;
            _indexPath = index;

            SelectionStyle = UITableViewCellSelectionStyle.Gray;
            _headingLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };
            _subheadingLabel = new UILabel
            {
                Font = ViewPreferences.LittleFont,
                TextColor = ViewPreferences.GrayColor,
                BackgroundColor = UIColor.Clear
            };
            _priceLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };
            _cartBtn = new UIButton(UIButtonType.RoundedRect);
            _cartBtn.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("cartEmptyImage")), UIControlState.Normal);
			_arrow = new UIButton(UIButtonType.RoundedRect);
			_arrow.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("downArrowImage")), UIControlState.Normal);
            _cartBtn.TouchUpInside += AddToCart;
			_headingLabel.LineBreakMode = UILineBreakMode.WordWrap;
			_headingLabel.Lines = 0;
			_subheadingLabel.LineBreakMode = UILineBreakMode.WordWrap;
			_subheadingLabel.Lines = 0;
			ContentView.AddSubviews(_headingLabel, _subheadingLabel, _priceLabel, _cartBtn, _arrow);
            ContentView.UserInteractionEnabled = true;
        }

        public void UpdateCell(DishModel dish, UIImage ui)
        {
            _dish = dish;
            _cartBtn.UserInteractionEnabled = !(OrderComponent.isMade || SettingsComponent.closed);
            _cartBtn.SetImage(ui, UIControlState.Normal);
            _headingLabel.Text = dish.Name;
            _subheadingLabel.Text = dish.Description;
            _priceLabel.Text = Convert.ToInt32(dish.Cost).ToString();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
           
			_cartBtn.Frame = new CGRect(ContentView.Bounds.Width - 60, 8, 60, 60);
			_priceLabel.Frame = new CGRect(ContentView.Frame.Width - 100, 28, 60, 20);
			_headingLabel.Frame = new CGRect(15, 25, ContentView.Frame.Width - 125, 25);
			_subheadingLabel.Frame = new CGRect(15, 50, ContentView.Frame.Width - 125, 20);
			if (!NeedsToExpand())
			{
				_arrow.Hidden = true;
			}
			else
			{
				if (MenuController.CellExpanded && MenuController.DishId == _dish.Id)
				{
					_cartBtn.Frame = new CGRect(ContentView.Bounds.Width - 60, 25, 60, 60);
					_priceLabel.Frame = new CGRect(ContentView.Frame.Width - 100, 45, 60, 20);
					_headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 125, 50);
					_subheadingLabel.Frame = new CGRect(15, 70, ContentView.Frame.Width - 125, 40);
					_arrow.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("upArrowImage")), UIControlState.Normal);
					_arrow.Frame = new CGRect(ContentView.Frame.Width / 2 - 20, 95, 30, 25);
				}
				else
				{
					_cartBtn.Frame = new CGRect(ContentView.Bounds.Width - 60, 3, 60, 60);
					_priceLabel.Frame = new CGRect(ContentView.Frame.Width - 100, 23, 60, 20);
					_headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 125, 25);
					_subheadingLabel.Frame = new CGRect(15, 45, ContentView.Frame.Width - 125, 20);
					_arrow.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("downArrowImage")), UIControlState.Normal);
					_arrow.Frame = new CGRect(ContentView.Frame.Width / 2 - 20, 55, 30, 25);
				}
				_arrow.Hidden = false;
			}
           
        }

        private void AddToCart(object sender, EventArgs e)
        {
            OrderComponent.ToggleOrderItem(_dish);
            UpdateItems();
        }

        private static void UpdateItems()
        {
			MenuController.ReloadCart();
            MenuController.ReloadMenu();
        }

		private bool NeedsToExpand()
		{
			return _headingLabel.AttributedText.Size.Width > _headingLabel.Frame.Width || _subheadingLabel.AttributedText.Size.Width > _subheadingLabel.Frame.Width;
		}

		public override void TouchesEnded(NSSet touches, UIEvent evt)
		{
		    if (!NeedsToExpand()) return;
		    if (!MenuController.CellExpanded)
		        MenuController.DishId = _dish.Id;
		    MenuController.CellExpanded = !MenuController.CellExpanded;
		    MenuController.MenuTable.ReloadRows(new[] { _indexPath }, UITableViewRowAnimation.Fade);
		}
    }
}

