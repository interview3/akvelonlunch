﻿using CoreGraphics;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.UserList
{
    public sealed class UserCell : UITableViewCell
    {
        public UITableViewCellStyle Style { get; }
        private readonly UILabel _headingLabel;
        private readonly UILabel _subLabel;

        public UserCell(UITableViewCellStyle style, string cellId) : base(UITableViewCellStyle.Default, cellId)
        {
            Style = style;
            _headingLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };

            _subLabel = new UILabel
            {
                Font = ViewPreferences.LittleFont,
                TextColor = ViewPreferences.GrayColor,
                BackgroundColor = UIColor.Clear
            };

            ContentView.AddSubviews(new UIView[] {_headingLabel, _subLabel});
        }

        public void UpdateCell(UserModel user)
        {
            _headingLabel.Text = user.FirstName + ' ' + user.LastName;
            _subLabel.Text = ResourcesComponent.GetStringFor("userBalance") + ": " + user.Balance;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            _headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 20, 25);
            _subLabel.Frame = new CGRect(15, 45, ContentView.Frame.Width - 20, 20);
        }
    }
}

