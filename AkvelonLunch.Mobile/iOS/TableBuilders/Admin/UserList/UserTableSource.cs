﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.UserList
{
    public class UserTableSource : UITableViewSource
    {
        public IList<UserModel> Items;
        public readonly string CellIdentifier = "TableCell";

        public delegate void Del(UserModel user);

        private readonly Del _onRowSelected;

        public UserTableSource(IList<UserModel> items, Del onClick)
        {
            Items = items;
            _onRowSelected = onClick;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _onRowSelected(Items[indexPath.Row]);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (UserCell) tableView.DequeueReusableCell(CellIdentifier);
            var user = Items.ElementAt(indexPath.Row);
            if (cell == null)
            {
                cell = new UserCell(UITableViewCellStyle.Default, CellIdentifier);
            }
            cell.UpdateCell(user);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items?.Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return ViewPreferences.RowHeight;
        }
    }
}

