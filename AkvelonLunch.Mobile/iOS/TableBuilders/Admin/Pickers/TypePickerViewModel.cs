﻿using System;
using System.Collections.Generic;
using System.Drawing;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.Pickers
{
    public class TypePickerViewModel : UIPickerViewModel
    {
        readonly IList<DishType> _dishtypes;

        public TypePickerViewModel(IList<DishType> dict)
        {
            _dishtypes = dict;
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return _dishtypes?.Count ?? 0;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return _dishtypes == null ? "" : _dishtypes[(int) row].Name;
        }

        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }

        public override UIView GetView(UIPickerView pickerView, nint row, nint component, UIView view)
        {
            var lbl = new UILabel
            {
                Frame = new RectangleF(0, 0, (float) pickerView.Frame.Width, (float) pickerView.Frame.Height),
                BackgroundColor = UIColor.Clear,
                TextColor = UIColor.Black,
                Font = ViewPreferences.PickerFont,
                TextAlignment = UITextAlignment.Center,
                Text = _dishtypes[(int) row].Name
            };
            return lbl;
        }
    }
}


