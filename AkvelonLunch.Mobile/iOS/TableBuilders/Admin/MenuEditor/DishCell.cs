﻿using System;
using CoreGraphics;
using lunch.iOS.Controllers;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.MenuEditor
{
    public sealed class DishCell : UITableViewCell
    {
        public UITableViewCellStyle Style { get; }
        private DishModel _dish;
        private readonly UILabel _headingLabel;
        readonly UILabel _priceLabel;
        readonly UIButton _addDishBtn;

        public DishCell(UITableViewCellStyle style, string cellId) : base(UITableViewCellStyle.Default, cellId)
        {
            Style = style;
            _headingLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };

            _addDishBtn = new UIButton(UIButtonType.RoundedRect);
            _addDishBtn.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("addDishImage")), UIControlState.Normal);
			_addDishBtn.TintColor = ViewPreferences.GrayColor;
            _addDishBtn.TouchUpInside += AddDish;
            _priceLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };
            ContentView.AddSubviews(_headingLabel, _addDishBtn, _priceLabel);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            _addDishBtn.Frame = new CGRect(ContentView.Bounds.Width - 60, 13, 45, 45);
            _priceLabel.Frame = new CGRect(ContentView.Frame.Width - 100, 23, 60, 30);
            _headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 125, 35);
        }

        public void UpdateCell(DishModel myDish, UIImage ui, UIColor color)
        {
            _dish = myDish;
            _addDishBtn.TintColor = color;
            _addDishBtn.SetImage(ui, UIControlState.Normal);
            _headingLabel.Text = _dish.Name;
            _priceLabel.Text = Convert.ToInt32(_dish.Cost).ToString();
        }

        private void AddDish(object sender, EventArgs e)
        {
			bool added = AdminMenuComponent.ToggleMenuItem(_dish);
            AdminEditMenuController.ReloadMenuData();
            MenuController.ReloadMenu();
			if (added)
			{
				_addDishBtn.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("dishAddedImage")), UIControlState.Normal);
				_addDishBtn.TintColor = ViewPreferences.OrangeColor;
			}
			else
			{
				_addDishBtn.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("addDishImage")), UIControlState.Normal);
				_addDishBtn.TintColor = ViewPreferences.GrayColor;
			}
           
        }
    }
}

