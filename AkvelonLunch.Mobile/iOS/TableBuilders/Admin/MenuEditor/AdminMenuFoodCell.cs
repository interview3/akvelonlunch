﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using lunch.iOS.Controllers;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.MenuEditor
{
    public sealed class AdminMenuFoodCell : UITableViewCell
    {
        public Dictionary<string, List<DishModel>> I { get; }
        public UITableViewCellStyle Style { get; }
        private DishModel _dish;
        private readonly UILabel _headingLabel;
        private readonly UILabel _subheadingLabel;
        readonly UILabel _priceLabel;
        private readonly UIButton _removeBtn;

        public AdminMenuFoodCell(UITableViewCellStyle style, string cellId, Dictionary<string, List<DishModel>> i)
            : base(UITableViewCellStyle.Default, cellId)
        {
            I = i;
            Style = style;
            SelectionStyle = UITableViewCellSelectionStyle.Gray;
            _headingLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };
            _subheadingLabel = new UILabel
            {
                Font = ViewPreferences.LittleFont,
                TextColor = ViewPreferences.GrayColor,
                BackgroundColor = UIColor.Clear
            };
            _priceLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };
            _removeBtn = new UIButton(UIButtonType.RoundedRect);
            _removeBtn.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("removeImage")), UIControlState.Normal);
            _removeBtn.TouchUpInside += RemoveFromMenu;
            ContentView.AddSubviews(_headingLabel, _subheadingLabel, _priceLabel, _removeBtn);
            ContentView.UserInteractionEnabled = true;
        }

        public void UpdateCell(DishModel dish, UIImage ui)
        {
            _dish = dish;
            _removeBtn.UserInteractionEnabled = !(OrderComponent.isMade || SettingsComponent.closed);
            _removeBtn.SetImage(ui, UIControlState.Normal);
            _headingLabel.Text = dish.Name;
            _subheadingLabel.Text = dish.Description;
            _priceLabel.Text = Convert.ToInt32(dish.Cost).ToString();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            _removeBtn.Frame = new CGRect(ContentView.Bounds.Width - 60, 10, 45, 45);
            _priceLabel.Frame = new CGRect(ContentView.Frame.Width - 100, 23, 60, 20);
            _headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 125, 25);
            _subheadingLabel.Frame = new CGRect(15, 45, ContentView.Frame.Width - 125, 20);
        }

        private void RemoveFromMenu(object sender, EventArgs e)
        {
            AdminMenuComponent.RemoveItem(_dish.Id);
            UpdateItems();
        }

        private static void UpdateItems()
        {
            AdminEditMenuController.ReloadMenuData();
            AdminEditMenuController.ReloadDishes();
        }
    }
}

