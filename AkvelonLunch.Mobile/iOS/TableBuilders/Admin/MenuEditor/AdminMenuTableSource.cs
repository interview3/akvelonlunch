﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.MenuEditor
{
    public class AdminMenuTableSource : UITableViewSource
    {
        readonly Dictionary<string, List<DishModel>> _items;
        private const string CellIdentifier = "TableCell";

        public AdminMenuTableSource(Dictionary<string, List<DishModel>> items)
        {
            _items = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (AdminMenuFoodCell) tableView.DequeueReusableCell(CellIdentifier);
            var key = _items.Keys.ElementAt(indexPath.Section);
            if (cell == null)
            {
                cell = new AdminMenuFoodCell(UITableViewCellStyle.Default, CellIdentifier, _items);
            }
            var uiImg = UIImage.FromFile(ResourcesComponent.GetStringFor("removeImage"));
            var dish = _items[key][indexPath.Row];
            cell.UpdateCell(dish, uiImg);
            return cell;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return _items == null ? "" : _items.Keys.ElementAt((int) section);
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _items?.Keys.Count ?? 0;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _items?[_items.Keys.ElementAt((int) section)].Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return ViewPreferences.RowHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            if (section == 0)
                return ViewPreferences.HeaderHeight;
            return ViewPreferences.HeaderHeight/2;
        }
    }
}

