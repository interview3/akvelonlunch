﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.MenuEditor
{
    public class DishTableSource : UITableViewSource
    {
        public IList<DishModel> Items;
        private const string CellIdentifier = "TableCell";

        public DishTableSource(IList<DishModel> items)
        {
            Items = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (DishCell) tableView.DequeueReusableCell(CellIdentifier);
            var dish = Items.ElementAt(indexPath.Row);
            var addedDishButton = dish.DishIsInMenu;
            if (cell == null)
            {
                cell = new DishCell(UITableViewCellStyle.Default, CellIdentifier);
            }
            var color = ViewPreferences.GrayColor;
            var ui = UIImage.FromFile(ResourcesComponent.GetStringFor("addDishImage"));
            if (addedDishButton)
            {
                ui = UIImage.FromFile(ResourcesComponent.GetStringFor("dishAddedImage"));
                color = ViewPreferences.OrangeColor;
            }
            cell.UpdateCell(dish, ui, color);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items?.Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return ViewPreferences.RowHeight;
        }
    }
}

