﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.OrderList
{
    public class OrderTableSource : UITableViewSource
    {
        public Dictionary<string, int> Items;
        private const string CellIdentifier = "TableCell";

        public OrderTableSource(Dictionary<string, int> items)
        {
            Items = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (OrderCell) tableView.DequeueReusableCell(CellIdentifier);
            var name = Items.Keys.ElementAt(indexPath.Row);
            var count = Items[name];
            if (cell == null)
            {
                cell = new OrderCell(UITableViewCellStyle.Default, CellIdentifier);
            }
            cell.UpdateCell(name, count);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Items?.Count ?? 0;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return ViewPreferences.RowHeight;
        }
    }
}

