﻿using CoreGraphics;
using UIKit;

namespace lunch.iOS.TableBuilders.Admin.OrderList
{
    public sealed class OrderCell : UITableViewCell
    {
        public UITableViewCellStyle Style { get; }
        readonly UILabel _headingLabel, _countLabel;

        public OrderCell(UITableViewCellStyle style, string cellId) : base(UITableViewCellStyle.Default, cellId)
        {
            Style = style;
            _headingLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };

            _countLabel = new UILabel
            {
                Font = ViewPreferences.BigFont,
                BackgroundColor = UIColor.Clear
            };

            ContentView.AddSubviews(_headingLabel, _countLabel);
        }

        public void UpdateCell(string name, int count)
        {
            _headingLabel.Text = name;
            _countLabel.Text = count.ToString();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            _headingLabel.Frame = new CGRect(15, 20, ContentView.Frame.Width - 90, 35);
            _countLabel.Frame = new CGRect(ContentView.Frame.Width - 60, 20, 50, 35);
        }
    }
}

