using UIKit;
namespace lunch.iOS
{
    public static class ViewPreferences
    {
        #region FONTS
        public static UIFont LittleFont = UIFont.FromName("GillSans", 12f);
        public static UIFont BigFont = UIFont.FromName("GillSans", 20f);
        public static UIFont TitleFont = UIFont.FromName("GillSans", 24f);
        public static UIFont StatusBoldFont = UIFont.FromName("Georgia-BoldItalic", 18f);
        public static UIFont StatusFont = UIFont.FromName("Georgia-Italic", 18f);
        public static UIFont PickerFont = UIFont.FromName("GillSans", 16f);
        #endregion

        #region COLORS
        public static UIColor GreenColor = UIColor.FromRGB(31, 196, 136);
        public static UIColor OrangeColor = UIColor.FromRGB(255, 128, 0);
        public static UIColor RedColor = UIColor.FromRGB(255, 102, 102);
        public static UIColor GrayColor = UIColor.FromRGB(102, 102, 102);
        public static UIColor LightGrayColor = UIColor.FromRGB(230, 230, 230);
        #endregion

        #region VALUES
        public static double AnimDuration = 0.2;
        public static float BlockingAlpha = 0.5f;
        public static float RowHeight = 75;
		public static float ExpandedRowHeight = 115;
        public static float HeaderHeight = 30;
        public static int CornerRadius = 5;
        public static int SmallUpdatingPeriod = 5;
        public static int LargeUpdatingPeriod = 2;
        #endregion

    }
}

