﻿Start testing with turning ON the debug mode in Resources file. It is located in root folder of "lunch" project;
Change value of entry "DEBUG_MODE" to true.
If you want to export .ipa archive, change configuration to Release|IPhone, click right button on lunch.iOS project and
pick Archive for publishing. Follow the instructions then (Don't forget to Resign with another identity).
You can change the design in Storyboards document LaunchScreen;
You can change some exporting settings in Info.pslist;
The code which interacts with design is located in Controllers namespace (and folder as well);
All Table Adapters (aka Sources) are located in TableBuilders folder;
All the images are located in Resources folder;
All the strings are located in general resources file; You can get access to them via ResourcesComponent. 

You can change your provisioner profile in XCode in your account; Upload .ipa archive to Hockey App.
Good luck!