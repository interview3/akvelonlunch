﻿using Foundation;
using UIKit;

namespace lunch.iOS
{
	[Register("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert, null);
			UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			var storyboard = UIStoryboard.FromName("LaunchScreen", null);
			var initialViewController = storyboard.InstantiateInitialViewController();
		    var window = new UIWindow(UIScreen.MainScreen.Bounds) {RootViewController = initialViewController};
		    window.MakeKeyAndVisible();

			return true;
		}

	}
}

