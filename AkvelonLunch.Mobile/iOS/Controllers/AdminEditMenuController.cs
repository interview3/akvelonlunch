using System;
using System.Threading.Tasks;
using CoreGraphics;
using lunch.iOS.IosComponents;
using lunch.iOS.TableBuilders.Admin.MenuEditor;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AdminEditMenuController : UIViewController
    {
        public AdminEditMenuController(IntPtr handle) : base(handle)
        {
        }

        private UITextField _search;
        private UIView _menuView;
        private UIView _dishesView;
        private static UITableView _menuTable;
        private static UITableView _dishesTable;
        private UIButton _backBtn;
        private UIButton _backIcon;
        private UIButton _menuIcon;
        private UILabel _barTitle;
        private UIActivityIndicatorView _actView;

        partial void swipedLeft(UISwipeGestureRecognizer sender)
        {
            if (tabControl.SelectedSegment != 0) return;
            tabControl.SelectedSegment = 1;
            DishesSegment();
        }

        partial void swipedRight(UISwipeGestureRecognizer sender)
        {
            if (tabControl.SelectedSegment != 1) return;
            tabControl.SelectedSegment = 0;
            MenuSegment();
        }

        partial void segmentNew(UISegmentedControl sender)
        {
            if (tabControl.SelectedSegment == 0)
            {
                MenuSegment();
            }
            else
            {
                DishesSegment();
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateSpinner();
            CreateBar();
            SetControlsText();
            CreateViews();
            CreateTables();
            CreateButtons();
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
        }

        public static void ReloadMenuData()
        {
            if (_menuTable == null) return;
            var menuResponse = AdminMenuComponent.GetMenuDictionary();
            if (!menuResponse.ok) return;
            _menuTable.Source = new AdminMenuTableSource(menuResponse.responseObject);
            _menuTable.ReloadData();
        }

        public static void ReloadDishes()
        {
            var dishesResponse = DishComponent.GetAllDishes();
            _dishesTable.Source = dishesResponse.ok ? new DishTableSource(dishesResponse.responseObject) : null;
            _dishesTable.ReloadData();
        }

        #region PRIVATE

        private void CreateSpinner()
        {
            _actView = new UIActivityIndicatorView
            {
                Frame = new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60),
                ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
                Alpha = 0.75f,
                Hidden = true
            };
            Add(_actView);
        }

        private void StartSpinner()
        {
            _menuView.Alpha = 0.5f;
            _actView.Hidden = false;
            _actView.StartAnimating();
        }

        private void StopSpinner()
        {
            _menuView.Alpha = 1;
            _actView.Hidden = true;
            _actView.StopAnimating();
        }

        private void GoBack(object sender, EventArgs e)
        {
            tabControl.SelectedSegment = 0;
            MenuSegment();
        }

        private void MenuSegment()
        {
            ReloadMenuData();
            _menuView.Hidden = false;
            _dishesView.Hidden = true;
            _backBtn.Hidden = true;
            _backIcon.Hidden = true;
            _menuIcon.Hidden = false;
        }

        private void DishesSegment()
        {
            ReloadDishes();
            _menuView.Hidden = true;
            _dishesView.Hidden = false;
            _backBtn.Hidden = false;
            _backIcon.Hidden = false;
            _menuIcon.Hidden = true;
        }

        private void CreateViews()
        {
            _dishesView = new UIView(new CGRect(0, 90, View.Frame.Width, View.Frame.Height - 125));
            _menuView = new UIView(new CGRect(0, 90, View.Frame.Width, View.Frame.Height - 125));
            _dishesView.Hidden = true;
            Add(_menuView);
            Add(_dishesView);
            if (!(OrderComponent.isMade || SettingsComponent.closed))
                _menuView.Alpha = 1;
            else
                _menuView.Alpha = ViewPreferences.BlockingAlpha;
        }

        private static void ToggleMenu(object sender, EventArgs e)
        {
            AdminController.Navigation.ToggleMenu();
        }

        private void GobackIcon_TouchUpInside(object sender, EventArgs e)
        {
            var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
            ShowViewController(mc, this);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create back button

            _backBtn = new UIButton(new CGRect(20, 10, 70, 50));
            _backBtn.TouchUpInside += GoBack;
            _backBtn.SetTitleColor(ViewPreferences.OrangeColor, UIControlState.Normal);
            _backBtn.TitleLabel.Font = ViewPreferences.TitleFont;
            _backBtn.Hidden = true;

            #endregion

            #region create back icon

            _backIcon = new UIButton(new CGRect(5, 20, 30, 30));
            _backIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("backBtnImage")), UIControlState.Normal);
            _backIcon.TouchUpInside += GoBack;
            _backIcon.Hidden = true;

            #endregion

            #region create bar title

            _barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 40, 20, 80, 30),
                Font = ViewPreferences.TitleFont
            };

            #endregion

            #region create goback

            var gobackIcon = new UIButton(new CGRect(View.Frame.Width - 60, 10, 48, 48));
            gobackIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("gotomenuImage")),
                UIControlState.Normal);
            gobackIcon.TouchUpInside += GobackIcon_TouchUpInside;

            #endregion

            #region create menu icon

            _menuIcon = new UIButton(new CGRect(5, 10, 48, 48));
            _menuIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("menuImage")), UIControlState.Normal);
            _menuIcon.TouchUpInside += ToggleMenu;

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(_barTitle);
            commonBar.AddSubview(_backBtn);
            commonBar.AddSubview(_backIcon);
            commonBar.AddSubview(_menuIcon);
            commonBar.AddSubview(gobackIcon);

            #endregion

            Add(commonBar);
        }

        private void CreateTables()
        {
            #region create menu table

            _menuTable = new UITableView(
                new CGRect(_menuView.Bounds.X, _menuView.Bounds.Y, _menuView.Bounds.Width, _menuView.Bounds.Height - 60),
                UITableViewStyle.Grouped)
            {
                AutoresizingMask = UIViewAutoresizing.All,
                ScrollEnabled = true
            };
            AdminMenuComponent.UpdateAdminMenu();
            var menuResponse = AdminMenuComponent.GetMenuDictionary();
            _menuTable.Source = menuResponse.ok ? new AdminMenuTableSource(menuResponse.responseObject) : null;
            _menuView.Add(_menuTable);
            _menuTable.AllowsSelection = false;

            #endregion

            #region create dishes table

            _dishesTable =
                new UITableView
                {
                    Frame = new CGRect(_menuView.Bounds.X, _menuView.Bounds.Y + 40, _menuView.Bounds.Width,
                        _menuView.Bounds.Height - 100),
                    AutoresizingMask = UIViewAutoresizing.All,
                    ScrollEnabled = true,
                    AllowsSelection = false
                };

            var dishesResponse = DishComponent.GetAllDishes();
            if (dishesResponse.ok)
            {
                _dishesTable.Source = new DishTableSource(dishesResponse.responseObject);
            }
            else
            {
                _dishesTable.Source = null;
                var alert =
                    AlertService.GetAlert(ResourcesComponent.GetStringFor("noDishes") + "\n" +
                                          ResourcesComponent.GetStringFor("serverResponse") + dishesResponse.message);
                ShowAlert(alert);
            }
            _dishesView.Add(_dishesTable);

            #endregion
        }

        private void CreateButtons()
        {
            var confirmButton = new UIButton
            {
                Frame = new CGRect(10, _menuView.Frame.Height - 45, _menuView.Frame.Width - 20, 40),
                BackgroundColor = ViewPreferences.GreenColor,
                UserInteractionEnabled = !(OrderComponent.isMade || SettingsComponent.closed)
            };
            confirmButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            confirmButton.Layer.CornerRadius = ViewPreferences.CornerRadius;
            confirmButton.TouchUpInside += ConfirmButton_TouchUpInside;
            confirmButton.TitleLabel.Font = ViewPreferences.TitleFont;
            confirmButton.SetTitle(ResourcesComponent.GetStringFor("confirmMenuName"), UIControlState.Normal);

            var addDish = new UIButton
            {
                Frame = new CGRect(10, _dishesView.Frame.Height - 45, _dishesView.Frame.Width - 20, 40),
                BackgroundColor = ViewPreferences.GreenColor
            };
            addDish.SetTitleColor(UIColor.White, UIControlState.Normal);
            addDish.Layer.CornerRadius = ViewPreferences.CornerRadius;
            addDish.TouchUpInside += AddDish_TouchUpInside;
            addDish.TitleLabel.Font = ViewPreferences.TitleFont;
            addDish.SetTitle(ResourcesComponent.GetStringFor("addDishName"), UIControlState.Normal);

            _search = new UITextField
            {
                Frame = new CGRect(10, 10, _dishesView.Frame.Width - 20, 30),
                Placeholder = ResourcesComponent.GetStringFor("searchLabel"),
                Font = ViewPreferences.TitleFont,
                ClearButtonMode = UITextFieldViewMode.WhileEditing
            };
            _search.Layer.BorderWidth = 1;
            _search.Layer.CornerRadius = ViewPreferences.CornerRadius;
            _search.Layer.BorderColor = ViewPreferences.LightGrayColor.CGColor;
            _search.ShouldReturn = (tf) =>
            {
                Search(_search.Text);
                View.EndEditing(true);
                return true;
            };
            _search.EditingChanged += Search_EditingChanged;

            _dishesView.Add(_search);
            _dishesView.Add(addDish);
            _menuView.Add(confirmButton);
        }

        private async void AddDish_TouchUpInside(object sender, EventArgs e)
        {
            StartSpinner();

            await Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(delegate
                {
                    var mc = Storyboard.InstantiateViewController("AddDishController") as AddDishController;
                    ShowViewController(mc, this);
                });
            });
        }

        private static void Search_EditingChanged(object sender, EventArgs e)
        {
            var textField = (UITextField) sender;
            Search(textField.Text);
        }

        private static void Search(string key)
        {
            var dishesResponse = DishComponent.GetAllDishesByName(key);
            _dishesTable.Source = dishesResponse.ok ? new DishTableSource(dishesResponse.responseObject) : null;
            _dishesTable.ReloadData();
        }

        CustomWebResponse<bool> _createResponse;

        private async void ConfirmButton_TouchUpInside(object sender, EventArgs e)
        {
            StartSpinner();
            await Task.Factory.StartNew(() => { _createResponse = AdminMenuComponent.SaveMenu(); });

            if (_createResponse.ok)
            {
                StopSpinner();
                var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("menuCreatedName"));
                ShowAlert(alert);
            }
            else
            {
                StopSpinner();
                var alert =
                    AlertService.GetAlert(ResourcesComponent.GetStringFor("menuNotCreatedName") + "\n" +
                                          ResourcesComponent.GetStringFor("serverResponse") + _createResponse.message);
                ShowAlert(alert);
            }
        }

        private void ShowAlert(UIViewController alert)
        {
            PresentViewController(alert, true, null);
        }

        private void SetControlsText()
        {
            _backBtn.SetTitle(ResourcesComponent.GetStringFor("goBackLabel"), UIControlState.Normal);
            _barTitle.Text = ResourcesComponent.GetStringFor("AdminPanelLabel");
            tabControl.SetTitle(ResourcesComponent.GetStringFor("menuName"), 0);
            tabControl.SetTitle(ResourcesComponent.GetStringFor("dishesName"), 1);
        }

        #endregion
    }
}