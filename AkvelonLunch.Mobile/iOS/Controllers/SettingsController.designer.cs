// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("SettingsController")]
    partial class SettingsController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton enBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel langText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel minLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField moneyValue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel remBalanceLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch remBalanceSwitcher { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel remindAtLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel remindMeLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch remSwitcher { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ruBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker timePicker { get; set; }

        [Action ("OnEnSelected:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnEnSelected (UIKit.UIButton sender);

        [Action ("OnRuSelected:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnRuSelected (UIKit.UIButton sender);

        [Action ("OnSwitcherChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnSwitcherChanged (UIKit.UISwitch sender);

        [Action ("OnTimeChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnTimeChanged (UIKit.UIDatePicker sender);

        [Action ("switcherBalanceChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void switcherBalanceChanged (UIKit.UISwitch sender);

        void ReleaseDesignerOutlets ()
        {
            if (enBtn != null) {
                enBtn.Dispose ();
                enBtn = null;
            }

            if (langText != null) {
                langText.Dispose ();
                langText = null;
            }

            if (minLabel != null) {
                minLabel.Dispose ();
                minLabel = null;
            }

            if (moneyValue != null) {
                moneyValue.Dispose ();
                moneyValue = null;
            }

            if (remBalanceLabel != null) {
                remBalanceLabel.Dispose ();
                remBalanceLabel = null;
            }

            if (remBalanceSwitcher != null) {
                remBalanceSwitcher.Dispose ();
                remBalanceSwitcher = null;
            }

            if (remindAtLabel != null) {
                remindAtLabel.Dispose ();
                remindAtLabel = null;
            }

            if (remindMeLabel != null) {
                remindMeLabel.Dispose ();
                remindMeLabel = null;
            }

            if (remSwitcher != null) {
                remSwitcher.Dispose ();
                remSwitcher = null;
            }

            if (ruBtn != null) {
                ruBtn.Dispose ();
                ruBtn = null;
            }

            if (timePicker != null) {
                timePicker.Dispose ();
                timePicker = null;
            }
        }
    }
}