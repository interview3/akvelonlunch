using System;
using CoreGraphics;
using lunch.iOS.IosComponents;
using lunch.iOS.TableBuilders.Admin.OrderList;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AdminOrdersController : UIViewController
    {
        private static UILabel _total;
        private UIButton _ordersDeliveredBtn;
        private static UITableView _ordersTable;

        public AdminOrdersController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateBar();
            CreateTables();
            CreateButtons();
            SetControlsText();
        }

        #region PRIVATE

        public static void Refresh()
        {
            if (_ordersTable == null) return;
            var orderedDishes = AdminOrderComponent.GetOrderedDishes(true);
            _ordersTable.Source = new OrderTableSource(orderedDishes);
            _ordersTable.ReloadData();
            var summ = AdminOrderComponent.GetCostOfAllOrders(true);
            _total.Text = ResourcesComponent.GetStringFor("totalSummOfOrders") + Convert.ToInt32(summ) + "P";
        }

        private void OrdersDeliveredBtn_TouchUpInside(object sender, EventArgs e)
        {
            var alert = AlertService.GetConfirmingAlert(DeliverOrders);
            ShowAlert(alert);
        }

        private void DeliverOrders()
        {
            var deliverResponse = AdminOrderComponent.DeliverOrders();
            if (deliverResponse.ok)
            {
                var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("ordersDelivered"));
                ShowAlert(alert);
            }
            else
            {
                var alert =
                    AlertService.GetAlert(ResourcesComponent.GetStringFor("ordersNotDelivered") + "\n" +
                                          ResourcesComponent.GetStringFor("serverResponse") + deliverResponse.message);
                ShowAlert(alert);
            }
        }

        private void SetControlsText()
        {
            _ordersDeliveredBtn.SetTitle(ResourcesComponent.GetStringFor("orderDeliveredButtonAdmin"),
                UIControlState.Normal);
        }

        private void CreateTables()
        {
            _ordersTable = new UITableView
            {
                Frame = new CGRect(View.Frame.X, View.Frame.Y + 100, View.Frame.Width, View.Frame.Height - 200),
                AutoresizingMask = UIViewAutoresizing.All,
                ScrollEnabled = true
            };
            View.Add(_ordersTable);
            var orderedDishes = AdminOrderComponent.GetOrderedDishes(true);
            _ordersTable.Source = new OrderTableSource(orderedDishes);
        }

        private void ShowAlert(UIViewController alert)
        {
            PresentViewController(alert, true, null);
        }

        private void CreateButtons()
        {
            _ordersDeliveredBtn = new UIButton
            {
                Frame = new CGRect(10, View.Frame.Height - 45, View.Frame.Width - 20, 40),
                BackgroundColor = ViewPreferences.GreenColor
            };
            _ordersDeliveredBtn.SetTitleColor(UIColor.White, UIControlState.Normal);
            _ordersDeliveredBtn.Layer.CornerRadius = ViewPreferences.CornerRadius;
            _ordersDeliveredBtn.TouchUpInside += OrdersDeliveredBtn_TouchUpInside;
            _ordersDeliveredBtn.TitleLabel.Font = ViewPreferences.TitleFont;

            var summ = AdminOrderComponent.GetCostOfAllOrders();
            _total = new UILabel
            {
                Frame = new CGRect(10, View.Frame.Height - 80, View.Frame.Width - 20, 30),
                Font = ViewPreferences.TitleFont,
                Text = ResourcesComponent.GetStringFor("totalSummOfOrders") + Convert.ToInt32(summ) + "P"
            };
            View.Add(_ordersDeliveredBtn);
            View.Add(_total);
        }

        private static void ToggleMenu(object sender, EventArgs e)
        {
            AdminController.Navigation.ToggleMenu();
        }

        private void GobackIcon_TouchUpInside(object sender, EventArgs e)
        {
            var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
            ShowViewController(mc, this);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create menuicon

            var menuIcon = new UIButton(new CGRect(5, 10, 48, 48));
            menuIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("menuImage")), UIControlState.Normal);
            menuIcon.TouchUpInside += ToggleMenu;

            #endregion

            #region create bar title

            var barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 40, 20, 150, 30),
                Font = ViewPreferences.TitleFont,
                Text = ResourcesComponent.GetStringFor("ordersLabel")
            };

            #endregion

            #region create goback icon

            var gobackIcon = new UIButton(new CGRect(View.Frame.Width - 60, 10, 48, 48));
            gobackIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("gotomenuImage")),
                UIControlState.Normal);
            gobackIcon.TouchUpInside += GobackIcon_TouchUpInside;

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(barTitle);
            commonBar.AddSubview(menuIcon);
            commonBar.AddSubview(gobackIcon);

            #endregion

            Add(commonBar);
        }

        #endregion
    }
}