using System;
using FlyoutNavigation;
using MonoTouch.Dialog;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AdminController : UIViewController
    {
        public static FlyoutNavigationController Navigation;

        public AdminController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Navigation = new FlyoutNavigationController
            {
                NavigationRoot = new RootElement("Navigation")
                {
                    new Section(ResourcesComponent.GetStringFor("menuLabel"))
                    {
                        new StringElement(ResourcesComponent.GetStringFor("menuEditor")),
                        new StringElement(ResourcesComponent.GetStringFor("usersEditor")),
                        new StringElement(ResourcesComponent.GetStringFor("ordersEditor")),
                        new StringElement(ResourcesComponent.GetStringFor("settingsAdm"))
                    }
                },
                ViewControllers = new[]
                {
                    Storyboard.InstantiateViewController("AdminEditMenuController"),
                    Storyboard.InstantiateViewController("AdminUsersController"),
                    Storyboard.InstantiateViewController("AdminOrdersController"),
                    Storyboard.InstantiateViewController("AdminSettingsController")
                },
                MenuBorderColor = ViewPreferences.GrayColor,
                ShadowViewColor = ViewPreferences.LightGrayColor,
                TintColor = ViewPreferences.OrangeColor
            };
            View.AddSubview(Navigation.View);
        }
    }
}

