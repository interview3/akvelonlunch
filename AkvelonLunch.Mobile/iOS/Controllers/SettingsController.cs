using System;
using CoreGraphics;
using Foundation;
using lunch.iOS.IosComponents;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace lunch.iOS.Controllers
{
    public partial class SettingsController : UIViewController
    {
        private UIButton _backBtn;
        private UILabel _barTitle;

        partial void switcherBalanceChanged(UISwitch sender)
        {
            if (remBalanceSwitcher.On)
            {
                moneyValue.UserInteractionEnabled = true;
                moneyValue.Alpha = 1;
                NotificationService.SetBalanceNotifications();
            }
            else
            {
                moneyValue.UserInteractionEnabled = false;
                moneyValue.Alpha = ViewPreferences.BlockingAlpha;
                NotificationService.RemoveBalanceNotificantions();
            }
        }

        partial void OnEnSelected(UIButton sender)
        {
            Settings.language = "en";
            ruBtn.Layer.BorderWidth = 0;
            enBtn.Layer.BorderWidth = 1;
            SetControlsText();
        }

        partial void OnRuSelected(UIButton sender)
        {
            Settings.language = "ru";
            ruBtn.Layer.BorderWidth = 1;
            enBtn.Layer.BorderWidth = 0;
            SetControlsText();
        }

        partial void OnTimeChanged(UIDatePicker sender)
        {
            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            Settings.RemHour = (int) gregCalendar.GetComponentFromDate(NSCalendarUnit.Hour, timePicker.Date);
            Settings.RemMinute = (int) gregCalendar.GetComponentFromDate(NSCalendarUnit.Minute, timePicker.Date);
            NotificationService.SetNotifications();
        }

        partial void OnSwitcherChanged(UISwitch sender)
        {
            if (remSwitcher.On)
            {
                timePicker.UserInteractionEnabled = true;
                timePicker.Alpha = 1;
                NotificationService.SetNotifications();
            }
            else
            {
                timePicker.UserInteractionEnabled = false;
                timePicker.Alpha = ViewPreferences.BlockingAlpha;
                NotificationService.RemoveNotificantions();
            }
        }

        public SettingsController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateBar();
            SetControlsText();
            SetControlsAppearance();
            SetControlsValues();
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
        }

        #region PRIVATE

        private void GoBack(object sender, EventArgs e)
        {
            int number;
            int.TryParse(moneyValue.Text, out number);
            Settings.remBalanceVal = number;
            var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
            ShowViewController(mc, this);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create back button

            _backBtn = new UIButton(new CGRect(20, 10, 70, 50));
            _backBtn.TouchUpInside += GoBack;
            _backBtn.TitleLabel.Font = ViewPreferences.TitleFont;
            _backBtn.SetTitleColor(ViewPreferences.OrangeColor, UIControlState.Normal);

            #endregion

            #region create back icon

            var backIcon = new UIButton(new CGRect(5, 20, 30, 30));
            backIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("backBtnImage")), UIControlState.Normal);
            backIcon.TouchUpInside += GoBack;

            #endregion

            #region create bar title

            _barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 50, 20, 150, 30),
                Font = ViewPreferences.TitleFont
            };

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(_barTitle);
            commonBar.AddSubview(_backBtn);
            commonBar.AddSubview(backIcon);

            #endregion

            Add(commonBar);
        }

        private void SetControlsText()
        {
            langText.Text = ResourcesComponent.GetStringFor("langLabel");
            remindMeLabel.Text = ResourcesComponent.GetStringFor("remindText");
            remindAtLabel.Text = ResourcesComponent.GetStringFor("remAtLabel");
            _backBtn.SetTitle(ResourcesComponent.GetStringFor("goBackLabel"), UIControlState.Normal);
            _barTitle.Text = ResourcesComponent.GetStringFor("settingsLabel");
            remBalanceLabel.Text = ResourcesComponent.GetStringFor("remindBalanceText");
            minLabel.Text = ResourcesComponent.GetStringFor("minLabel");
        }

        private void SetControlsAppearance()
        {
            ruBtn.Layer.CornerRadius = ViewPreferences.CornerRadius;
            ruBtn.Layer.BorderColor = ViewPreferences.OrangeColor.CGColor;
            enBtn.Layer.BorderColor = ViewPreferences.OrangeColor.CGColor;
            enBtn.Layer.CornerRadius = ViewPreferences.CornerRadius;
            ruBtn.AddConstraint(NSLayoutConstraint.Create(ruBtn, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.Width, 25, 25));
            ruBtn.AddConstraint(NSLayoutConstraint.Create(ruBtn, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.Height, 20, 20));
            enBtn.AddConstraint(NSLayoutConstraint.Create(enBtn, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.Width, 25, 25));
            enBtn.AddConstraint(NSLayoutConstraint.Create(enBtn, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null,
                NSLayoutAttribute.Height, 20, 20));
        }

        private void SetControlsValues()
        {
            remSwitcher.On = Settings.Switcher;
            remBalanceSwitcher.On = Settings.remBalance;
            moneyValue.Text = Settings.remBalanceVal.ToString();
            if (Settings.language == "ru")
            {
                ruBtn.Layer.BorderWidth = 1;
            }
            else
            {
                enBtn.Layer.BorderWidth = 1;
            }
            if (remSwitcher.On)
            {
                timePicker.UserInteractionEnabled = true;
                timePicker.Alpha = 1;
            }
            else
            {
                timePicker.UserInteractionEnabled = false;
                timePicker.Alpha = ViewPreferences.BlockingAlpha;
            }

            if (remBalanceSwitcher.On)
            {
                moneyValue.UserInteractionEnabled = true;
                moneyValue.Alpha = 1;
            }
            else
            {
                moneyValue.UserInteractionEnabled = false;
                moneyValue.Alpha = ViewPreferences.BlockingAlpha;
            }

            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            var components = gregCalendar.Components(NSCalendarUnit.Day | NSCalendarUnit.Year | NSCalendarUnit.Month,
                DateTime.Now.ToNSDate());
            components.Day = DateTime.Now.Day;
            components.Month = DateTime.Now.Month;
            components.Year = DateTime.Now.Year;
            var referenceDate = gregCalendar.DateFromComponents(components);
            var componentsForFireDate =
                gregCalendar.Components(NSCalendarUnit.Year | NSCalendarUnit.Hour | NSCalendarUnit.Minute, referenceDate);
            componentsForFireDate.Year = components.Year;
            componentsForFireDate.Month = components.Month;
            componentsForFireDate.Day = components.Day;
            componentsForFireDate.Hour = Settings.RemHour;
            componentsForFireDate.Minute = Settings.RemMinute;
            var fireDateOfNotification = gregCalendar.DateFromComponents(componentsForFireDate);
            timePicker.SetDate(fireDateOfNotification, false);

            moneyValue.ShouldReturn = tf =>
            {
                Settings.remBalanceVal = Convert.ToInt32(moneyValue.Text);
                timePicker.BecomeFirstResponder();
                return true;
            };
        }

        #endregion
    }
}