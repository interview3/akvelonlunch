using System;
using System.Globalization;
using System.Threading.Tasks;
using CoreGraphics;
using lunch.iOS.IosComponents;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AdminUserPageController : UIViewController
    {
        private UserModel _user;
        private UIActivityIndicatorView _actView;

        public AdminUserPageController(IntPtr handle) : base(handle)
        {
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _user = AdminUserComponent.EditingUser();
            CreateSpinner();
            CreateBar();
            SetControlsAppearanceAndBehaviour();
            SetControlsText();
        }

        #region PRIVATE

        private void CreateSpinner()
        {
            _actView = new UIActivityIndicatorView
            {
                Frame = new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60),
                ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
                Alpha = 0.75f,
                Hidden = true
            };
            Add(_actView);
        }

        private void StartSpinner()
        {
            mainView.Alpha = 0.5f;
            _actView.Hidden = false;
            _actView.StartAnimating();
        }

        private void StopSpinner()
        {
            mainView.Alpha = 1;
            _actView.Hidden = true;
            _actView.StopAnimating();
        }

        private async void GoBack(object sender, EventArgs e)
        {
            StartSpinner();

            await Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(delegate
                {
                    var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
                    ShowViewController(mc, this);
                });
            });
        }

        private CustomWebResponse<bool> _userResponse;

        private async void Submit_TouchUpInside(object sender, EventArgs e)
        {
            StartSpinner();
            try
            {
                var price = Convert.ToDecimal(balanceInput.Text);
                _user.Balance = price;
                await Task.Factory.StartNew(() => { _userResponse = AdminUserComponent.Edit(_user); });

                if (_userResponse.ok)
                {
                    StopSpinner();
                    var alert = UIAlertController.Create(ResourcesComponent.GetStringFor("attentionLabel"),
                        ResourcesComponent.GetStringFor("userEdited"), UIAlertControllerStyle.Alert);
                    alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, (action) =>
                    {
                        var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
                        ShowViewController(mc, this);
                    }));
                    ShowAlert(alert);
                }
                else
                {
                    StopSpinner();
                    var alert =
                        AlertService.GetAlert(ResourcesComponent.GetStringFor("userNotEdited") + "\n" +
                                              ResourcesComponent.GetStringFor("serverResponse") + _userResponse.message);
                    ShowAlert(alert);
                }
            }
            catch (Exception ex)
            {
                StopSpinner();
                var alert = AlertService.GetAlert(ex.Message.Contains("correct format")
                        ? ResourcesComponent.GetStringFor("onlyNumbers")
                        : ex.Message);
                ShowAlert(alert);
            }
        }

        private void ShowAlert(UIViewController alert)
        {
            PresentViewController(alert, true, null);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create back button

            var backBtn = new UIButton(new CGRect(20, 10, 70, 50));
            backBtn.TouchUpInside += GoBack;
            backBtn.SetTitleColor(ViewPreferences.OrangeColor, UIControlState.Normal);
            backBtn.TitleLabel.Font = ViewPreferences.TitleFont;
            backBtn.SetTitle(ResourcesComponent.GetStringFor("goBackLabel"), UIControlState.Normal);

            #endregion

            #region create back icon

            var backIcon = new UIButton(new CGRect(5, 20, 30, 30));
            backIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("backBtnImage")), UIControlState.Normal);
            backIcon.TouchUpInside += GoBack;

            #endregion

            #region create bar title

            var barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 55, 20, 150, 30),
                Text = ResourcesComponent.GetStringFor("editUserLabel"),
                Font = ViewPreferences.TitleFont
            };

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(barTitle);
            commonBar.AddSubview(backBtn);
            commonBar.AddSubview(backIcon);

            #endregion

            Add(commonBar);
        }

        private void SetControlsText()
        {
            userName.Text = _user.FirstName;
            userName2.Text = _user.LastName;
            balanceInput.Placeholder = ResourcesComponent.GetStringFor("userBalance");
            balanceInput.Text = _user.Balance.ToString(CultureInfo.InvariantCulture);
        }

        private void SetControlsAppearanceAndBehaviour()
        {
            var submit = new UIButton
            {
                Frame = new CGRect(23, View.Frame.Height - 50, View.Frame.Width - 46, 40),
                BackgroundColor = ViewPreferences.GreenColor
            };
            submit.SetTitle(ResourcesComponent.GetStringFor("submitLabel"), UIControlState.Normal);
            submit.SetTitleColor(UIColor.White, UIControlState.Normal);
            submit.Layer.CornerRadius = ViewPreferences.CornerRadius;
            submit.TouchUpInside += Submit_TouchUpInside;
            Add(submit);

            balanceInput.ShouldReturn = tf =>
            {
                View.EndEditing(true);
                return true;
            };
        }

        #endregion
    }
}