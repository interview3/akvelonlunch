using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreGraphics;
using lunch.iOS.IosComponents;
using lunch.iOS.TableBuilders.Admin.Pickers;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AddDishController : UIViewController
    {
        private IList<DishType> _dishtypes;
        private IList<ProvisionerModel> _provisioners;
        private UIPickerView _dishType;
        private UIPickerView _provType;
        private UIActivityIndicatorView _actView;

        public AddDishController(IntPtr handle) : base(handle)
        {
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateSpinner();
            CreateBar();
            SetControlsAppearanceAndBehaviour();
            SetControlsText();
        }

        #region PRIVATE

        private void CreateSpinner()
        {
            _actView = new UIActivityIndicatorView()
            {
                Frame = new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60),
                ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
                Alpha = 0.75f,
                Hidden = true
            };
            Add(_actView);
        }

        private void StartSpinner()
        {
            mainView.Alpha = 0.5f;
            _actView.Hidden = false;
            _actView.StartAnimating();
        }

        private void StopSpinner()
        {
            mainView.Alpha = 1;
            _actView.Hidden = true;
            _actView.StopAnimating();
        }

        private async void GoBack(object sender, EventArgs e)
        {
            StartSpinner();

            await Task.Factory.StartNew(() =>
            {
                InvokeOnMainThread(delegate
                {
                    var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
                    ShowViewController(mc, this);
                });
            });
        }

        private CustomWebResponse<int> _createResponse;

        private async void Submit_TouchUpInside(object sender, EventArgs e)
        {
            StartSpinner();

            decimal price;
            try
            {
                var name = dishName.Text;
                var description = dishDescr.Text;
                if (name.Length == 0)
                    throw new Exception(ResourcesComponent.GetStringFor("emptyField"));
                price = Convert.ToDecimal(dishPrice.Text);
                var row = (int) _dishType.SelectedRowInComponent(0);
                var dishtype = _dishtypes[row];
                row = (int) _provType.SelectedRowInComponent(0);
                var prov = _provisioners[row];
                await
                    Task.Factory.StartNew(() =>
                        {
                            _createResponse = DishComponent.Create(new DishModel(name, description, price, dishtype, prov));
                        });
                if (_createResponse.ok)
                {
                    StopSpinner();
                    var alert = UIAlertController.Create(ResourcesComponent.GetStringFor("attentionLabel"),
                        ResourcesComponent.GetStringFor("dishCreatedName"), UIAlertControllerStyle.Alert);
                    alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, (action) =>
                    {
                        var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
                        ShowViewController(mc, this);
                    }));
                    ShowAlert(alert);
                }
                else
                {
                    StopSpinner();
                    var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("dishNotCreatedName") + "\n" +
                                              ResourcesComponent.GetStringFor("serverResponse") +_createResponse.message);
                    ShowAlert(alert);
                }
            }
            catch (Exception ex)
            {
                StopSpinner();
                var alert = AlertService.GetAlert(ex.Message.Contains("correct format") ? ResourcesComponent.GetStringFor("onlyNumbers") : ex.Message);
                ShowAlert(alert);
            }
        }

        private void ShowAlert(UIViewController alert)
        {
            PresentViewController(alert, true, null);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create back button

            var backBtn = new UIButton(new CGRect(20, 10, 70, 50));
            backBtn.TouchUpInside += GoBack;
            backBtn.SetTitleColor(ViewPreferences.OrangeColor, UIControlState.Normal);
            backBtn.SetTitle(ResourcesComponent.GetStringFor("goBackLabel"), UIControlState.Normal);
            backBtn.TitleLabel.Font = ViewPreferences.TitleFont;

            #endregion

            #region create back icon

            var backIcon = new UIButton(new CGRect(5, 20, 30, 30));
            backIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("backBtnImage")), UIControlState.Normal);
            backIcon.TouchUpInside += GoBack;

            #endregion

            #region create bar title

            var barTitle = new UILabel
            {
                Font = ViewPreferences.TitleFont,
                Frame = new CGRect(View.Frame.Width/2 - 55, 20, 150, 30),
                Text = ResourcesComponent.GetStringFor("addDishLabel")
            };

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(barTitle);
            commonBar.AddSubview(backBtn);
            commonBar.AddSubview(backIcon);

            #endregion

            Add(commonBar);
        }

        private void SetControlsText()
        {
            dishName.Placeholder = ResourcesComponent.GetStringFor("dishNamePlaceholder");
            dishDescr.Placeholder = ResourcesComponent.GetStringFor("dishDescrPlaceholder");
            priceLabel.Text = ResourcesComponent.GetStringFor("priceLabel");
            typeLabel.Text = ResourcesComponent.GetStringFor("typeLabel");
            provLabel.Text = ResourcesComponent.GetStringFor("provLabel");
        }

        private void SetControlsAppearanceAndBehaviour()
        {
            _dishtypes = new List<DishType>();
            var dishTypesResponse = DishComponent.GetAllDishTypes();
            if (dishTypesResponse.ok && dishTypesResponse.responseObject != null)
            {
                _dishtypes = dishTypesResponse.responseObject;
            }

            _provisioners = new List<ProvisionerModel>();
            var provisionersResponse = ProvisionerComponent.GetAllProvisioners();
            if (dishTypesResponse.ok && dishTypesResponse.responseObject != null)
            {
                _provisioners = provisionersResponse.responseObject;
            }

            _dishType = new UIPickerView
            {
                Frame = new CGRect(23, typeLabel.Frame.Y + 10, View.Frame.Width/2 - 40, 100),
                Model = new TypePickerViewModel(_dishtypes)
            };


            _provType = new UIPickerView
            {
                Frame = new CGRect(View.Frame.Width/2 + 10, typeLabel.Frame.Y + 10, View.Frame.Width/2 - 40, 100),
                Model = new ProvisionerPickerViewModel(_provisioners)
            };


            var submit = new UIButton
            {
                Frame = new CGRect(23, View.Frame.Height - 35, View.Frame.Width - 46, 30),
                BackgroundColor = ViewPreferences.GreenColor
            };

            submit.SetTitle(ResourcesComponent.GetStringFor("addDishName"), UIControlState.Normal);
            submit.SetTitleColor(UIColor.White, UIControlState.Normal);
            submit.Layer.CornerRadius = ViewPreferences.CornerRadius;
            submit.TouchUpInside += Submit_TouchUpInside;

            dishName.ShouldReturn = tf =>
            {
                dishDescr.BecomeFirstResponder();
                return true;
            };
            dishDescr.ShouldReturn = tf =>
            {
                dishPrice.BecomeFirstResponder();
                return true;
            };
            dishPrice.ShouldReturn = tf =>
            {
                View.EndEditing(true);
                return true;
            };

            Add(_dishType);
            Add(_provType);
            Add(submit);
        }

        #endregion
    }
}