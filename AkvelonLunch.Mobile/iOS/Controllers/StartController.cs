using System;
using lunch.iOS.IosComponents;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class StartController : UIViewController
    {
        public StartController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            OrderComponent.Subscribe();
            if (AuthorisationComponent.GetUser().ok)
            {
                if (AuthorisationComponent.GetUser().responseObject != null)
                {
                    if (Settings.Switcher)
                    {
                        NotificationService.SetNotifications();
                    }
                    else
                    {
                        NotificationService.RemoveNotificantions();
                    }


                    if (Settings.remBalance)
                    {
                        NotificationService.SetBalanceNotifications();
                    }
                    else
                    {
                        NotificationService.RemoveBalanceNotificantions();
                    }
                    var mc2 = Storyboard.InstantiateViewController("MenuController") as MenuController;
                    ShowViewController(mc2, this);
                }
                else
                {
                    var mc = Storyboard.InstantiateViewController("AuthController") as AuthController;
                    ShowViewController(mc, this);
                }
            }
            else
            {
                var mc = Storyboard.InstantiateViewController("AuthController") as AuthController;
                ShowViewController(mc, this);
            }
        }
    }
}