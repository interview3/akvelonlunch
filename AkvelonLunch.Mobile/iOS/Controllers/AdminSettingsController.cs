using System;
using CoreGraphics;
using Foundation;
using lunch.iOS.IosComponents;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace lunch.iOS.Controllers
{
    public partial class AdminSettingsController : UIViewController
    {
        public AdminSettingsController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateBar();
            SetTextAndValues();
            CreateButtons();
        }

        #region PRIVATE

        private void ShowAlert(UIViewController alert)
        {
            PresentViewController(alert, true, null);
        }

        private static void ToggleMenu(object sender, EventArgs e)
        {
            AdminController.Navigation.ToggleMenu();
        }

        private void GobackIcon_TouchUpInside(object sender, EventArgs e)
        {
            var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
            ShowViewController(mc, this);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create bar title

            var barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 50, 20, 150, 30),
                Font = ViewPreferences.TitleFont,
                Text = ResourcesComponent.GetStringFor("settingsLabel")
            };

            #endregion

            #region create menuicon

            var menuIcon = new UIButton(new CGRect(5, 10, 48, 48));
            menuIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("menuImage")), UIControlState.Normal);
            menuIcon.TouchUpInside += ToggleMenu;

            #endregion

            #region create goback icon

            var gobackIcon = new UIButton(new CGRect(View.Frame.Width - 60, 10, 48, 48));
            gobackIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("gotomenuImage")),
                UIControlState.Normal);
            gobackIcon.TouchUpInside += GobackIcon_TouchUpInside;

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(barTitle);
            commonBar.AddSubview(menuIcon);
            commonBar.AddSubview(gobackIcon);

            #endregion

            Add(commonBar);
        }

        private void SetTextAndValues()
        {
            blHourLabel.Text = ResourcesComponent.GetStringFor("setBlockHour");
            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            var components = gregCalendar.Components(NSCalendarUnit.Day | NSCalendarUnit.Year | NSCalendarUnit.Month,
                DateTime.Now.ToNSDate());
            components.Day = DateTime.Now.Day;
            components.Month = DateTime.Now.Month;
            components.Year = DateTime.Now.Year;
            var referenceDate = gregCalendar.DateFromComponents(components);
            var componentsForFireDate =
                gregCalendar.Components(NSCalendarUnit.Year | NSCalendarUnit.Hour | NSCalendarUnit.Minute, referenceDate);
            componentsForFireDate.Year = components.Year;
            componentsForFireDate.Month = components.Month;
            componentsForFireDate.Day = components.Day;
            var clTime = Convert.ToDateTime(Settings.closingTime);
            componentsForFireDate.Hour = clTime.Hour;
            componentsForFireDate.Minute = clTime.Minute;
            var fireDateOfNotification = gregCalendar.DateFromComponents(componentsForFireDate);
            blockHour.SetDate(fireDateOfNotification, false);
        }

        private void ConfirmButton_TouchUpInside(object sender, EventArgs e)
        {
            var gregCalendar = new NSCalendar(NSCalendarType.Gregorian);
            var hour = (int) gregCalendar.GetComponentFromDate(NSCalendarUnit.Hour, blockHour.Date);
            var minute = (int) gregCalendar.GetComponentFromDate(NSCalendarUnit.Minute, blockHour.Date);

            var closingHourResponse = SettingsComponent.SetClosingHour(hour, minute);
            if (!closingHourResponse.ok)
            {
                var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("cantSetTime") + "\n" +
                                                  ResourcesComponent.GetStringFor("serverResponse") +
                                                  closingHourResponse.message);
                ShowAlert(alert);
            }
            else
            {
                var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
                ShowViewController(mc, this);
            }
        }

        private void CreateButtons()
        {
			var confirmButton = new UIButton
			{
				Frame = new CGRect(10, View.Frame.Height - 50, View.Frame.Width - 20, 40),
				BackgroundColor = ViewPreferences.GreenColor
			};
			confirmButton.SetTitle(ResourcesComponent.GetStringFor("submitLabel"), UIControlState.Normal);
            confirmButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            confirmButton.Layer.CornerRadius = ViewPreferences.CornerRadius;
            confirmButton.TouchUpInside += ConfirmButton_TouchUpInside;
            confirmButton.TitleLabel.Font = ViewPreferences.TitleFont;
            View.Add(confirmButton);
        }

        #endregion
    }
}