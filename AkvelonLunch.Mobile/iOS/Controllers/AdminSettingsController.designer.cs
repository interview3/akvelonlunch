// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("AdminSettingsController")]
    partial class AdminSettingsController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel blHourLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker blockHour { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (blHourLabel != null) {
                blHourLabel.Dispose ();
                blHourLabel = null;
            }

            if (blockHour != null) {
                blockHour.Dispose ();
                blockHour = null;
            }
        }
    }
}