using System;
using CoreGraphics;
using lunch.iOS.TableBuilders.Admin.UserList;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AdminUsersController : UIViewController
    {
        private UITableView _usersTable;

        public AdminUsersController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateBar();
            CreateTables();
            CreateButtons();
        }

        #region PRIVATE

        private static void ToggleMenu(object sender, EventArgs e)
        {
            AdminController.Navigation.ToggleMenu();
        }

        private void GoToUserPage(UserModel user)
        {
            AdminUserComponent.EditingUserId = user.Id;
            var mc = Storyboard.InstantiateViewController("AdminUserPageController") as AdminUserPageController;
            ShowViewController(mc, this);
        }

        private void CreateTables()
        {
            _usersTable = new UITableView
            {
                Frame = new CGRect(View.Frame.X, View.Frame.Y + 150, View.Frame.Width, View.Frame.Height - 160),
                AutoresizingMask = UIViewAutoresizing.All,
                ScrollEnabled = true
            };
            View.Add(_usersTable);

            var usersResponse = AdminUserComponent.GetAllUsers(true);
            _usersTable.Source = usersResponse.ok 
                ? new UserTableSource(usersResponse.responseObject, GoToUserPage)
                : null;
        }

        private void CreateButtons()
        {
            var search = new UITextField
            {
                Frame = new CGRect(10, 110, View.Frame.Width - 20, 30),
                Placeholder = ResourcesComponent.GetStringFor("searchLabel"),
                Font = ViewPreferences.TitleFont,
                ClearButtonMode = UITextFieldViewMode.WhileEditing
            };
            search.Layer.BorderWidth = 1;
            search.Layer.CornerRadius = ViewPreferences.CornerRadius;
            search.Layer.BorderColor = ViewPreferences.LightGrayColor.CGColor;
            search.EditingChanged += Search_EditingChanged;
            search.ShouldReturn = tf =>
            {
                Search(search.Text);
                View.EndEditing(true);
                return true;
            };
            View.Add(search);
        }

        private void Search_EditingChanged(object sender, EventArgs e)
        {
            var textField = (UITextField) sender;
            Search(textField.Text);
        }

        private void Search(string key)
        {
            var usersResponse = AdminUserComponent.GetAllUsersByKey(key);
            _usersTable.Source = usersResponse.ok
                ? new UserTableSource(usersResponse.responseObject, GoToUserPage)
                : null;
            _usersTable.ReloadData();
        }

        private void GobackIcon_TouchUpInside(object sender, EventArgs e)
        {
            var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
            ShowViewController(mc, this);
        }

        private void CreateBar()
        {
            var commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

            #region create menuicon

            var menuIcon = new UIButton(new CGRect(5, 10, 48, 48));
            menuIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("menuImage")), UIControlState.Normal);
            menuIcon.TouchUpInside += ToggleMenu;

            #endregion

            #region create gobackicon

            var gobackIcon = new UIButton(new CGRect(View.Frame.Width - 60, 10, 48, 48));
            gobackIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("gotomenuImage")),
                UIControlState.Normal);
            gobackIcon.TouchUpInside += GobackIcon_TouchUpInside;

            #endregion

            #region create bar title

            var barTitle = new UILabel
            {
                Frame = new CGRect(View.Frame.Width/2 - 60, 20, 150, 30),
                Font = ViewPreferences.TitleFont,
                Text = ResourcesComponent.GetStringFor("usersLabel")
            };

            #endregion

            #region add all items to the bar

            commonBar.AddSubview(barTitle);
            commonBar.AddSubview(menuIcon);
            commonBar.AddSubview(gobackIcon);

            #endregion

            Add(commonBar);
        }

        #endregion
    }
}