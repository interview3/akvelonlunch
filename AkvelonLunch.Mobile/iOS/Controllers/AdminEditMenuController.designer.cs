// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("AdminEditMenuController")]
    partial class AdminEditMenuController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl tabControl { get; set; }

        [Action ("segmentNew:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void segmentNew (UIKit.UISegmentedControl sender);

        [Action ("swipedLeft:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void swipedLeft (UIKit.UISwipeGestureRecognizer sender);

        [Action ("swipedRight:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void swipedRight (UIKit.UISwipeGestureRecognizer sender);

        void ReleaseDesignerOutlets ()
        {
            if (tabControl != null) {
                tabControl.Dispose ();
                tabControl = null;
            }
        }
    }
}