using System;
using System.Threading.Tasks;
using CoreAnimation;
using CoreGraphics;
using lunch.iOS.IosComponents;
using lunch.iOS.TableBuilders.User;
using UIKit;
using Xamarin.Forms;

namespace lunch.iOS.Controllers
{
	public partial class MenuController : UIViewController
	{
	    private bool _animate;
	    private static UILabel _noMenu;
		public static UIButton CartButton, BackBtn, BackIcon;
		public static UILabel OrderStatus;
	    private UILabel _balanceLabel;
		public static UIButton ConfirmButton, ClearButton;
	    private UINavigationBar _commonBar;
		public static UITableView MenuTable, CartTable;
	    private static UIView _cartView;
	    private static UIView _menuView;
	    private UIActivityIndicatorView _actView;
		public static UIButton CountItemsBtn;
		public static UILabel TotalSummLabel, TotSummInCart, BarTitle;
		public static bool CellExpanded;
		public static int DishId;

        #region PRIVATE

	    private void CreateSpinner()
	    {
            _actView = new UIActivityIndicatorView
            {
                Frame = new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60),
                ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
                Alpha = 0.75f
            };
            Add(_actView);
			_actView.Hidden = true;
        }

	    private void StartSpinner()
        {
            _menuView.Alpha = 0.5f;
            _cartView.Alpha = 0.5f;
            _actView.Hidden = false;
            _actView.StartAnimating();
        }

        private void StopSpinner()
        {
            _menuView.Alpha = 1;
            _cartView.Alpha = 1;
            _actView.Hidden = true;
            _actView.StopAnimating();
        }

        private static void CheckClosingHour()
		{
			var date = DateTime.Now;
			var closingHour = SettingsComponent.GetClosingHour().responseObject.Value;
			var closingServiceHour = Convert.ToDateTime(closingHour);
			if (date > closingServiceHour)
			{
				CloseService();
			}
			else
			{
				SettingsComponent.closed = false;
				Forms.Init();
				PingService.StartCheckingClosingHour();

                #region start notifications checking timer
                var checkingDate = Settings.RemHour + ":" + (Settings.RemMinute - 1) + ":00";
				var dateNotif = Convert.ToDateTime(checkingDate);
				var timeFromNow = date.Subtract(dateNotif);
				if (dateNotif > date)
					timeFromNow = dateNotif.Subtract(date);
				Device.StartTimer(timeFromNow, NotificationService.NotifCanceller);
                #endregion

                #region start balance notifications checking timer
                var balanceCheckingDate = "8:59";
				var dateNotif2 = Convert.ToDateTime(balanceCheckingDate);
				var timeFromNow2 = date.Subtract(dateNotif2);
				if (dateNotif2 > date)
					timeFromNow2 = dateNotif2.Subtract(date);
				Device.StartTimer(timeFromNow2, NotificationService.BalanceNotifCanceller);
                #endregion
            }
		}

		private void LogOut()
		{
			var logoutResponse = AuthorisationComponent.Logout();
			if (logoutResponse.ok)
			{
				NotificationService.StopServices();
				PingService.StopChecking();
				ShowAuthController();
			}
			else
			{
				var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("notLoggedOutName") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + logoutResponse.message);
				ShowAlert(alert);
			}
		}

	    private CustomWebResponse<bool> _cancelResponse;

	    private async void CancelOrderOrClearOrder(object sender, EventArgs e)
		{
			if (OrderComponent.isMade)
			{
				StartSpinner();

				await Task.Factory.StartNew(() =>
				{
					_cancelResponse = OrderComponent.CancelOrder();
				});

				if (!_cancelResponse.ok)
				{
					StopSpinner();
					var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("orderNotCancelledName") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + _cancelResponse.message);
					ShowAlert(alert);
				}
				else
				{
					UnlockOrder();
					StopSpinner();
				}
			}
			else
			{
				OrderComponent.ClearOrder();
				ClearOrderTable();
			}
		}

	    private CustomWebResponse<int> _makeOrderResponse;

		private async void MakeOrder(object sender, EventArgs e)
		{
			StartSpinner();

			await Task.Factory.StartNew(() =>
			{
				_makeOrderResponse = OrderComponent.MakeOrder();
			});
			if (_makeOrderResponse.ok)
			{
				LockOrder();
				StopSpinner();
			}
			else
			{
				StopSpinner();
				ReloadMenu();
				ReloadCart();
				var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("problemOrder") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + _makeOrderResponse.message);
				ShowAlert(alert);
			}
		}

		#endregion

		#region PUBLIC

		public MenuController(IntPtr handle) : base(handle)
		{
			
		}

		partial void swiped(UISwipeGestureRecognizer sender)
		{
		    if (!_cartView.Hidden) return;
		    _animate = true;
		    GoToCart(sender, EventArgs.Empty);
		}

		partial void swipedRight(UISwipeGestureRecognizer sender)
		{
		    if (!_menuView.Hidden) return;
		    _animate = true;
		    GoToMenu(sender, EventArgs.Empty);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			CreateBasicViews();
			CreateTables();
			CreateBar();
			CreateButtons();
            CreateSpinner();
            SetControlsDependingOnOrderStatus();
			CheckClosingHour();
		}
		#endregion

		#region DRAWING

		public static bool CloseService()
		{
			SettingsComponent.closed = true;
			PingService.StopChecking();
			var closedServiceImage = UIImage.FromFile(ResourcesComponent.GetStringFor("closedServiceImage"));
		    var closedServiceView = new UIImageView
		        {
                    Frame = new CGRect((MenuTable.Frame.Width - closedServiceImage.Size.Width) / 2,
                    (MenuTable.Frame.Height - closedServiceImage.Size.Height) / 2, closedServiceImage.Size.Width,
                    closedServiceImage.Size.Height),
                    Image = closedServiceImage
		        };
		    _menuView.Add(closedServiceView);
			SettingsComponent.closed = true;
			ReloadMenu();
			MenuTable.Alpha = ViewPreferences.BlockingAlpha;
			ClearButton.Hidden = true;
			ConfirmButton.Hidden = true;
			if (OrderComponent.isMade)
				NotificationService.StartCheckingIfOrderArrived();
			return false;
		}

	    private async void ShowAuthController()
		{
			StartSpinner();

			await Task.Factory.StartNew(() =>
			{
				InvokeOnMainThread(delegate
				{
					var mc = Storyboard.InstantiateViewController("AuthController") as AuthController;
					ShowViewController(mc, this);
				});
			});
		}

	    private void CreateBasicViews()
		{
			_cartView = new UIView(new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60));
			_menuView = new UIView(new CGRect(0, 45, View.Frame.Width, View.Frame.Height - 60));
            _cartView.Hidden = true;
			Add(_menuView);
			Add(_cartView);
		}

	    private void CreateBar()
		{
			_commonBar = new UINavigationBar(new CGRect(0, 20, View.Frame.Width, 70));

			#region create counter with number of items above cart
			CountItemsBtn = new UIButton(new CGRect(View.Frame.Width - 70, 12, 15, 15));
			CountItemsBtn.SetTitle(OrderComponent.NumberOfItems.ToString(), UIControlState.Normal);
			CountItemsBtn.Layer.CornerRadius = CountItemsBtn.Frame.Width / 2;
			CountItemsBtn.TitleLabel.Font = ViewPreferences.LittleFont;
			#endregion

			#region create back button
			BackBtn = new UIButton(new CGRect(20, 10, 70, 50));
			BackBtn.TouchUpInside += GoToMenu;
			BackBtn.TitleLabel.Font = ViewPreferences.TitleFont;
			BackBtn.SetTitleColor(ViewPreferences.OrangeColor, UIControlState.Normal);
			#endregion

			#region create back icon
			BackIcon = new UIButton(new CGRect(5, 20, 30, 30));
			BackIcon.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("backBtnImage")), UIControlState.Normal);
			BackIcon.TouchUpInside += GoToMenu;
			BackIcon.Hidden = true;
			#endregion

			#region create bar title

		    BarTitle = new UILabel
		    {
                Frame = new CGRect(View.Frame.Width / 2 - 35, 20, 100, 30),
                Text = ResourcesComponent.GetStringFor("menuName"),
		        Font = ViewPreferences.TitleFont
		    };

		    #endregion

			#region create account button
			var i = UIImage.FromFile(ResourcesComponent.GetStringFor("accountImage"));
			var accountButton = new UIButton(new CGRect(View.Frame.Width - 60, 0, i.Size.Width, i.Size.Width));
			accountButton.SetImage(i, UIControlState.Normal);
			accountButton.TouchUpInside += GoToUserPref;
			#endregion

			#region create cart button
			CartButton = new UIButton(new CGRect(View.Frame.Width - 120, 0, i.Size.Width, i.Size.Width));
			CartButton.TouchUpInside += GoToCart;
			#endregion

			#region create total summ of the order with discount label under the cart button

		    TotalSummLabel = new UILabel
		    {
                Frame = new CGRect(View.Frame.Width - 100, 55, 50, 11),
                Text = Convert.ToInt32(OrderComponent.TotalSumm) + "P",
		        Font = ViewPreferences.LittleFont
		    };

		    #endregion

			#region create user balance label under the account button
			_balanceLabel = new UILabel(new CGRect(View.Frame.Width - 45, 55, 50, 11));
			_balanceLabel.Layer.CornerRadius = CountItemsBtn.Frame.Width / 2;
			_balanceLabel.Font = ViewPreferences.LittleFont;
            SetBalance();
            #endregion

            #region add all items to the bar
            _commonBar.AddSubview(_balanceLabel);
			_commonBar.AddSubview(CountItemsBtn);
			_commonBar.AddSubview(CartButton);
			_commonBar.AddSubview(accountButton);
			_commonBar.AddSubview(TotalSummLabel);
			_commonBar.AddSubview(BarTitle);
			_commonBar.AddSubview(BackBtn);
			_commonBar.AddSubview(BackIcon);
			#endregion

			Add(_commonBar);
		}

		private void SetControlsDependingOnOrderStatus()
		{
			OrderComponent.OrderArrived();
			var isMade = OrderComponent.isMade;
			var orderArrived = OrderComponent.isArrived;
			var numberOfItems = OrderComponent.NumberOfItems;
			CountItemsBtn.Hidden = (!orderArrived && numberOfItems == 0);
			TotSummInCart.Hidden = orderArrived;
			ConfirmButton.Hidden = orderArrived;
			ClearButton.Hidden = orderArrived;
            UIImage i;
            if (!orderArrived)
			{
				if (isMade)
				{
					CountItemsBtn.BackgroundColor = ViewPreferences.OrangeColor;
					i = UIImage.FromFile(ResourcesComponent.GetStringFor("cartImage"));
					LockOrder(true);
					OrderStatus.Text = ResourcesComponent.GetStringFor("orderLabel") + " " + OrderComponent.OrderId + " " + ResourcesComponent.GetStringFor("orderSent");
					OrderStatus.Font = ViewPreferences.StatusFont;
				}
				else
				{
					UnlockOrder(true);
					CountItemsBtn.BackgroundColor = ViewPreferences.GrayColor;
					i = UIImage.FromFile(ResourcesComponent.GetStringFor("disCartImage"));
				}
			}
			else
			{
				OrderStatus.Text = ResourcesComponent.GetStringFor("orderLabel") + " " + OrderComponent.OrderId + " " + ResourcesComponent.GetStringFor("orderArrived");
				OrderStatus.Font = ViewPreferences.StatusBoldFont;
				CountItemsBtn.BackgroundColor = ViewPreferences.GreenColor;
				i = UIImage.FromFile(ResourcesComponent.GetStringFor("delCartImage"));
			}
			CartButton.SetImage(i, UIControlState.Normal);

		    if (!orderArrived) return;
		    MenuTable.Alpha = ViewPreferences.BlockingAlpha;
		    CartTable.Alpha = ViewPreferences.BlockingAlpha;
		}

		private void SetBalance()
		{
			var balanceResponse = AuthorisationComponent.GetUserBalance();
			if (balanceResponse.ok)
				_balanceLabel.Text = Convert.ToInt32(balanceResponse.responseObject) + "P";
			else
			{
				var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("unableToGetBalance") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + balanceResponse.message);
				ShowAlert(alert);
			}
		}

		private void TryToGetUser()
		{
			var getuserResponse = AuthorisationComponent.GetUser();
		    if (getuserResponse.ok) return;
		    var alert = AlertService.GetAlert(ResourcesComponent.GetStringFor("unableToGetUser") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + getuserResponse.message);
		    ShowAlert(alert);
		}
		private void CreateTables()
		{
		    _noMenu = new UILabel(new CGRect(10, 10, _menuView.Frame.Width, 50))
		    {
		        Text = ResourcesComponent.GetStringFor("noMenu"),
		        Font = ViewPreferences.BigFont,
		        Hidden = true
		    };

		    #region create menu table

		    MenuTable = new UITableView( new CGRect(_menuView.Frame.X, _menuView.Frame.Y, _menuView.Frame.Width, _menuView.Frame.Height - 100),
		            UITableViewStyle.Grouped)
		        {
		            AutoresizingMask = UIViewAutoresizing.All,
		            ScrollEnabled = true
		        };
		    MenuTable.Add(_noMenu);

			var menuResponse = MenuComponent.GetMenuDictionary();
			if (menuResponse.ok)
			{
				if (menuResponse.responseObject == null)
				{
					PingService.StartCheckingMenu();
					_noMenu.Hidden = false;
				}
				else
				{
					PingService.StartCheckingMenuUpdate();
					MenuTable.Source = new MenuTableSource(menuResponse.responseObject);
				}
			}
			else
			{
				MenuTable.Source = null;
				ShowAlert(AlertService.GetAlert(ResourcesComponent.GetStringFor("noMenuFail") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + menuResponse.message));
			}

			_menuView.Add(MenuTable);
			MenuTable.AllowsSelection = false;
			#endregion

			#region create cart table

		    CartTable = new UITableView
		        {
                    Frame = new CGRect(_cartView.Frame.X - 1, _cartView.Frame.Y - 1, _cartView.Frame.Width + 2, _cartView.Frame.Height - 160),
                    AutoresizingMask = UIViewAutoresizing.All,
		            ScrollEnabled = true,
		            Source = new CartTableSource()
		        };
		    _cartView.Add(CartTable);
			CartTable.AllowsSelection = false;
			CartTable.Layer.BorderWidth = 1;
			CartTable.Layer.BorderColor = ViewPreferences.LightGrayColor.CGColor;
			#endregion
			PingService.StartCheckingOrder();
			PingService.StartCheckingOrdersAdmin();
			PingService.StartCheckingMoney();
		}


		private void CreateButtons()
		{
			#region create order status label
		    OrderStatus = new UILabel
		        {
                    Frame = new CGRect(_cartView.Frame.X + 10, _cartView.Frame.Height - 80, _cartView.Frame.Width - 20, 30),
                    Text = string.Empty,
		            Font = ViewPreferences.StatusFont
		        };
			#endregion

			#region create total summ in cart label
		    TotSummInCart = new UILabel
		        {
                    Frame = new CGRect(_cartView.Frame.X + 10, _cartView.Frame.Height - 115, _cartView.Frame.Width - 20, 40),
                    Text = ResourcesComponent.GetStringFor("itogoPlusDiscountLabel") + Convert.ToInt32(OrderComponent.TotalSumm) + "P",
		            Font = ViewPreferences.BigFont
		        };
			#endregion

			#region create clear/cancel button
		    ClearButton = new UIButton
		        {
                    Frame = new CGRect(_cartView.Frame.X + 10, _cartView.Frame.Height - 40, _cartView.Frame.Width / 2 - 20, 40),
                    BackgroundColor = ViewPreferences.RedColor
		        };

		    ClearButton.SetTitle(ResourcesComponent.GetStringFor("clearOrderName"), UIControlState.Normal);
            ClearButton.SetTitleColor(UIColor.White, UIControlState.Normal);
			ClearButton.Layer.CornerRadius = ViewPreferences.CornerRadius;
			ClearButton.TouchUpInside += CancelOrderOrClearOrder;
			ClearButton.TitleLabel.Font = ViewPreferences.TitleFont;
			#endregion

			#region create gotocart button
		    var gotocartButton = new UIButton
		        {
                    Frame = new CGRect(_menuView.Frame.X + 10, _menuView.Frame.Height - 40, _menuView.Frame.Width - 20, 40),
                    BackgroundColor = ViewPreferences.GreenColor
		        };

		    gotocartButton.SetTitleColor(UIColor.White, UIControlState.Normal);
			gotocartButton.Layer.CornerRadius = ViewPreferences.CornerRadius;
			gotocartButton.TouchUpInside += GoToCart;
			gotocartButton.TitleLabel.Font = ViewPreferences.TitleFont;
            gotocartButton.SetTitle(ResourcesComponent.GetStringFor("gotocartButtonName"), UIControlState.Normal);
			#endregion

			#region create confirm button
		    ConfirmButton = new UIButton
		        {
                    Frame = new CGRect(_cartView.Frame.Width / 2 + 10, _cartView.Frame.Height - 40, _cartView.Frame.Width / 2 - 20, 40),
                    BackgroundColor = ViewPreferences.GreenColor
		        };

		    ConfirmButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            ConfirmButton.SetTitle(ResourcesComponent.GetStringFor("confirmOrderName"), UIControlState.Normal);
            ConfirmButton.Layer.CornerRadius = ViewPreferences.CornerRadius;
			ConfirmButton.TouchUpInside += MakeOrder;
			ConfirmButton.TitleLabel.Font = ViewPreferences.TitleFont;
            #endregion

            #region add items
            _cartView.Add(ConfirmButton);
            _menuView.Add(gotocartButton);
            _cartView.Add(ClearButton);
            _cartView.Add(TotSummInCart);
            _cartView.Add(OrderStatus);
            #endregion
        }

		private void LockOrder(bool fromFunc = false)
		{
			NotificationService.RemoveNotificantions();
			ClearButton.SetTitle(ResourcesComponent.GetStringFor("cancelName"), UIControlState.Normal);
			ClearButton.Frame = new CGRect(_cartView.Frame.X + 10, _cartView.Frame.Height - 40, _cartView.Frame.Width - 20, 40);
			ConfirmButton.Hidden = true;
			CartTable.Source = new CartTableSource();
			CartTable.ReloadData();
			CartTable.Alpha = ViewPreferences.BlockingAlpha;
			MenuTable.Alpha = ViewPreferences.BlockingAlpha;
			ReloadMenu();
			if (!fromFunc) SetControlsDependingOnOrderStatus();
			TryToGetUser();
			SetBalance();
		}

		private void UnlockOrder(bool fromFunc = false)
		{
			MenuTable.Alpha = 1f;
			OrderStatus.Text = string.Empty;
			ClearButton.SetTitle(ResourcesComponent.GetStringFor("clearOrderName"), UIControlState.Normal);
			ClearButton.Frame = new CGRect(_cartView.Frame.X + 10, _cartView.Frame.Height - 40, _cartView.Frame.Width / 2 - 20, 40);
			ConfirmButton.Hidden = false;
			CartTable.Alpha = 1f;
			CartTable.Source = new CartTableSource();
			CartTable.ReloadData();
			ReloadMenu();
			NotificationService.SetNotifications();
			if (!fromFunc) SetControlsDependingOnOrderStatus();
			TryToGetUser();
			SetBalance();
		}

		private void ShowSettings()
		{
			var mc = Storyboard.InstantiateViewController("SettingsController") as SettingsController;
			ShowViewController(mc, this);
		}

		private async void ShowAdminPanel()
		{
			if (!OrderComponent.isMade)
				OrderComponent.ClearOrder();
			StartSpinner();

			await Task.Factory.StartNew(() =>
			{
				InvokeOnMainThread(delegate
				{
					var mc = Storyboard.InstantiateViewController("AdminController") as AdminController;
					ShowViewController(mc, this);
				});
			});
		}

		private static void ClearOrderTable()
		{
			TotalSummLabel.Text = Convert.ToInt32(OrderComponent.TotalSumm) + "P";
			TotSummInCart.Text = ResourcesComponent.GetStringFor("itogoPlusDiscountLabel") + Convert.ToInt32(OrderComponent.TotalSumm) + "P";
			CartButton.SetImage(UIImage.FromFile(ResourcesComponent.GetStringFor("disCartImage")), UIControlState.Normal);
			CountItemsBtn.SetTitle("0", UIControlState.Normal);
			CountItemsBtn.Hidden = true;
			CartTable.Source = new CartTableSource();
			CartTable.ReloadData();
			ReloadMenu();
			LockMakingOrder();
		}

		public static void ReloadMenu()
		{
			var menuResponse = MenuComponent.GetMenuDictionary();
			if (menuResponse.ok)
			{
				if (menuResponse.responseObject == null)
				{
					PingService.StartCheckingMenu();
					_noMenu.Hidden = false;
					MenuTable.Source = new MenuTableSource(null);
				}
				else
				{
					PingService.StartCheckingMenuUpdate();
					_noMenu.Hidden = true;
					MenuTable.Source = new MenuTableSource(menuResponse.responseObject);
				}
			}
			else
			{
				MenuTable.Source = null;
			}
			MenuTable.ReloadData();
		}

		public static void ReloadCart()
		{
			CartTable.Source = new CartTableSource();
			CartTable.ReloadData();
			CountItemsBtn.SetTitle(OrderComponent.NumberOfItems.ToString(), UIControlState.Normal);
			if (OrderComponent.NumberOfItems == 0)
			{
				CountItemsBtn.Hidden = true;
				LockMakingOrder();
			}
			else 
			{
				CountItemsBtn.Hidden = false;
				UnLockMakingOrder();
			}
			TotalSummLabel.Text = Convert.ToInt32(OrderComponent.TotalSumm) + "P";
			TotSummInCart.Text = ResourcesComponent.GetStringFor("itogoPlusDiscountLabel") + Convert.ToInt32(OrderComponent.TotalSumm) + "P";
			if (OrderComponent.isArrived)
			{
				CartTable.UserInteractionEnabled = false;
				CartTable.Alpha = ViewPreferences.BlockingAlpha;
			}
			else 
			{
				CartTable.UserInteractionEnabled = true;
				CartTable.Alpha = 1f;
			}
		}

		public static void LockMakingOrder()
		{
			ConfirmButton.UserInteractionEnabled = false;
			ConfirmButton.Alpha = ViewPreferences.BlockingAlpha;
			ClearButton.UserInteractionEnabled = false;
			ClearButton.Alpha = ViewPreferences.BlockingAlpha;
		}

		private static void UnLockMakingOrder()
		{
			ConfirmButton.UserInteractionEnabled = true;
			ConfirmButton.Alpha = 1f;
			ClearButton.UserInteractionEnabled = true;
			ClearButton.Alpha = 1f;
		}

		private void DoTransitionToCart()
		{
			_animate = false;
			var frompt = new CGPoint(View.Frame.Width + _cartView.Layer.Position.X, _cartView.Layer.Position.Y);
			var prevX = _cartView.Layer.Position.X;
			var path = new CGPath();
			path.AddLines(new[] { frompt, new CGPoint(prevX, _cartView.Layer.Position.Y) });
			var animPosition = CAKeyFrameAnimation.FromKeyPath("position");
			animPosition.Path = path;
			animPosition.Duration = ViewPreferences.AnimDuration;
			_cartView.Layer.AddAnimation(animPosition, "position");
		}

		private void DoTransitionToMenu()
		{
			_animate = false;
			var frompt = new CGPoint(_menuView.Layer.Position.X - View.Frame.Width, _menuView.Layer.Position.Y);
			var prevX = _menuView.Layer.Position.X;
			var path = new CGPath();
			path.AddLines(new[] { frompt, new CGPoint(prevX, _menuView.Layer.Position.Y) });
			var animPosition = CAKeyFrameAnimation.FromKeyPath("position");
			animPosition.Path = path;
			animPosition.Duration = ViewPreferences.AnimDuration;
			_menuView.Layer.AddAnimation(animPosition, "position");
		}

		private void GoToCart(object sender, EventArgs e)
		{
			_cartView.Hidden = false;
			_menuView.Hidden = true;

			if (_animate)
			{
				DoTransitionToCart();
			}

			BackIcon.Hidden = false;
			BackBtn.SetTitle(ResourcesComponent.GetStringFor("menuName"), UIControlState.Normal);
			BarTitle.Text = ResourcesComponent.GetStringFor("cartName");
			BarTitle.Center = Settings.language == "en" ? new CGPoint(View.Frame.Width / 2 + 15, 35) : new CGPoint(View.Frame.Width / 2 + 5, 35);
			CartTable.Source = new CartTableSource();
			CartTable.ReloadData();
			if (OrderComponent.NumberOfItems == 0)
			{
				LockMakingOrder();
			}
			else
			{
				UnLockMakingOrder();
			}
		}


		private void GoToMenu(object sender, EventArgs e)
		{
			_cartView.Hidden = true;
			_menuView.Hidden = false;
			if (_animate)
			{
				DoTransitionToMenu();
			}
			BackBtn.SetTitle(string.Empty, UIControlState.Normal);
			BarTitle.Text = ResourcesComponent.GetStringFor("menuName");
			BarTitle.Center = new CGPoint(View.Frame.Width / 2 + 15, 35);
			BackIcon.Hidden = true;
		}

	    private CustomWebResponse<string> _userResponse;
		private async void GoToUserPref(object sender, EventArgs e)
		{
			StartSpinner();
			var firstName = string.Empty;
			await Task.Factory.StartNew(() =>
			{
				_userResponse = AuthorisationComponent.GetUserFirstName();
			});

			if (!_userResponse.ok)
			{
				var eralert = UIAlertController.Create(ResourcesComponent.GetStringFor("unableToGetUser") + "\n" + ResourcesComponent.GetStringFor("serverResponse") + _userResponse.message, null, UIAlertControllerStyle.Alert);
				eralert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, (action) => { ShowPref("User"); }));
				ShowAlert(eralert);
			}
			else firstName = _userResponse.responseObject;
			ShowPref(firstName);
			StopSpinner();
		}

		private void ShowPref(string name)
		{
			var alert = UIAlertController.Create(name, null, UIAlertControllerStyle.Alert);
			alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("cancelLabel"), UIAlertActionStyle.Cancel, null));
			var getuserResponse = AuthorisationComponent.GetUser();
			if (getuserResponse.ok && getuserResponse.responseObject!=null)
			{
				if (getuserResponse.responseObject.RoleId == 1)
				{
					alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("AdminLabel"), UIAlertActionStyle.Default, (action) => { ShowAdminPanel(); }));
				}
			}
			alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("settingsLabel"), UIAlertActionStyle.Default, (action) => { ShowSettings(); }));
			alert.AddAction(UIAlertAction.Create(ResourcesComponent.GetStringFor("logOutLabel"), UIAlertActionStyle.Default, (action) => { LogOut(); }));
			ShowAlert(alert);
		}

		private void ShowAlert(UIViewController alert)
		{
			PresentViewController(alert, true, null);
		}

		#endregion
	}
}