// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("MenuController")]
    partial class MenuController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwipeGestureRecognizer swipeLeft { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwipeGestureRecognizer swipeRight { get; set; }

        [Action ("swiped:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void swiped (UIKit.UISwipeGestureRecognizer sender);

        [Action ("swipedRight:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void swipedRight (UIKit.UISwipeGestureRecognizer sender);

        void ReleaseDesignerOutlets ()
        {
            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (swipeLeft != null) {
                swipeLeft.Dispose ();
                swipeLeft = null;
            }

            if (swipeRight != null) {
                swipeRight.Dispose ();
                swipeRight = null;
            }
        }
    }
}