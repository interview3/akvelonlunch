// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("AdminUserPageController")]
    partial class AdminUserPageController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField balanceInput { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel userName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel userName2 { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (balanceInput != null) {
                balanceInput.Dispose ();
                balanceInput = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (userName != null) {
                userName.Dispose ();
                userName = null;
            }

            if (userName2 != null) {
                userName2.Dispose ();
                userName2 = null;
            }
        }
    }
}