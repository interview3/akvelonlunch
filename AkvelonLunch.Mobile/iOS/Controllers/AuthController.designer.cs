// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("AuthController")]
    partial class AuthController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch keepLoggedIn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton loginBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pswd { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel remMe { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField userName { get; set; }

        [Action ("loginBtnClicked:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void loginBtnClicked (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (keepLoggedIn != null) {
                keepLoggedIn.Dispose ();
                keepLoggedIn = null;
            }

            if (loginBtn != null) {
                loginBtn.Dispose ();
                loginBtn = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (pswd != null) {
                pswd.Dispose ();
                pswd = null;
            }

            if (remMe != null) {
                remMe.Dispose ();
                remMe = null;
            }

            if (userName != null) {
                userName.Dispose ();
                userName = null;
            }
        }
    }
}