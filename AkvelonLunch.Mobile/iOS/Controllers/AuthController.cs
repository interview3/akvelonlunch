using System;
using System.Threading.Tasks;
using CoreGraphics;
using lunch.iOS.IosComponents;
using UIKit;

namespace lunch.iOS.Controllers
{
    public partial class AuthController : UIViewController
    {
        private CustomWebResponse<bool> _response;
        private UIActivityIndicatorView _actView;
        private string _usr;
        private string _psswd;
        private bool _keep;

        async partial void loginBtnClicked(UIButton sender)
        {
            InvokeOnMainThread(() =>
            {
                StartSpinner();
                _usr = userName.Text;
                _psswd = pswd.Text;
                _keep = keepLoggedIn.On;
            });

            await Task.Factory.StartNew(() => { _response = AuthorisationComponent.Login(_usr, _psswd, _keep); });

            if (_response.ok)
            {
                if (_response.responseObject)
                    StartApplication();
                else
                {
                    StopSpinner();
                    var alert =
                        AlertService.GetAlert(ResourcesComponent.GetStringFor("unableToLogin") +
                                              ResourcesComponent.GetStringFor("wrongPswdLabel"));
                    PresentViewController(alert, true, null);
                }
            }
            else
            {
                StopSpinner();
                var alert =
                    AlertService.GetAlert(ResourcesComponent.GetStringFor("unableToLogin") +
                                          ResourcesComponent.GetStringFor("wrongPswdLabel") + "\n" +
                                          ResourcesComponent.GetStringFor("serverResponse") + _response.message);
                PresentViewController(alert, true, null);
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateSpinner();
            SetControlsTextAndBehaviour();
        }

        public AuthController(IntPtr handle) : base(handle)
        {
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
        }

        #region PRIVATE

        private void CreateSpinner()
        {
            _actView = new UIActivityIndicatorView
            {
                Frame = new CGRect(View.Frame.Width - 50, 10, 50, 50),
                ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray,
                Hidden = true
            };
            Add(_actView);
        }

        private void StartSpinner()
        {
            _actView.Hidden = false;
            _actView.StartAnimating();
        }

        private void StopSpinner()
        {
            _actView.Hidden = true;
            _actView.StopAnimating();
        }

        private void SetControlsTextAndBehaviour()
        {
            Title = ResourcesComponent.GetStringFor("loginLabel");
            userName.Placeholder = ResourcesComponent.GetStringFor("userNameLabel");
            pswd.Placeholder = ResourcesComponent.GetStringFor("pswdLabel");
            remMe.Text = ResourcesComponent.GetStringFor("rememberMeLabel");

            userName.ShouldReturn = tf =>
            {
                pswd.BecomeFirstResponder();
                return true;
            };
            pswd.ShouldReturn = tf =>
            {
                loginBtnClicked(loginBtn);
                return true;
            };
            userName.BecomeFirstResponder();
            loginBtn.SetTitle(ResourcesComponent.GetStringFor("loginLabel"), UIControlState.Normal);
        }

        private void StartApplication()
        {
            if (Settings.Switcher)
            {
                NotificationService.SetNotifications();
            }
            else
            {
                NotificationService.RemoveNotificantions();
            }
            if (Settings.remBalance)
            {
                NotificationService.SetBalanceNotifications();
            }
            else
            {
                NotificationService.RemoveBalanceNotificantions();
            }
            var mc = Storyboard.InstantiateViewController("MenuController") as MenuController;
            ShowViewController(mc, this);
        }

        #endregion
    }
}
