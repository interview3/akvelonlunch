// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//

using System.CodeDom.Compiler;
using Foundation;

namespace lunch.iOS.Controllers
{
    [Register ("AddDishController")]
    partial class AddDishController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField dishDescr { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField dishName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField dishPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView mainView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel priceLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel provLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel typeLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (dishDescr != null) {
                dishDescr.Dispose ();
                dishDescr = null;
            }

            if (dishName != null) {
                dishName.Dispose ();
                dishName = null;
            }

            if (dishPrice != null) {
                dishPrice.Dispose ();
                dishPrice = null;
            }

            if (mainView != null) {
                mainView.Dispose ();
                mainView = null;
            }

            if (priceLabel != null) {
                priceLabel.Dispose ();
                priceLabel = null;
            }

            if (provLabel != null) {
                provLabel.Dispose ();
                provLabel = null;
            }

            if (typeLabel != null) {
                typeLabel.Dispose ();
                typeLabel = null;
            }
        }
    }
}